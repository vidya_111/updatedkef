package com.observatory.Assessment.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.observatory.Assessment.ConstantManager;
import com.observatory.Assessment.Model.AssessmentModel;
import com.observatory.Assessment.Model.ChapterModel;
import com.observatory.Assessment.Model.QuestionImageModel;
import com.observatory.Assessment.Model.QuestionModel;
import com.observatory.Assessment.Model.SubjectModel;
import com.observatory.Assessment.Model.TopicModel;
import com.observatory.Assessment.Reports.ResultModel;
import com.observatory.database.Master;
import com.observatory.utils.Constants;
import com.observatory.utils.DateUtil;
import com.observatory.utils.EmailHandler;
import com.observatory.utils.SettingPreffrences;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SQLiteDB extends SQLiteOpenHelper {

    private AtomicInteger mOpenCounter = new AtomicInteger();
    private static String TAG = "SQLiteDB"; // Tag just for the LogCat window
    private String DB_PATH = "";
    public static String DB_NAME = "PRD00008-AS.db";// Database name
    SQLiteDatabase mDataBase;
    private static String _sDBFilePath = "";
    Context appContext;
    private static final int DATABASE_VERSION = 2;
    private static SQLiteDB sqLiteDB = null;
    // Master master = Master.listAll(Master.class).get(0);
    private static String TableName_Subject = "Subject";
    private static String TableName_Chapter = "Chapter";
    private static String TableName_Topic = "Topic";
    private static String TableName_Question = "Question";
    private static String TableName_QuestionImage = "QuestionImage";
    private static String TableName_Results_Details = "TestResultDetails";
    private static String TableName_Score_Individual_Details = "TestScoreIndividualDetails";
    private static String TableName_Summary_Details = "TestScoreSummaryDetails";

    public String Test_Result_Details_Id = "TestResultDetailsId";
    public String Test_Score_Individual_Details_Id = "TestScoreIndividualDetailsId";
    public String Product_Code = "ProductCode";
    public String Course_Content_Code = "CourseContentCode";
    public String Test_Code = "TestCode";
    public String Question_Code = "QuestionCode";
    public String Is_Question_Skipped = "IsQuestionSkipped";
    public String Is_Answered_Correct = "IsAnsweredCorrect";
    public String Attempted_Answer = "AttemptedAnswer";
    public String Question_Marks = "QuestionMarks";
    public String Question_Marks_Obtained = "QuestionMarksObtained";
    public String Attempted_Test_Date = "AttemptedTestDate";
    public String Answer_Order_Seq = "AnswerOrderSeq";
    public String Is_Sync = "IsSync";


    public String Test_Type_Code = "TestTypeCode";
    public String Test_Actual_Duration = "TestActualDuration";
    public String Test_Attempted_Duration = "TestAttemptedDuration";
    public String Test_Total_Marks = "TestTotalMarks";
    public String Marks_Obtained = "MarksObtained";
    public String Total_Question = "TotalQuestion";
    public String Skip_Question_Count = "SkipQuestionCount";
    public String Right_Answer_Count = "RightAnswerCount";
    public String Wrong_Answer_Count = "WrongAnswerCount";
    public String Percentage = "Percentage";
    public String Access_Date_Time = "AccessDateTime";


    public String Test_Score_Summary_Details_Id = "TestScoreSummaryDetailsId";
    public String Test_Total_Attempted_Duration = "TestTotalAttemptedDuration";
    public String Total_Marks_Obtained = "TotalMarksObtained";
    public String Test_Attempted_Count = "TestAttemptedCount";
    public String Test_Total_Question = "TestTotalQuestion";
    public String Total_Skip_Question_Count = "TotalSkipQuestionCount";
    public String Total_Right_Answer_Count = "TotalRightAnswerCount";
    public String Total_Wrong_Answer_Count = "TotalWrongAnswerCount";
    public String Test_Best_Score_Obtained = "TestBestScoreObtained";
    public String First_Attempt_Marks_Obtained = "FirstAttemptMarksObtained";
    public String Last_Attempt_Marks_Obtained = "LastAttemptMarksObtained";
    public String Test_Rank = "TestRank";
    public String First_Test_Score_Individual_Details_Id = "FirstTestScoreIndividualDetailsId";
    public String Last_Test_Score_Individual_Details_Id = "LastTestScoreIndividualDetailsId";
    public String Is_Solution_Viewed = "IsSolutionViewed";
    public String Test_Status = "TestStatus";
    public String Solution_Status = "SolutionStatus";
    public String Last_Access_Time = "LastAccessTime";


    public String Student_Code = "StudentUserCode";
    public String Mac_Id = "MacId";


    public static SQLiteDB getInstance(Context context, String dbFilePath) {
        //isReadOnly=isRead;
        // if (sqLiteDB == null) {
        Log.d("updateAssessment", "dbpath: " + dbFilePath);
        sqLiteDB = new SQLiteDB(context, dbFilePath);
        //}
        return sqLiteDB;
    }

    private SQLiteDB(Context context, String dbFilePath) {
        super(context, DB_NAME, null, DATABASE_VERSION);
        _sDBFilePath = dbFilePath;

        try {
            createDataBase();
            getWritableDatabase();
        } catch (Exception ioe) {

            throw new Error("Unable to create database");

        }
        this.appContext = context;
    }

    public void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();

        if (!dbExist) {
            getReadableDatabase();
            close();
        }
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            checkDB = SQLiteDatabase.openDatabase(_sDBFilePath, null, SQLiteDatabase.OPEN_READWRITE);
        } catch (SQLiteException e) {
            Log.e("CheckDBException", e.toString());
        }
        if (checkDB != null) {
            this.mDataBase = checkDB;
        }

        return checkDB != null;
    }

    public ArrayList<AssessmentModel> getSyncPendingIndividualId() {

        ArrayList<AssessmentModel> modelList = new ArrayList<>();

        String query = "select TestScoreIndividualDetailsId, TotalQuestion from TestScoreIndividualDetails where IsSync = 0";
        Cursor cursor = mDataBase.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                AssessmentModel model = new AssessmentModel();
                model.setIndividualID(cursor.getString(0));
                model.setTestTotalQuestion(cursor.getString(1));
                modelList.add(model);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return modelList;
    }

    public ArrayList<AssessmentModel> getSyncPendingDetailsByID(String individualID) {

        ArrayList<AssessmentModel> modelArrayList = new ArrayList<>();

        String query = "SELECT TestScoreSummaryDetailsId, StudentUserCode, CourseContentCode, TestTotalMarks, TotalMarksObtained, TestTotalQuestion, TotalRightAnswerCount,\n" +
                "TotalWrongAnswerCount, LastAccessTime FROM TestScoreSummaryDetails where FirstTestScoreIndividualDetailsId = '" + individualID + "'";

        Cursor cursor = mDataBase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                AssessmentModel model = new AssessmentModel();
                model.setTestScoreSummaryDetailsId(cursor.getString(0));
                model.setStudentUserCode(cursor.getString(1));
                model.setCourseContentCode(cursor.getString(2));
                model.setTestTotalMarks(cursor.getString(3));
                model.setTotalMarksObtain(cursor.getString(4));
                model.setTestTotalQuestion(cursor.getString(5));
                model.setCounterRight(cursor.getInt(6));
                model.setCounterWrong(cursor.getInt(7));
                model.setLastAccessTime(cursor.getString(8));

                modelArrayList.add(model);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return modelArrayList;
    }

    public ArrayList<QuestionModel> getSyncPendingQuestions(String individualID) {
        ArrayList<QuestionModel> questionList = new ArrayList<>();

        String query = "SELECT TestResultDetailsId, QuestionCode, AttemptedAnswer, IsAnsweredCorrect, QuestionMarks, QuestionMarksObtained, AttemptedTestDate \n" +
                "FROM TestResultDetails where TestScoreIndividualDetailsId = '" + individualID + "'";

        Cursor cursor = mDataBase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                QuestionModel model = new QuestionModel();
                model.setTestResultDetailsId(cursor.getString(0));
                model.setQuestionCode(cursor.getString(1));
                model.setYourAns(cursor.getString(2));
                model.setRightAnsflag(cursor.getInt(3));
                model.setRightAnsMarks(cursor.getString(4));
                model.setRightMObt(cursor.getDouble(5));
                model.setAttemptedTestDate(cursor.getString(6));

                questionList.add(model);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return questionList;
    }

    public ArrayList<SubjectModel> getAllSubject(boolean checkForChapter) {
        ArrayList<SubjectModel> resultList = new ArrayList<SubjectModel>();
        //openDatabase();
        String query = "Select * from " + TableName_Subject + " where isDeleted=" + 0 + " order by SubjectSequenceNo";
        Cursor cursor = mDataBase.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                SubjectModel objSubject = new SubjectModel();
                objSubject.setSubjectId(cursor.getInt(0));
                objSubject.setSubjectName(cursor.getString(1));
                objSubject.setSubjectDisplayName(cursor.getString(2));
                objSubject.setSubjectSequenceNo(cursor.getInt(3));
                objSubject.setSubjectIcon(cursor.getString(4));
                objSubject.setIsDeleted(cursor.getString(5));
                if (!checkForChapter || isChapterExist(objSubject.getSubjectId())) {
                    Log.e("SubjectChapter", objSubject.SubjectDisplayName);
                    resultList.add(objSubject);
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        //mDataBase.close();
        return resultList;
    }

    public ArrayList<ResultModel> getScoreDetails(int subId, String startdate, String enddate) {
        ArrayList<ResultModel> lstResult = new ArrayList<ResultModel>();
        String usercode = SettingPreffrences.getUserid(appContext);
        String query = "Select TestScoreSummaryDetailsId,ProductCode,CourseContentCode,TestTotalQuestion,TotalRightAnswerCount,TotalWrongAnswerCount,LastAccessTime,StudentUserCode from " + TableName_Summary_Details + " summery where summery.CourseContentcode=" + subId + " and summery.StudentUserCode=" + (!usercode.isEmpty() ? usercode : "''") + " and summery.LastAccessTime >= '" + startdate + "' and summery.LastAccessTime <= '" + enddate + "'";
        Cursor cursor = mDataBase.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                ResultModel objchapter = new ResultModel();
                objchapter.setTestScoreSummaryDetailsId(cursor.getString(0));
                objchapter.setProductCode(cursor.getString(1));
                objchapter.setCourseContentCode(cursor.getString(2));
                objchapter.setTestTotalQuestion(cursor.getString(3));
                objchapter.setTotalRightAnswerCount(cursor.getString(4));
                objchapter.setTotalWrongAnswerCount(cursor.getString(5));
                objchapter.setLastAccessTime(cursor.getString(6));
                objchapter.setStudentUserCode(cursor.getString(7));
                lstResult.add(objchapter);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return lstResult;
    }

    public ArrayList<ChapterModel> getChapterBySubjectId(int subId) {
        ArrayList<ChapterModel> lstChapter = new ArrayList<ChapterModel>();

        String query = "Select distinct * from " + TableName_Chapter + " chp where SubjectId=" + subId + " and chp.isDeleted=" + 0 + " order by chp.ChapterSequenceNo";
        Cursor cursor = mDataBase.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                ChapterModel objchapter = new ChapterModel();
                String chapterid = cursor.getString(0);
                objchapter.setChapterId(chapterid);
                objchapter.setChapterName(cursor.getString(1));
                objchapter.setChapterDisplayName(cursor.getString(2));
                objchapter.setChapterSequenceNo(cursor.getInt(3));
                objchapter.setIsDeleted(cursor.getString(4));
                objchapter.setSubjectId(cursor.getInt(5));
                objchapter.setmTopicList(getTopicByChapterId(chapterid));
                lstChapter.add(objchapter);
            } while (cursor.moveToNext());
        }
        cursor.close();
        //mDataBase.close();
        return lstChapter;
    }

    public boolean isChapterExist(int subId) {
        //   select count(group_is_paid) from ds_groupdetails where group_is_paid = 1
        String query = "select * from " + TableName_Chapter + " chp where SubjectId=" + subId;
        Cursor cursor = mDataBase.rawQuery(query, null);
        boolean isExist = false;
        if (cursor != null && cursor.moveToFirst()) {
            isExist = true;
            cursor.close();
        }
        return isExist;
    }

    public ArrayList<TopicModel> getTopicByChapterId(String ChpId) {
        ArrayList<TopicModel> lstTopic = new ArrayList<TopicModel>();

        String query = "Select distinct * from " + TableName_Topic + " tp where ChapterId=" + ChpId + " and tp.isDeleted=" + 0 + " order by tp.TopicSequenceNo";
        Cursor cursor = mDataBase.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                TopicModel objtopic = new TopicModel();
                objtopic.setTopicId(cursor.getInt(0));
                objtopic.setTopicName(cursor.getString(1));
                objtopic.setTopicDisplayName(cursor.getString(2));
                objtopic.setTopicSequenceNo(cursor.getInt(3));
                objtopic.setIsDeleted(cursor.getString(4));
                objtopic.setChapterId(cursor.getInt(5));
                objtopic.setTopicChecked(ConstantManager.CHECK_BOX_CHECKED_FALSE);
                lstTopic.add(objtopic);
            } while (cursor.moveToNext());
        }
        cursor.close();
        //mDataBase.close();
        return lstTopic;
    }

    public ArrayList<QuestionModel> getQuestionByIds(String Chapter_ids, String Topic_ids, String Level_ids, int subid) {
        ArrayList<QuestionModel> resultdata = new ArrayList<QuestionModel>();
        String query = "";
        query = "Select * from " + TableName_Question + " where ChapterID IN (" + ((Chapter_ids != "") ? (Chapter_ids) : "'chapterId'") + ") and QuestionLevelId IN (" + Level_ids + ") and isDeleted=" + 0 + " and AnswerText is not null and AnswerText != '' union Select * from " + TableName_Question + " where TopicId IN(" + (Topic_ids != "" ? (Topic_ids) : "'topicId'") + ") and QuestionLevelId IN (" + Level_ids + ") and isDeleted=" + 0 + " and AnswerText is not null and AnswerText != ''";
        Cursor cursor = mDataBase.rawQuery(query, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    QuestionModel objQuestion = new QuestionModel();
                    objQuestion.setQuestionCode(cursor.getString(0));
                    objQuestion.setQuestionText(cursor.getString(1));
                    objQuestion.setOption1(cursor.getString(2));
                    objQuestion.setOption2(cursor.getString(3));
                    objQuestion.setOption3(cursor.getString(4));
                    objQuestion.setOption4(cursor.getString(5));
                    objQuestion.setOption5(cursor.getString(6));
                    objQuestion.setOption6(cursor.getString(7));
                    objQuestion.setRightAnsMarks(cursor.getString(8));
                    objQuestion.setWrongAnsMarks(cursor.getString(9));
                    objQuestion.setSkipAnsMarks(cursor.getString(10));
                    objQuestion.setAnswerText(cursor.getString(11));
                    objQuestion.setSolution(cursor.getString(12));
                    objQuestion.setIsDeleted(cursor.getString(13));
                    objQuestion.setOptionsNo(cursor.getInt(14));
                    objQuestion.setDuration(cursor.getInt(15));
                    objQuestion.setQuestionLevelId(cursor.getInt(16));
                    objQuestion.setCourseCode(cursor.getString(17));
                    objQuestion.setSubjectId(cursor.getInt(18));
                    objQuestion.setChapterId(cursor.getInt(19));
                    objQuestion.setTopicId(cursor.getInt(20));
                    resultdata.add(objQuestion);

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception c) {
            cursor.close();
        }
        return resultdata;
    }

    public boolean InsertDemo(SubjectModel objsub) {
        boolean breturn = false;
        ContentValues contentValues = new ContentValues();
        contentValues.put("SubjectID", objsub.getSubjectId());
        contentValues.put("SubjectName", objsub.getSubjectName());
        contentValues.put("SubjectDisplayName", objsub.getSubjectDisplayName());
        contentValues.put("SubjectSequenceNo", objsub.getSubjectSequenceNo());
        contentValues.put("SubjectIcon", objsub.getSubjectIcon());
        contentValues.put("IsDeleted", objsub.getIsDeleted());
        long i = mDataBase.insert("Subject", null, contentValues);
        Log.e("Row Inserted", i + "");
        breturn = true;
        return breturn;
    }

    public boolean updateSyncAssessmentData(String individualID) {
        boolean result = false;
        ContentValues values = new ContentValues();
        values.put(Is_Sync, "1");
        int id = mDataBase.update(TableName_Score_Individual_Details, values, "TestScoreIndividualDetailsId ='" + individualID + "'", null);
        if (id > 0) {
            result = true;
        }
        Log.d("AssessmentLogs", "Updated: " + id + " boolean: " + result);
        return result;
    }


    public boolean InsertTestResult(QuestionModel mQuestion, int i, String IndividualId, String uuid) {
        boolean result = false;
        if (mQuestion != null) {
            double totalMarks;
            if (mQuestion.getWrongMObt() > 0) {
                totalMarks = mQuestion.getRightMObt() - mQuestion.getWrongMObt();
            } else {
                totalMarks = mQuestion.getRightMObt() + mQuestion.getWrongMObt();
            }
            ContentValues values = new ContentValues();
            values.put(Test_Result_Details_Id, uuid);
            values.put(Test_Score_Individual_Details_Id, IndividualId);
            values.put(Product_Code, SettingPreffrences.getStandard_ID(appContext));
            values.put(Course_Content_Code, ConstantManager.SUBJECT_ID);
            values.put(Test_Code, "0");
            values.put(Question_Code, mQuestion.getQuestionCode());
            values.put(Is_Question_Skipped, mQuestion.getSkipflag());
            values.put(Is_Answered_Correct, mQuestion.getRightAnsflag());
            values.put(Attempted_Answer, mQuestion.getYourAns());
            values.put(Question_Marks, mQuestion.getRightAnsMarks());
            values.put(Question_Marks_Obtained, mQuestion.getRightMObt());
            values.put(Attempted_Test_Date, DateUtil.getCurrentDateTimeInFormat("yyyy-MM-dd HH:mm:ss"));
            values.put(Answer_Order_Seq, "");
            values.put(Is_Sync, "0");
            values.put(Student_Code, SettingPreffrences.getUserid(appContext));
            values.put(Mac_Id, SettingPreffrences.getMacId(appContext));
            long id = mDataBase.insert(TableName_Results_Details, null, values);
            Log.e("Statistics", "" + id);

        }
        return result;
    }

    public long insertTestSummeryData_Sqlite(AssessmentModel assessment, String id, String test_Score_Summary_Details_Id) {
        long result = 0;
        if (assessment != null) {
            double totalMarks = 0;
            if (assessment.getTotalWrongmarksObtained() > 0) {
                totalMarks = assessment.getTotalRightmarksObtained() - assessment.getTotalWrongmarksObtained();
            } else {
                totalMarks = assessment.getTotalRightmarksObtained() + assessment.getTotalWrongmarksObtained();
            }

            Log.d("DataChecker", "total: " + assessment.getTestTotalMarks() + " Right: " + assessment.getTotalRightmarksObtained() + " Wrong: " + assessment.getTotalWrongmarksObtained() + " string: " + totalMarks);

            ContentValues values = new ContentValues();
            values.put(Test_Score_Summary_Details_Id, test_Score_Summary_Details_Id);
            values.put(Test_Type_Code, "");
            values.put(Product_Code, SettingPreffrences.getStandard_ID(appContext));
            values.put(Course_Content_Code, ConstantManager.SUBJECT_ID);
            values.put(Test_Code, assessment.getTestCode());
            values.put(Test_Actual_Duration, " ");
            values.put(Test_Total_Attempted_Duration, " ");
            values.put(Test_Total_Marks, assessment.getTestTotalMarks());
            values.put(Total_Marks_Obtained, totalMarks);
            values.put(Test_Attempted_Count, "1");
            values.put(Test_Total_Question, assessment.getTestTotalQuestion());
            values.put(Total_Skip_Question_Count, assessment.getCounterSkip());
            values.put(Total_Right_Answer_Count, assessment.getCounterRight());
            values.put(Total_Wrong_Answer_Count, assessment.getCounterWrong());
            values.put(Test_Best_Score_Obtained, totalMarks);
            values.put(First_Attempt_Marks_Obtained, totalMarks);
            values.put(Last_Attempt_Marks_Obtained, totalMarks);
            values.put(Test_Rank, "");
            values.put(First_Test_Score_Individual_Details_Id, id);
            values.put(Last_Test_Score_Individual_Details_Id, id);
            values.put(Is_Solution_Viewed, 0);
            values.put(Test_Status, " ");
            values.put(Solution_Status, " ");
            values.put(Is_Sync, 0);
            values.put(Last_Access_Time, DateUtil.getCurrentDateTimeInFormat("yyyy-MM-dd HH:mm:ss"));
            values.put(Student_Code, SettingPreffrences.getUserid(appContext));
            values.put(Mac_Id, SettingPreffrences.getMacId(appContext));
            long i = mDataBase.insert(TableName_Summary_Details, null, values);
            Log.e("Statistics", "" + result);
        }
        return result;
    }

    public String insertIntoTestScoreIndividualDetails(AssessmentModel mAssessment) {
        double totalMarks = 0;

        /*if (mAssessment.getTotalWrongmarksObtained() > 0) {
            totalMarks = mAssessment.getTotalRightmarksObtained() - mAssessment.getTotalWrongmarksObtained();
        } else {
            totalMarks = mAssessment.getTotalRightmarksObtained() + mAssessment.getTotalWrongmarksObtained();
        }
        if (totalMarks < 0) {
            percentage = 0;
        } else {
            percentage = (totalMarks * 100) / Double.valueOf(mAssessment.getTotalRightmarksObtained());
        }*/

        String id = UUID.randomUUID() + "";
        ContentValues values = new ContentValues();
        values.put(Test_Score_Individual_Details_Id, id);
        values.put(Test_Type_Code, "0");
        values.put(Product_Code, SettingPreffrences.getStandard_ID(appContext));
        values.put(Course_Content_Code, ConstantManager.SUBJECT_ID);
        values.put(Test_Code, 0);
        values.put(Test_Actual_Duration, "0");
        values.put(Test_Attempted_Duration, "0");
        values.put(Test_Total_Marks, mAssessment.getTestTotalMarks());
        values.put(Marks_Obtained, mAssessment.getTotalRightmarksObtained());
        values.put(Total_Question, mAssessment.getTestTotalQuestion());
        values.put(Skip_Question_Count, mAssessment.getCounterSkip());
        values.put(Right_Answer_Count, mAssessment.getCounterRight());
        values.put(Wrong_Answer_Count, mAssessment.getCounterWrong());
        values.put(Percentage, ConstantManager.SCORE);
        values.put(Access_Date_Time, DateUtil.getCurrentDateTimeInFormat("yyyy-MM-dd HH:mm:ss"));
        values.put(Is_Sync, 0);
        values.put(Student_Code, SettingPreffrences.getUserid(appContext));
        values.put(Mac_Id, SettingPreffrences.getMacId(appContext));
        long i = mDataBase.insert(TableName_Score_Individual_Details, null, values);
        Log.e("Statistics", "" + i);
        return id;
    }

    public ArrayList<QuestionImageModel> getQuestionImage(String QuestionCode) {
        Log.e("Questioncode", QuestionCode);
        ArrayList<QuestionImageModel> lstQuest_img = new ArrayList<QuestionImageModel>();
      /*  try {
        //    SELECT id, length(blobcolumn) FROM mytable WHERE length(blobcolumn) > 1000000
            String Query = "Select * from " + TableName_QuestionImage + " where QuestionCode='" + QuestionCode + "'";
            Cursor cursor = mDataBase.rawQuery(Query, null);
            Log.d("McqImage", "Query: " + Query);
            if (cursor.moveToFirst()) {
                do {
                    QuestionImageModel objQuestimg = new QuestionImageModel();
                    objQuestimg.setQuestionCode(cursor.getString(0));
                    Log.d("McqImage", "ImageName: " + cursor.getString(1));
                    objQuestimg.setImageName(cursor.getString(1));
                    objQuestimg.setDataUri(cursor.getString(2));
                    lstQuest_img.add(objQuestimg);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }catch(Exception e){
            Log.e("Error",e.getMessage());
        }*/

        try {
            //      //select length(city_name) FROM ds_city where city_name = 'Mumbai'
            String lengthQuery = "select length(DataUri),ImageName FROM " + TableName_QuestionImage + " where QuestionCode='" + QuestionCode + "'";
            Cursor lenCursor = mDataBase.rawQuery(lengthQuery, null);

            if (lenCursor.moveToFirst()) {
                do {
                    long totalLen = lenCursor.getLong(0);
                    if (totalLen > 1000000) {
                        StringBuilder strImage = new StringBuilder("");
                        Log.d("McqImage", "lengthQuery: " + totalLen);
                        int countOfImgs = ((int) (totalLen / 1000000)) + 1;
                        long countOfLength = 1000000;
                        long initialCount = 1;
                        for (int i = 0; i < countOfImgs; i++) {
                            if (i == countOfImgs - 1) {
                                countOfLength = (totalLen % 1000000);
                            }
                            String subQuery = "select substr(DataUri," + initialCount + "," + countOfLength + ") FROM " + TableName_QuestionImage + " where QuestionCode='" + QuestionCode + "' and ImageName='"+lenCursor.getString(1)+"'";
                            Cursor cursor = mDataBase.rawQuery(subQuery, null);
                            if (cursor.moveToFirst()) {
                                strImage.append(cursor.getString(0));
                            }
                            initialCount = initialCount + countOfLength;
                            cursor.close();
                        }
                        QuestionImageModel objQuestimg = new QuestionImageModel();
                        objQuestimg.setQuestionCode(QuestionCode);
                        Log.d("McqImage", "ImageName: " + lenCursor.getString(1));
                        objQuestimg.setImageName(lenCursor.getString(1));
                        objQuestimg.setDataUri(strImage.toString());
                        lstQuest_img.add(objQuestimg);
                    } else {
                        String subQuery = "select DataUri FROM " + TableName_QuestionImage + " where QuestionCode='" + QuestionCode + "' and ImageName='"+lenCursor.getString(1)+"'";
                        Cursor cursor = mDataBase.rawQuery(subQuery, null);
                        Log.d("McqImage", "Query: " + subQuery);
                        if (cursor.moveToFirst()) {
                            QuestionImageModel objQuestimg = new QuestionImageModel();
                            objQuestimg.setQuestionCode(QuestionCode);
                            Log.d("McqImage", "ImageName: " + lenCursor.getString(1));
                            objQuestimg.setImageName(lenCursor.getString(1));
                            objQuestimg.setDataUri(cursor.getString(0));
                            lstQuest_img.add(objQuestimg);
                        }
                        cursor.close();
                    }

                } while (lenCursor.moveToNext());
            }

            lenCursor.close();
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }


        return lstQuest_img;
    }


    public static AssessmentModel GetAssessmentByTestCode(String totalTestMarks) {
        AssessmentModel assessmentModel = null;
        assessmentModel = new AssessmentModel();
        assessmentModel.setCourseContentCode(String.valueOf(ConstantManager.SUBJECT_ID));
        assessmentModel.setCourseContentName("");
        assessmentModel.setCourseContentDisplayName("");
        assessmentModel.setTestCode("0");
        assessmentModel.setTestDisplayName("");
        assessmentModel.setTestQuestionCode("0");
        assessmentModel.setTestAttemptedCount("1");
        assessmentModel.setTestTotalMarks(String.valueOf(totalTestMarks));
        assessmentModel.setTestTotalQuestion(String.valueOf(ConstantManager.questionmodel.size()));
        return assessmentModel;
    }

    public void alterTable() {

        ArrayList<String> tableArr = new ArrayList<String>();
        tableArr.add(TableName_Results_Details);
        tableArr.add(TableName_Summary_Details);
        tableArr.add(TableName_Score_Individual_Details);
        for (int i = 0; i < tableArr.size(); i++) {
            if (!isColumnExists(tableArr.get(i), "StudentUserCode")) {
                String sql1 = "ALTER TABLE " + tableArr.get(i) + " ADD StudentUserCode TEXT";
                mDataBase.execSQL(sql1);
            }
            if (!isColumnExists(tableArr.get(i), "MacId")) {
                String sql2 = "ALTER TABLE " + tableArr.get(i) + " ADD MacId TEXT";
                mDataBase.execSQL(sql2);
            }
        }
    }

    public boolean isColumnExists(String tableName, String columnName) {
        boolean result = false;
        Cursor cursor = null;
        try {
            String sql = "SELECT " + columnName + " from " + tableName;
            cursor = mDataBase.rawQuery(sql, null);
            if (cursor != null) {
                result = true;
            }

        } catch (Exception e) {
            try {
                // //System.err.println(e.getClass().getName() + ": " + e.getMessage());

            } catch (SQLException ex) {
                Logger.getLogger(SQLiteDB.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return result;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
