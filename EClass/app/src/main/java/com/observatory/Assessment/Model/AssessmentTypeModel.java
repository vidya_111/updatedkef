package com.observatory.Assessment.Model;

public class AssessmentTypeModel {
    
    public String TypenName;
    public int TypeIcon;

    public AssessmentTypeModel(String mcq, int mcq_test) {
        this.TypenName=mcq;
        this.TypeIcon=mcq_test;
    }

    public String getTypenName() {
        return TypenName;
    }

    public void setTypenName(String typenName) {
        TypenName = typenName;
    }

    public int getTypeIcon() {
        return TypeIcon;
    }

    public void setTypeIcon(int typeIcon) {
        TypeIcon = typeIcon;
    }
}
