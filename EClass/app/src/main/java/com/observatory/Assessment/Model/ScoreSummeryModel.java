package com.observatory.Assessment.Model;

public class ScoreSummeryModel {
    public String TestScoreSummaryDetailsId;
    public int TestTypeCode;
    public String ProductCode;
    public String CourseContentCode;
    public String TestCode;
    public boolean TestActualDuration;
    public String TestTotalAttemptedDuration;
    public String TestTotalMarks;
    public String TotalMarksObtained;
    public String TestAttemptedCount;
    public String TestTotalQuestion;
    public String TotalSkipQuestionCount;
    public String TotalRightAnswerCount;
    public String TotalWrongAnswerCount;
    public String TestBestScoreObtained;
    public String FirstAttemptMarksObtained;
    public String LastAttemptMarksObtained;
    public String TestRank;
    public String FirstTestScoreIndividualDetailsId;
    public String LastTestScoreIndividualDetailsId;
    public String IsSolutionViewed;
    public int TestStatus;
    public int SolutionStatus;
    public int IsSync;
    public String LastAccessTime;
    public String StudentUserCode;
    public String MacId;

    public String getTestScoreSummaryDetailsId() {
        return TestScoreSummaryDetailsId;
    }

    public void setTestScoreSummaryDetailsId(String testScoreSummaryDetailsId) {
        TestScoreSummaryDetailsId = testScoreSummaryDetailsId;
    }

    public int getTestTypeCode() {
        return TestTypeCode;
    }

    public void setTestTypeCode(int testTypeCode) {
        TestTypeCode = testTypeCode;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getCourseContentCode() {
        return CourseContentCode;
    }

    public void setCourseContentCode(String courseContentCode) {
        CourseContentCode = courseContentCode;
    }

    public String getTestCode() {
        return TestCode;
    }

    public void setTestCode(String testCode) {
        TestCode = testCode;
    }

    public boolean isTestActualDuration() {
        return TestActualDuration;
    }

    public void setTestActualDuration(boolean testActualDuration) {
        TestActualDuration = testActualDuration;
    }

    public String getTestTotalAttemptedDuration() {
        return TestTotalAttemptedDuration;
    }

    public void setTestTotalAttemptedDuration(String testTotalAttemptedDuration) {
        TestTotalAttemptedDuration = testTotalAttemptedDuration;
    }

    public String getTestTotalMarks() {
        return TestTotalMarks;
    }

    public void setTestTotalMarks(String testTotalMarks) {
        TestTotalMarks = testTotalMarks;
    }

    public String getTotalMarksObtained() {
        return TotalMarksObtained;
    }

    public void setTotalMarksObtained(String totalMarksObtained) {
        TotalMarksObtained = totalMarksObtained;
    }

    public String getTestAttemptedCount() {
        return TestAttemptedCount;
    }

    public void setTestAttemptedCount(String testAttemptedCount) {
        TestAttemptedCount = testAttemptedCount;
    }

    public String getTestTotalQuestion() {
        return TestTotalQuestion;
    }

    public void setTestTotalQuestion(String testTotalQuestion) {
        TestTotalQuestion = testTotalQuestion;
    }

    public String getTotalSkipQuestionCount() {
        return TotalSkipQuestionCount;
    }

    public void setTotalSkipQuestionCount(String totalSkipQuestionCount) {
        TotalSkipQuestionCount = totalSkipQuestionCount;
    }

    public String getTotalRightAnswerCount() {
        return TotalRightAnswerCount;
    }

    public void setTotalRightAnswerCount(String totalRightAnswerCount) {
        TotalRightAnswerCount = totalRightAnswerCount;
    }

    public String getTotalWrongAnswerCount() {
        return TotalWrongAnswerCount;
    }

    public void setTotalWrongAnswerCount(String totalWrongAnswerCount) {
        TotalWrongAnswerCount = totalWrongAnswerCount;
    }

    public String getTestBestScoreObtained() {
        return TestBestScoreObtained;
    }

    public void setTestBestScoreObtained(String testBestScoreObtained) {
        TestBestScoreObtained = testBestScoreObtained;
    }

    public String getFirstAttemptMarksObtained() {
        return FirstAttemptMarksObtained;
    }

    public void setFirstAttemptMarksObtained(String firstAttemptMarksObtained) {
        FirstAttemptMarksObtained = firstAttemptMarksObtained;
    }

    public String getLastAttemptMarksObtained() {
        return LastAttemptMarksObtained;
    }

    public void setLastAttemptMarksObtained(String lastAttemptMarksObtained) {
        LastAttemptMarksObtained = lastAttemptMarksObtained;
    }

    public String getTestRank() {
        return TestRank;
    }

    public void setTestRank(String testRank) {
        TestRank = testRank;
    }

    public String getFirstTestScoreIndividualDetailsId() {
        return FirstTestScoreIndividualDetailsId;
    }

    public void setFirstTestScoreIndividualDetailsId(String firstTestScoreIndividualDetailsId) {
        FirstTestScoreIndividualDetailsId = firstTestScoreIndividualDetailsId;
    }

    public String getLastTestScoreIndividualDetailsId() {
        return LastTestScoreIndividualDetailsId;
    }

    public void setLastTestScoreIndividualDetailsId(String lastTestScoreIndividualDetailsId) {
        LastTestScoreIndividualDetailsId = lastTestScoreIndividualDetailsId;
    }

    public String getIsSolutionViewed() {
        return IsSolutionViewed;
    }

    public void setIsSolutionViewed(String isSolutionViewed) {
        IsSolutionViewed = isSolutionViewed;
    }

    public int getTestStatus() {
        return TestStatus;
    }

    public void setTestStatus(int testStatus) {
        TestStatus = testStatus;
    }

    public int getSolutionStatus() {
        return SolutionStatus;
    }

    public void setSolutionStatus(int solutionStatus) {
        SolutionStatus = solutionStatus;
    }

    public int getIsSync() {
        return IsSync;
    }

    public void setIsSync(int isSync) {
        IsSync = isSync;
    }

    public String getLastAccessTime() {
        return LastAccessTime;
    }

    public void setLastAccessTime(String lastAccessTime) {
        LastAccessTime = lastAccessTime;
    }

    public String getStudentUserCode() {
        return StudentUserCode;
    }

    public void setStudentUserCode(String studentUserCode) {
        StudentUserCode = studentUserCode;
    }

    public String getMacId() {
        return MacId;
    }

    public void setMacId(String macId) {
        MacId = macId;
    }
}
