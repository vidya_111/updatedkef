package com.observatory.Assessment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import android.util.Pair;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.observatory.Assessment.DataBase.SQLiteDB;
import com.observatory.Assessment.DataBase.WebService;
import com.observatory.Assessment.Model.AssessmentModel;
import com.observatory.Assessment.Model.AssessmentTypeModel;
import com.observatory.Assessment.Model.QuestionModel;
import com.observatory.Assessment.PracticeTest.PracticesubjectActivity;
import com.observatory.Assessment.Reports.ReportsActivity;
import com.observatory.AsyncTaskMain.AsyncTaskHelper;
import com.observatory.AsyncTaskMain.OnTaskComplete;
import com.observatory.kefcorner.Doubt_Conversations;
import com.observatory.kefcorner.R;
import com.observatory.kefcorner.Splash;
import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;

import io.fabric.sdk.android.services.concurrency.AsyncTask;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.observatory.Assessment.DataBase.WebService.BASE_URL_ASSESS;


public class AssessmentTypesActivity extends BaseActivity {


    private RecyclerView rvSubjects;
    AssessmentTypeAdapter typeAdapter;
    ArrayList<AssessmentTypeModel> arr_type;
    GridLayoutManager mLayoutManager;
    String MediumName = "", Path = "";
    boolean istablet = false;
    boolean Revise_check = false, Practice_check = false, Others_check = false;
    SQLiteDB dbhelper;
    /*Webservice*/
    private AsyncTaskHelper asyncTaskHelper;
    private String params;
    DownloadDbFile downloadDbFile;
    String filename = "";
    String filepath = "";
    String new_file_name = "";
    ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessment_types);
        actionBar.setTitle("Select Assessment Type");
        dbhelper = SQLiteDB.getInstance(AssessmentTypesActivity.this, ConstantManager.DB_PATH);
        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            istablet = isTablet(AssessmentTypesActivity.this);

        } else {
          //  Toast.makeText(this, "Landscape Mode", Toast.LENGTH_LONG).show();
        }
        arr_type = new ArrayList<>();
        CheckDirectory();
        typeAdapter = new AssessmentTypeAdapter(arr_type);
        if (arr_type.size() == 4 || arr_type.size() == 2) {
            mLayoutManager = new GridLayoutManager(this, 2);
        } else {
            mLayoutManager = new GridLayoutManager(this, 6);
            mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int i) {
                    if (i < 3) {
                        return 2;
                    } else {
                        if (i == 3)
                            return 1;
                        else
                            return 2;
                    }
                }
            });
        }
        rvSubjects.setLayoutManager(mLayoutManager);
        rvSubjects.setAdapter(typeAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.assessment_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    public void CheckDirectory() {
        MediumName = ConstantManager.STANDARD_FOLDERNAME;
        Log.d("AssessmentLogs", "mediumName: " + MediumName + " BasePath: " + App.BASE_PATH);
        if (!App.BASE_PATH.contains(MediumName)) {

            Log.d("AssessmentLogs", "IsSingleStandard: " + ConstantManager.ISSINGLESTANDARD);

            if (!ConstantManager.ISSINGLESTANDARD) {
                Path = App.BASE_PATH + MediumName + "/" + "Assess" + "/";
            } else {
                Path = App.BASE_PATH + "Assess" + "/";
            }


        } else {
            Path = App.BASE_PATH + "Assess" + "/";
        }

        Log.d("AssessmentLogs", "Path: " + Path);

        File directory = new File(Path);
        if (directory != null && directory.exists()) {
            File[] files = directory.listFiles();
            if (files != null) {
                for (File inFile : files) {
                    if (inFile.isDirectory()) {
                        File subj_dir = new File(inFile.getAbsolutePath());
                        File[] sub_files = subj_dir.listFiles();
                        if (sub_files != null) {
                            for (File infile_subj : sub_files) {
                                if (infile_subj.getName().equals("RV")) {
                                    Revise_check = true;
                                } else if (infile_subj.getName().equals("PP")) {
                                    Practice_check = true;
                                } else if (infile_subj.getName().equals("OF")) {
                                    Others_check = true;
                                }
                            }
                        }
                        if (Revise_check && Practice_check && Others_check) {
                            break;
                        }
                    }
                }
            }
        }
        if (Revise_check && Practice_check && Others_check) {

            arr_type.add(new AssessmentTypeModel("mcq", R.drawable.mcq_test));
            arr_type.add(new AssessmentTypeModel("practicetest", R.drawable.practice_test));
            arr_type.add(new AssessmentTypeModel("revisetest", R.drawable.revise_test));
            arr_type.add(new AssessmentTypeModel("", 0));
            arr_type.add(new AssessmentTypeModel("report", R.drawable.report));
            arr_type.add(new AssessmentTypeModel("otherfiles", R.drawable.ic_otherfiles));


        } else {
            arr_type.add(new AssessmentTypeModel("mcq", R.drawable.mcq_test));
            if (Practice_check)
                arr_type.add(new AssessmentTypeModel("practicetest", R.drawable.practice_test));
            if (Revise_check)
                arr_type.add(new AssessmentTypeModel("revisetest", R.drawable.revise_test));
            arr_type.add(new AssessmentTypeModel("report", R.drawable.report));
            if (Others_check)
                arr_type.add(new AssessmentTypeModel("otherfiles", R.drawable.ic_otherfiles));

        }
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    @Override
    protected void onLayout() {
        rvSubjects = (RecyclerView) findViewById(R.id.rv_type);
    }

    @Override
    public void onBackPressed() {
        if (App.IS_HAVING_MULTIPLE_STANDARD) {
          /*  startActivity(new Intent(this, Splash.class));
            finish();*/
        } else {
            //super.onBackPressed();
            if (ConstantManager.ISSINGLESTANDARD) {
              /*  startActivity(new Intent(this, Splash.class));
                finish();*/
            } else {
              //  super.onBackPressed();
            }

        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            startActivity(new Intent(this, Splash.class));
            finish();
            return true;
        }

        if (id == R.id.downloadData) {
            showPermissionToSyncData();
        }
        return false;
    }

    private void showPermissionToSyncData() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        builder.setTitle("");
        builder.setMessage("Are you sure, you want to sync the data?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                syncPendingData();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public void syncPendingData() {

        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {
            ArrayList<AssessmentModel> pendingList = new ArrayList<>();
            pendingList = dbhelper.getSyncPendingIndividualId();

            if (pendingList.size() > 0) {
               // for (int i = 0; i < pendingList.size(); i++) {
                    Log.d("PendingList", "IndividualID: " + pendingList.get(0).getIndividualID());
                    Log.d("PendingList", "TotalQuestions: " + pendingList.get(0).getTestTotalQuestion());
                    getDetailsFromPendingId(pendingList.get(0).getIndividualID());
               // }
            } else {
                //Replace and download new data
                Log.d("PendingList", "Size: " + pendingList.size());
                updateAssessment(SettingPreffrences.getCourseId(getApplicationContext()), "A");
            }
        } else {
            alert("Internet Connection not available");
        }

    }

    public void getDetailsFromPendingId(String individualId) {
        ArrayList<AssessmentModel> dataList = new ArrayList<>();
        dataList = dbhelper.getSyncPendingDetailsByID(individualId);

        if (dataList.size() > 0) {
            for (int i = 0; i < dataList.size(); i++) {
                Log.d("PendingList11", "TotalQuestion: " + dataList.get(i).getTestTotalQuestion() + "Size: " + dataList.size());
                insertDetailsToServer(
                        individualId,
                        dataList.get(i).getTestScoreSummaryDetailsId(),
                        dataList.get(i).getStudentUserCode(),
                        SettingPreffrences.getCourseId(getApplicationContext()),
                        dataList.get(i).getCourseContentCode(),
                        Integer.parseInt(String.valueOf(Double.valueOf(dataList.get(i).getTestTotalMarks()).intValue())),
                        Integer.parseInt(String.valueOf(Double.valueOf(dataList.get(i).getTotalMarksObtain()).intValue())),
                        dataList.get(i).getTestTotalQuestion(),
                        dataList.get(i).getCounterRight(),
                        dataList.get(i).getCounterWrong(),
                        dataList.get(i).getLastAccessTime(),
                        "1",
                        "KEFM"
                );
            }
        }

    }

    public void insertDetailsToServer(String TestScoreIndividualDetailID, String TestScoreSummaryDetailID, String UserID, String CourseID,
                                      String SubjectID, int TestTotalMarks, int TestMarksObtained, String TotalQuestion, int RightAnswerCount,
                                      int WrongAnswerCount, String TestAttemptedDatetime, String TestAttemptedCount, String AppType) {

        StringBuilder str = new StringBuilder();
        ArrayList<QuestionModel> questionList = new ArrayList<>();
        questionList = dbhelper.getSyncPendingQuestions(TestScoreIndividualDetailID);

        if (questionList.size() > 0) {
            for (int i = 0; i < questionList.size(); i++) {
                str.append(createXmlData(
                        questionList.get(i).getTestResultDetailsId(),
                        questionList.get(i).getQuestionCode(),
                        questionList.get(i).getYourAns(),
                        questionList.get(i).getRightAnsflag(),
                        questionList.get(i).getRightAnsMarks(),
                        Integer.parseInt(String.valueOf(questionList.get(i).getRightMObt().intValue())),
                        questionList.get(i).getAttemptedTestDate()
                ));
            }
        }

        StringBuilder finalQuestionData = FinalXmlData(str.toString());

        JsonObject data = new JsonObject();
        data.addProperty("TestScoreIndividualDetailID", TestScoreIndividualDetailID);
        data.addProperty("TestScoreSummaryDetailID", TestScoreSummaryDetailID);
        data.addProperty("UserID", UserID);
        data.addProperty("EdzamCourseID", CourseID);
        data.addProperty("SubjectID", SubjectID);
        data.addProperty("SchoolID",SettingPreffrences.getSchoolId(getApplicationContext()));
        data.addProperty("TestTotalMarks", TestTotalMarks);
        data.addProperty("TestMarksObtained", TestMarksObtained);
        data.addProperty("TotalQuestion", TotalQuestion);
        data.addProperty("RightAnswerCount", String.valueOf(RightAnswerCount));
        data.addProperty("WrongAnswerCount", String.valueOf(WrongAnswerCount));
        data.addProperty("TestAttemptedDatetime", TestAttemptedDatetime);
        data.addProperty("TestAttemptedCount", TestAttemptedCount);
        data.addProperty("AppType", AppType);
        data.addProperty("QuestionXML", finalQuestionData.toString());

        Log.d("PendingList22", "JsonData: " + data.toString());

        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {
            try {
                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.KEF_mcqDataHost + NetworkUrl.KEF_insertMcqData, data, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {
                        Log.d("PendingList22", "ApiResponse -> InsertUpdateMCQTestResultDetails: " + response);
                        if (!response.equals("error")) {
                            boolean result = dbhelper.updateSyncAssessmentData(TestScoreIndividualDetailID);
                            if (result) {
                                syncPendingData();
                            }
                        }
                    }
                }, "POST");

                asyncTaskHelper.execute();

            } catch (Exception e) {
                Log.d("PendingList22", "ApiResponse -> InsertUpdateMCQTestResultDetails: Error :" + e.getMessage());
                e.printStackTrace();
            }
        }

    }

    private StringBuilder FinalXmlData(String value) {
        StringBuilder str = new StringBuilder();
        str.append("<Questions>");
        str.append(value);
        str.append("</Questions>");
        return str;
    }

    private StringBuilder createXmlData(String id, String questionCode, String attemptedAns, int isAnsCorrect,
                                        String questionMarks, int QuestionMarksObtain, String dateTime) {
        String isAnswerCorrect_value = "";
        if (isAnsCorrect == 0) {
            isAnswerCorrect_value = "False";
        } else {
            isAnswerCorrect_value = "True";
        }

        StringBuilder builder = new StringBuilder();
        builder.append("<Question>");
        builder.append("<TestScoreResultDetailId>");
        builder.append(id);
        builder.append("</TestScoreResultDetailId>");
        builder.append("<QuestionCode>");
        builder.append(questionCode);
        builder.append("</QuestionCode>");
        builder.append("<AttemptedAnswer>");
        builder.append(attemptedAns);
        builder.append("</AttemptedAnswer>");
        builder.append("<IsAnswerCorrect>");
        builder.append(isAnswerCorrect_value);
        builder.append("</IsAnswerCorrect>");
        builder.append("<QuestionMarks>");
        builder.append(questionMarks);
        builder.append("</QuestionMarks>");
        builder.append("<QuestionMarksObtained>");
        builder.append(QuestionMarksObtain);
        builder.append("</QuestionMarksObtained>");
        builder.append("<QuestionAttemptedDateTime>");
        builder.append(dateTime);
        builder.append("</QuestionAttemptedDateTime>");
        builder.append("</Question>");
        return builder;
    }

    public static long getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long bytesAvailable = stat.getBlockSize() * (long) stat.getBlockCount();
        long megAvailable = bytesAvailable / 1048576;
        return megAvailable;
    }

    public <T> T createService(Class<T> serviceClass, String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(new OkHttpClient.Builder().build())
                .build();
        return retrofit.create(serviceClass);
    }

    private void downloadData(String fileURL) {
        if (getAvailableInternalMemorySize() > 200) {
            if (CommonFunctions.isNetworkConnected(getApplicationContext())) {
                WebService downloadService = createService(WebService.class, BASE_URL_ASSESS);
                Call<ResponseBody> call = downloadService.downloadFileByUrl(fileURL);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            downloadDbFile = new DownloadDbFile();
                            downloadDbFile.execute(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }
        } else {
            alert("Insufficient Internal Storage!!");
        }
    }

    public void updateAssessment(String CourseID, String type) {
        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {
            try {

                params = "edzamCourseID=" + URLEncoder.encode(CourseID, "UTF-8")
                        + "&type=" + URLEncoder.encode(type, "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.DownloadAssessment, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {
                        Log.d("updateAssessment", "ApiResponse -> " + response);
                        if (!response.equals("error")) {
                            filepath = NetworkUrl.DownloadDbPath + response.replaceAll("^\"|\"$", "");
                            filename = filepath.substring(filepath.lastIndexOf('/') + 1);
                            downloadData(filepath);
                        }
                    }
                }, "POST");

                asyncTaskHelper.execute();

            } catch (Exception e) {
                Log.d("updateAssessment", "ApiResponse -> Error :" + e.getMessage());
                e.printStackTrace();
            }
        }
    }


    private class DownloadDbFile extends AsyncTask<ResponseBody, Pair<Integer, Long>, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //CommonFunctions.showDialog(AssessmentTypesActivity.this);
            progressBar = new ProgressDialog(AssessmentTypesActivity.this);
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setMessage("Updating and downloading Data.. Please Wait!");
            progressBar.show();
        }

        @Override
        protected String doInBackground(ResponseBody... sUrl) {
            if (CommonFunctions.isNetworkConnected(getApplicationContext())) {
                saveToDisk(sUrl[0]);

            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //CommonFunctions.hideDialog();
            progressBar.dismiss();
            if (result != null) {
                Log.d("updateAssessment", "result -> error: " + result);
                Toast.makeText(getApplicationContext(), "Download error: " + result, Toast.LENGTH_LONG).show();
            } else {
                Log.d("updateAssessment", "Download done");
                Toast.makeText(getApplicationContext(), "File downloaded", Toast.LENGTH_SHORT).show();

                ConstantManager.DB_PATH = ConstantManager.Directory_PATH + "/" + new_file_name;
                dbhelper = SQLiteDB.getInstance(AssessmentTypesActivity.this, ConstantManager.DB_PATH);
                dbhelper.alterTable();
            }
        }
    }

    private void saveToDisk(ResponseBody body) {
        boolean isDeleted = false;

        try {
            File file = new File(ConstantManager.Directory_PATH);
            if (file != null && file.exists()) {
                File[] files = file.listFiles();
                if (files != null) {
                    for (int i = 0; i < files.length; i++) {
                        if (files[i].getName().contains(".db")) {
                            files[i].delete();
                            isDeleted = true;
                        }
                    }
                }
            }

            if (isDeleted) {


                int contentLength = (int) body.contentLength();

                String[] split = filename.split("-");

                new_file_name = split[0] + "-AS.db";

                File destinationFile = new File(ConstantManager.Directory_PATH + "/" + new_file_name);


                Log.d("updateAssessment", "DownloadPath: " + destinationFile + " contentLength: " + contentLength);

                DataInputStream stream = new DataInputStream(body.byteStream());
                byte[] buffer = new byte[contentLength];
                int offset = 0;
                int bytesRead;
                while ((bytesRead = stream.read(buffer, offset, buffer.length - offset)) > -1) {
                    offset += bytesRead;
                }
                stream.close();

                DataOutputStream fos = new DataOutputStream(new FileOutputStream(destinationFile));
                fos.write(buffer);
                fos.flush();
                fos.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Exception", "Failed to save the file!");
            return;

        }


    }

    private void CopyDB(String src, String dest, String dbname) {
        try {
            boolean transfer = false;
            File dbFile = new File(src);
            InputStream mInput = new FileInputStream(dbFile);
            String outFileName = dest + "/" + dbname;
            OutputStream mOutput = new FileOutputStream(outFileName);
            byte[] mBuffer = new byte[1024];
            int mLength;
            while ((mLength = mInput.read(mBuffer)) > 0) {
                mOutput.write(mBuffer, 0, mLength);
            }
            mOutput.flush();
            mOutput.close();
            mInput.close();
        } catch (Exception e) {
            Log.e("Exception", "Copy File exc " + e.getMessage());
        }
    }

    private class AssessmentTypeAdapter extends RecyclerView.Adapter<AssessmentTypeAdapter.MyViewHolder> {

        ArrayList<AssessmentTypeModel> mList;
        private int selectedPos = RecyclerView.NO_POSITION;

        public AssessmentTypeAdapter(ArrayList<AssessmentTypeModel> arrayList) {
            this.mList = arrayList;
        }

        @NonNull
        @Override
        public AssessmentTypeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_assessmenttype, parent, false);
            return new AssessmentTypeAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull AssessmentTypeAdapter.MyViewHolder holder, int position) {
            holder.itemView.setSelected(selectedPos == position);
            if (istablet) {
                holder.imageView.setImageResource(mList.get(position).getTypeIcon());
            } else {
                holder.imageView.getLayoutParams().height = 250;
                holder.imageView.setImageResource(mList.get(position).getTypeIcon());
            }
        }

        @Override
        public int getItemCount() {
            return mList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView imageView;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.img);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        notifyItemChanged(selectedPos);
                        selectedPos = getAdapterPosition();
                        String typename = mList.get(selectedPos).getTypenName();
                        if (typename.equals("mcq")) {
                            ConstantManager.ASSESSMENT_TYPE = "MCQ";
                            startActivity(new Intent(AssessmentTypesActivity.this, AssessmentSubjects.class));
                        } else if (typename.equals("practicetest")) {
                            ConstantManager.ASSESSMENT_TYPE = "PP";
                            startActivity(new Intent(AssessmentTypesActivity.this, PracticesubjectActivity.class));
                        } else if (typename.equals("revisetest")) {
                            ConstantManager.ASSESSMENT_TYPE = "RV";
                            startActivity(new Intent(AssessmentTypesActivity.this, PracticesubjectActivity.class));
                        } else if (typename.equals("report")) {
                            ConstantManager.ASSESSMENT_TYPE = "REP";
                            startActivity(new Intent(AssessmentTypesActivity.this, ReportsActivity.class));
                        } else if (typename.equals("otherfiles")) {
                            ConstantManager.ASSESSMENT_TYPE = "OF";
                            startActivity(new Intent(AssessmentTypesActivity.this, PracticesubjectActivity.class));
                        }
                    }
                });
            }
        }
    }
}
