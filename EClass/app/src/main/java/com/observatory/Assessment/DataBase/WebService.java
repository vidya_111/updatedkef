package com.observatory.Assessment.DataBase;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface WebService {

    public static final String BASE_URL_ASSESS = "http://eclasswindows.in/";

    @Streaming
    @GET
    Call<ResponseBody> downloadFileByUrl(@Url String fileUrl);
}
