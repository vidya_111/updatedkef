package com.observatory.Assessment.Model;

import java.util.ArrayList;

public class QuestionImageModel {
    public String QuestionCode;
    public String DataUri;
    public String ImageName;


    public String getQuestionCode() {
        return QuestionCode;
    }

    public void setQuestionCode(String questionCode) {
        QuestionCode = questionCode;
    }

    public String getDataUri() {
        return DataUri;
    }

    public void setDataUri(String dataUri) {
        DataUri = dataUri;
    }

    public String getImageName() {
        return ImageName;
    }

    public void setImageName(String imageName) {
        ImageName = imageName;
    }
}
