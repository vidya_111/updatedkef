package com.observatory.Assessment.Model;

import com.orm.SugarRecord;

public class SubjectModel extends SugarRecord {

    public Integer SubjectId;
    public String SubjectName;
    public String SubjectDisplayName;
    public Integer SubjectSequenceNo;
    public String SubjectIcon;
    public String IsDeleted;

    public SubjectModel() {
    }


    public Integer getSubjectId() {
        return SubjectId;
    }

    public void setSubjectId(Integer subjectId) {
        SubjectId = subjectId;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getSubjectDisplayName() {
        return SubjectDisplayName;
    }

    public void setSubjectDisplayName(String subjectDisplayName) {
        SubjectDisplayName = subjectDisplayName;
    }

    public Integer getSubjectSequenceNo() {
        return SubjectSequenceNo;
    }

    public void setSubjectSequenceNo(Integer subjectSequenceNo) {
        SubjectSequenceNo = subjectSequenceNo;
    }

    public String getSubjectIcon() {
        return SubjectIcon;
    }

    public void setSubjectIcon(String subjectIcon) {
        SubjectIcon = subjectIcon;
    }

    public String getIsDeleted() {
        return IsDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        IsDeleted = isDeleted;
    }
}
