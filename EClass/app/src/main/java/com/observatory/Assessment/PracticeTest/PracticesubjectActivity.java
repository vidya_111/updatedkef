package com.observatory.Assessment.PracticeTest;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.observatory.Assessment.A_ChapterActivity;
import com.observatory.Assessment.ConstantManager;
import com.observatory.Assessment.DataBase.SQLiteDB;
import com.observatory.Assessment.Model.PDFModel;
import com.observatory.Assessment.Model.SubjectModel;
import com.observatory.database.Master;
import com.observatory.database.UserHistory;
import com.observatory.kefcorner.R;
import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.Constants;
import com.observatory.utils.EmailHandler;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;
import com.orm.query.Condition;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class PracticesubjectActivity extends BaseActivity {

    private static final String TAG = "Practice Subjects";
    private GridView grdSubjects;
    private RecyclerView rvSubjects;
    PracticesubjectActivity.SubjectAdapter subjectAdapter;
    private ArrayList<SubjectModel> subjectsArray;
    private SQLiteDB dbhelper;
    ArrayList<PDFModel> arr_pdfquestlist, arr_pdfsolutionlist, arr_listing;
    String MediumName = "", Path = "";
    int subjectid = 0;
    File Directory;
    boolean isquestion = false;
    boolean issolution = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practicesubject);
        actionBar.setTitle(SettingPreffrences.getSubjectTitle(getApplicationContext()));
        SettingPreffrences.setStandard(getApplicationContext(), SettingPreffrences.getSubjectTitle(getApplicationContext()));

        dbhelper = SQLiteDB.getInstance(this, ConstantManager.DB_PATH);
        subjectsArray = dbhelper.getAllSubject(false);
        subjectAdapter = new PracticesubjectActivity.SubjectAdapter(subjectsArray);
        rvSubjects.setLayoutManager(new GridLayoutManager(this, 2));
        rvSubjects.setAdapter(subjectAdapter);
        grdSubjects.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(PracticesubjectActivity.this, PracticepdfActivity.class);
                SettingPreffrences.setSubject(PracticesubjectActivity.this, subjectsArray.get(position).getSubjectDisplayName());
                i.putExtra(A_ChapterActivity.SUBJECT_NAME, subjectsArray.get(position).getSubjectName());
                App.getInstance().trackEvent("Chapter", "Screen", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " + "selectedStandard - " + SettingPreffrences.getStandard(PracticesubjectActivity.this) + "" + " ; " + "selectedSubject - " + SettingPreffrences.getSubject(PracticesubjectActivity
                        .this), "");
                startActivity(i);
            }
        });

        if(!SettingPreffrences.isUserSynced(this)) {
            updateLastSeen();
            autoSyncLoginHistory();
            SettingPreffrences.setUserSync(getApplicationContext(),true);
        }
    }

    private void autoSyncLoginHistory() {
        long lastSyncedId = SettingPreffrences.getLastSyncedLoginHistoryId(getApplicationContext());
        List<UserHistory> userHistoryList;

        if (lastSyncedId != -1) {
            userHistoryList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com.observatory.database.UserHistory.class).where("id > " + lastSyncedId).orderBy("id asc") /* ordering in ascending so that latest appear at the end
					. */.list();

        } else {
//			userHistoryList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com
//					.observatory.database.UserHistory.class)
//					.list();

            userHistoryList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com.observatory.database.UserHistory.class).orderBy("id asc") /* ordering in ascending so that latest appear at the end
					. */.list();
        }

        // there is data to sync
        if (userHistoryList.size() > 0) {
            String statsJson = formatLoginHistoryToJson(userHistoryList);
            callSyncLoginHistoryWebService(statsJson, userHistoryList);
        }
    }

    private void callSyncLoginHistoryWebService(String json, final List<com.observatory.database.UserHistory> userHistoryList) {

        if (CommonFunctions.isNetworkConnected(this)) {

            String userId = SettingPreffrences.getUserid(getApplicationContext()).isEmpty() ? "0" : SettingPreffrences.getUserid(getApplicationContext());
            String token = SettingPreffrences.getToken(getApplicationContext()).isEmpty() ? "0000" : SettingPreffrences.getToken(getApplicationContext());
            String deviceId = CommonFunctions.getUniqueDeviceId(getApplicationContext());

            Ion.with(this).load(NetworkUrl.syncLoginHistory)
                    .setBodyParameter("userId", userId)
                    .setBodyParameter("token", token)
                    .setBodyParameter("os", Build.VERSION.RELEASE)
                    .setBodyParameter("make",Build.MANUFACTURER)
                    .setBodyParameter("model",Build.MODEL)
                    .setBodyParameter("historyDetails", json)
                    .asString().setCallback(new FutureCallback<String>() {

                @Override
                public void onCompleted(Exception e, String result) {

                    if (e == null) {
                        // update last synced Id of UserHistory table
                        com.observatory.database.UserHistory lastSyncedUserHistory = userHistoryList.get(userHistoryList.size() - 1);
                        SettingPreffrences.setLastSyncedLoginHistoryId(getApplicationContext(), lastSyncedUserHistory.getId());
                    }else{
                        new EmailHandler().sendMail("phone@millicent.in","KEF Sync",e.getMessage());
                    }
                }
            });
        }

    }

    private void updateLastSeen() {

        if (SettingPreffrences.isAppLaunched(getApplicationContext())) {
            String medium = getMedium();
            String userid = SettingPreffrences.getUserid(this);
            String time = new SimpleDateFormat(Constants.UI_TIME_FORMAT).format(Calendar.getInstance().getTime());
            String date = new SimpleDateFormat(Constants.UI_DATE_FORMAT_ONLY_DATE).format(Calendar.getInstance().getTime());

            com.observatory.database.UserHistory userHistory = new com.observatory.database.UserHistory(date, time, userid.isEmpty() ? "0" : userid, medium);
            userHistory.save();

            Log.d(TAG, "updateLastSeen: " + userid);
        }

    }

    private ArrayList<String> getCourseList() {

        String userid = SettingPreffrences.getUserid(getApplicationContext());

        ArrayList<String> courseList = new ArrayList<>();
        ArrayList<com.observatory.database.UserHistory> userHistoryArrayList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com.observatory.database.UserHistory.class).groupBy("medium").orderBy("id asc").where(Condition.prop("userId").eq((userid.isEmpty() ? "0" : userid))).list();

        for (com.observatory.database.UserHistory userHistory : userHistoryArrayList) {
            courseList.add(userHistory.getMedium());
        }

        return courseList;
    }

    public String getMedium() {
        String medium;

        List<Master> masterList = Master.listAll(Master.class);
        if (masterList.size() > 0) {
            Master master = masterList.get(0);
            medium = master.getMedium();
        } else {
            medium = "Medium could not be found";
            Toast.makeText(getApplicationContext(), medium, Toast.LENGTH_LONG).show();
        }

        return medium;
    }

    private String formatLoginHistoryToJson(List<com.observatory.database.UserHistory> arrayUserHistory) {

        JSONArray mainJsonArr = new JSONArray();

        ArrayList<String> courseList = getCourseList();

        for (int i = 0; i < courseList.size(); i++) {

            JSONObject jsonObject = new JSONObject();

            try {

                jsonObject.put("medium", courseList.get(i));

                JSONArray dataJsonArr = new JSONArray();

                for (com.observatory.database.UserHistory userHistory : arrayUserHistory) {

                    if (userHistory.getMedium().equals(courseList.get(i))) {

                        try {

                            JSONObject dateTimeJson = new JSONObject();
                            dateTimeJson.put("date", userHistory.getDate());
                            dateTimeJson.put("time", userHistory.getTime());
                            dataJsonArr.put(dateTimeJson);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

                // send data only if current medium has login data
                if (dataJsonArr.length() > 0) {
                    jsonObject.put("data", dataJsonArr);
                    mainJsonArr.put(jsonObject);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Log.d("Tag", "formatLoginHistoryToJson: " + mainJsonArr.toString());

        return mainJsonArr.toString();
    }

    @Override
    protected void onLayout() {
        grdSubjects = (GridView) findViewById(R.id.grdSubjects);
        rvSubjects = (RecyclerView) findViewById(R.id.rv_test_subjects);
    }

    public class SubjectAdapter extends RecyclerView.Adapter<SubjectAdapter.MyViewHolder> {
        private ArrayList<SubjectModel> mSubjectData;

        private int selectedPos = RecyclerView.NO_POSITION;

        public SubjectAdapter(ArrayList<SubjectModel> data) {

            this.mSubjectData = data;

        }

        @Override
        public SubjectAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid, parent, false);
            return new SubjectAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(SubjectAdapter.MyViewHolder holder, int position) {
            holder.itemView.setSelected(selectedPos == position);
            holder.textView.setText(mSubjectData.get(position).getSubjectDisplayName());
            String SubjectIcon = mSubjectData.get(position).getSubjectIcon();
            //holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.marathi250));
            if (SubjectIcon == null || SubjectIcon.equals("")) {
                holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.no_item));
            } else {
                holder.imageView.setImageBitmap(getIconBitmap(mSubjectData.get(position).getSubjectIcon()));
            }
        }

        private Bitmap getIconBitmap(String subjectIcon) {
            try {
                String cleanImage = subjectIcon.replace("data:image/png;base64,", "");
                byte[] encodeByte = Base64.decode(cleanImage, Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                return bitmap;
            } catch (Exception e) {
                e.getMessage();
                return null;
            }
        }

        @Override
        public int getItemCount() {
            return mSubjectData.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView textView;
            ImageView imageView;

            public MyViewHolder(final View itemView) {
                super(itemView);
                //clearConstants();
                textView = itemView.findViewById(R.id.text);
                imageView = itemView.findViewById(R.id.img);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        subjectid=subjectsArray.get(getAdapterPosition()).getSubjectId();
                        arr_pdfquestlist = new ArrayList<PDFModel>();
                        arr_pdfsolutionlist = new ArrayList<PDFModel>();
                        arr_listing = new ArrayList<PDFModel>();
                        arr_listing = getDirectoryInfo();
                        if (arr_listing.size() > 0) {
                            notifyItemChanged(selectedPos);
                            selectedPos = getAdapterPosition();
                            ConstantManager.SUBJECT_ID = subjectsArray.get(getAdapterPosition()).getSubjectId();
                            notifyItemChanged(selectedPos);
                            Intent i = new Intent(PracticesubjectActivity.this, PracticepdfActivity.class);
                            SettingPreffrences.setSubject(PracticesubjectActivity.this, subjectsArray.get(getAdapterPosition()).getSubjectDisplayName());
                            i.putExtra(A_ChapterActivity.SUBJECT_NAME, subjectsArray.get(getAdapterPosition()).getSubjectName());
                            i.putExtra(A_ChapterActivity.SUBJECT_ID, subjectsArray.get(getAdapterPosition()).getSubjectId().toString());
                            App.getInstance().trackEvent("Chapter", "Screen", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " + "selectedStandard - " + SettingPreffrences.getStandard(PracticesubjectActivity.this) + "" + " ; " + "selectedSubject - " + SettingPreffrences.getSubject(PracticesubjectActivity
                                    .this), "");
                            startActivity(i);
                        } else {
                            showAlert(PracticesubjectActivity.this, "Info", "No Data Available!!");
                        }
                    }
                });
            }
        }
    }

    public void showAlert(Context context, String alert, String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(alert);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("Ok", null);
        AlertDialog dialog = alertDialog.create();
        dialog.show();
    }

    public ArrayList<PDFModel> getDirectoryInfo() {
        try {
            if (!App.BASE_PATH.contains(MediumName)) {
                if (!ConstantManager.ISSINGLESTANDARD) {
                    Path = App.BASE_PATH + MediumName + "/" + "Assess" + "/" + subjectid + "/" + ConstantManager.ASSESSMENT_TYPE + "/";
                } else {
                    Path = App.BASE_PATH + "Assess" + "/" + subjectid + "/" + ConstantManager.ASSESSMENT_TYPE + "/";
                }
            } else {
                Path = App.BASE_PATH + "Assess" + "/" + subjectid + "/" + ConstantManager.ASSESSMENT_TYPE + "/";
            }
            Directory = new File(Path + "Q" + "/");
            if (Directory.exists()) {
                File[] files = Directory.listFiles();
                if (files != null) {
                    for (int i = 0; i < files.length; i++) {
                        PDFModel pdfmodel = new PDFModel();
                        pdfmodel.setFileid(i + 1);
                        pdfmodel.setFilename(files[i].getName());
                        pdfmodel.setFiledisplayname(files[i].getName().substring(0, files[i].getName().lastIndexOf(".")));
                        pdfmodel.setFilepath(Path);
                        pdfmodel.setFileextension(files[i].getName().substring(files[i].getName().lastIndexOf(".")));
                        arr_pdfquestlist.add(pdfmodel);
                    }
                }
            } else {
                isquestion = true;
            }

            Directory = new File(Path + "S" + "/");
            if (Directory.exists()) {
                File[] files1 = Directory.listFiles();
                if (files1 != null) {
                    int size = arr_pdfquestlist.size();
                    if (size > 0)
                        for (int i = 0; i < files1.length; i++) {
                            boolean check = false;
                            String name = files1[i].getName();
                            for (int j = 0; j < size; j++) {
                                if (name.equals(arr_pdfquestlist.get(j).getFilename())) {
                                    check = true;
                                    break;

                                }
                            }
                            if (check == false) {
                                PDFModel pdfmodel = new PDFModel();
                                pdfmodel.setFileid(i + 1);
                                pdfmodel.setFilename(files1[i].getName());
                                pdfmodel.setFiledisplayname(files1[i].getName().substring(0, files1[i].getName().lastIndexOf(".")));
                                pdfmodel.setFilepath(Path);
                                pdfmodel.setFileextension(files1[i].getName().substring(files1[i].getName().lastIndexOf(".")));
                                arr_pdfquestlist.add(pdfmodel);
                            }
                        }
                }
            } else {
                issolution = true;
            }
        } catch (Exception ex) {
            new EmailHandler().sendMail("phone@millicent.in", "Eclass Error Logs", ex.getMessage());
        }
        return arr_pdfquestlist;
    }
}
