package com.observatory.Assessment.Model;

import java.util.ArrayList;
import java.util.List;

public class ChapterModel {

    public String ChapterId;
    public String ChapterName;
    public String ChapterDisplayName;
    public Integer ChapterSequenceNo;
    public String IsDeleted;
    public Integer SubjectId;
    public String isChapterChecked = "NO";
    public List<TopicModel> mTopicList;
    //private boolean updateTopicList;


    public String getChapterId() {
        return ChapterId;
    }

    public void setChapterId(String chapterId) {
        ChapterId = chapterId;
    }

    public String getChapterName() {
        return ChapterName;
    }

    public void setChapterName(String chapterName) {
        ChapterName = chapterName;
    }

    public String getChapterDisplayName() {
        return ChapterDisplayName;
    }

    public void setChapterDisplayName(String chapterDisplayName) {
        ChapterDisplayName = chapterDisplayName;
    }

    public Integer getChapterSequenceNo() {
        return ChapterSequenceNo;
    }

    public void setChapterSequenceNo(Integer chapterSequenceNo) {
        ChapterSequenceNo = chapterSequenceNo;
    }

    public String getIsDeleted() {
        return IsDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        IsDeleted = isDeleted;
    }

    public Integer getSubjectId() {
        return SubjectId;
    }

    public void setSubjectId(Integer subjectId) {
        SubjectId = subjectId;
    }

    public String getChapterChecked() {
        return isChapterChecked;
    }

    public void setChapterChecked(String chapterChecked) {
        isChapterChecked = chapterChecked;
    }

    public List<TopicModel> getmTopicList() {
        return mTopicList;
    }

    public void setmTopicList(List<TopicModel> mTopicList) {
        this.mTopicList = mTopicList;
    }

}
