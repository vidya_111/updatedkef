package com.observatory.Assessment.Model;

public class TopicModel {

    public Integer ChapterId;
    public Integer TopicId;
    public String TopicName;
    public String TopicDisplayName;
    public Integer TopicSequenceNo;
    public String IsDeleted;
    public String isTopicChecked;

    public Integer getChapterId() {
        return ChapterId;
    }

    public void setChapterId(Integer chapterId) {
        ChapterId = chapterId;
    }

    public Integer getTopicId() {
        return TopicId;
    }

    public void setTopicId(Integer topicId) {
        TopicId = topicId;
    }

    public String getTopicName() {
        return TopicName;
    }

    public void setTopicName(String topicName) {
        TopicName = topicName;
    }

    public String getTopicDisplayName() {
        return TopicDisplayName;
    }

    public void setTopicDisplayName(String topicDisplayName) {
        TopicDisplayName = topicDisplayName;
    }

    public Integer getTopicSequenceNo() {
        return TopicSequenceNo;
    }

    public void setTopicSequenceNo(Integer topicSequenceNo) {
        TopicSequenceNo = topicSequenceNo;
    }

    public String getIsDeleted() {
        return IsDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        IsDeleted = isDeleted;
    }

    public String getTopicChecked() {
        return isTopicChecked;
    }

    public void setTopicChecked(String topicChecked) {
        isTopicChecked = topicChecked;
    }

}
