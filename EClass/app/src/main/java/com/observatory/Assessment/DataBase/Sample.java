package com.observatory.Assessment.DataBase;


import android.content.Context;

import com.observatory.Assessment.ConstantManager;
import com.observatory.Assessment.Model.QuestionModel;

public class Sample {

    private static final String UTF_8_ENCODING_TYPE = "UTF-8";
    //    private String strFontPath = getClass().getResource("/Fonts/").toExternalForm();
    private String strFontPath = "file:///android_assets/fonts/";
    private String strCssPath ="" ,strJsPath = "";;


    public StringBuilder LoadData(Context context, QuestionModel drQuestion, int i, int ttlQuestion)
    {
        StringBuilder str = new StringBuilder();
        //String Cssstring=getCssAsString(context);
        try {
            String btnNextBgColor = "rgb(200, 200, 200)";
            String btnPrevBgColor = "rgb(200, 200, 200)";
            //bool btnNextdisabled = true;
            //bool btnPrevdisabled = true;
            String strNextdisabled = "disabled";
            String strPrevdisabled = "disabled";
            if (!IsNullOrEmpty(ConstantManager.questionmodel.get(i).getYourAns()))
                {
                    if ((ttlQuestion - 1) == 0) {
                        btnPrevBgColor = "rgb(200, 200, 200)";
                        btnNextBgColor = "rgb(200, 200, 200)";
                        //btnNextdisabled = true;
                        //btnPrevdisabled = true;
                    } else if (i == 0) {
                        btnPrevBgColor = "rgb(200, 200, 200)";
                        //btnPrevdisabled = true;
                        btnNextBgColor = "rgb(63, 71, 204)";
                        strNextdisabled = "";
                    } else if (i == (ttlQuestion - 1)) {
                        btnNextBgColor = "rgb(200, 200, 200)";
                        //btnNextdisabled = true;
                        btnPrevBgColor = "rgb(63, 71, 204)";
                        strPrevdisabled = "";
                    } else {
                        btnNextBgColor = "rgb(63, 71, 204)";
                        strNextdisabled = "";
                        btnPrevBgColor = "rgb(63, 71, 204)";
                        strPrevdisabled = "";
                    }
                }


                str.append("<!DOCTYPE html>\n");
                str.append("<html lang =\"en -US\">\n");
                str.append("<head>");
                str.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">\n");
                str.append("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=9; IE=8; IE=7; IE=EDGE\" />\n");
                str.append("<meta name = \"viewport\" content = \"width=device-width, initial-scale=1\" >\n");
                str.append("<link rel=\"stylesheet\" href=\"" + strCssPath + "ecl_template.css" + "\" type=\"text/css\" />");
                str.append("<style type=\"text/css\">\n" +
                        "@font-face {font-family:\"SUBAK-1\";" +
                        "src:url(\"" + strFontPath + "SUBAK0.ttf" + "\") format(\"truetype\");}" +
                        "\n" +
                        "@font-face {font-family:\"SUBAK-1\";" +
                        "src:url(\"" + strFontPath + "SUBAK1.ttf" + "\") format(\"truetype\");}" +
                        "\n" +
                        "@font-face {font-family: 'krutidev';" +
                        "src: url('" + strFontPath + "/k010.eot');" +
                        "src: local('k010'), url('" + strFontPath + "/k010.woff') format('woff'), url('" + strFontPath + "/k010.ttf') format('truetype');}" +
                        "\n" +
                        "@font-face {font-family:\"urdu\";" +
                        "src: url('" + strFontPath + "/JameelNooriNastaleeq.eot');" +
                        "src: local('JameelNooriNastaleeq'), url('" + strFontPath + "/JameelNooriNastaleeq.woff') format('woff'), url('" + strFontPath + "/JameelNooriNastaleeq.ttf') format('truetype');}" +
                        "\n" +
                        "@font-face {font-family:\"urdu\";" +
                        "src:url(\"" + strFontPath + "Jameel_Noori_Nastaleeq.ttf" + "\") format(\"truetype\");}" +
                        "\n");
                str.append("</style>");
                //str.append("<script src=\"" + strJsPath + "mathmlsupport_min.js" + "\"></script>");
                str.append("<script type=\"text/javascript\" src=\"file:///android_asset/MathJax/MathJax.js?config=MML_HTMLorMML-full" + "\" ></script>");
                str.append("</head>\n");
                str.append("<body>\n");
                str.append("<div class=\"article\">\n");
                str.append("<h3 class=\"qstHeaderText\"> Question " + (i + 1) + " of " + ttlQuestion + "</h3>\n");
                str.append("<div class=\"dvQstText\">\n <div class=\"dvQstNo\"> " + (i + 1) + ".</div> <div class=\"qstText\">" + drQuestion.getQuestionText().replace("ایکل-", "").replace("mathsize=\"12px\"", "mathsize=\"20px\"")+ "</div></div>\n");
                String questionList = "";
                questionList = getSingleChoiceQuestionList(drQuestion, i, ttlQuestion);
                if (!IsNullOrEmpty(questionList)) {
                    str.append(questionList.toString());
                }
                str.append("<div class=\"dvFooter\">\n");
                str.append("<div onclick=\"java.SubmitTestCommandExecuted()\" class=\"btnSubmit\"><b>Submit</b></div>\n");
                str.append("<span id=\"btnNext\" onclick=\"java.NextQuestion()\" class=\"btnNext\" style=\"background-color:" + btnNextBgColor + ";\" " + strNextdisabled + ">Next</span>\n");
                str.append("<span id=\"btnPrev\" onclick=\"java.PrevQuestion()\" class=\"btnPrevious\" style=\"background-color:" + btnPrevBgColor + "; \" " + strPrevdisabled + ">Prev</span>\n");
                str.append("</div>\n");
                str.append("</div>\n");
                str.append("<script type=\"text/javascript\">\n");
                str.append("function getUserOption(selectedValue, currQue, ttlQue) {\n");
                str.append("\n");
                str.append("if ((ttlQue - 1) == 0)\n");
                str.append("{\n");
                str.append("    document.getElementById(\"btnNext\").disabled = true;\n");
                str.append("    document.getElementById(\"btnNext\").style.backgroundColor = \"#c8c8c8\";\n");
                str.append("    document.getElementById(\"btnPrev\").disabled = true;\n");
                str.append("    document.getElementById(\"btnPrev\").style.backgroundColor = \"#c8c8c8\";\n");
                str.append("}\n");
                str.append("else if (currQue == 0)\n");
                str.append("{\n");
                str.append("    document.getElementById(\"btnNext\").disabled = false;\n");
                str.append("    document.getElementById(\"btnNext\").style.backgroundColor = \"#3f47cc\";\n");
                str.append("    document.getElementById(\"btnPrev\").disabled = true;\n");
                str.append("    document.getElementById(\"btnPrev\").style.backgroundColor = \"#c8c8c8\";\n");
                str.append("}\n");
                str.append("else if (currQue == (ttlQue - 1))\n");
                str.append("{\n");
                str.append("    document.getElementById(\"btnNext\").disabled = true;\n");
                str.append("    document.getElementById(\"btnNext\").style.backgroundColor = \"#c8c8c8\";\n");
                str.append("    document.getElementById(\"btnPrev\").disabled = false;\n");
                str.append("    document.getElementById(\"btnPrev\").style.backgroundColor = \"#3f47cc\";\n");
                str.append("}\n");
                str.append("else\n");
                str.append("{\n");
                str.append("    document.getElementById(\"btnNext\").disabled = false;\n");
                str.append("    document.getElementById(\"btnNext\").style.backgroundColor = \"#3f47cc\";\n");
                str.append("    document.getElementById(\"btnPrev\").disabled = false;\n");
                str.append("    document.getElementById(\"btnPrev\").style.backgroundColor = \"#3f47cc\";\n");
                str.append("}\n");
                str.append("java.getUserOption(selectedValue);\n");
                str.append("\n");
                str.append("}\n");
                str.append("var imgs = document.getElementsByTagName(\"img\");\n");
                str.append("for (var i = 0; i < imgs.length; i++)\n");
                str.append("{\n");
                str.append("    if ((imgs[i].naturalWidth+62) >= window.outerWidth)\n");
                str.append("    {\n");
                str.append("        imgs[i].style.width = \"100%\";\n");
                str.append("        imgs[i].style.height = \"auto\";\n");
                str.append("    }\n");
                str.append("}\n");
                str.append("</script>\n");
                str.append("</body>\n</html>");
        }
        catch (Exception ex)
        {
        }
        return str;
    }



    public String getSingleChoiceQuestionList(QuestionModel dr, int i, int ttlQuestion)
    {
        StringBuilder str = new StringBuilder();
        try {
            String strLabelTransparent = "<label class=\"strLabelTransparent\">{0}</label>\n";

            if (!IsNullOrEmpty(ConstantManager.questionmodel.get(i).getOption1().toString())) {
                    String strOption1 = "<div class=\"dvLabelTransparent\">\n" + strLabelTransparent.replace("{0}","A.") +
                            "\n<input type=\"radio\" id=\"r1\" class=\"css-checkbox rb\" onchange=\"getUserOption(this.value, " + i + ", " + ttlQuestion + ");\" name=\"radioName\" value=\"1\">\n";
                if (ConstantManager.questionmodel.size() > i && !IsNullOrEmpty(ConstantManager.questionmodel.get(i).getYourAns()) && ConstantManager.questionmodel.get(i).getYourAns().equals("1"))  {
                        strOption1 = "<div class=\"dvLabelTransparent\"\n>" + strLabelTransparent.replace("{0}","A.") +
                                "\n<input type=\"radio\" id=\"r1\" class=\"css-checkbox rb\" onchange=\"getUserOption(this.value, " + i + ", " + ttlQuestion + ");\" name=\"radioName\" value=\"1\" checked=\"checked\">\n";
                    }
                    str.append(strOption1);
                    str.append("<label for=\"r1\" class=\"css-label cb0 optionText\">\n" + "  " + ConstantManager.questionmodel.get(i).getOption1().toString().replace("&nbsp;", " ").replace("ایکل-", "<span style=\"color:#fff;\">ایکل-</span>").replace("mathsize=\"12px\"", "mathsize=\"25px\"") + "\n</label>\n</div>");


               // str.append("<label for=\"r1\" class=\"css-label cb0 optionText\">\n" + "  " +  ConstantManager.questionmodel.get(i).getOption1().replace("ایکل-", "").replace("mathsize=\"12px\"", "mathsize=\"20px\"") + "\n</label>\n</div>");

                str.append("<br><br>");
                }
            if (!IsNullOrEmpty(ConstantManager.questionmodel.get(i).getOption2().toString())){
                    String strOption2 = "<div class=\"dvLabelTransparent\">\n" + strLabelTransparent.replace("{0}","B.") +
                            "\n<input type=\"radio\" id=\"r2\" class=\"css-checkbox rb\" onchange=\"getUserOption(this.value, " + i + ", " + ttlQuestion + ");\" name=\"radioName\" value=\"2\">";
                if (ConstantManager.questionmodel.size() > i && !IsNullOrEmpty(ConstantManager.questionmodel.get(i).getYourAns()) && ConstantManager.questionmodel.get(i).getYourAns().equals("2")) {
                        strOption2 = "\n<div class=\"dvLabelTransparent\">\n" + strLabelTransparent.replace("{0}","B.") +
                                "\n<input type=\"radio\" id=\"r2\" class=\"css-checkbox rb\" onchange=\"getUserOption(this.value, " + i + ", " + ttlQuestion + ");\" name=\"radioName\" value=\"2\" checked=\"checked\">\n";
                    }
                    str.append(strOption2);
                    str.append("\n<label for=\"r2\" class=\"css-label cb0 optionText\">\n" + "  " + ConstantManager.questionmodel.get(i).getOption2().toString().replace("&nbsp;", " ").replace("ایکل-", "<span style=\"color:#fff;\">ایکل-</span>").replace("mathsize=\"12px\"", "mathsize=\"25px\"") + "\n</label>\n</div>");
              //  str.append("<label for=\"r1\" class=\"css-label cb0 optionText\">\n" + "  " +  ConstantManager.questionmodel.get(i).getOption2().replace("ایکل-", "").replace("mathsize=\"12px\"", "mathsize=\"20px\"") + "\n</label>\n</div>");

                str.append("<br><br>");
                }

            if (!IsNullOrEmpty(ConstantManager.questionmodel.get(i).getOption3().toString())) {
                    String strOption3 = "<div class=\"dvLabelTransparent\">\n" + strLabelTransparent.replace("{0}","C.") +
                            "\n<input type=\"radio\" id=\"r3\" class=\"css-checkbox rb\" onchange=\"getUserOption(this.value, " + i + ", " + ttlQuestion + ");\" name=\"radioName\" value=\"3\">\n";
                if (ConstantManager.questionmodel.size() > i && !IsNullOrEmpty(ConstantManager.questionmodel.get(i).getYourAns()) && ConstantManager.questionmodel.get(i).getYourAns().equals("3")){
                        strOption3 = "<div class=\"dvLabelTransparent\">\n" + strLabelTransparent.replace("{0}","C.") +
                                "\n<input type=\"radio\" id=\"r3\" class=\"css-checkbox rb\" onchange=\"getUserOption(this.value, " + i + ", " + ttlQuestion + ");\" name=\"radioName\" value=\"3\" checked=\"checked\">\n";
                    }
                    str.append(strOption3);
                    str.append("\n<label for=\"r3\" class=\"css-label cb0 optionText\">\n" + "  " + ConstantManager.questionmodel.get(i).getOption3().toString().replace("&nbsp;", " ").replace("ایکل-", "<span style=\"color:#fff;\">ایکل-</span>").replace("mathsize=\"12px\"", "mathsize=\"25px\"") + "\n</label>\n</div>");
              //  str.append("<label for=\"r1\" class=\"css-label cb0 optionText\">\n" + "  " +  ConstantManager.questionmodel.get(i).getOption3().replace("ایکل-", "").replace("mathsize=\"12px\"", "mathsize=\"20px\"") + "\n</label>\n</div>");

                str.append("<br><br>");
                }
            if (!IsNullOrEmpty(ConstantManager.questionmodel.get(i).getOption4().toString())) {
                    String strOption4 = "\n<div class=\"dvLabelTransparent\">\n" + strLabelTransparent.replace("{0}","D.") +
                            "\n<input type=\"radio\" id=\"r4\" class=\"css-checkbox rb\" onchange=\"getUserOption(this.value, " + i + ", " + ttlQuestion + ");\" name=\"radioName\" value=\"4\">\n";
                if (ConstantManager.questionmodel.size() > i && !IsNullOrEmpty(ConstantManager.questionmodel.get(i).getYourAns()) && ConstantManager.questionmodel.get(i).getYourAns().equals("4")) {
                        strOption4 = "\n<div class=\"dvLabelTransparent\">\n" + strLabelTransparent.replace("{0}","D.")+
                                "\n<input type=\"radio\" id=\"r4\" class=\"css-checkbox rb\" onchange=\"getUserOption(this.value, " + i + ", " + ttlQuestion + ");\" name=\"radioName\" value=\"4\" checked=\"checked\">\n";
                    }
                    str.append(strOption4);
                    str.append("\n<label for=\"r4\" class=\"css-label cb0 optionText\">\n" + "  " + ConstantManager.questionmodel.get(i).getOption4().toString().replace("&nbsp;", " ").replace("ایکل-", "<span style=\"color:#fff;\">ایکل-</span>").replace("mathsize=\"12px\"", "mathsize=\"25px\"") + "\n</label>\n</div>");
               // str.append("<label for=\"r1\" class=\"css-label cb0 optionText\">\n" + "  " +  ConstantManager.questionmodel.get(i).getOption4().replace("ایکل-", "").replace("mathsize=\"12px\"", "mathsize=\"20px\"") + "\n</label>\n</div>");

                str.append("<br><br>");
                }
            if (!IsNullOrEmpty(ConstantManager.questionmodel.get(i).getOption5().toString())) {
                    String strOption5 = "\n<div class=\"dvLabelTransparent\">\n" + strLabelTransparent.replace("{0}","E.") +
                            "\n<input type=\"radio\" id=\"r5\" class=\"css-checkbox rb\" onchange=\"getUserOption(this.value, " + i + ", " + ttlQuestion + ");\" name=\"radioName\" value=\"5\">\n";
                    if (ConstantManager.questionmodel.size() > i && !IsNullOrEmpty(ConstantManager.questionmodel.get(i).getYourAns()) && ConstantManager.questionmodel.get(i).getYourAns().equals("5")) {
                        strOption5 = "\n<div class=\"dvLabelTransparent\">\n" + strLabelTransparent.replace("{0}","E.") +
                                "\n<input type=\"radio\" id=\"r5\" class=\"css-checkbox rb\" onchange=\"getUserOption(this.value, " + i + ", " + ttlQuestion + ");\" name=\"radioName\" value=\"5\" checked=\"checked\">\n";
                    }
                    str.append(strOption5);
                //str.append("<label for=\"r1\" class=\"css-label cb0 optionText\">\n" + "  " +  ConstantManager.questionmodel.get(i).getOption5().replace("ایکل-", "").replace("mathsize=\"12px\"", "mathsize=\"20px\"") + "\n</label>\n</div>");

                str.append("\n<label for=\"r5\" class=\"css-label cb0 optionText\">\n" + "  " + ConstantManager.questionmodel.get(i).getOption6().toString().replace("&nbsp;", " ").replace("ایکل-", "<span style=\"color:#fff;\">ایکل-</span>").replace("mathsize=\"12px\"", "mathsize=\"25px\"")+ "\n</label>\n</div>");
                    str.append("<br><br>");
                }
            if (!IsNullOrEmpty(ConstantManager.questionmodel.get(i).getOption6().toString())) {
                    String strOption6 = "\n<div class=\"dvLabelTransparent\">\n" + strLabelTransparent.replace("{0}","F.") +
                            "\n<input type=\"radio\" id=\"r6\" class=\"css-checkbox rb\" onchange=\"getUserOption(this.value, " + i + ", " + ttlQuestion + ");\" name=\"radioName\" value=\"6\">\n";
                if (ConstantManager.questionmodel.size() > i && !IsNullOrEmpty(ConstantManager.questionmodel.get(i).getYourAns()) && ConstantManager.questionmodel.get(i).getYourAns().equals("6")) {
                        strOption6 = "\n<div class=\"dvLabelTransparent\">\n" + strLabelTransparent.replace("{0}","F.") +
                                "\n<input type=\"radio\" id=\"r6\" class=\"css-checkbox rb\" onchange=\"getUserOption(this.value, " + i + ", " + ttlQuestion + ");\" name=\"radioName\" value=\"6\" checked=\"checked\">\n";
                    }
                    str.append(strOption6);
                    str.append("\n<label for=\"r6\" class=\"css-label cb0 optionText\">\n" + "  " + ConstantManager.questionmodel.get(i).getOption6().toString().replace("&nbsp;", " ").replace("ایکل-", "<span style=\"color:#fff;\">ایکل-</span>").replace("mathsize=\"12px\"", "mathsize=\"25px\"") + "\n</label>\n</div>");
                    str.append("<br><br>");
                }
        }
        catch (Exception ex)
        {
        }

        return str.toString();
    }

    private boolean IsNullOrEmpty(String s) {

        if (s == null || s.isEmpty()) {
            return true;
        }
        return false;
    }


}
