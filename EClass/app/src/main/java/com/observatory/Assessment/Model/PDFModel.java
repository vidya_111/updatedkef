package com.observatory.Assessment.Model;

public class PDFModel {
    public int fileid;
    public String filename;
    public String filedisplayname;
    public String filepath;
    public String fileextension;
    public String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getFileid() {
        return fileid;
    }

    public void setFileid(int fileid) {
        this.fileid = fileid;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFiledisplayname() {
        return filedisplayname;
    }

    public void setFiledisplayname(String filedisplayname) {
        this.filedisplayname = filedisplayname;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public String getFileextension() {
        return fileextension;
    }

    public void setFileextension(String fileextension) {
        this.fileextension = fileextension;
    }
}
