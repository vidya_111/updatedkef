package com.observatory.Assessment.Reports;

import android.app.Activity;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.observatory.Assessment.Model.SubjectModel;
import com.observatory.kefcorner.R;

import java.util.ArrayList;

public class ReportSubjectAdapter extends RecyclerView.Adapter<ReportSubjectAdapter.ReportSubjectViewHolder> {

    Activity context;
    ArrayList<SubjectModel> msubjectlist;
    int row_index=-1;
    public ReportSubjectAdapter(ReportsActivity reportsActivity, ArrayList<SubjectModel> arr_subjects) {
        this.context=reportsActivity;
        this.msubjectlist=arr_subjects;
    }

    @Override
    public ReportSubjectViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_assess_reports, parent, false);
        ReportSubjectViewHolder myViewHolder = new ReportSubjectViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(ReportSubjectViewHolder holder, final int i) {
        holder.tv_subjectname.setText(msubjectlist.get(i).getSubjectName());
        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int subj_Id=msubjectlist.get(i).getSubjectId();
                row_index=i;
                notifyDataSetChanged();
                if(context!=null)
                {
                    ((ReportsActivity)context).Resultbind(subj_Id);
                }
            }
        });
        if(row_index==i){
            holder.cardview.setBackgroundResource(R.drawable.fillcolor);
            holder.tv_subjectname.setTextColor(Color.WHITE);
        }
        else
        {
            holder.cardview.setBackgroundResource(R.drawable.border);
            holder.tv_subjectname.setTextColor(Color.BLACK);
        }
    }

    @Override
    public int getItemCount() {
        return msubjectlist.size();
    }

    public class ReportSubjectViewHolder extends RecyclerView.ViewHolder {
        TextView tv_subjectname;
        LinearLayout cardview;
        public ReportSubjectViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_subjectname=(TextView) itemView.findViewById(R.id.tv_subjectname);
            cardview=(LinearLayout) itemView.findViewById(R.id.cardview);
        }


    }
}
