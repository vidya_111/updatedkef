package com.observatory.Assessment.Model;

public class AssessmentModel {

    private String CourseContentCode;
    private String CourseContentName;
    private String CourseContentDisplayName;
    private String TestCode;
    private String TestDisplayName;
    private String TestQuestionCode;
    private String TestAttemptedCount;
    private String TestTotalMarks;
    private String TestTotalQuestion;

    //user defined fields
    private int totalWrongmarksObtained;
    private int totalRightmarksObtained;
    private int totalSkipmarksObtained;


    private int counterRight;
    private int counterWrong;
    private int CounterSkip;

    private String individualID;
    private String TestScoreSummaryDetailsId;
    private String StudentUserCode;
    private String LastAccessTime;
    private String TotalMarksObtain;

    public String getTotalMarksObtain() {
        return TotalMarksObtain;
    }

    public void setTotalMarksObtain(String totalMarksObtain) {
        TotalMarksObtain = totalMarksObtain;
    }



    public String getTestScoreSummaryDetailsId() {
        return TestScoreSummaryDetailsId;
    }

    public void setTestScoreSummaryDetailsId(String testScoreSummaryDetailsId) {
        TestScoreSummaryDetailsId = testScoreSummaryDetailsId;
    }

    public String getStudentUserCode() {
        return StudentUserCode;
    }

    public void setStudentUserCode(String studentUserCode) {
        StudentUserCode = studentUserCode;
    }

    public String getLastAccessTime() {
        return LastAccessTime;
    }

    public void setLastAccessTime(String lastAccessTime) {
        LastAccessTime = lastAccessTime;
    }



    public String getIndividualID() {
        return individualID;
    }

    public void setIndividualID(String individualID) {
        this.individualID = individualID;
    }



    public String getCourseContentCode() {
        return CourseContentCode;
    }

    public void setCourseContentCode(String courseContentCode) {
        CourseContentCode = courseContentCode;
    }

    public String getCourseContentName() {
        return CourseContentName;
    }

    public void setCourseContentName(String courseContentName) {
        CourseContentName = courseContentName;
    }

    public String getCourseContentDisplayName() {
        return CourseContentDisplayName;
    }

    public void setCourseContentDisplayName(String courseContentDisplayName) {
        CourseContentDisplayName = courseContentDisplayName;
    }

    public String getTestCode() {
        return TestCode;
    }

    public void setTestCode(String testCode) {
        TestCode = testCode;
    }

    public String getTestDisplayName() {
        return TestDisplayName;
    }

    public void setTestDisplayName(String testDisplayName) {
        TestDisplayName = testDisplayName;
    }

    public String getTestQuestionCode() {
        return TestQuestionCode;
    }

    public void setTestQuestionCode(String testQuestionCode) {
        TestQuestionCode = testQuestionCode;
    }

    public String getTestAttemptedCount() {
        return TestAttemptedCount;
    }

    public void setTestAttemptedCount(String testAttemptedCount) {
        TestAttemptedCount = testAttemptedCount;
    }

    public String getTestTotalMarks() {
        return TestTotalMarks;
    }

    public void setTestTotalMarks(String testTotalMarks) {
        TestTotalMarks = testTotalMarks;
    }

    public String getTestTotalQuestion() {
        return TestTotalQuestion;
    }

    public void setTestTotalQuestion(String testTotalQuestion) {
        TestTotalQuestion = testTotalQuestion;
    }

    public int getTotalWrongmarksObtained() {
        return totalWrongmarksObtained;
    }

    public void setTotalWrongmarksObtained(int totalWrongmarksObtained) {
        this.totalWrongmarksObtained = totalWrongmarksObtained;
    }

    public int getTotalRightmarksObtained() {
        return totalRightmarksObtained;
    }

    public void setTotalRightmarksObtained(int totalRightmarksObtained) {
        this.totalRightmarksObtained = totalRightmarksObtained;
    }

    public int getTotalSkipmarksObtained() {
        return totalSkipmarksObtained;
    }

    public void setTotalSkipmarksObtained(int totalSkipmarksObtained) {
        this.totalSkipmarksObtained = totalSkipmarksObtained;
    }

    public int getCounterRight() {
        return counterRight;
    }

    public void setCounterRight(int counterRight) {
        this.counterRight = counterRight;
    }

    public int getCounterWrong() {
        return counterWrong;
    }

    public void setCounterWrong(int counterWrong) {
        this.counterWrong = counterWrong;
    }

    public int getCounterSkip() {
        return CounterSkip;
    }

    public void setCounterSkip(int counterSkip) {
        CounterSkip = counterSkip;
    }
}
