package com.observatory.Assessment;

import android.app.Activity;

import com.observatory.Assessment.Model.AssessmentModel;
import com.observatory.Assessment.Model.QuestionModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ConstantManager {

    public static String ASSESSMENT_TYPE="type";
    public static boolean ISSINGLESTANDARD=false;
    //MCQ Assessment Constants
    public static final String CHECK_BOX_CHECKED_TRUE = "YES";
    public static final String CHECK_BOX_CHECKED_FALSE = "NO";
    public static String IS_ALL_CHECKED = "NO";
    public static ArrayList<Activity> arrPreviousActivity=new ArrayList<Activity>();
    public static ArrayList<String> Selected_Chapters_Id =new ArrayList<>();
    public static ArrayList<String> Selected_Topic_Id = new ArrayList<>();
    public static ArrayList<String> Selected_level=new ArrayList<>() ;
    public static HashMap<Integer, QuestionModel> questionmodel = null;
    //public static ArrayList<QuestionModel> arr_finalQuestionList=new ArrayList<QuestionModel>();
    public static ArrayList<ArrayList<HashMap<String, String>>> childItems = new ArrayList<>();
    public static ArrayList<HashMap<String, String>> parentItems = new ArrayList<>();
    public static AssessmentModel selectedAssessmentModel;
    public static String DB_PATH;
    public static String Directory_PATH;
    public static String Directory_FolderName;
    public static Integer SUBJECT_ID;
    public static double SCORE;
    public static boolean isAssessment=false;
    public class Parameter {
        public static final String IS_CHECKED = "is_checked";
        public static final String TOPIC_NAME = "topic_name";
        public static final String CHAPTER_NAME = "chapter_name";
        public static final String CHAPTER_ID = "chapter_id";
        public static final String TOPIC_ID = "topic_id";
    }

    public static boolean IsNullOrEmpty(String s) {

        if (s == null || s.isEmpty() || s.equals("null")) {
            return true;
        }
        return false;
    }
    //MCQ Assessment Constants



    //Practice Test Assessment Constants
    public static String STANDARD_FOLDERNAME="stdname";
    //Practice Test Assessment Constants
    public static int Selected_Standard_Position = -1;
    public static String Standard_Name = "";
    public static String School_Id = "";
}
