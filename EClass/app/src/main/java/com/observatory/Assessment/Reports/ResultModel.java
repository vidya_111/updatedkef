package com.observatory.Assessment.Reports;

public class ResultModel {
    public String TestScoreSummaryDetailsId;
    public String ProductCode;
    public String CourseContentCode;
    public String TestTotalQuestion;
    public String TotalRightAnswerCount;
    public String TotalWrongAnswerCount;
    public String LastAccessTime;
    public String StudentUserCode;


    public String getTestScoreSummaryDetailsId() {
        return TestScoreSummaryDetailsId;
    }

    public void setTestScoreSummaryDetailsId(String testScoreSummaryDetailsId) {
        TestScoreSummaryDetailsId = testScoreSummaryDetailsId;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getCourseContentCode() {
        return CourseContentCode;
    }

    public void setCourseContentCode(String courseContentCode) {
        CourseContentCode = courseContentCode;
    }

    public String getTestTotalQuestion() {
        return TestTotalQuestion;
    }

    public void setTestTotalQuestion(String testTotalQuestion) {
        TestTotalQuestion = testTotalQuestion;
    }

    public String getTotalRightAnswerCount() {
        return TotalRightAnswerCount;
    }

    public void setTotalRightAnswerCount(String totalRightAnswerCount) {
        TotalRightAnswerCount = totalRightAnswerCount;
    }

    public String getTotalWrongAnswerCount() {
        return TotalWrongAnswerCount;
    }

    public void setTotalWrongAnswerCount(String totalWrongAnswerCount) {
        TotalWrongAnswerCount = totalWrongAnswerCount;
    }

    public String getLastAccessTime() {
        return LastAccessTime;
    }

    public void setLastAccessTime(String lastAccessTime) {
        LastAccessTime = lastAccessTime;
    }

    public String getStudentUserCode() {
        return StudentUserCode;
    }

    public void setStudentUserCode(String studentUserCode) {
        StudentUserCode = studentUserCode;
    }
}
