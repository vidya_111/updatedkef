package com.observatory.Assessment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Environment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.observatory.Assessment.DataBase.SQLiteDB;
import com.observatory.Assessment.DataBase.Sample;
import com.observatory.Assessment.Model.AssessmentModel;
import com.observatory.Assessment.Model.QuestionImageModel;
import com.observatory.AsyncTaskMain.AsyncTaskHelper;
import com.observatory.AsyncTaskMain.OnTaskComplete;
import com.observatory.database.userStatistics;
import com.observatory.kefcorner.R;
import com.observatory.utils.App;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.Constants;
import com.observatory.utils.DateUtil;
import com.observatory.utils.Network;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

import static com.observatory.Assessment.ConstantManager.IsNullOrEmpty;
import static com.observatory.utils.Constants.KEF_ContentTypeId_MCQ;
import static com.observatory.utils.Constants.KEF_ContentTypeName_MCQ;

public class AMCQActivity extends AppCompatActivity {

    int count = 0;
    double rAns = 0;
    double wAns = 0;
    WebView htmlWebView;
    SQLiteDB dbhelper;
    String s;
    private Dialog dialog;
    int counterRight = 0;
    int counterWrong = 0;
    int CounterSkip = 0;
    double totalTestMarks = 0;
    boolean isCheckedAns = true;
    String separator, superDataDirPath, superDBDirPath, fileName, CssFileString;
    ArrayList<QuestionImageModel> arrQuest_img;
    private static final String UTF_8_ENCODING_TYPE = "UTF-8";

    /*Webservice*/
    private AsyncTaskHelper asyncTaskHelper;
    private String params;

    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amcq);
        ((AppCompatActivity) this).getSupportActionBar().setTitle("Concept Clarity Test");
        htmlWebView = (WebView) findViewById(R.id.webView);
        WebSettings webSetting = htmlWebView.getSettings();
        webSetting.setJavaScriptEnabled(true);
        webSetting.setDisplayZoomControls(true);
        webSetting.setJavaScriptEnabled(true);
        webSetting.setAllowFileAccessFromFileURLs(true);
        webSetting.setAllowFileAccess(true);
        webSetting.setJavaScriptCanOpenWindowsAutomatically(true);
        webSetting.setDomStorageEnabled(true);
        separator = "/";
        superDataDirPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        superDataDirPath = "/data/data/" + getPackageName();
        superDBDirPath = superDataDirPath + separator + "assessment";
        fileName = "mcq.html";

        String questionText = ConstantManager.questionmodel.get(count).getQuestionText();
       // if (questionText.contains("<img")) {
            String QuestionCode = ConstantManager.questionmodel.get(count).getQuestionCode();
            dbhelper = SQLiteDB.getInstance(AMCQActivity.this, ConstantManager.DB_PATH);
            arrQuest_img = new ArrayList<QuestionImageModel>();
            arrQuest_img = dbhelper.getQuestionImage(QuestionCode);
            Log.d("McqImage","11: "+arrQuest_img.size());
            if (arrQuest_img.size() > 0) {
                String ImageName = arrQuest_img.get(0).getImageName();
                String Imageuri = arrQuest_img.get(0).getDataUri();
                String QuestionText = ConstantManager.questionmodel.get(count).getQuestionText();
                String UpdatedQuestion = replaceImage(QuestionText,arrQuest_img);
                ConstantManager.questionmodel.get(count).setQuestionText(UpdatedQuestion);
                ConstantManager.questionmodel.get(count).Option1 = replaceImage(ConstantManager.questionmodel.get(count).Option1, arrQuest_img);
                ConstantManager.questionmodel.get(count).Option2 = replaceImage(ConstantManager.questionmodel.get(count).Option2, arrQuest_img);
                ConstantManager.questionmodel.get(count).Option3 = replaceImage(ConstantManager.questionmodel.get(count).Option3, arrQuest_img);
                ConstantManager.questionmodel.get(count).Option4 = replaceImage(ConstantManager.questionmodel.get(count).Option4, arrQuest_img);
                ConstantManager.questionmodel.get(count).Option5 = replaceImage(ConstantManager.questionmodel.get(count).Option5, arrQuest_img);
                ConstantManager.questionmodel.get(count).Option6 = replaceImage(ConstantManager.questionmodel.get(count).Option6, arrQuest_img);
            }
       // }
        final String s = new Sample().LoadData(AMCQActivity.this, ConstantManager.questionmodel.get(0), 0, ConstantManager.questionmodel.size()).toString();
        CssFileString = getCssAsString();
        File dir = new File(superDBDirPath);
        if (!dir.exists()) {
            dir.mkdir();
        }
        final File file = new File(superDBDirPath, fileName);
        File cssfiles = new File(superDBDirPath, "ecl_template.css");
        filecopy(file, s);
        if (!cssfiles.exists()) {
            filecopy(cssfiles, CssFileString);
        }
        htmlWebView.loadUrl("file://" + file.getAbsolutePath());
     /*   htmlWebView.setWebChromeClient(new WebChromeClient() {
            //Other methods for your WebChromeClient here, if needed..

            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                Log.e("JS Error",message);
                return super.onJsAlert(view, url, message, result);
            }
        });*/
        htmlWebView.addJavascriptInterface(new Object() {

            @JavascriptInterface           // For API 17+
            public void SubmitTestCommandExecuted() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rAns = 0;
                        wAns = 0;

                        AssessmentModel assem, assessmentModel = null;
                        showDialog();
                        for (int j = 0; j < ConstantManager.questionmodel.size(); j++) {
                            String answer = ConstantManager.questionmodel.get(j).getYourAns();
                            if (IsNullOrEmpty(answer)) {
                                isCheckedAns = false;

                                AlertDialog.Builder builder = new AlertDialog.Builder(AMCQActivity.this);
                                builder.setMessage("Please Attempt All Questions")
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });
                                if (dialog != null && dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                final AlertDialog alert = builder.create();
                                alert.show();
                                break;
                            } else {
                                isCheckedAns = true;
                                if (answer.equalsIgnoreCase(ConstantManager.questionmodel.get(j).getAnswerText())) {
                                    double rightMarks = Double.valueOf(ConstantManager.questionmodel.get(j).getRightAnsMarks());
                                    ConstantManager.questionmodel.get(j).setRightAnsflag(1);
                                    ConstantManager.questionmodel.get(j).setRightMObt(rightMarks);
                                    ConstantManager.questionmodel.get(j).setWrongAnsflag(0);
                                    ConstantManager.questionmodel.get(j).setWrongMObt(0.0);
                                    rAns += rightMarks;
                                    counterRight++;
                                } else {
                                    double wrongMarks = Double.valueOf(ConstantManager.questionmodel.get(j).getWrongAnsMarks());
                                    //double wrongMarks = Double.valueOf(0);
                                    ConstantManager.questionmodel.get(j).setWrongAnsflag(1);
                                    ConstantManager.questionmodel.get(j).setWrongMObt(wrongMarks);
                                    ConstantManager.questionmodel.get(j).setRightAnsflag(0);
                                    ConstantManager.questionmodel.get(j).setRightMObt(0.0);
                                    wAns += wrongMarks;
                                    counterWrong++;
                                }
                                totalTestMarks = totalTestMarks + Double.valueOf(ConstantManager.questionmodel.get(j).getRightAnsMarks());
                            }
                        }

                        if (isCheckedAns) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(AMCQActivity.this);
                            builder.setMessage("Are you sure, you want to submit?")
                                    .setCancelable(false)
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            ConstantManager.selectedAssessmentModel = (AssessmentModel) SQLiteDB.GetAssessmentByTestCode(String.valueOf(totalTestMarks));
                                            ConstantManager.selectedAssessmentModel.setTestTotalQuestion(String.valueOf(ConstantManager.questionmodel.size()));
                                            ConstantManager.selectedAssessmentModel.setCounterRight(counterRight);
                                            ConstantManager.selectedAssessmentModel.setCounterWrong(counterWrong);
                                            ConstantManager.selectedAssessmentModel.setCounterSkip(CounterSkip);
                                            ConstantManager.selectedAssessmentModel.setTotalWrongmarksObtained((int) wAns);
                                            ConstantManager.selectedAssessmentModel.setTotalRightmarksObtained((int) rAns);
                                            ConstantManager.selectedAssessmentModel.setTestTotalMarks(totalTestMarks + "");
                                            double score = (counterRight / totalTestMarks) * 100;
                                            ConstantManager.SCORE = Double.valueOf(String.format("%.2f", score));

                                            dbhelper = SQLiteDB.getInstance(AMCQActivity.this, ConstantManager.DB_PATH);
                                            String individualId = dbhelper.insertIntoTestScoreIndividualDetails(ConstantManager.selectedAssessmentModel);
                                            String ID = UUID.randomUUID() + "";
                                            dbhelper.insertTestSummeryData_Sqlite(ConstantManager.selectedAssessmentModel, individualId, ID);

                                            StringBuilder str = new StringBuilder();
                                            for (int i = 0; i < ConstantManager.questionmodel.size(); i++) {
                                                String new_id = UUID.randomUUID()+"";
                                                dbhelper.InsertTestResult(ConstantManager.questionmodel.get(i), i, individualId, new_id);
                                                str.append(createXmlData(new_id,
                                                        ConstantManager.questionmodel.get(i).QuestionCode,
                                                        ConstantManager.questionmodel.get(i).getYourAns(), //AttemptedAns
                                                        ConstantManager.questionmodel.get(i).getRightAnsflag(), //isAnsCorrect
                                                        ConstantManager.questionmodel.get(i).getRightAnsMarks(), //QuestionMarks
                                                        Integer.parseInt(String.valueOf(ConstantManager.questionmodel.get(i).getRightMObt().intValue())), //QuestionMarkObtained
                                                        DateUtil.getCurrentDateTimeInFormat("yyyy-MM-dd HH:mm:ss")
                                                ));
                                            }

                                                insertDetails(
                                                        individualId,
                                                        ID,
                                                        SettingPreffrences.getUserid(getApplicationContext()),
                                                        SettingPreffrences.getCourseId(getApplicationContext()),
                                                        SettingPreffrences.getSubjectId(getApplicationContext()),
                                                        ConstantManager.selectedAssessmentModel,
                                                        str.toString());



                                            if (dialog != null) {
                                                dialog.dismiss();
                                            }
                                            startActivity(new Intent(AMCQActivity.this, TestResultActivity.class)
                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

                                            finish();
                                        }
                                    });
                            final AlertDialog alert = builder.create();
                            alert.show();


                        }
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }

                    }
                });

            }

            @JavascriptInterface
            public void getUserOption(String selectedValue) {

                ConstantManager.questionmodel.get(count).setYourAns(selectedValue);

            }

            @JavascriptInterface
            public void NextQuestion() {
                String ans = ConstantManager.questionmodel.get(count).getYourAns();

                if (ans != null && !ans.isEmpty()) {
                    count++;
                    if (count < ConstantManager.questionmodel.size()) {
                        String questionText = ConstantManager.questionmodel.get(count).getQuestionText();
                       // if (questionText.contains("<img")) {
                            String QuestionCode = ConstantManager.questionmodel.get(count).getQuestionCode();
                            dbhelper = SQLiteDB.getInstance(AMCQActivity.this, ConstantManager.DB_PATH);
                            arrQuest_img = new ArrayList<QuestionImageModel>();
                            arrQuest_img = dbhelper.getQuestionImage(QuestionCode);
                            Log.d("McqImage","22: "+arrQuest_img.size());
                            if (arrQuest_img.size() > 0) {
                                String ImageName = arrQuest_img.get(0).getImageName();
                                String Imageuri = arrQuest_img.get(0).getDataUri();
                                String QuestionText = ConstantManager.questionmodel.get(count).getQuestionText();
                                //String UpdatedQuestion = QuestionText.replace(ImageName, Imageuri);
                                String UpdatedQuestion = replaceImage(QuestionText, arrQuest_img);
                                ConstantManager.questionmodel.get(count).setQuestionText(UpdatedQuestion);
                                ConstantManager.questionmodel.get(count).Option1 = replaceImage(ConstantManager.questionmodel.get(count).Option1, arrQuest_img);
                                ConstantManager.questionmodel.get(count).Option2 = replaceImage(ConstantManager.questionmodel.get(count).Option2, arrQuest_img);
                                ConstantManager.questionmodel.get(count).Option3 = replaceImage(ConstantManager.questionmodel.get(count).Option3, arrQuest_img);
                                ConstantManager.questionmodel.get(count).Option4 = replaceImage(ConstantManager.questionmodel.get(count).Option4, arrQuest_img);
                                ConstantManager.questionmodel.get(count).Option5 = replaceImage(ConstantManager.questionmodel.get(count).Option5, arrQuest_img);
                                ConstantManager.questionmodel.get(count).Option6 = replaceImage(ConstantManager.questionmodel.get(count).Option6, arrQuest_img);

                            }
                     //   }
                        final String s = new Sample().LoadData(AMCQActivity.this, ConstantManager.questionmodel.get(count), count, ConstantManager.questionmodel.size()).toString();
                        File dir = new File(superDBDirPath);
                        if (!dir.exists()) {
                            dir.mkdir();
                        }
                        final File file = new File(superDBDirPath, fileName);
                        File cssfiles = new File(superDBDirPath, "ecl_template.css");
                        filecopy(file, s);
                        if (!cssfiles.exists()) {
                            filecopy(cssfiles, CssFileString);
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                htmlWebView.loadUrl("file://" + file.getAbsolutePath());

                            }
                        });
                    } else {
                        count--;
                    }
                }
            }

            @JavascriptInterface
            public void PrevQuestion() {
                String ans = ConstantManager.questionmodel.get(count).getYourAns();
                if (ans != null && !ans.isEmpty()) {
                    if (count != 0) {
                        count--;
                        String questionText = ConstantManager.questionmodel.get(count).getQuestionText();
                      //  if (questionText.contains("<img")) {
                            String QuestionCode = ConstantManager.questionmodel.get(count).getQuestionCode();
                            dbhelper = SQLiteDB.getInstance(AMCQActivity.this, ConstantManager.DB_PATH);
                            arrQuest_img = new ArrayList<QuestionImageModel>();
                            arrQuest_img = dbhelper.getQuestionImage(QuestionCode);
                            Log.d("McqImage","33: "+arrQuest_img.size());
                            if (arrQuest_img.size() > 0) {
                                String ImageName = arrQuest_img.get(0).getImageName();
                                String Imageuri = arrQuest_img.get(0).getDataUri();
                                String QuestionText = ConstantManager.questionmodel.get(count).getQuestionText();
                                //String UpdatedQuestion = QuestionText.replace(ImageName, Imageuri);
                                String UpdatedQuestion = replaceImage(QuestionText, arrQuest_img);
                                ConstantManager.questionmodel.get(count).setQuestionText(UpdatedQuestion);
                                ConstantManager.questionmodel.get(count).Option1 = replaceImage(ConstantManager.questionmodel.get(count).Option1, arrQuest_img);
                                ConstantManager.questionmodel.get(count).Option2 = replaceImage(ConstantManager.questionmodel.get(count).Option2, arrQuest_img);
                                ConstantManager.questionmodel.get(count).Option3 = replaceImage(ConstantManager.questionmodel.get(count).Option3, arrQuest_img);
                                ConstantManager.questionmodel.get(count).Option4 = replaceImage(ConstantManager.questionmodel.get(count).Option4, arrQuest_img);
                                ConstantManager.questionmodel.get(count).Option5 = replaceImage(ConstantManager.questionmodel.get(count).Option5, arrQuest_img);
                                ConstantManager.questionmodel.get(count).Option6 = replaceImage(ConstantManager.questionmodel.get(count).Option6, arrQuest_img);

                            }
                      //  }
                        final String s = new Sample().LoadData(AMCQActivity.this, ConstantManager.questionmodel.get(count), count, ConstantManager.questionmodel.size()).toString();
                        File dir = new File(superDBDirPath);
                        if (!dir.exists()) {
                            dir.mkdir();
                        }
                        final File file = new File(superDBDirPath, fileName);
                        File cssfiles = new File(superDBDirPath, "ecl_template.css");
                        filecopy(file, s);
                        if (!cssfiles.exists()) {
                            filecopy(cssfiles, CssFileString);
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                htmlWebView.loadUrl("file://" + file.getAbsolutePath());
                            }
                        });

                    }

                }
            }
        }, "java");

    }

    private StringBuilder FinalXmlData(String value){

        StringBuilder str = new StringBuilder();
        str.append("<Questions>");
        str.append(value);
        str.append("</Questions>");
        return str;
    }

    private StringBuilder createXmlData(String id, String questionCode, String attemptedAns, int isAnsCorrect,
                                        String questionMarks, int QuestionMarksObtain, String dateTime){
        String isAnswerCorrect_value = "";
        if (isAnsCorrect == 0){
            isAnswerCorrect_value = "False";
        }else {
            isAnswerCorrect_value = "True";
        }

        StringBuilder builder = new StringBuilder();
        builder.append("<Question>");
        builder.append("<TestScoreResultDetailId>");
        builder.append(id);
        builder.append("</TestScoreResultDetailId>");
        builder.append("<QuestionCode>");
        builder.append(questionCode);
        builder.append("</QuestionCode>");
        builder.append("<AttemptedAnswer>");
        builder.append(attemptedAns);
        builder.append("</AttemptedAnswer>");
        builder.append("<IsAnswerCorrect>");
        builder.append(isAnswerCorrect_value);
        builder.append("</IsAnswerCorrect>");
        builder.append("<QuestionMarks>");
        builder.append(questionMarks);
        builder.append("</QuestionMarks>");
        builder.append("<QuestionMarksObtained>");
        builder.append(QuestionMarksObtain);
        builder.append("</QuestionMarksObtained>");
        builder.append("<QuestionAttemptedDateTime>");
        builder.append(dateTime);
        builder.append("</QuestionAttemptedDateTime>");
        builder.append("</Question>");
        return builder;
    }

    private void updateUserStatistics(float score) {
        String userId = SettingPreffrences.getUserid(getApplicationContext()).isEmpty() ? "0" : SettingPreffrences.getUserid(getApplicationContext());
        String date = new SimpleDateFormat(Constants.DATABASE_DATE_FORMAT).format(new Date());
        String formattedScore = String.format("%.2f", score);

        Log.d("AddAssessment", "userStat: userID: "+userId+" CourseID: "+SettingPreffrences.getCourseId(getApplicationContext())
                +" SubjectID: "+SettingPreffrences.getSubjectId(getApplicationContext())+
                " ChapterID: "+ConstantManager.Selected_Chapters_Id+" :: "+ConstantManager.Parameter.CHAPTER_ID
                +" formattedScore: "+formattedScore);

        userStatistics userStatistics = new userStatistics(
                userId,
                SettingPreffrences.getCourseId(getApplicationContext()),
                SettingPreffrences.getSubjectId(getApplicationContext()),
                SettingPreffrences.getChapterId(getApplicationContext()),
                KEF_ContentTypeId_MCQ,
                KEF_ContentTypeName_MCQ,
                "0",
                "0",
                date,
                date,
                formattedScore
        );
        userStatistics.save();
    }

    private void insertDetails(String TestScoreIndividualDetailID, String TestScoreSummaryDetailID,
                               String UserID, String CourseID, String SubjectID,
                               AssessmentModel assessment, String questionXml){

        StringBuilder finalQuestionData = FinalXmlData(questionXml);

        Double TotalMarksValue = Double.valueOf(assessment.getTestTotalMarks());
        int tmValue = TotalMarksValue.intValue();

        if (assessment != null){
            int totalMarks = 0;
            if (assessment.getTotalWrongmarksObtained() > 0) {
                totalMarks = assessment.getTotalRightmarksObtained() - assessment.getTotalWrongmarksObtained();
            } else {
                totalMarks = assessment.getTotalRightmarksObtained() + assessment.getTotalWrongmarksObtained();
            }

            JsonObject data = new JsonObject();
            data.addProperty("TestScoreIndividualDetailID",TestScoreIndividualDetailID);
            data.addProperty("TestScoreSummaryDetailID",TestScoreSummaryDetailID);
            data.addProperty("UserID",UserID);
            data.addProperty("EdzamCourseID",CourseID);
            data.addProperty("SubjectID",SubjectID);
            data.addProperty("SchoolID",SettingPreffrences.getSchoolId(getApplicationContext()));
            data.addProperty("TestTotalMarks",String.valueOf(tmValue));
            data.addProperty("TestMarksObtained",String.valueOf(totalMarks));
            data.addProperty("TotalQuestion",String.valueOf(assessment.getTestTotalQuestion()));
            data.addProperty("RightAnswerCount",String.valueOf(assessment.getCounterRight()));
            data.addProperty("WrongAnswerCount",String.valueOf(assessment.getCounterWrong()));
            data.addProperty("TestAttemptedDatetime", DateUtil.getCurrentDateTimeInFormat("yyyy-MM-dd HH:mm:ss"));
            data.addProperty("TestAttemptedCount","1");
            data.addProperty("AppType","KEFM");
            data.addProperty("QuestionXML",finalQuestionData.toString());

            Log.w("myJson","data: "+data.toString());

            //System.out.println("Json:  "+data.toString());


            if (CommonFunctions.isNetworkConnected(getApplicationContext())){
                try {
                    asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.KEF_mcqDataHost + NetworkUrl.KEF_insertMcqData, data, new OnTaskComplete() {
                        @Override
                        public void getResponse(String response) {
                            Log.d("ApiResponse","InsertUpdateMCQTestResultDetails: "+response);
                            if (!response.equals("error")){
                                dbhelper.updateSyncAssessmentData(TestScoreIndividualDetailID);
                            }
                        }
                    },"POST");

                    asyncTaskHelper.execute();

                }catch (Exception e){
                    Log.d("ApiResponse","InsertUpdateMCQTestResultDetails: Error"+e.getMessage());
                    e.printStackTrace();
                }
            }

        }
    }


    private void filecopy(File file, String s) {
        try {
            FileOutputStream out = new FileOutputStream(file);
            byte[] data = s.getBytes();
            out.write(data);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getCssAsString() {
        String css = "";
        Scanner cssScanner = null;
        String TOKEN_BOUNDARY = "\\A";
        try {

            InputStream cssInputStream = this.getAssets().open("ecl_template.css");
            cssScanner = new Scanner(
                    cssInputStream,
                    UTF_8_ENCODING_TYPE
            ).useDelimiter(TOKEN_BOUNDARY);

            if (cssScanner.hasNext()) {
                css = cssScanner.next();
            }

        } catch (IOException e) {
            Log.e("Tag", e.toString());
        } finally {
            if (cssScanner != null) {
                cssScanner.close();
            }
        }

        return css;
    }

    private String replaceImage(String input, List<QuestionImageModel> listOfQues) {
        String output = input;

        try {

            if (!IsNullOrEmpty(input) && input.contains("<img")) {
                for (QuestionImageModel model : listOfQues) {
                    if(input.contains(model.ImageName)) {
                        output = output.replace(model.ImageName, model.DataUri);
                       // break;
                    }
                }
            }
            Log.d("McqImage",":: InReplaceImage");
        } catch (Exception e) {
            Log.e("ImageError", e.getMessage());
        }

        return output;
    }

    public String getjsAsString() {
        String css = "";
        Scanner cssScanner = null;
        String TOKEN_BOUNDARY = "\\A";
        try {

            InputStream cssInputStream = this.getAssets().open("mathmlsupport_min.js");
            cssScanner = new Scanner(
                    cssInputStream,
                    UTF_8_ENCODING_TYPE
            ).useDelimiter(TOKEN_BOUNDARY);

            if (cssScanner.hasNext()) {
                css = cssScanner.next();
            }

        } catch (IOException e) {
            Log.e("Tag", e.toString());
        } finally {
            if (cssScanner != null) {
                cssScanner.close();
            }
        }

        return css;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("");
        builder.setMessage("Are you sure want exit?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ConstantManager.Selected_level.clear();
                ConstantManager.Selected_Chapters_Id.clear();
                ConstantManager.Selected_Topic_Id.clear();
                ConstantManager.IS_ALL_CHECKED = "NO";
                ConstantManager.questionmodel.clear();
                ConstantManager.childItems.clear();
                ConstantManager.parentItems.clear();
                for (int i = 0; i < ConstantManager.arrPreviousActivity.size(); i++) {
                    ConstantManager.arrPreviousActivity.get(i).finish();
                }
                Intent i = new Intent(AMCQActivity.this, AssessmentSubjects.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                App.getInstance().trackEvent("AssessmentMCQ", "Screen", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " + "selectedStandard - " + SettingPreffrences.getStandard(AMCQActivity.this) + "" + " ; " + "selectedSubject - " + SettingPreffrences.getSubject(AMCQActivity
                        .this), "");
                startActivity(i);
                finish();

            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        builder.show();
    }

    public void showDialog() {

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setCancelable(false);
        ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.pb);
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.blue), PorterDuff.Mode.SRC_IN);
        dialog.show();

    }

    public static String StreamToString(InputStream in) throws IOException {
        if (in == null) {
            return "";
        }
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } finally {
        }
        return writer.toString();
    }

    private class MyJavaInterface implements JavascriptInterface {

        public void SubmitTestCommandExecuted() {
            Toast.makeText(AMCQActivity.this, "Submit", Toast.LENGTH_SHORT).show();
        }


        @Override
        public boolean equals(Object obj) {
            return false;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return null;
        }

        @Override
        public Class<? extends Annotation> annotationType() {
            return null;
        }
    }

}
