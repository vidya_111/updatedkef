package com.observatory.Assessment;

import android.content.Context;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.observatory.kefcorner.R;

import java.util.ArrayList;
import java.util.HashMap;

public class ChapterTopicExpandableListAdapter extends BaseExpandableListAdapter {

    public ArrayList<ArrayList<HashMap<String, String>>> childItems;
    public ArrayList<HashMap<String, String>> parentItems;
    private LayoutInflater inflater;
    private A_ChapterActivity activity;
    private HashMap<String, String> child;
    private int count = 0;
    private boolean isFromMyCategoriesFragment;

    public ChapterTopicExpandableListAdapter(A_ChapterActivity activity, ArrayList<HashMap<String, String>> parentItems,
                                             ArrayList<ArrayList<HashMap<String, String>>> childItems,boolean isFromMyCategoriesFragment) {

        this.parentItems = parentItems;
        this.childItems = childItems;
        this.activity = activity;
        this.isFromMyCategoriesFragment = isFromMyCategoriesFragment;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getGroupCount() {
        return parentItems.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return (childItems.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int i) {
        return null;
    }

    @Override
    public Object getChild(int i, int i1) {
        return null;
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i1) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }


    @Override
    public View getGroupView(final int groupPosition, final boolean b, View convertView, ViewGroup viewGroup) {
        final ViewHolderParent viewHolderParent;
        if (convertView == null) {

            if(isFromMyCategoriesFragment) {
                //convertView = inflater.inflate(R.layout.expandable_header, null);
            }else {
                convertView = inflater.inflate(R.layout.group_list_layout_choose_categories, null);
            }
            viewHolderParent = new ViewHolderParent();
            viewHolderParent.cardview=convertView.findViewById(R.id.cardview);
            viewHolderParent.tvChaptersName = convertView.findViewById(R.id.tvChaptersName);
            viewHolderParent.cbChapter = convertView.findViewById(R.id.cbChapter);
            viewHolderParent.ivCategory = convertView.findViewById(R.id.ivCategory);
            convertView.setTag(viewHolderParent);
        } else {

            viewHolderParent = (ViewHolderParent) convertView.getTag();
        }

        if(childItems.get(groupPosition).size()>0){
            viewHolderParent.ivCategory.setVisibility(View.VISIBLE);
        }else{
            viewHolderParent.ivCategory.setVisibility(View.GONE);
        }

        if (parentItems.get(groupPosition).get(ConstantManager.Parameter.IS_CHECKED).equalsIgnoreCase(ConstantManager.CHECK_BOX_CHECKED_TRUE)) {
            viewHolderParent.cbChapter.setChecked(true);
            notifyDataSetChanged();

        } else {
            viewHolderParent.cbChapter.setChecked(false);
            notifyDataSetChanged();
        }

        viewHolderParent.cbChapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewHolderParent.cbChapter.isChecked()) {
                    parentItems.get(groupPosition).put(ConstantManager.Parameter.IS_CHECKED, ConstantManager.CHECK_BOX_CHECKED_TRUE);
                    boolean isAllChecked=true;
                    for (int i = 0; i < childItems.get(groupPosition).size(); i++) {
                        childItems.get(groupPosition).get(i).put(ConstantManager.Parameter.IS_CHECKED, ConstantManager.CHECK_BOX_CHECKED_TRUE);
                    }
                    for (int i = 0; i < parentItems.size(); i++) {
                        if(parentItems.get(i).get(ConstantManager.Parameter.IS_CHECKED)=="NO")
                        {
                           isAllChecked=false;
                            break;
                        }
                    }
                    if(isAllChecked)
                    {
                        ConstantManager.IS_ALL_CHECKED="YES";
                    }
                    else
                    {
                        ConstantManager.IS_ALL_CHECKED="NO";
                    }
                    activity.refresh_allCheck();
                    notifyDataSetChanged();

                }
                else {
                    parentItems.get(groupPosition).put(ConstantManager.Parameter.IS_CHECKED, ConstantManager.CHECK_BOX_CHECKED_FALSE);
                    for (int i = 0; i < childItems.get(groupPosition).size(); i++) {
                        childItems.get(groupPosition).get(i).put(ConstantManager.Parameter.IS_CHECKED, ConstantManager.CHECK_BOX_CHECKED_FALSE);
                    }
                    ConstantManager.IS_ALL_CHECKED="NO";
                    activity.refresh_allCheck();
                    notifyDataSetChanged();
                }
            }
        });

        ConstantManager.childItems = childItems;
        ConstantManager.parentItems = parentItems;

        viewHolderParent.tvChaptersName.setText(parentItems.get(groupPosition).get(ConstantManager.Parameter.CHAPTER_NAME));

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, final boolean b, View convertView, ViewGroup viewGroup) {

        final ViewHolderChild viewHolderChild;
        child = childItems.get(groupPosition).get(childPosition);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.child_list_layout_choose_category, null);
            viewHolderChild = new ViewHolderChild();

            viewHolderChild.tvTopicName = convertView.findViewById(R.id.tvTopicName);
            viewHolderChild.cbTopic = convertView.findViewById(R.id.cbTopic);
            viewHolderChild.viewDivider = convertView.findViewById(R.id.viewDivider);
            convertView.setTag(viewHolderChild);
        } else {
            viewHolderChild = (ViewHolderChild) convertView.getTag();
        }

        if (childItems.get(groupPosition).get(childPosition).get(ConstantManager.Parameter.IS_CHECKED).equalsIgnoreCase(ConstantManager.CHECK_BOX_CHECKED_TRUE)) {
            viewHolderChild.cbTopic.setChecked(true);
            notifyDataSetChanged();
        } else {
            viewHolderChild.cbTopic.setChecked(false);
            notifyDataSetChanged();
        }

        viewHolderChild.tvTopicName.setText(child.get(ConstantManager.Parameter.TOPIC_NAME));
        viewHolderChild.cbTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewHolderChild.cbTopic.isChecked()) {
                    count = 0;
                    childItems.get(groupPosition).get(childPosition).put(ConstantManager.Parameter.IS_CHECKED, ConstantManager.CHECK_BOX_CHECKED_TRUE);
                    notifyDataSetChanged();
                } else {
                    count = 0;
                    childItems.get(groupPosition).get(childPosition).put(ConstantManager.Parameter.IS_CHECKED, ConstantManager.CHECK_BOX_CHECKED_FALSE);
                    ConstantManager.IS_ALL_CHECKED="NO";
                    activity.refresh_allCheck();
                    notifyDataSetChanged();
                }

                for (int i = 0; i < childItems.get(groupPosition).size(); i++) {
                    if (childItems.get(groupPosition).get(i).get(ConstantManager.Parameter.IS_CHECKED).equalsIgnoreCase(ConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                        count++;
                    }
                }
                if (count == childItems.get(groupPosition).size()) {
                    parentItems.get(groupPosition).put(ConstantManager.Parameter.IS_CHECKED, ConstantManager.CHECK_BOX_CHECKED_TRUE);
                    notifyDataSetChanged();
                } else {
                    parentItems.get(groupPosition).put(ConstantManager.Parameter.IS_CHECKED, ConstantManager.CHECK_BOX_CHECKED_FALSE);
                    notifyDataSetChanged();
                }
                boolean isAllChecked=true;
                for (int i = 0; i < parentItems.get(groupPosition).size(); i++) {
                    if(parentItems.get(i).get(ConstantManager.Parameter.IS_CHECKED)=="NO")
                    {
                        isAllChecked=false;
                        break;
                    }

                }
                if(isAllChecked)
                {
                    ConstantManager.IS_ALL_CHECKED="YES";
                }
                else
                {
                    ConstantManager.IS_ALL_CHECKED="NO";
                }
                activity.refresh_allCheck();
                ConstantManager.childItems = childItems;
                ConstantManager.parentItems = parentItems;
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }
    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        int childcount= ConstantManager.childItems.get(groupPosition).size();
        if(childcount<=0)
        {
           // Toast.makeText(activity, "Topic not found.", Toast.LENGTH_LONG).show();
        }
        else
        {
            super.onGroupExpanded(groupPosition);
        }
    }

    private class ViewHolderParent {

        TextView tvChaptersName;
        CheckBox cbChapter;
        ImageView ivCategory;
        CardView cardview;
    }

    private class ViewHolderChild {

        TextView tvTopicName;
        CheckBox cbTopic;
        View viewDivider;
    }
}
