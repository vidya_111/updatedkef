package com.observatory.Assessment;

import android.content.Intent;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.observatory.Assessment.DataBase.SQLiteDB;
import com.observatory.Assessment.Model.ChapterModel;
import com.observatory.Assessment.Model.TopicModel;
import com.observatory.kefcorner.R;
import com.observatory.utils.BaseActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class A_ChapterActivity extends BaseActivity {

    private ExpandableListView lvCategory;
    public static final String SUBJECT_NAME = "subject_name";
    public static final String SUBJECT_ID = "subject_id";
    private ArrayList<ChapterModel> arrChapter;
    private ArrayList<TopicModel> arrTopic;
    private ArrayList<ArrayList<TopicModel>> arrTopicFinal;
    private ArrayList<HashMap<String, String>> parentItems;
    private ArrayList<ArrayList<HashMap<String, String>>> childItems;
    private ChapterTopicExpandableListAdapter ChapterTopicExpandableListAdapter;
    Button btn_next;
    private SQLiteDB dbhelper;
    CheckBox chk_allchapters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a__chapter);
        actionBar.setTitle("Select Chapter");
    }

    @Override
    protected void onLayout() {

        setupReferences();

        chk_allchapters = (CheckBox) findViewById(R.id.chk_allchapters);
        chk_allchapters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isChecked = chk_allchapters.isChecked();
                if (isChecked) {
                    ConstantManager.IS_ALL_CHECKED = "YES";
                } else {
                    ConstantManager.IS_ALL_CHECKED = "NO";
                }
                setupReferences();
            }
        });

        btn_next = (Button) findViewById(R.id.btn_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int parentcount = 0, childcount = 0;
                for (int i = 0; i < ChapterTopicExpandableListAdapter.parentItems.size(); i++) {

                    String isChecked = ChapterTopicExpandableListAdapter.parentItems.get(i).get(ConstantManager.Parameter.IS_CHECKED);

                    if (isChecked.equalsIgnoreCase(ConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                        String id = ChapterTopicExpandableListAdapter.parentItems.get(i).get(ConstantManager.Parameter.CHAPTER_ID);
                        ConstantManager.Selected_Chapters_Id.add(id);
                        parentcount++;
                    }

                    for (int j = 0; j < ChapterTopicExpandableListAdapter.childItems.get(i).size(); j++) {

                        String isChildChecked = ChapterTopicExpandableListAdapter.childItems.get(i).get(j).get(ConstantManager.Parameter.IS_CHECKED);
                        if (isChildChecked.equalsIgnoreCase(ConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                            String id = ChapterTopicExpandableListAdapter.childItems.get(i).get(j).get(ConstantManager.Parameter.TOPIC_ID);
                            ConstantManager.Selected_Topic_Id.add(id);
                            childcount++;
                        }
                    }
                }
                if (parentcount > 0 || childcount > 0) {
                    ConstantManager.arrPreviousActivity.add(A_ChapterActivity.this);
                    if (ConstantManager.Selected_level != null) {
                        ConstantManager.Selected_level.clear();
                    }
                    startActivity(new Intent(A_ChapterActivity.this, LevelActivity.class));
                } else {
                    Toast.makeText(A_ChapterActivity.this, "Please select chapter", Toast.LENGTH_LONG).show();
                }
            }


        });
    }


    private void setupReferences() {

        lvCategory = (ExpandableListView) findViewById(R.id.lvCategory);
        arrChapter = new ArrayList<>();
        arrTopic = new ArrayList<>();
        parentItems = new ArrayList<>();
        childItems = new ArrayList<>();
        int subjectid = Integer.parseInt(getIntent().getStringExtra(SUBJECT_ID));
        dbhelper = SQLiteDB.getInstance(this, ConstantManager.DB_PATH);
        arrChapter = dbhelper.getChapterBySubjectId(subjectid);


        for (ChapterModel data : arrChapter) {
//                        Log.i("Item id",item.id);
            ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ConstantManager.Parameter.CHAPTER_ID, data.getChapterId());
            mapParent.put(ConstantManager.Parameter.CHAPTER_NAME, data.getChapterName());


            int countIsChecked = 0;
            for (TopicModel subCategoryItem : data.getmTopicList()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ConstantManager.Parameter.TOPIC_ID, String.valueOf(subCategoryItem.getTopicId()));
                mapChild.put(ConstantManager.Parameter.TOPIC_NAME, subCategoryItem.getTopicName());
                mapChild.put(ConstantManager.Parameter.CHAPTER_ID, String.valueOf(subCategoryItem.getChapterId()));
                mapChild.put(ConstantManager.Parameter.IS_CHECKED, subCategoryItem.getTopicChecked());
                if (ConstantManager.IS_ALL_CHECKED == "YES") {
                    mapChild.put(ConstantManager.Parameter.IS_CHECKED, "YES");
                } else {
                    mapChild.put(ConstantManager.Parameter.IS_CHECKED, "NO");
                }
                if (subCategoryItem.getTopicChecked().equalsIgnoreCase(ConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }

                childArrayList.add(mapChild);
            }

            if (countIsChecked == data.getmTopicList().size()) {
                data.setChapterChecked(ConstantManager.CHECK_BOX_CHECKED_TRUE);
            } else {
                data.setChapterChecked(ConstantManager.CHECK_BOX_CHECKED_FALSE);
            }
            if (ConstantManager.IS_ALL_CHECKED == "YES") {
                mapParent.put(ConstantManager.Parameter.IS_CHECKED, "YES");
            } else {
                mapParent.put(ConstantManager.Parameter.IS_CHECKED, "NO");
            }

            childItems.add(childArrayList);
            parentItems.add(mapParent);
        }

        ConstantManager.parentItems = parentItems;
        ConstantManager.childItems = childItems;

        ChapterTopicExpandableListAdapter = new ChapterTopicExpandableListAdapter(this, parentItems, childItems, false);
        lvCategory.setAdapter(ChapterTopicExpandableListAdapter);

      /*  lvCategory.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                int childcount= ConstantManager.childItems.get(i).size();
                if(childcount<=0)
                {
                    Toast.makeText(activity, "Topic not found.", Toast.LENGTH_LONG).show();
                }
                return true;
            }
        });*/
    }

    public void refresh_allCheck() {
        if (ConstantManager.IS_ALL_CHECKED == "YES") {
            chk_allchapters.setChecked(true);
        } else {
            chk_allchapters.setChecked(false);
        }
    }

    @Override
    public void onBackPressed() {
        clearConstants();
        finish();
    }

    public void clearConstants() {
        if (ConstantManager.Selected_level != null) {
            ConstantManager.Selected_level.clear();
        }
        if (ConstantManager.Selected_Chapters_Id != null) {
            ConstantManager.Selected_Chapters_Id.clear();
        }
        if (ConstantManager.Selected_Topic_Id != null) {
            ConstantManager.Selected_Topic_Id.clear();
        }
        if (ConstantManager.questionmodel != null) {
            ConstantManager.questionmodel.clear();
        }
        if (ConstantManager.childItems != null) {
            ConstantManager.childItems.clear();
        }
        if (ConstantManager.parentItems != null) {
            ConstantManager.parentItems.clear();
        }
        ConstantManager.IS_ALL_CHECKED = "NO";
        for (int i = 0; i < ConstantManager.arrPreviousActivity.size(); i++) {
            ConstantManager.arrPreviousActivity.get(i).finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            clearConstants();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
