package com.observatory.Assessment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.observatory.Assessment.DataBase.SQLiteDB;
import com.observatory.Assessment.Model.AssessmentModel;
import com.observatory.Assessment.Model.QuestionModel;
import com.observatory.Assessment.Model.SubjectModel;
import com.observatory.AsyncTaskMain.AsyncTaskHelper;
import com.observatory.AsyncTaskMain.OnTaskComplete;
import com.observatory.database.Master;
import com.observatory.kefcorner.R;
import com.observatory.kefcorner.Subjects;
import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.Constants;
import com.observatory.utils.EmailHandler;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;
import com.orm.query.Condition;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import android.util.Base64;


public class AssessmentSubjects extends BaseActivity {

    private static final String TAG = "Assessment Subjects";
    private GridView grdSubjects;
    private RecyclerView rvSubjects;
    AssessmentSubjects.SubjectAdapter subjectAdapter;

    private int[] colors;
    private String[] subjects;
    private String[] displaySubjects;

    private int[] images;

    private HashMap<String, Integer> mapImageID = new HashMap<String, Integer>();
    private HashMap<String, Integer> mapColors = new HashMap<String, Integer>();
    private ArrayList<SubjectModel> subjectsArray;
    private SQLiteDB dbhelper;
    private HashMap<String, String> dimensions;
    private TextView tvFromDate;
    private TextView tvToDate;
    private String fromDate;
    private String toDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessment_subjects);

        actionBar.setTitle(SettingPreffrences.getSubjectTitle(getApplicationContext()));
        SettingPreffrences.setStandard(getApplicationContext(), SettingPreffrences.getSubjectTitle(getApplicationContext()));

        dbhelper = SQLiteDB.getInstance(this, ConstantManager.DB_PATH);
        Log.d("UpdateAssessment", "DB_PATH: " + ConstantManager.DB_PATH);
        subjectsArray = dbhelper.getAllSubject(true);
        // dbhelper.insertdemo();
       /* subjects = new String[subjectsArray.size()];
        //images = new int[subjectsArray.size()];
        //colors = new int[subjectsArray.size()];
        displaySubjects = new String[subjectsArray.size()];

        for (int i = 0; i < subjectsArray.size(); i++) {
            //Changes
            String subjectName = subjectsArray.get(i).getSubjectName().toString();//.toUpperCase();
            subjects[i] = subjectName;
            //images[i] = mapImageID.get(subjectName);
            //colors[i] = mapColors.get(subjectName);
            displaySubjects[i] = subjectsArray.get(i).getSubjectDisplayName().toUpperCase();

        }*/

        subjectAdapter = new AssessmentSubjects.SubjectAdapter(subjectsArray);
        rvSubjects.setLayoutManager(new GridLayoutManager(this, 2));
        rvSubjects.setAdapter(subjectAdapter);

        grdSubjects.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(AssessmentSubjects.this, A_ChapterActivity.class);
                SettingPreffrences.setSubject(AssessmentSubjects.this, subjectsArray.get(position).getSubjectDisplayName());
                i.putExtra(A_ChapterActivity.SUBJECT_NAME, subjectsArray.get(position).getSubjectName());
                App.getInstance().trackEvent("Chapter", "Screen", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " + "selectedStandard - " + SettingPreffrences.getStandard(AssessmentSubjects.this) + "" + " ; " + "selectedSubject - " + SettingPreffrences.getSubject(AssessmentSubjects
                        .this), "");
                startActivity(i);
            }
        });


        //autoSyncStatistics();
        if (!SettingPreffrences.isUserSynced(this)) {
            updateLastSeen();
            autoSyncLoginHistory();
            SettingPreffrences.setUserSync(getApplicationContext(),true);
        }
        // new EmailHandler().sendMail("phone@millicent.in", "Eclass Error Logs", "testing");
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    private void autoSyncLoginHistory() {
        long lastSyncedId = SettingPreffrences.getLastSyncedLoginHistoryId(getApplicationContext());
        List<com.observatory.database.UserHistory> userHistoryList;

        if (lastSyncedId != -1) {
            userHistoryList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com.observatory.database.UserHistory.class).where("id > " + lastSyncedId).orderBy("id asc") /* ordering in ascending so that latest appear at the end
					. */.list();

        } else {
//			userHistoryList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com
//					.observatory.database.UserHistory.class)
//					.list();

            userHistoryList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com.observatory.database.UserHistory.class).orderBy("id asc") /* ordering in ascending so that latest appear at the end
					. */.list();
        }

        // there is data to sync
        if (userHistoryList.size() > 0) {
            String statsJson = formatLoginHistoryToJson(userHistoryList);
            callSyncLoginHistoryWebService(statsJson, userHistoryList);
        }
    }

    private void callSyncLoginHistoryWebService(String json, final List<com.observatory.database.UserHistory> userHistoryList) {

        if (CommonFunctions.isNetworkConnected(this)) {

            String userId = SettingPreffrences.getUserid(getApplicationContext()).isEmpty() ? "0" : SettingPreffrences.getUserid(getApplicationContext());
            String token = SettingPreffrences.getToken(getApplicationContext()).isEmpty() ? "0000" : SettingPreffrences.getToken(getApplicationContext());
            String deviceId = CommonFunctions.getUniqueDeviceId(getApplicationContext());

            Ion.with(this).load(NetworkUrl.syncLoginHistory)
                    .setBodyParameter("userId", userId)
                    .setBodyParameter("token", token)
                    .setBodyParameter("os", Build.VERSION.RELEASE)
                    .setBodyParameter("make", Build.MANUFACTURER)
                    .setBodyParameter("model", Build.MODEL)
                    .setBodyParameter("historyDetails", json)
                    .asString().setCallback(new FutureCallback<String>() {

                @Override
                public void onCompleted(Exception e, String result) {

                    if (e == null) {
                         // update last synced Id of UserHistory table
                        com.observatory.database.UserHistory lastSyncedUserHistory = userHistoryList.get(userHistoryList.size() - 1);
                        SettingPreffrences.setLastSyncedLoginHistoryId(getApplicationContext(), lastSyncedUserHistory.getId());
                    }else{
                        new EmailHandler().sendMail("phone@millicent.in","KEF Sync",e.getMessage());
                    }
                }
            });
        }

    }

    private void updateLastSeen() {

        if (SettingPreffrences.isAppLaunched(getApplicationContext())) {
            String medium = getMedium();
            String userid = SettingPreffrences.getUserid(this);
            String time = new SimpleDateFormat(Constants.UI_TIME_FORMAT).format(Calendar.getInstance().getTime());
            String date = new SimpleDateFormat(Constants.UI_DATE_FORMAT_ONLY_DATE).format(Calendar.getInstance().getTime());

            com.observatory.database.UserHistory userHistory = new com.observatory.database.UserHistory(date, time, userid.isEmpty() ? "0" : userid, medium);
            userHistory.save();

            Log.d(TAG, "updateLastSeen: " + userid);
        }

    }

    private ArrayList<String> getCourseList() {

        String userid = SettingPreffrences.getUserid(getApplicationContext());

        ArrayList<String> courseList = new ArrayList<>();
        ArrayList<com.observatory.database.UserHistory> userHistoryArrayList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com.observatory.database.UserHistory.class).groupBy("medium").orderBy("id asc").where(Condition.prop("userId").eq((userid.isEmpty() ? "0" : userid))).list();

        for (com.observatory.database.UserHistory userHistory : userHistoryArrayList) {
            courseList.add(userHistory.getMedium());
        }

        return courseList;
    }

    public String getMedium() {
        String medium;

        List<Master> masterList = Master.listAll(Master.class);
        if (masterList.size() > 0) {
            Master master = masterList.get(0);
            medium = master.getMedium();
        } else {
            medium = "Medium could not be found";
            Toast.makeText(getApplicationContext(), medium, Toast.LENGTH_LONG).show();
        }

        return medium;
    }

    private String formatLoginHistoryToJson(List<com.observatory.database.UserHistory> arrayUserHistory) {

        JSONArray mainJsonArr = new JSONArray();

        ArrayList<String> courseList = getCourseList();

        for (int i = 0; i < courseList.size(); i++) {

            JSONObject jsonObject = new JSONObject();

            try {

                jsonObject.put("medium", courseList.get(i));

                JSONArray dataJsonArr = new JSONArray();

                for (com.observatory.database.UserHistory userHistory : arrayUserHistory) {

                    if (userHistory.getMedium().equals(courseList.get(i))) {

                        try {

                            JSONObject dateTimeJson = new JSONObject();
                            dateTimeJson.put("date", userHistory.getDate());
                            dateTimeJson.put("time", userHistory.getTime());
                            dataJsonArr.put(dateTimeJson);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

                // send data only if current medium has login data
                if (dataJsonArr.length() > 0) {
                    jsonObject.put("data", dataJsonArr);
                    mainJsonArr.put(jsonObject);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Log.d("Tag", "formatLoginHistoryToJson: " + mainJsonArr.toString());

        return mainJsonArr.toString();
    }

    @Override
    protected void onLayout() {
        grdSubjects = (GridView) findViewById(R.id.grdSubjects);
        rvSubjects = (RecyclerView) findViewById(R.id.rv_test_subjects);
    }

    class SubjectAdapter extends RecyclerView.Adapter<AssessmentSubjects.SubjectAdapter.MyViewHolder> {

        private ArrayList<SubjectModel> mSubjectData;

        private int selectedPos = RecyclerView.NO_POSITION;

        public SubjectAdapter(ArrayList<SubjectModel> data) {

            this.mSubjectData = data;

        }

        @Override
        public AssessmentSubjects.SubjectAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid, parent, false);
            return new AssessmentSubjects.SubjectAdapter.MyViewHolder(itemView);

        }

        @Override
        public void onBindViewHolder(AssessmentSubjects.SubjectAdapter.MyViewHolder holder, int position) {
            holder.itemView.setSelected(selectedPos == position);
            holder.textView.setText(mSubjectData.get(position).getSubjectDisplayName());
            String SubjectIcon = mSubjectData.get(position).getSubjectIcon();
            //holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.marathi250));
            if (SubjectIcon == null || SubjectIcon.equals("")) {
                holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.no_item));
            } else {
                holder.imageView.setImageBitmap(getIconBitmap(mSubjectData.get(position).getSubjectIcon()));
            }
        }

        private Bitmap getIconBitmap(String subjectIcon) {
            try {
                String cleanImage = subjectIcon.replace("data:image/png;base64,", "");
                byte[] encodeByte = Base64.decode(cleanImage, Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                return bitmap;
            } catch (Exception e) {
                e.getMessage();
                return null;
            }
        }


        @Override
        public int getItemCount() {
            return mSubjectData.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView textView;
            ImageView imageView;

            public MyViewHolder(final View itemView) {
                super(itemView);
                clearConstants();
                textView = itemView.findViewById(R.id.text);
                imageView = itemView.findViewById(R.id.img);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //selector
                        notifyItemChanged(selectedPos);
                        selectedPos = getAdapterPosition();
                        ConstantManager.SUBJECT_ID = subjectsArray.get(getAdapterPosition()).getSubjectId();
                        notifyItemChanged(selectedPos);
                        //ConstantManager.arrPreviousActivity.add(AssessmentSubjects.this);
                        Intent i = new Intent(AssessmentSubjects.this, A_ChapterActivity.class);
                        SettingPreffrences.setSubjectId(AssessmentSubjects.this, subjectsArray.get(getAdapterPosition()).getSubjectId().toString());
                        SettingPreffrences.setSubject(AssessmentSubjects.this, subjectsArray.get(getAdapterPosition()).getSubjectDisplayName());
                        i.putExtra(A_ChapterActivity.SUBJECT_NAME, subjectsArray.get(getAdapterPosition()).getSubjectName());
                        i.putExtra(A_ChapterActivity.SUBJECT_ID, subjectsArray.get(getAdapterPosition()).getSubjectId().toString());
                        App.getInstance().trackEvent("Chapter", "Screen", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " + "selectedStandard - " + SettingPreffrences.getStandard(AssessmentSubjects.this) + "" + " ; " + "selectedSubject - " + SettingPreffrences.getSubject(AssessmentSubjects
                                .this), "");
                        startActivity(i);

                    }
                });


            }
        }

        public void clearConstants() {
            if (ConstantManager.Selected_level != null) {
                ConstantManager.Selected_level.clear();
            }
            if (ConstantManager.Selected_Chapters_Id != null) {
                ConstantManager.Selected_Chapters_Id.clear();
            }
            if (ConstantManager.Selected_Topic_Id != null) {
                ConstantManager.Selected_Topic_Id.clear();
            }
            if (ConstantManager.questionmodel != null) {
                ConstantManager.questionmodel.clear();
            }
            if (ConstantManager.childItems != null) {
                ConstantManager.childItems.clear();
            }
            if (ConstantManager.parentItems != null) {
                ConstantManager.parentItems.clear();
            }
            ConstantManager.IS_ALL_CHECKED = "NO";
            for (int i = 0; i < ConstantManager.arrPreviousActivity.size(); i++) {
                ConstantManager.arrPreviousActivity.get(i).finish();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.assessment_refresh_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.sync_data) {
            syncPendingData();
        }
        return super.onOptionsItemSelected(item);
    }


    public void syncPendingData() {
        try {
            if (CommonFunctions.isNetworkConnected(getApplicationContext())) {
                CommonFunctions.showDialog(this, "Syncing...");
                ArrayList<AssessmentModel> pendingList = new ArrayList<>();
                pendingList = dbhelper.getSyncPendingIndividualId();
                Log.d("AssessmentLogs", "pending list size " + pendingList.size());
                if (pendingList.size() > 0) {
                    // for (int i = 0; i < pendingList.size(); i++) {
                    Log.d("PendingList", "IndividualID: " + pendingList.get(0).getIndividualID());
                    Log.d("PendingList", "TotalQuestions: " + pendingList.get(0).getTestTotalQuestion());
                    getDetailsFromPendingId(pendingList.get(0).getIndividualID());
                    // }
                } else {
                    Log.d("AssessmentLogs", "hideDialog");
                    CommonFunctions.hideDialog();
                    alert("Data is synced");
                }
            } else {
                alert("Internet Connection not available");
            }
        } catch (Exception e) {
            Log.e("SyncError", e.getMessage());
        }
    }

    public void getDetailsFromPendingId(String individualId) {
        ArrayList<AssessmentModel> dataList = new ArrayList<>();
        dataList = dbhelper.getSyncPendingDetailsByID(individualId);

        if (dataList.size() > 0) {
            for (int i = 0; i < dataList.size(); i++) {
                Log.d("PendingList11", "TotalQuestion: " + dataList.get(i).getTestTotalQuestion() + "Size: " + dataList.size());
                insertDetailsToServer(
                        individualId,
                        dataList.get(i).getTestScoreSummaryDetailsId(),
                        dataList.get(i).getStudentUserCode(),
                        SettingPreffrences.getCourseId(getApplicationContext()),
                        dataList.get(i).getCourseContentCode(),
                        Integer.parseInt(String.valueOf(Double.valueOf(dataList.get(i).getTestTotalMarks()).intValue())),
                        Integer.parseInt(String.valueOf(Double.valueOf(dataList.get(i).getTotalMarksObtain()).intValue())),
                        dataList.get(i).getTestTotalQuestion(),
                        dataList.get(i).getCounterRight(),
                        dataList.get(i).getCounterWrong(),
                        dataList.get(i).getLastAccessTime(),
                        "1",
                        "KEFM"
                );
            }
        }

    }

    public void insertDetailsToServer(String TestScoreIndividualDetailID, String TestScoreSummaryDetailID, String UserID, String CourseID,
                                      String SubjectID, int TestTotalMarks, int TestMarksObtained, String TotalQuestion, int RightAnswerCount,
                                      int WrongAnswerCount, String TestAttemptedDatetime, String TestAttemptedCount, String AppType) {

        StringBuilder str = new StringBuilder();
        ArrayList<QuestionModel> questionList = new ArrayList<>();
        questionList = dbhelper.getSyncPendingQuestions(TestScoreIndividualDetailID);

        if (questionList.size() > 0) {
            for (int i = 0; i < questionList.size(); i++) {
                str.append(createXmlData(
                        questionList.get(i).getTestResultDetailsId(),
                        questionList.get(i).getQuestionCode(),
                        questionList.get(i).getYourAns(),
                        questionList.get(i).getRightAnsflag(),
                        questionList.get(i).getRightAnsMarks(),
                        Integer.parseInt(String.valueOf(questionList.get(i).getRightMObt().intValue())),
                        questionList.get(i).getAttemptedTestDate()
                ));
            }
        }

        StringBuilder finalQuestionData = FinalXmlData(str.toString());

        JsonObject data = new JsonObject();
        data.addProperty("TestScoreIndividualDetailID", TestScoreIndividualDetailID);
        data.addProperty("TestScoreSummaryDetailID", TestScoreSummaryDetailID);
        data.addProperty("UserID", UserID);
        data.addProperty("EdzamCourseID", CourseID);
        data.addProperty("SubjectID", SubjectID);
        data.addProperty("SchoolID",SettingPreffrences.getSchoolId(getApplicationContext()));
        data.addProperty("TestTotalMarks", TestTotalMarks);
        data.addProperty("TestMarksObtained", TestMarksObtained);
        data.addProperty("TotalQuestion", TotalQuestion);
        data.addProperty("RightAnswerCount", String.valueOf(RightAnswerCount));
        data.addProperty("WrongAnswerCount", String.valueOf(WrongAnswerCount));
        data.addProperty("TestAttemptedDatetime", TestAttemptedDatetime);
        data.addProperty("TestAttemptedCount", TestAttemptedCount);
        data.addProperty("AppType", AppType);
        data.addProperty("QuestionXML", finalQuestionData.toString());

        Log.d("PendingList22", "JsonData: " + data.toString());

        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {
            try {
                AsyncTaskHelper asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.KEF_mcqDataHost + NetworkUrl.KEF_insertMcqData, data, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {
                        Log.d("PendingList22", "ApiResponse -> InsertUpdateMCQTestResultDetails: " + response);
                        if (!response.equals("error")) {
                            boolean result = dbhelper.updateSyncAssessmentData(TestScoreIndividualDetailID);
                            if (result) {
                                syncPendingData();
                            }
                        }
                    }
                }, "POST");

                asyncTaskHelper.execute();

            } catch (Exception e) {
                Log.d("PendingList22", "ApiResponse -> InsertUpdateMCQTestResultDetails: Error :" + e.getMessage());
                e.printStackTrace();
            }
        }

    }

    private StringBuilder FinalXmlData(String value) {
        StringBuilder str = new StringBuilder();
        str.append("<Questions>");
        str.append(value);
        str.append("</Questions>");
        return str;
    }

    private StringBuilder createXmlData(String id, String questionCode, String attemptedAns, int isAnsCorrect,
                                        String questionMarks, int QuestionMarksObtain, String dateTime) {
        String isAnswerCorrect_value = "";
        if (isAnsCorrect == 0) {
            isAnswerCorrect_value = "False";
        } else {
            isAnswerCorrect_value = "True";
        }

        StringBuilder builder = new StringBuilder();
        builder.append("<Question>");
        builder.append("<TestScoreResultDetailId>");
        builder.append(id);
        builder.append("</TestScoreResultDetailId>");
        builder.append("<QuestionCode>");
        builder.append(questionCode);
        builder.append("</QuestionCode>");
        builder.append("<AttemptedAnswer>");
        builder.append(attemptedAns);
        builder.append("</AttemptedAnswer>");
        builder.append("<IsAnswerCorrect>");
        builder.append(isAnswerCorrect_value);
        builder.append("</IsAnswerCorrect>");
        builder.append("<QuestionMarks>");
        builder.append(questionMarks);
        builder.append("</QuestionMarks>");
        builder.append("<QuestionMarksObtained>");
        builder.append(QuestionMarksObtain);
        builder.append("</QuestionMarksObtained>");
        builder.append("<QuestionAttemptedDateTime>");
        builder.append(dateTime);
        builder.append("</QuestionAttemptedDateTime>");
        builder.append("</Question>");
        return builder;
    }

}
