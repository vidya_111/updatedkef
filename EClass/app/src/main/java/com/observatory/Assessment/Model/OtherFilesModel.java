package com.observatory.Assessment.Model;

public class OtherFilesModel {

    String fileName;
    String fileDate;

    public OtherFilesModel(String fileName, String fileDate) {
        this.fileName = fileName;
        this.fileDate = fileDate;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileDate() {
        return fileDate;
    }

    public void setFileDate(String fileDate) {
        this.fileDate = fileDate;
    }
}
