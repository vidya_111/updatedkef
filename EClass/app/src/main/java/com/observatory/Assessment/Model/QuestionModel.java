package com.observatory.Assessment.Model;

public class QuestionModel {


    public String QuestionCode;
    public String QuestionText;
    public String Option1;
    public String Option2;
    public String Option3;
    public String Option4;
    public String Option5;
    public String Option6;
    public String RightAnsMarks;
    public String WrongAnsMarks;
    public String SkipAnsMarks;
    public String AnswerText;
    public String Solution;
    public String IsDeleted;
    public Integer OptionsNo;
    public Integer Duration;
    public Integer QuestionLevelId;
    public String CourseCode;
    public Integer SubjectId;
    public Integer ChapterId;

    private String YourAns;
    private Integer Skipflag;
    private Integer RightAnsflag;
    private Integer WrongAnsflag;
    private Double RightMObt;
    private Double SkipMObt;
    private String AttemptedTestDate;
    private String TestResultDetailsId;

    public String getTestResultDetailsId() {
        return TestResultDetailsId;
    }

    public void setTestResultDetailsId(String testResultDetailsId) {
        TestResultDetailsId = testResultDetailsId;
    }



    public String getAttemptedTestDate() {
        return AttemptedTestDate;
    }

    public void setAttemptedTestDate(String attemptedTestDate) {
        AttemptedTestDate = attemptedTestDate;
    }



    public Integer getSkipflag() {
        return Skipflag;
    }

    public void setSkipflag(Integer skipflag) {
        Skipflag = skipflag;
    }

    public Integer getRightAnsflag() {
        return RightAnsflag;
    }

    public void setRightAnsflag(Integer rightAnsflag) {
        RightAnsflag = rightAnsflag;
    }

    public Integer getWrongAnsflag() {
        return WrongAnsflag;
    }

    public void setWrongAnsflag(Integer wrongAnsflag) {
        WrongAnsflag = wrongAnsflag;
    }

    public Double getRightMObt() {
        return RightMObt;
    }

    public void setRightMObt(Double rightMObt) {
        RightMObt = rightMObt;
    }

    public Double getSkipMObt() {
        return SkipMObt;
    }

    public void setSkipMObt(Double skipMObt) {
        SkipMObt = skipMObt;
    }

    public Double getWrongMObt() {
        return WrongMObt;
    }

    public void setWrongMObt(Double wrongMObt) {
        WrongMObt = wrongMObt;
    }

    private Double WrongMObt;


    public String getYourAns() {
        return YourAns;
    }

    public void setYourAns(String yourAns) {
        YourAns = yourAns;
    }

    public Integer TopicId;


    public String getQuestionCode() {
        return QuestionCode;
    }

    public void setQuestionCode(String questionCode) {
        QuestionCode = questionCode;
    }

    public String getQuestionText() {
        return QuestionText;
    }

    public void setQuestionText(String questionText) {
        QuestionText = questionText;
    }

    public String getOption1() {
        return Option1;
    }

    public void setOption1(String option1) {
        Option1 = option1;
    }

    public String getOption2() {
        return Option2;
    }

    public void setOption2(String option2) {
        Option2 = option2;
    }

    public String getOption3() {
        return Option3;
    }

    public void setOption3(String option3) {
        Option3 = option3;
    }

    public String getOption4() {
        return Option4;
    }

    public void setOption4(String option4) {
        Option4 = option4;
    }

    public String getOption5() {
        return Option5;
    }

    public void setOption5(String option5) {
        Option5 = option5;
    }

    public String getOption6() {
        return Option6;
    }

    public void setOption6(String option6) {
        Option6 = option6;
    }

    public String getRightAnsMarks() {
        return RightAnsMarks;
    }

    public void setRightAnsMarks(String rightAnsMarks) {
        RightAnsMarks = rightAnsMarks;
    }

    public String getWrongAnsMarks() {
        return WrongAnsMarks;
    }

    public void setWrongAnsMarks(String wrongAnsMarks) {
        WrongAnsMarks = wrongAnsMarks;
    }

    public String getSkipAnsMarks() {
        return SkipAnsMarks;
    }

    public void setSkipAnsMarks(String skipAnsMarks) {
        SkipAnsMarks = skipAnsMarks;
    }

    public String getAnswerText() {
        return AnswerText;
    }

    public void setAnswerText(String answerText) {
        AnswerText = answerText;
    }

    public String getSolution() {
        return Solution;
    }

    public void setSolution(String solution) {
        Solution = solution;
    }

    public String getIsDeleted() {
        return IsDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        IsDeleted = isDeleted;
    }

    public Integer getOptionsNo() {
        return OptionsNo;
    }

    public void setOptionsNo(Integer optionsNo) {
        OptionsNo = optionsNo;
    }

    public Integer getDuration() {
        return Duration;
    }

    public void setDuration(Integer duration) {
        Duration = duration;
    }

    public Integer getQuestionLevelId() {
        return QuestionLevelId;
    }

    public void setQuestionLevelId(Integer questionLevelId) {
        QuestionLevelId = questionLevelId;
    }

    public String getCourseCode() {
        return CourseCode;
    }

    public void setCourseCode(String courseCode) {
        CourseCode = courseCode;
    }

    public Integer getSubjectId() {
        return SubjectId;
    }

    public void setSubjectId(Integer subjectId) {
        SubjectId = subjectId;
    }

    public Integer getChapterId() {
        return ChapterId;
    }

    public void setChapterId(Integer chapterId) {
        ChapterId = chapterId;
    }

    public Integer getTopicId() {
        return TopicId;
    }

    public void setTopicId(Integer topicId) {
        TopicId = topicId;
    }

}
