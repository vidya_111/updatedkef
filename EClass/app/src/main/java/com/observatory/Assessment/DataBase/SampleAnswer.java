package com.observatory.Assessment.DataBase;

import com.observatory.Assessment.ConstantManager;
import com.observatory.Assessment.Model.QuestionModel;

public class SampleAnswer {

    // private String strFontPath = getClass().getResource("/Fonts/").toExternalForm();
    private String strFontPath = "file:///android_assets/fonts/";
    private String strCssPath = "" ,strJsPath = "";

    public StringBuilder loadTestSolutionData(int ttlQuestion) {
        StringBuilder str = new StringBuilder();
        try {
            str.append("<!DOCTYPE html>");
            str.append("<html lang =\"en -US\">");
            str.append("<head>");
            str.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">");
            str.append("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=9; IE=8; IE=7; IE=EDGE\" />");
            str.append("<meta name = \"viewport\" content = \"width=device-width, initial-scale=1\" >");
            str.append("<link rel=\"stylesheet\" href=\"" + strCssPath + "ecl_template.css" + "\" type=\"text/css\" />");

            str.append("<style type=\"text/css\">\n" +
                    "@font-face {font-family:\"SUBAK-1\";" +
                    "src:url(\"" + strFontPath + "SUBAK0.ttf" + "\") format(\"truetype\");}" +
                    "\n" +
                    "@font-face {font-family:\"SUBAK-1\";" +
                    "src:url(\"" + strFontPath + "SUBAK1.ttf" + "\") format(\"truetype\");}" +
                    "\n" +
                    "@font-face {font-family: 'krutidev';" +
                    "src: url('" + strFontPath + "/k010.eot');" +
                    "src: local('k010'), url('" + strFontPath + "/k010.woff') format('woff'), url('" + strFontPath + "/k010.ttf') format('truetype');}" +
                    "\n" +
                    "@font-face {font-family:\"urdu\";" +
                    "src: url('" + strFontPath + "/JameelNooriNastaleeq.eot');" +
                    "src: local('JameelNooriNastaleeq'), url('" + strFontPath + "/JameelNooriNastaleeq.woff') format('woff'), url('" + strFontPath + "/JameelNooriNastaleeq.ttf') format('truetype');}" +
                    "\n" +
                    "@font-face {font-family:\"urdu\";" +
                    "src:url(\"" + strFontPath + "Jameel_Noori_Nastaleeq.ttf" + "\") format(\"truetype\");}" +
                    "\n");
            str.append("</style>\n");
            //str.append("<script src=\"" + strJsPath + "mathmlsupport_min.js" + "\"></script>\n");
            str.append("<script type=\"text/javascript\" src=\"file:///android_asset/MathJax/MathJax.js?config=MML_HTMLorMML-full" + "\" ></script>");
            str.append("</head>\n");
            str.append("<body onmouseup=\"unFocus()\" onmousemove=\"unFocus()\">");
            for (int i = 0; i < ConstantManager.questionmodel.size(); i++) {
                StringBuilder questionList = new StringBuilder();
                questionList.append("<form action=\"\">");
                questionList.append("<div class=\"article\">");
                questionList.append("<div class=\"dvQstText\">\n <div class=\"dvQstNo\"> " + (i + 1) + ".</div> <div class=\"qstText\">" + ConstantManager.questionmodel.get(i).getQuestionText().toString().replace("mathsize=\"12px\"", "mathsize=\"20px\"") + "</div></div>\n");
                String optionList = "";
                optionList = getTestSolutionSingleChoice(ConstantManager.questionmodel.get(i), i, ttlQuestion);
                if (!IsNullOrEmpty(questionList.toString())) {
                    questionList.append(optionList.toString());
                }
                String yourAns = "", rightAns = "";
                if (ConstantManager.questionmodel.get(i).getYourAns() != null && ConstantManager.questionmodel.get(i).getYourAns().equals("1")) {
                    yourAns = "A";
                } else if (ConstantManager.questionmodel.get(i).getYourAns() != null && ConstantManager.questionmodel.get(i).getYourAns().equals("2")) {
                    yourAns = "B";
                } else if (ConstantManager.questionmodel.get(i).getYourAns() != null && ConstantManager.questionmodel.get(i).getYourAns().equals("3")) {
                    yourAns = "C";
                } else if (ConstantManager.questionmodel.get(i).getYourAns() != null && ConstantManager.questionmodel.get(i).getYourAns().equals("4")) {
                    yourAns = "D";
                } else if (ConstantManager.questionmodel.get(i).getYourAns() != null && ConstantManager.questionmodel.get(i).getYourAns().equals("5")) {
                    yourAns = "E";
                } else if (ConstantManager.questionmodel.get(i).getYourAns() != null && ConstantManager.questionmodel.get(i).getYourAns().equals("6")) {
                    yourAns = "F";
                }

                if (Float.parseFloat(ConstantManager.questionmodel.get(i).getAnswerText()) == 1)
                    rightAns = "A";
                else if (Float.parseFloat(ConstantManager.questionmodel.get(i).getAnswerText()) == 2)
                    rightAns = "B";
                else if (Float.parseFloat(ConstantManager.questionmodel.get(i).getAnswerText()) == 3)
                    rightAns = "C";
                else if (Float.parseFloat(ConstantManager.questionmodel.get(i).getAnswerText()) == 4)
                    rightAns = "D";
                else if (Float.parseFloat(ConstantManager.questionmodel.get(i).getAnswerText()) == 5)
                    rightAns = "E";
                else if (Float.parseFloat(ConstantManager.questionmodel.get(i).getAnswerText()) == 6)
                    rightAns = "F";

                questionList.append("<div class=\"dvSolFooter\">");
                questionList.append("<h3 class=\"dvYourAns\"><b>Your Answer : </b> " + (IsNullOrEmpty(yourAns) ? "-" : yourAns) + "</h3>");
                questionList.append("<h3 class=\"dvRightAns\"><b> Correct Answer : </b> " + rightAns + "</h3>");
                questionList.append("</div>");
                questionList.append("</div>");
                questionList.append("</form >");
                if (!IsNullOrEmpty(questionList.toString())) {
                    str.append(questionList.toString());
                }

            }
            str.append("<script type=\"text/javascript\">\n");
            str.append("var imgs = document.getElementsByTagName(\"img\");\n");
            str.append("for (var i = 0; i < imgs.length; i++)\n");
            str.append("{\n");
            str.append("    if ((imgs[i].naturalWidth+62) > window.outerWidth)\n");
            str.append("    {\n");
            str.append("        imgs[i].style.width = \"100%\";\n");
            str.append("        imgs[i].style.height = \"auto\";\n");
            str.append("    }\n");
            str.append("}\n");
            str.append("</script>\n");
            str.append("</body>");
            str.append("</html>");
        } catch (Exception ex) {

        }
        return str;
    }

    public String getTestSolutionSingleChoice(QuestionModel answerModel, int i, int ttlQuestion) {
        StringBuilder str = new StringBuilder();
        try {
            String strIconLabel = "<label class=\"strLabelTransparent\">{0}</label>";
            if (!IsNullOrEmpty(ConstantManager.questionmodel.get(i).getOption1())) {
                String strOption1 = "<div class=\"dvLabelTransparent\">" +strIconLabel.replace("{0}","A.") +
                        "<input type=\"radio\" id=\"r1\" class=\"css-checkbox rb\" name=\"radioName\" value=\"1\" disabled=\"disabled\">";
                String strRadioLabel = "<label for=\"r1\" class=\"css-label cb0 optionText\">" + "  " + answerModel.getOption1().replace("&nbsp;", " ").replace("mathsize=\"12px\"", "mathsize=\"25px\"")  + "</label></div>";
                if (ConstantManager.questionmodel.size() > i && !IsNullOrEmpty(ConstantManager.questionmodel.get(i).getYourAns()) && ConstantManager.questionmodel.get(i).getYourAns().equals("1")) {
                    strOption1 = "<div class=\"dvLabelTransparent\">" + strIconLabel.replace("{0}","A.") +
                            "<input type=\"radio\" id=\"r1\" class=\"css-checkbox rb\" name=\"radioName\" value=\"1\" disabled=\"disabled\" checked=\"checked\">";
                }
                if (Float.parseFloat(answerModel.getAnswerText()) == 1) {
                    strOption1 = "<div class=\"dvLabelTransparent\">" + strIconLabel.replace("{0}","A.") +
                            "<input type=\"radio\" id=\"r1\" class=\"css-checkbox rb hide\" name=\"radioName\" value=\"1\" disabled=\"disabled\">";
                    strRadioLabel = "<label for=\"r1\" class=\"css-label cb0 selectrdo optionText\">" + "  " + answerModel.getOption1().replace("&nbsp;", " ").replace("mathsize=\"12px\"", "mathsize=\"25px\"") + "</label></div>";
                }
                str.append(strOption1);
                str.append(strRadioLabel);
                str.append("<br><br>");
            }
            if (!IsNullOrEmpty(ConstantManager.questionmodel.get(i).getOption2())) {
                String strOption2 = "<div class=\"dvLabelTransparent\">" + strIconLabel.replace("{0}","B.") +
                        "<input type=\"radio\" id=\"r2\" class=\"css-checkbox rb\" name=\"radioName\" value=\"2\" disabled=\"disabled\">";
                String strRadioLabel = "<label for=\"r2\" class=\"css-label cb0 optionText\">" + "  " + answerModel.getOption2().replace("&nbsp;", " ").replace("mathsize=\"12px\"", "mathsize=\"25px\"")+ "</label></div>";
                if (ConstantManager.questionmodel.size() > i && !IsNullOrEmpty(ConstantManager.questionmodel.get(i).getYourAns()) && ConstantManager.questionmodel.get(i).getYourAns().equals("2")) {
                    strOption2 = "<div class=\"dvLabelTransparent\">" + strIconLabel.replace("{0}","B.") +
                            "<input type=\"radio\" id=\"r2\" class=\"css-checkbox rb\" name=\"radioName\" value=\"2\" disabled=\"disabled\" checked=\"checked\">";
                }
                if (Float.parseFloat(answerModel.getAnswerText()) == 2) {
                    strOption2 = "<div class=\"dvLabelTransparent\">" + strIconLabel.replace("{0}","B.") +
                            "<input type=\"radio\" id=\"r2\" class=\"css-checkbox rb hide\" name=\"radioName\" value=\"2\" disabled=\"disabled\">";
                    strRadioLabel = "<label for=\"r2\" class=\"css-label cb0 selectrdo optionText\">" + "  " + answerModel.getOption2().replace("&nbsp;", " ").replace("mathsize=\"12px\"", "mathsize=\"25px\"") + "</label></div>";
                }
                str.append(strOption2);
                str.append(strRadioLabel);
                str.append("<br><br>");
            }
            if (!IsNullOrEmpty(ConstantManager.questionmodel.get(i).getOption3())) {
                String strOption3 = "<div class=\"dvLabelTransparent\">" + strIconLabel.replace("{0}","C.") +
                        "<input type=\"radio\" id=\"r3\" class=\"css-checkbox rb\" name=\"radioName\" value=\"3\" disabled=\"disabled\">";
                String strRadioLabel = "<label for=\"r3\" class=\"css-label cb0 optionText\">" + "  " + answerModel.getOption3().replace("&nbsp;", " ").replace("mathsize=\"12px\"", "mathsize=\"25px\"") + "</label></div>";
                if (ConstantManager.questionmodel.size() > i && !IsNullOrEmpty(ConstantManager.questionmodel.get(i).getYourAns()) && ConstantManager.questionmodel.get(i).getYourAns().equals("3")) {
                    strOption3 = "<div class=\"dvLabelTransparent\">" + strIconLabel.replace("{0}","C.") +
                            "<input type=\"radio\" id=\"r3\" class=\"css-checkbox rb\" name=\"radioName\" value=\"3\" disabled=\"disabled\" checked=\"checked\">";
                }
                if (Float.parseFloat(answerModel.getAnswerText()) == 3) {
                    strOption3 = "<div class=\"dvLabelTransparent\">" + strIconLabel.replace("{0}","C.") +
                            "<input type=\"radio\" id=\"r3\" class=\"css-checkbox rb hide\" name=\"radioName\" value=\"3\" disabled=\"disabled\">";
                    strRadioLabel = "<label for=\"r3\" class=\"css-label cb0 selectrdo optionText\">" + "  " + answerModel.getOption3().replace("&nbsp;", " ").replace("mathsize=\"12px\"", "mathsize=\"25px\"")+ "</label></div>";
                }
                str.append(strOption3);
                str.append(strRadioLabel);
                str.append("<br><br>");
            }
            if (!IsNullOrEmpty(ConstantManager.questionmodel.get(i).getOption4())) {
                String strOption4 = "<div class=\"dvLabelTransparent\">" + strIconLabel.replace("{0}","D.") +
                        "<input type=\"radio\" id=\"r4\" class=\"css-checkbox rb\" name=\"radioName\" value=\"4\" disabled=\"disabled\">";
                String strRadioLabel = "<label for=\"r4\" class=\"css-label cb0 optionText\">" + "  " + answerModel.getOption4().replace("&nbsp;", " ").replace("mathsize=\"12px\"", "mathsize=\"25px\"")+ "</label></div>";
                if (ConstantManager.questionmodel.size() > i && !IsNullOrEmpty(ConstantManager.questionmodel.get(i).getYourAns()) && ConstantManager.questionmodel.get(i).getYourAns().equals("4")) {
                    strOption4 = "<div class=\"dvLabelTransparent\">" + strIconLabel.replace("{0}","D.") +
                            "<input type=\"radio\" id=\"r4\" class=\"css-checkbox rb\" name=\"radioName\" value=\"4\" disabled=\"disabled\" checked=\"checked\">";
                }
                if (Float.parseFloat(answerModel.getAnswerText()) == 4) {
                    strOption4 = "<div class=\"dvLabelTransparent\">" + strIconLabel.replace("{0}","D.") +
                            "<input type=\"radio\" id=\"r4\" class=\"css-checkbox rb hide\" name=\"radioName\" value=\"4\" disabled=\"disabled\">";
                    strRadioLabel = "<label for=\"r4\" class=\"css-label cb0 selectrdo optionText\">" + "  " + answerModel.getOption4().replace("&nbsp;", " ").replace("mathsize=\"12px\"", "mathsize=\"25px\"")+ "</label></div>";
                }
                str.append(strOption4);
                str.append(strRadioLabel);
                str.append("<br><br>");
            }
            if (!IsNullOrEmpty(ConstantManager.questionmodel.get(i).getOption5())) {
                String strOption5 = "<div class=\"dvLabelTransparent\">" + strIconLabel.replace("{0}","E.") +
                        "<input type=\"radio\" id=\"r5\" class=\"css-checkbox rb\" name=\"radioName\" value=\"5\" disabled=\"disabled\">";
                String strRadioLabel = "<label for=\"r5\" class=\"css-label cb0 optionText\">" + "  " + answerModel.getOption5().replace("&nbsp;", " ").replace("mathsize=\"12px\"", "mathsize=\"25px\"") + "</label></div>";
                if (ConstantManager.questionmodel.size() > i && !IsNullOrEmpty(ConstantManager.questionmodel.get(i).getYourAns()) && ConstantManager.questionmodel.get(i).getYourAns().equals("5")) {
                    strOption5 = "<div class=\"dvLabelTransparent\">" + strIconLabel.replace("{0}","E.") +
                            "<input type=\"radio\" id=\"r5\" class=\"css-checkbox rb\" name=\"radioName\" value=\"5\" disabled=\"disabled\" checked=\"checked\">";
                }
                if (Float.parseFloat(answerModel.getAnswerText()) == 5) {
                    strOption5 = "<div class=\"dvLabelTransparent\">" + strIconLabel.replace("{0}","E.") +
                            "<input type=\"radio\" id=\"r5\" class=\"css-checkbox rb hide\" name=\"radioName\" value=\"5\" disabled=\"disabled\">";
                    strRadioLabel = "<label for=\"r5\" class=\"css-label cb0 selectrdo optionText\">" + "  " + answerModel.getOption5().replace("&nbsp;", " ").replace("mathsize=\"12px\"", "mathsize=\"25px\"") + "</label></div>";
                }
                str.append(strOption5);
                str.append(strRadioLabel);
                str.append("<br><br>");
            }
            if (!IsNullOrEmpty(ConstantManager.questionmodel.get(i).getOption6())) {
                String strOption6 = "<div class=\"dvLabelTransparent\">" + strIconLabel.replace("{0}","F.") +
                        "<input type=\"radio\" id=\"r6\" class=\"css-checkbox rb\" name=\"radioName\" value=\"6\" disabled=\"disabled\">";
                String strRadioLabel = "<label for=\"r6\" class=\"css-label cb0 optionText\">" + "  " + answerModel.getOption6().replace("&nbsp;", " ").replace("mathsize=\"12px\"", "mathsize=\"25px\"") + "</label></div>";
                if (ConstantManager.questionmodel.size() > i && !IsNullOrEmpty(ConstantManager.questionmodel.get(i).getYourAns()) && ConstantManager.questionmodel.get(i).getYourAns().equals("6")) {
                    strOption6 = "<div class=\"dvLabelTransparent\">" + strIconLabel.replace("{0}","F.") +
                            "<input type=\"radio\" id=\"r6\" class=\"css-checkbox rb\" name=\"radioName\" value=\"6\" disabled=\"disabled\" checked=\"checked\">";
                }
                if (Float.parseFloat(answerModel.getAnswerText()) == 6) {
                    strOption6 = "<div class=\"dvLabelTransparent\">" + strIconLabel.replace("{0}","F.") +
                            "<input type=\"radio\" id=\"r6\" class=\"css-checkbox rb hide\" name=\"radioName\" value=\"6\" disabled=\"disabled\">";
                    strRadioLabel = "<label for=\"r6\" class=\"css-label cb0 selectrdo optionText\">" + "  " + answerModel.getOption6().replace("&nbsp;", " ").replace("mathsize=\"12px\"", "mathsize=\"25px\"") + "</label></div>";
                }
                str.append(strOption6);
                str.append(strRadioLabel);
                str.append("<br><br>");
            }
        } catch (Exception ex) {
        }
        return str.toString();

    }

    public static boolean IsNullOrEmpty(String s) {

        if (s == null || s.isEmpty()) {
            return true;
        }
        return false;
    }
}
