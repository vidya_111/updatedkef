package com.observatory.Assessment.PracticeTest;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.observatory.Assessment.ConstantManager;
import com.observatory.Assessment.Model.PDFModel;
import com.observatory.kefcorner.BuildConfig;
import com.observatory.kefcorner.R;
import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.EmailHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import static com.observatory.Assessment.A_ChapterActivity.SUBJECT_ID;

public class PracticepdfActivity extends BaseActivity {

    int subjectid = 0;
    String MediumName = "";
    File Directory;
    RecyclerView rv_pdfdata;
    PDFAdapter mPDFAdapter;
    ArrayList<PDFModel> arr_pdfquestlist, arr_pdfsolutionlist, arr_listing;
    String Path = "";
    String separator, superDataDirPath, superDBDirPath, fileName, DestPath;
    TextView tv_nodata;
    boolean isquestion = false;
    boolean issolution = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practicepdf);
        actionBar.setTitle("Select Your Option");
        subjectid = Integer.parseInt(getIntent().getStringExtra(SUBJECT_ID));
        MediumName = ConstantManager.STANDARD_FOLDERNAME;
        tv_nodata = (TextView) findViewById(R.id.tv_nodata);
        arr_pdfquestlist = new ArrayList<PDFModel>();
        arr_pdfsolutionlist = new ArrayList<PDFModel>();
        arr_listing = new ArrayList<PDFModel>();
        arr_listing = getDirectoryInfo();
        if (arr_listing.size() > 0) {
            rv_pdfdata.setLayoutManager(new LinearLayoutManager(PracticepdfActivity.this, LinearLayoutManager.VERTICAL, false));
            mPDFAdapter = new PDFAdapter(this, arr_listing);
            rv_pdfdata.setAdapter(mPDFAdapter);
        } else {
            rv_pdfdata.setVisibility(View.INVISIBLE);
            tv_nodata.setVisibility(View.VISIBLE);
        }

    }

    public ArrayList<PDFModel> getDirectoryInfo() {
        try {
            if (!App.BASE_PATH.contains(MediumName)) {
                if (!ConstantManager.ISSINGLESTANDARD) {
                    Path = App.BASE_PATH + MediumName + "/" + "Assess" + "/" + subjectid + "/" + ConstantManager.ASSESSMENT_TYPE + "/";
                } else {
                    Path = App.BASE_PATH + "Assess" + "/" + subjectid + "/" + ConstantManager.ASSESSMENT_TYPE + "/";
                }
            } else {
                Path = App.BASE_PATH + "Assess" + "/" + subjectid + "/" + ConstantManager.ASSESSMENT_TYPE + "/";
            }
            Directory = new File(Path + "Q" + "/");
            if (Directory.exists()) {
                File[] files = Directory.listFiles();
                if (files != null) {
                    for (int i = 0; i < files.length; i++) {
                        PDFModel pdfmodel = new PDFModel();
                        pdfmodel.setFileid(i + 1);
                        pdfmodel.setFilename(files[i].getName());
                        pdfmodel.setFiledisplayname(files[i].getName().substring(0, files[i].getName().lastIndexOf(".")));
                        pdfmodel.setFilepath(Path);
                        pdfmodel.setFileextension(files[i].getName().substring(files[i].getName().lastIndexOf(".")));
                        arr_pdfquestlist.add(pdfmodel);
                    }
                }
            } else {
                isquestion = true;
            }

            Directory = new File(Path + "S" + "/");
            if (Directory.exists()) {
                File[] files1 = Directory.listFiles();
                if (files1 != null) {
                    int size = arr_pdfquestlist.size();
                    if (size > 0)
                        for (int i = 0; i < files1.length; i++) {
                            boolean check = false;
                            String name = files1[i].getName();
                            for (int j = 0; j < size; j++) {
                                if (name.equals(arr_pdfquestlist.get(j).getFilename())) {
                                    check = true;
                                    break;

                                }
                            }
                            if (check == false) {
                                PDFModel pdfmodel = new PDFModel();
                                pdfmodel.setFileid(i + 1);
                                pdfmodel.setFilename(files1[i].getName());
                                pdfmodel.setFiledisplayname(files1[i].getName().substring(0, files1[i].getName().lastIndexOf(".")));
                                pdfmodel.setFilepath(Path);
                                pdfmodel.setFileextension(files1[i].getName().substring(files1[i].getName().lastIndexOf(".")));
                                arr_pdfquestlist.add(pdfmodel);
                            }
                        }
                }
            } else {
                issolution = true;
            }
        }
        catch (Exception ex)
        {
            new EmailHandler().sendMail("phone@millicent.in", "Eclass Error Logs", ex.getMessage());
        }
        return arr_pdfquestlist;
    }

    @Override
    protected void onLayout() {
        rv_pdfdata = (RecyclerView) findViewById(R.id.rv_pdfdata);

    }

    public class PDFAdapter extends RecyclerView.Adapter<PDFAdapter.PDFAdapterViewHolder> {

        Context context;
        ArrayList<PDFModel> mPDFListData;

        public PDFAdapter(Context mcontext, ArrayList<PDFModel> arr_listing) {
            this.context = mcontext;
            this.mPDFListData = arr_listing;
        }

        @Override
        public PDFAdapterViewHolder onCreateViewHolder(ViewGroup parent, int i) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_pdf_item, parent, false);
            PDFAdapterViewHolder myViewHolder = new PDFAdapterViewHolder(view);
            return myViewHolder;
        }

        @Override
        public void onBindViewHolder(PDFAdapterViewHolder holder, int i) {
            holder.tv_name.setText(mPDFListData.get(i).getFiledisplayname());
        }

        @Override
        public int getItemCount() {
            return mPDFListData.size();
        }

        public class PDFAdapterViewHolder extends RecyclerView.ViewHolder {
            TextView tv_name;
            Button btn_solution, btn_question;

            public PDFAdapterViewHolder(@NonNull View itemView) {
                super(itemView);
                tv_name = (TextView) itemView.findViewById(R.id.tv_name);
                btn_question = (Button) itemView.findViewById(R.id.btn_question);
                btn_solution = (Button) itemView.findViewById(R.id.btn_solution);
                if (isquestion) {
                    btn_question.setVisibility(View.GONE);
                } else if (issolution) {
                    btn_solution.setVisibility(View.GONE);
                }

                btn_question.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String pdfPath = mPDFListData.get(getAdapterPosition()).getFilepath() + "Q" + "/" + mPDFListData.get(getAdapterPosition()).getFilename();
                        //new EmailHandler().sendMail("phone@millicent.in", "PDF Path", pdfPath);

                       /* fileName = mPDFListData.get(getAdapterPosition()).getFilename();
                        DecryptFile(pdfPath,fileName);*/
                        openPdf(context, pdfPath, fileName);
                    }
                });
                btn_solution.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String pdfPath = mPDFListData.get(getAdapterPosition()).getFilepath() + "S" + "/" + mPDFListData.get(getAdapterPosition()).getFilename();
                        /*fileName = mPDFListData.get(getAdapterPosition()).getFilename();
                        DecryptFile(pdfPath,fileName);*/
                        openPdf(context, pdfPath, fileName);
                    }
                });

            }
        }

        private void DecryptFile(String pdfPath, String filename) {
            separator = "/";
            superDataDirPath = Environment.getExternalStorageDirectory().getAbsolutePath();
            //superDataDirPath = "/data/data/" + getPackageName();
            superDBDirPath = superDataDirPath + separator + ".EclassPdf";
            fileName = filename;
            File dir = new File(superDBDirPath + separator);
            if (!dir.exists()) {
                dir.mkdir();
            }
            if (dir != null) {
                File[] filenames = dir.listFiles();
                if (filenames != null) {
                    for (File tmpf : filenames) {
                        tmpf.delete();
                    }
                }
            }
            DestPath = superDBDirPath + separator + fileName;
            File src = new File(pdfPath);
            File dest = new File(DestPath);
            try {
                copyFileUsingStream(src, dest);
            } catch (IOException e) {
                e.printStackTrace();
            }
            openPdf(context, pdfPath, fileName);
        }
    }

    private void openPdf(Context context, String filePath, String name) {
        try {
            File file = new File(filePath);
            Uri fileUri=null;
            //srj
            if (file.exists()) {
                //new EmailHandler().sendMail("phone@millicent.in", "PDF file exist condition : ", "true");

                Intent target = new Intent(Intent.ACTION_VIEW);
                if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT){
                     fileUri = FileProvider.getUriForFile(PracticepdfActivity.this, BuildConfig.APPLICATION_ID + ".provider", file);
                } else{
                     fileUri = Uri.fromFile(file.getAbsoluteFile());
                }
                target.setDataAndType(fileUri, "application/pdf");
                target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                target.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                //new EmailHandler().sendMail("phone@millicent.in", "Pdf Before createChooser :  ", fileUri.getEncodedPath());
                Intent intent = Intent.createChooser(target, "Open File");
                try {
                    //new EmailHandler().sendMail("phone@millicent.in", "Pdf Before Intent :  ", "true");
                    startActivity(intent);
                    //new EmailHandler().sendMail("phone@millicent.in", "Pdf After Intent :", "true");
                } catch (ActivityNotFoundException e) {
                   // new EmailHandler().sendMail("phone@millicent.in", "Pdf Catch Exception", e.getMessage());
                    Toast.makeText(getApplicationContext(), "Please install a PDF Viewer to view this " + "document", Toast.LENGTH_LONG).show();
                }
            } else {
                //new EmailHandler().sendMail("phone@millicent.in", "File not available  ", "true");
                Toast.makeText(PracticepdfActivity.this, "File not available", Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception ex)
        {
            new EmailHandler().sendMail("phone@millicent.in", "OpenPdf Method Exception", ex.getMessage());

        }
    }

    private void CopyFile(String src, String dest, String filename) {
        try {
            boolean transfer = false;
            File dbFile = new File(src);
            InputStream mInput = new FileInputStream(dbFile);
            String outFileName = dest + "/" + filename;
            OutputStream mOutput = new FileOutputStream(outFileName);
            byte[] mBuffer = new byte[1024];
            int mLength;
            while ((mLength = mInput.read(mBuffer)) > 0) {
                mOutput.write(mBuffer, 0, mLength);
            }
            mOutput.flush();
            mOutput.close();
            mInput.close();
        } catch (Exception e) {
            Log.e("Exception", "Copy File exc " + e.getMessage());
        }
    }

  /*  @Override
    protected void onResume() {
        File dir = new File(superDBDirPath + separator);
        if (dir != null) {
            File[] filenames = dir.listFiles();
            if (filenames != null) {
                for (File tmpf : filenames) {
                    tmpf.delete();
                }
            }
        }
        super.onResume();
    }*/

    private static void copyFileUsingStream(File source, File dest) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }

}
