package com.observatory.Assessment.Model;

public class LevelModel {
    public Integer LevelId;
    public String LevelName;
    public String LevelInfo;

    public LevelModel(Integer levelId, String levelName, String levelInfo) {
        LevelId = levelId;
        LevelName = levelName;
        LevelInfo = levelInfo;
    }

    public Integer getLevelId() {
        return LevelId;
    }

    public void setLevelId(Integer levelId) {
        LevelId = levelId;
    }

    public String getLevelName() {
        return LevelName;
    }

    public void setLevelName(String levelName) {
        LevelName = levelName;
    }

    public String getLevelInfo() {
        return LevelInfo;
    }

    public void setLevelInfo(String levelInfo) {
        LevelInfo = levelInfo;
    }
}