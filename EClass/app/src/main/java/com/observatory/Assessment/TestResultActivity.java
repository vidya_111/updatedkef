package com.observatory.Assessment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.observatory.Assessment.DataBase.SampleAnswer;
import com.observatory.kefcorner.R;
import com.observatory.utils.App;
import com.observatory.utils.SettingPreffrences;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Scanner;

public class TestResultActivity extends AppCompatActivity {

    TextView tv_your_score,tv_start_new;
    String separator,superDataDirPath,superDBDirPath,fileName,CssFileString;
    private static final String UTF_8_ENCODING_TYPE = "UTF-8";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testresult);
        //actionBar.setTitle("Test Report");
        ((AppCompatActivity)this).getSupportActionBar().setTitle("Test Report");
        WebView htmlWebView = (WebView)findViewById(R.id.webView);
        tv_your_score=(TextView) findViewById(R.id.tv_your_score);
        tv_start_new=(TextView) findViewById(R.id.tv_start_new);
        String s = new SampleAnswer().loadTestSolutionData(ConstantManager.questionmodel.size()).toString();
        separator = "/";
        superDataDirPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        superDataDirPath = "/data/data/" + getPackageName();
        superDBDirPath = superDataDirPath + separator + "assessment";
        fileName="mcqresult.html";
        CssFileString=getCssAsString();
        File dir = new File(superDBDirPath);
        if (!dir.exists()) {
            dir.mkdir();
        }
        File file = new File(superDBDirPath, fileName);
        File cssfiles = new File(superDBDirPath, "ecl_template.css");
        filecopy(file,s);
        if(!cssfiles.exists())
        {
            filecopy(cssfiles,CssFileString);
        }
        WebSettings webSetting = htmlWebView.getSettings();
        webSetting.setJavaScriptEnabled(true);
        webSetting.setDisplayZoomControls(true);
        webSetting.setJavaScriptEnabled(true);
        webSetting.setAllowFileAccessFromFileURLs(true);
        webSetting.setAllowFileAccess(true);
        htmlWebView.loadUrl("file://"+file.getAbsolutePath());
        tv_your_score.setText("YOUR SCORE : "+ConstantManager.SCORE+"%");

        tv_start_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearConstants();
                startActivity(new Intent(TestResultActivity.this,AssessmentSubjects.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                App.getInstance().trackEvent("AssessmentMCQResult", "Screen", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " + "selectedStandard - " + SettingPreffrences.getStandard(TestResultActivity.this) + "" + " ; " + "selectedSubject - " + SettingPreffrences.getSubject(TestResultActivity
                        .this), "");
                finish();
            }
        });
    }
/*
    @Override
    protected void onLayout() {



    }*/
private void filecopy(File file ,String s) {
    try {
        FileOutputStream out = new FileOutputStream(file);
        byte[] data = s.getBytes();
        out.write(data);
        out.close();
    } catch (FileNotFoundException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
}

    public String getCssAsString()
    {
        String css = "";
        Scanner cssScanner = null;
        String TOKEN_BOUNDARY = "\\A";
        try {

            InputStream cssInputStream = this.getAssets().open("ecl_template.css");
            cssScanner = new Scanner(
                    cssInputStream,
                    UTF_8_ENCODING_TYPE
            ).useDelimiter(TOKEN_BOUNDARY);

            if (cssScanner.hasNext()) {
                css = cssScanner.next();
            }

        } catch (IOException e) {
            Log.e("Tag",e.toString());
        } finally {
            if (cssScanner != null) {
                cssScanner.close();
            }
        }

        return css;
    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("");
        builder.setMessage("Are you sure, you want to exit?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ConstantManager.IS_ALL_CHECKED = "NO";
                clearConstants();
                Intent i = new Intent(TestResultActivity.this, AssessmentSubjects.class);
                App.getInstance().trackEvent("AssessmentMCQResult", "Screen", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " + "selectedStandard - " + SettingPreffrences.getStandard(TestResultActivity.this) + "" + " ; " + "selectedSubject - " + SettingPreffrences.getSubject(TestResultActivity
                        .this), "");
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();

            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        builder.show();
    }

    public void clearConstants()
    {
        if(ConstantManager.Selected_level!=null)
        {
            ConstantManager.Selected_level.clear();
        }
        if(ConstantManager.Selected_Chapters_Id!=null)
        {
            ConstantManager.Selected_Chapters_Id.clear();
        }
        if(ConstantManager.Selected_Topic_Id!=null)
        {
            ConstantManager.Selected_Topic_Id.clear();
        }
        if(ConstantManager.questionmodel!=null)
        {
            ConstantManager.questionmodel.clear();
        }
        if(ConstantManager.childItems!=null)
        {
            ConstantManager.childItems.clear();
        }
        if(ConstantManager.parentItems!=null)
        {
            ConstantManager.parentItems.clear();
        }
        ConstantManager.IS_ALL_CHECKED = "NO";
        for(int i=0;i<ConstantManager.arrPreviousActivity.size();i++)
        {
            ConstantManager.arrPreviousActivity.get(i).finish();
        }
    }

    public static String StreamToString(InputStream in) throws IOException {
        if(in == null) {
            return "";
        }
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } finally {
        }
        return writer.toString();
    }


}
