package com.observatory.Assessment.Reports;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.observatory.kefcorner.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ResultAdapterViewHolder> {

    Context context;
    ArrayList<ResultModel> list;

    public ResultAdapter(ReportsActivity javaExampleActivity, ArrayList<ResultModel> singerList) {
        this.context=javaExampleActivity;
        this.list=singerList;
    }

    @Override
    public ResultAdapter.ResultAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_row, parent, false);
        ResultAdapterViewHolder myViewHolder = new ResultAdapterViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(ResultAdapter.ResultAdapterViewHolder holder, int position) {
        String input = list.get(position).getLastAccessTime();
        String date="";
        try {
            date=StringToDate(input);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat outputFormat = new SimpleDateFormat("KK:mm a");
        try {
            holder.tv_content_type.setText(date+" "+outputFormat.format(inputFormat.parse(input)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String score=list.get(position).getTotalRightAnswerCount()+"/"+list.get(position).getTestTotalQuestion();
        holder.tv_score.setText(score);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ResultAdapterViewHolder extends RecyclerView.ViewHolder {
        TextView tv_content_type,tv_score;
        public ResultAdapterViewHolder(View itemView) {
            super(itemView);
            tv_content_type=(TextView) itemView.findViewById(R.id.tv_content_type);
            tv_score=(TextView) itemView.findViewById(R.id.tv_score);
        }
    }

    public String StringToDate(String date) throws ParseException {

        String start_dt = date;
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date1 = (Date)formatter.parse(start_dt);
        SimpleDateFormat newFormat = new SimpleDateFormat("dd MMMM, yyyy");
        String finalString = newFormat.format(date1);
        return finalString;
    }
}
