package com.observatory.Assessment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.observatory.Assessment.DataBase.SQLiteDB;
import com.observatory.Assessment.Model.LevelModel;
import com.observatory.Assessment.Model.QuestionModel;
import com.observatory.kefcorner.R;
import com.observatory.utils.BaseActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class LevelActivity extends BaseActivity {

    Button btn_starttest;
    ListView listView;
    ListCheckBoxAdapter adapter;
    SQLiteDB dbhelper;
    ArrayList<QuestionModel> arrQuestionData;
    ArrayList<Integer> arrRandomList;
    HashMap<Integer, QuestionModel> arrQuestionFinal;

    Activity LevelActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);

        actionBar.setTitle("Select Difficulty Level");

        listView = (ListView) findViewById(R.id.listView);
        List<LevelModel> listitem = new ArrayList<LevelModel>();
        listitem.add(new LevelModel(1, "Level I ", " (Easy Level Questions)"));
        listitem.add(new LevelModel(2, "Level II ", " (Moderate Level Questions)"));
        listitem.add(new LevelModel(3, "Level III ", " (Difficult Level Questions)"));
        adapter = new ListCheckBoxAdapter(getApplicationContext(), listitem);
        listView.setAdapter(adapter);

    }

    @Override
    protected void onLayout() {
        btn_starttest = (Button) findViewById(R.id.btn_starttest);
        btn_starttest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConstantManager.Selected_level.size() > 0) {
                    String ChapterIds = ListToString(ConstantManager.Selected_Chapters_Id);
                    String TopicIds = ListToString(ConstantManager.Selected_Topic_Id);
                    String LevelIds = ListToString(ConstantManager.Selected_level);
                    dbhelper = SQLiteDB.getInstance(LevelActivity.this, ConstantManager.DB_PATH);
                    arrQuestionData = dbhelper.getQuestionByIds(ChapterIds, TopicIds, LevelIds, ConstantManager.SUBJECT_ID);
                    if(arrQuestionData.size()>0 && arrQuestionData !=null) {
                        int arrQuest_max = arrQuestionData.size();
                        if (arrQuest_max >= 20) {
                            arrRandomList = Random(0, arrQuest_max, 20);
                        } else {
                            arrRandomList = Random(0, arrQuest_max, arrQuest_max);
                        }
                        ConstantManager.questionmodel = new HashMap<Integer, QuestionModel>();
                        for (int i = 0; i < arrRandomList.size(); i++) {
                            ConstantManager.questionmodel.put(i, arrQuestionData.get(arrRandomList.get(i)));
                            //arrQuestionFinal.add(arrQuestionData.get(arrRandomList.get(i)));
                        }
                        startActivity(new Intent(LevelActivity.this, AMCQActivity.class));
                        finish();
                    }
                    else
                    {
                        Toast.makeText(LevelActivity.this, "No data available", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(LevelActivity.this, "Please select level", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private ArrayList<Integer> Random(int min, int max, int getdata) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        ArrayList<Integer> newlist = new ArrayList<Integer>();
        for (int i = min; i < max; i++) {
            list.add(new Integer(i));
        }
        Collections.shuffle(list);
        if(list.size()>0) {
            for (int i = 0; i < getdata; i++) {
                newlist.add(list.get(i));

            }
        }
        return newlist;
    }

    public static String ListToString(ArrayList<String> array) {
        String result = "";

        if (array.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (String s : array) {
                sb.append(s).append(",");
            }
            result = sb.deleteCharAt(sb.length() - 1).toString();
        }
        return result;
    }


    public class ListCheckBoxAdapter extends BaseAdapter {

        public List<LevelModel> mleveldata;
        Context context;
        LayoutInflater layoutInflater;


        public ListCheckBoxAdapter(Context applicationContext, List<LevelModel> listitem) {
            this.context = applicationContext;
            this.mleveldata = listitem;
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mleveldata.size();
        }

        @Override
        public Object getItem(int position) {
            return mleveldata.get(position);
        }

        @Override
        public long getItemId(int position) {
            return Long.parseLong(String.valueOf(mleveldata.get(position).getLevelId()));
        }

        private class ViewHolder {
            CheckBox cb_level;
            TextView tv_name, tv_info;

        }

        ViewHolder viewHolder = null;

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = layoutInflater.inflate(R.layout.custom_level, null);
                viewHolder.tv_name = (TextView) convertView.findViewById(R.id.level);
                viewHolder.tv_info = (TextView) convertView.findViewById(R.id.level_info);
                viewHolder.cb_level = (CheckBox) convertView.findViewById(R.id.cb_level);
            }
            viewHolder.tv_name.setText(mleveldata.get(position).getLevelName());
            viewHolder.tv_info.setText(mleveldata.get(position).getLevelInfo());


            /*viewHolder.cb_level.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Boolean isChecked = false;
                    String levelid = String.valueOf(mleveldata.get(position).getLevelId());
                    Log.d("CBChecker","levelid: "+levelid);
                    if (ConstantManager.Selected_level.contains(levelid)) {
                        Log.d("CBChecker","levelid: true: "+levelid);
                        isChecked = true;
                    }
                    if (isChecked) {
                        Log.d("CBChecker","isChecked: selectedLevel: "+ConstantManager.Selected_level+
                                " remove: "+ConstantManager.Selected_level.indexOf(levelid));
                        ConstantManager.Selected_level.remove(ConstantManager.Selected_level.indexOf(levelid));
                        viewHolder.cb_level.setChecked(false);
                        isChecked = false;
                    } else {
                        Log.d("CBChecker","isNotChecked: selectedLevel: "+ConstantManager.Selected_level+
                                " levelId: "+levelid);
                        ConstantManager.Selected_level.add(levelid);
                        viewHolder.cb_level.setChecked(true);
                        isChecked = true;
                    }
                }
            });*/

            viewHolder.cb_level.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.d("cb_checker: ","isChecked: "+isChecked);
                    String levelId = String.valueOf(mleveldata.get(position).getLevelId());
                    if (isChecked){
                        ConstantManager.Selected_level.add(levelId);
                    }else {
                        ConstantManager.Selected_level.remove(ConstantManager.Selected_level.indexOf(levelId));
                    }
                    Log.d("cb_checker: ","value: "+ConstantManager.Selected_level);
                }
            });

            return convertView;
        }
    }
}
