package com.observatory.Assessment.Reports;

import android.app.AlertDialog;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView;
import com.observatory.Assessment.ConstantManager;
import com.observatory.Assessment.DataBase.SQLiteDB;
import com.observatory.Assessment.Model.SubjectModel;
import com.observatory.kefcorner.R;
import com.observatory.utils.BaseActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;



public class ReportsActivity extends BaseActivity {

    private ArrayList<SubjectModel> arr_subjects;
    ArrayList<ResultModel> arr_scoredetails;
    private SQLiteDB dbhelper;
    RecyclerView recycterview_subject,recyclerView;
    ReportSubjectAdapter mSubjectAdapter;
    LinearLayoutManager HorizontalLayout;
    RecyclerView.LayoutManager RecyclerViewLayoutManager;
    RecyclerView.ItemDecoration itemDecoration;
    ImageView imgview_datepicker;
    DateRangeCalendarView calendar;
    Button btn_done, btn_cancle;
    TextView tv_startdate, tv_enddate;
    String startdatetime = "", enddatetime = "", display_startdate = "", display_enddate = "", current_date = "";
    LinearLayout lnr_subj, lnr_result,lnr_header, lnr_nodata;
    boolean ischeckdate=false;
    //TimeLineRecyclerView recyclerView;
    ResultAdapter mResultAdapter;
    DateFormat dateFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);
        actionBar.setTitle("Reports");
        imgview_datepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View dialogView = View.inflate(ReportsActivity.this, R.layout.dialog_datepicker, null);
                calendar = dialogView.findViewById(R.id.calendar);
                calendar.resetAllSelectedViews();
                startdatetime = ""; enddatetime = ""; display_startdate = ""; display_enddate = "";
                AlertDialog.Builder builder = new AlertDialog.Builder(ReportsActivity.this);
                builder.setView(dialogView);
                final AlertDialog dialog = builder.create();
                btn_done = (Button) dialogView.findViewById(R.id.btn_done);
                btn_cancle = (Button) dialogView.findViewById(R.id.btn_cancle);
                btn_done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ischeckdate) {
                            if (startdatetime.equals("") && enddatetime.equals("")) {
                                Toast.makeText(ReportsActivity.this, "Please select date range", Toast.LENGTH_SHORT).show();
                            } else {
                                if (startdatetime.equals("")) {
                                    Toast.makeText(ReportsActivity.this, "Please select start date", Toast.LENGTH_SHORT).show();
                                } else if (enddatetime.equals("")) {
                                    Toast.makeText(ReportsActivity.this, "Please select end date", Toast.LENGTH_SHORT).show();
                                } else {
                                    lnr_subj.setVisibility(View.VISIBLE);
                                    if (lnr_result.getVisibility() == View.VISIBLE) {
                                        lnr_result.setVisibility(View.GONE);
                                        lnr_header.setVisibility(View.GONE);
                                    }
                                    if (lnr_nodata.getVisibility() == View.VISIBLE) {
                                        lnr_nodata.setVisibility(View.GONE);
                                    }
                                    tv_startdate.setText(display_startdate);
                                    tv_enddate.setText(display_enddate);
                                    SubjectBind();
                                    dialog.dismiss();
                                }
                            }
                        }
                        else{
                            Toast.makeText(ReportsActivity.this, "Selected date range is incorrect", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                btn_cancle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                calendar.setCalendarListener(new DateRangeCalendarView.CalendarListener() {
                    @Override
                    public void onFirstDateSelected(Calendar startDate) {
                        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        startdatetime = dateFormat.format(startDate.getTime());
                    }

                    @Override
                    public void onDateRangeSelected(Calendar startDate, Calendar endDate) {
                        Calendar calendar = Calendar.getInstance();
                            if (startDate.after(endDate)) {
                                Toast.makeText(ReportsActivity.this, "StartDate cannot be grater than EndDate", Toast.LENGTH_LONG).show();
                            } else if (endDate.before(startDate)) {
                                Toast.makeText(ReportsActivity.this, "EndDate cannot be less than StartDate", Toast.LENGTH_LONG).show();

                            } else {
                                dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                startdatetime = dateFormat.format(startDate.getTime());
                                dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                enddatetime = dateFormat.format(endDate.getTime());
                                dateFormat = new SimpleDateFormat("dd-MMMM-yyyy");
                                display_enddate = dateFormat.format(endDate.getTime());
                                display_startdate = dateFormat.format(startDate.getTime());
                                ischeckdate = true;
                            }

                    }

                });
                dialog.show();
            }
        });


    }

    public void SubjectBind() {
        dbhelper = SQLiteDB.getInstance(this, ConstantManager.DB_PATH);
        arr_subjects = new ArrayList<SubjectModel>();
        arr_subjects = dbhelper.getAllSubject(false);
        RecyclerViewLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycterview_subject.setLayoutManager(RecyclerViewLayoutManager);
        mSubjectAdapter = new ReportSubjectAdapter(this, arr_subjects);
        HorizontalLayout = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycterview_subject.setLayoutManager(HorizontalLayout);
        recycterview_subject.setAdapter(mSubjectAdapter);
    }


    @Override
    protected void onLayout() {
        recycterview_subject = (RecyclerView) findViewById(R.id.recycterview_subject);
        recyclerView = findViewById(R.id.recycler_view);
        imgview_datepicker = (ImageView) findViewById(R.id.imgview_datepicker);
        tv_startdate = (TextView) findViewById(R.id.tv_startdate);
        tv_enddate = (TextView) findViewById(R.id.tv_enddate);
        lnr_subj = (LinearLayout) findViewById(R.id.lnr_subjects);
        lnr_result = (LinearLayout) findViewById(R.id.lnr_result);
        lnr_nodata = (LinearLayout) findViewById(R.id.lnr_nodata);
        lnr_header=  (LinearLayout) findViewById(R.id.lnr_header);
        Calendar calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd-MMMM-yyyy ");
        current_date = dateFormat.format(calendar.getTime());
        tv_startdate.setText(current_date);
        tv_enddate.setText(current_date);
    }

    public void Resultbind(int subjectid) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL,
                false));
        dbhelper = SQLiteDB.getInstance(this, ConstantManager.DB_PATH);

        arr_scoredetails = new ArrayList<ResultModel>();
        arr_scoredetails = dbhelper.getScoreDetails(subjectid, startdatetime + " 00:00:00", enddatetime + " 23:59:59");

        if (arr_scoredetails.size() > 0) {
            mResultAdapter = new ResultAdapter(ReportsActivity.this, arr_scoredetails);
            recyclerView.setAdapter(mResultAdapter);
            if (lnr_nodata.getVisibility() == View.VISIBLE) {
                lnr_nodata.setVisibility(View.GONE);
                lnr_result.setVisibility(View.VISIBLE);
                lnr_header.setVisibility(View.VISIBLE);
            }
            else {
                lnr_result.setVisibility(View.VISIBLE);
                lnr_header.setVisibility(View.VISIBLE);
            }
        } else {
            lnr_result.setVisibility(View.GONE);
            lnr_header.setVisibility(View.GONE);
            lnr_nodata.setVisibility(View.VISIBLE);
        }
    }



    public String StringToDate(String date) throws ParseException {

        String start_dt = date;
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date1 = (Date)formatter.parse(start_dt);
        SimpleDateFormat newFormat = new SimpleDateFormat("dd MMMM, yyyy");
        String finalString = newFormat.format(date1);
        return finalString;
    }

}
