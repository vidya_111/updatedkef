package com.observatory.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.observatory.database.StatDetails;
import com.observatory.kefcorner.R;

import java.util.List;

/**
 * Created by anuj on 04/04/17.
 */

public class StatisticDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int HEADER = 0;
    private static final int DETAIL = 1;
    private List<StatDetails> list;
    private StatDetails currentItem;
    private Context context;

    public StatisticDetailAdapter(Context context, List<StatDetails> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {

        currentItem = list.get(position);

        if (currentItem.isHeader) {

            return HEADER;
        } else {

            return DETAIL;

        }


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == HEADER) {
            View view = LayoutInflater.from(context).inflate(R.layout.custom_stat_header, parent, false);
            HeaderHolder viewHolder = new HeaderHolder(view);
            return viewHolder;

        } else {
            View view = LayoutInflater.from(context).inflate(R.layout.custom_stat_detail_row, parent, false);
            InnerData viewHolder = new InnerData(view);
            return viewHolder;

        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        currentItem = list.get(position);
        switch (holder.getItemViewType()) {

            case HEADER:
                HeaderHolder headerHolder = (HeaderHolder) holder;
                headerHolder.tvHeader.setText(currentItem.header);
                break;

            case DETAIL:
                InnerData innerHolder = (InnerData) holder;
                innerHolder.tvNoOfTime.setText(currentItem.noOfTimeSeen);
                innerHolder.tvLastSeen.setText(currentItem.lastSeen);
                innerHolder.tvDuration.setText(currentItem.duration);
                break;

        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class HeaderHolder extends RecyclerView.ViewHolder {

        private TextView tvHeader;

        public HeaderHolder(View itemView) {
            super(itemView);

            tvHeader = (TextView) itemView.findViewById(R.id.header);

        }
    }

    class InnerData extends RecyclerView.ViewHolder {

        private TextView tvNoOfTime;
        private TextView tvDuration;
        private TextView tvLastSeen;


        public InnerData(View itemView) {
            super(itemView);

            tvNoOfTime = (TextView) itemView.findViewById(R.id.no_of_ts);
            tvDuration = (TextView) itemView.findViewById(R.id.duration);
            tvLastSeen = (TextView) itemView.findViewById(R.id.last_seen);
        }
    }
}
