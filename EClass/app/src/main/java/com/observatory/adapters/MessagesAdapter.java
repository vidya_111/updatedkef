package com.observatory.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.observatory.model.MessagesModel;
import com.observatory.kefcorner.R;

import java.util.ArrayList;
import java.util.List;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MyViewHolder> {

    private List<MessagesModel> messagesModelList = null;

    public MessagesAdapter(List<MessagesModel> messagesModelList) {
        this.messagesModelList = messagesModelList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView messageText, messageTime, messageBy;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            messageText = itemView.findViewById(R.id.message_text);
            messageTime = itemView.findViewById(R.id.message_time);
            messageBy = itemView.findViewById(R.id.message_by);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.broadcast_message_list_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessagesAdapter.MyViewHolder holder, int position) {
        MessagesModel model = this.messagesModelList.get(position);
        holder.messageText.setText(model.getMessageText());
        holder.messageTime.setText(model.getMessageDateTime());
        holder.messageBy.setText(model.getMessageBy());
    }

    @Override
    public int getItemCount() {
        if(messagesModelList==null)
        {
            messagesModelList = new ArrayList<MessagesModel>();
        }
        return messagesModelList.size();
    }


}
