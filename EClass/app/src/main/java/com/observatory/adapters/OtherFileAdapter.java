package com.observatory.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.observatory.Assessment.Model.OtherFilesModel;
import com.observatory.kefcorner.R;
import com.observatory.model.MessagesModel;

import java.util.ArrayList;
import java.util.List;

public class OtherFileAdapter extends RecyclerView.Adapter<OtherFileAdapter.MyViewHolder> {

    private List<OtherFilesModel> modelList = null;

    public OtherFileAdapter(List<OtherFilesModel> modelList) {
        this.modelList = modelList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView fileName, fileTime;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            fileName = itemView.findViewById(R.id.file_name);
            fileTime = itemView.findViewById(R.id.file_date);
        }
    }
    @NonNull
    @Override
    public OtherFileAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.other_files_item, parent, false);
        return new OtherFileAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OtherFileAdapter.MyViewHolder holder, int position) {
        OtherFilesModel model = this.modelList.get(position);
        holder.fileName.setText(model.getFileName());
        holder.fileTime.setText(model.getFileDate());
    }

    @Override
    public int getItemCount() {
        if(modelList==null)
        {
            modelList = new ArrayList<OtherFilesModel>();
        }
        return modelList.size();
    }


}
