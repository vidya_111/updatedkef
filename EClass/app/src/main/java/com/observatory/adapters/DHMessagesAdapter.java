package com.observatory.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.observatory.kefcorner.R;

public class DHMessagesAdapter extends RecyclerView.Adapter<DHMessagesAdapter.MyViewHolder> {

    Context context;
    String[] questions;
    String[] answers;
    String[] time;
    int count;

    public DHMessagesAdapter(Context context, String[] questions, String[] answers, String[] time, int count) {
        this.context = context;
        this.questions = questions;
        this.answers = answers;
        this.time = time;
        this.count = count;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView TV_question, TV_answer, TV_time;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            TV_question = itemView.findViewById(R.id.DH_question_tv);
            TV_answer = itemView.findViewById(R.id.DH_answer_tv);
            TV_time = itemView.findViewById(R.id.DH_time_tv);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.doubt_history_list_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.TV_question.setText(questions[position]);
        holder.TV_answer.setText(answers[position]);
        holder.TV_time.setText(time[position]);
        Log.d("count",":"+count);
    }

    @Override
    public int getItemCount() {
        return count;

    }


}
