package com.observatory.adapters;

import android.graphics.Typeface;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.observatory.database.Subject;
import com.observatory.kefcorner.R;

import java.util.ArrayList;

/**
 * Created by anuj on 11/08/16.
 */
public class MediumAdapter extends BaseAdapter {


    Typeface bold, reg;
    ArrayList<Subject> subjects;
    private int[] bg = new int[]{R.drawable.one,
            R.drawable.two,
            R.drawable.three,
            R.drawable.four,
            R.drawable.five,
            R.drawable.six,
            R.drawable.seven,
            R.drawable.eight,
            R.drawable.nine,
            R.drawable.one,
            R.drawable.two,
            R.drawable.three,
            R.drawable.four,
            R.drawable.five,
            R.drawable.six,
            R.drawable.seven,
            R.drawable.eight,
            R.drawable.nine,
            R.drawable.one,
            R.drawable.two,
            R.drawable.three,
            R.drawable.four,
            R.drawable.five,
            R.drawable.six,
            R.drawable.seven,
            R.drawable.eight,
            R.drawable.nine
    };

    private int[] color = new int[]{R.color.std1,
            R.color.std2,
            R.color.std3,
            R.color.std4,
            R.color.std5,
            R.color.std6,
            R.color.std7,
            R.color.std8,
            R.color.std9,
            R.color.std1,
            R.color.std2,
            R.color.std3,
            R.color.std4,
            R.color.std5,
            R.color.std6,
            R.color.std7,
            R.color.std8,
            R.color.std9,
            R.color.std1,
            R.color.std2,
            R.color.std3,
            R.color.std4,
            R.color.std5,
            R.color.std6,
            R.color.std7,
            R.color.std8,
            R.color.std9

    };


    public MediumAdapter(Typeface bold, Typeface reg, ArrayList<Subject> subjects) {
        this.bold = bold;
        this.reg = reg;
        this.subjects = subjects;

    }

    @Override
    public int getCount() {
        return subjects.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mediums, parent, false);
            viewHolder.txtChapterName = (TextView) convertView.findViewById(R.id.chapterName);
            viewHolder.txtChapterNumber = (TextView) convertView.findViewById(R.id.chapterNumber);


            convertView.setTag(viewHolder);
        }

        viewHolder = (ViewHolder) convertView.getTag();


        //viewHolder.txtChapterNumber.setTextColor(ContextCompat.getColor(convertView.getContext(),color[position]));
        viewHolder.txtChapterNumber.setTextColor(ContextCompat.getColor(convertView.getContext(),color[position]));
        viewHolder.txtChapterName.setTextColor(ContextCompat.getColor(convertView.getContext(), color[position]));
        viewHolder.txtChapterName.setText(subjects.get(position).getChapterName());
        viewHolder.txtChapterNumber.setText((position + 1) + "");
        viewHolder.txtChapterNumber.setTypeface(bold);


        return convertView;
    }


    private class ViewHolder {

        TextView txtChapterNumber;
        TextView txtChapterName;


    }
}
