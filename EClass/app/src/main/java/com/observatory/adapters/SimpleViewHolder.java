package com.observatory.adapters;


import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

// A Abstract ViewHolder class which is extended for specific use cases.
public abstract class SimpleViewHolder extends RecyclerView.ViewHolder {

    // View representing a single row
    final View itemView;
    // Adapter instance for this view holder. It is
    //	used to notify adapter about dataset changes.
    final SimpleAdapter adapter;
    //	List required to add/remove items as needed.
    List list;
    final Context context;

    public SimpleViewHolder(View itemView, SimpleAdapter adapter, List list, Context context) {
        super(itemView);
        this.itemView = itemView;
        this.adapter = adapter;
        this.list = list;
        this.context = context;
    }

    public abstract void setData(Object object);
}

