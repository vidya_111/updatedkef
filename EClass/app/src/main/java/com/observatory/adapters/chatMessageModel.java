package com.observatory.adapters;

public class chatMessageModel {

    public final static String MSG_TYPE_SENT = "MSG_TYPE_SENT";
    public final static String MSG_TYPE_RECEIVED = "MSG_TYPE_RECEIVED";
    private String msgContent;
    private String msgTime;
    private String msgType;
    private String userType;

    public chatMessageModel(String msgContent, String msgType, String msgTime, String userType) {
        this.msgContent = msgContent;
        this.msgType = msgType;
        this.msgTime = msgTime;
        this.userType = userType;
    }

    public String getMsgContent() {
        return msgContent;
    }
    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }
    public String getMsgType() {
        return msgType;
    }
    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(String msgTime) {
        this.msgTime = msgTime;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

}
