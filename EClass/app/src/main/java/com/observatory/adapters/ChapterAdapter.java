package com.observatory.adapters;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.observatory.database.Subject;
import com.observatory.kefcorner.R;

import java.util.ArrayList;

/**
 * Created by rohit on 21/01/15.
 */
public class ChapterAdapter extends BaseAdapter {

    Typeface bold,reg;
    ArrayList<Subject> subjects;



    public ChapterAdapter(Typeface bold,Typeface reg, ArrayList<Subject>subjects) {
        this.bold=bold;
        this.reg=reg;
        this.subjects=subjects;

    }

    @Override
    public int getCount() {
        return subjects.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView==null){
            viewHolder=new ViewHolder();
            convertView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chapter,parent,false);
            viewHolder.txtChapterName= (TextView) convertView.findViewById(R.id.chapterName);
            viewHolder.txtChapterNumber= (TextView) convertView.findViewById(R.id.chapterNumber);



            convertView.setTag(viewHolder);
        }

        viewHolder= (ViewHolder) convertView.getTag();


        viewHolder.txtChapterName.setText(subjects.get(position).getChapterName());
        viewHolder.txtChapterNumber.setText((position+1)+"");
        viewHolder.txtChapterNumber.setTypeface(bold);





        return convertView;
    }


    private class ViewHolder{

        TextView txtChapterNumber;
        TextView txtChapterName;


    }

}
