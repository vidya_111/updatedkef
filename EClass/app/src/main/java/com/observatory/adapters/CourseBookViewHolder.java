package com.observatory.adapters;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.observatory.database.Subject;
import com.observatory.database.Textbook;
import com.observatory.kefcorner.R;
import com.observatory.kefcorner.TextbookActivity;
import com.observatory.utils.App;

import java.util.List;

public class CourseBookViewHolder extends SimpleViewHolder {

    int colorBlue;
    int colorRed;
    ImageView ivCoursePhoto;
    TextView tvCourseName;
    public BottomSheetDialog alertdialog;
    String courseName, courseId;

    public CourseBookViewHolder(View itemView, SimpleAdapter adapter, List list, Context context) {
        super(itemView, adapter, list, context);

        colorBlue = context.getResources().getColor(R.color.app_blue);
        colorRed = context.getResources().getColor(R.color.red);

        ivCoursePhoto = itemView.findViewById(R.id.iv_course_photo);
        tvCourseName = itemView.findViewById(R.id.tv_course_name);

    }

    @Override
    public void setData(Object object) {
       /* if (object instanceof CourseBookModel.Response) {
            CourseBookModel.Response course = (CourseBookModel.Response) object;
            showBookDetails(course);
        } else if (object instanceof SubCategoriesModel.Response) {
            SubCategoriesModel.Response course = (SubCategoriesModel.Response) object;
            showSubCategoryDetail(course);
        } else if (object instanceof TextBookCategoriesModel.Response) {
            TextBookCategoriesModel.Response course = (TextBookCategoriesModel.Response) object;
            showSuperCategoryDetail(course);
        }*/
        if (object instanceof Textbook) {
            Textbook course = (Textbook) object;
            tvCourseName.setText(course.getSubjectDisplayName());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String path = App.BASE_PATH + "textbook" + "/" + course.getFolderName() + "/" + course.getContentName();
                    Log.e("Textbook", path);
                    ((TextbookActivity) context).openPdf(path);
                }
            });
        }
    }




/*    private void showSuperCategoryDetail(final TextBookCategoriesModel.Response course) {
        courseName = course.superCategoryName;
        courseId = course.superCategoryId;
       // Picasso.with(context).load(R.drawable.ic_courses_selected).into(ivCoursePhoto);

        tvCourseName.setText(course.superCategoryName);
        ivCoursePhoto.setVisibility(View.VISIBLE);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Commented by vidya

                ((TextbookActivity) context).getAllSubCategories(course.superCategoryId);

            }
        });
    }*/
/*
    private void showSubCategoryDetail(final SubCategoriesModel.Response course) {
        courseName = course.superCategoryName;
        courseId = course.superCategoryId;
        Picasso.with(context).load(R.drawable.ic_courses_selected).into(ivCoursePhoto);

        tvCourseName.setText(course.superCategoryName);
        ivCoursePhoto.setVisibility(View.VISIBLE);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Commented by vidya

                ((TextbookActivity) context).getTextBooks(course.superCategoryId);

            }
        });
    }*/

/*    private void showBookDetails(final CourseBookModel.Response course) {
        courseName = course.contentName;
        courseId = course.superCategoryId;
        Picasso.with(context).load(R.drawable.ic_courses_selected).into(ivCoursePhoto);

        tvCourseName.setText(course.contentName);
        ivCoursePhoto.setVisibility(View.VISIBLE);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Commented by vidya

                ((TextbookActivity) context).openBook(course);

            }
        });
    }*/
}



