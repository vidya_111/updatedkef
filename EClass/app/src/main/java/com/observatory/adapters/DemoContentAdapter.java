package com.observatory.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.observatory.database.Subject;
import com.observatory.kefcorner.R;

import java.util.ArrayList;

/**
 * Created by anuj on 21/01/16.
 */
public class DemoContentAdapter extends BaseAdapter {

    Typeface bold, reg;
    ArrayList<Subject> subjects;
    private Context context;

    public DemoContentAdapter(Context context, ArrayList<Subject> subjects) {
        this.context = context;
        this.subjects = subjects;

    }

    @Override
    public int getCount() {
        return subjects.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_demo_youtube_video, parent, false);


            viewHolder.txtChapterName = (TextView) convertView.findViewById(R.id.tv_d_text);
            viewHolder.thumbnail = (ImageView) convertView.findViewById(R.id.iv_d_thumb);


            convertView.setTag(viewHolder);
        }

        viewHolder = (ViewHolder) convertView.getTag();


        viewHolder.txtChapterName.setText(subjects.get(position).getChapterName());

        Glide.with(context).load("http://img.youtube.com/vi/"+subjects.get(position).getFolderName()+"/0.jpg").into(viewHolder.thumbnail);


        return convertView;
    }


    private class ViewHolder {

        ImageView thumbnail;
        TextView txtChapterName;


    }
}
