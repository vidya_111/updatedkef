package com.observatory.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.observatory.kefcorner.CourseHeaderViewModel;
import com.observatory.kefcorner.R;
import com.observatory.kefcorner.UserHistoryViewModel;
import com.observatory.kefcorner.UserViewModel;

import java.util.List;

/**
 * Created by anuj on 09/03/17.
 */

public class UserProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    List<UserViewModel> userHistory;


    public UserProfileAdapter(List<UserViewModel> userHistory) {

        this.userHistory = userHistory;

    }

    @Override
    public int getItemCount() {
        return userHistory.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int itemViewType) {

        View convertView;

        if (itemViewType == UserViewModel.VIEW_USER_HISTORY) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_profile, parent, false);
            return new UserHistoryViewHolder(convertView);
        } else {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_std_medium, parent, false);
            return new CourseHeaderViewHolder(convertView);
        }

    }

    @Override
    public int getItemViewType(int position) {
        return userHistory.get(position).getItemViewType();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        UserViewModel userViewModel = userHistory.get(position);

        if (userViewModel.getItemViewType() == UserViewModel.VIEW_USER_HISTORY) {

            UserHistoryViewHolder userHistoryViewHolder = (UserHistoryViewHolder) holder;
            UserHistoryViewModel userHistoryViewModel = (UserHistoryViewModel) userViewModel;
            userHistoryViewHolder.tvDate.setText(userHistoryViewModel.date);
            userHistoryViewHolder.tvTime.setText(userHistoryViewModel.time);

        } else {

            CourseHeaderViewHolder courseHeaderViewHolder = (CourseHeaderViewHolder) holder;
            CourseHeaderViewModel courseHeaderViewModel = (CourseHeaderViewModel) userViewModel;
            courseHeaderViewHolder.tvCourse.setText(courseHeaderViewModel.course);

        }
    }

    private class UserHistoryViewHolder extends RecyclerView.ViewHolder {

        TextView tvDate;
        TextView tvTime;

        public UserHistoryViewHolder(View itemView) {
            super(itemView);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
        }
    }

    private class CourseHeaderViewHolder extends RecyclerView.ViewHolder {

        TextView tvCourse;

        public CourseHeaderViewHolder(View itemView) {
            super(itemView);
            tvCourse = (TextView) itemView.findViewById(R.id.tv_course);
        }
    }


}
