package com.observatory.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * A extension of RecyclerView.Adapter created to save time in creating adapters for each and
 * and every list. Hopefully, it can save time on boilerplate. Reflection is used to determine
 * the Type of ViewHolder used in each list. All ViewHolders must extend SimpleViewHolder.
 */
public class SimpleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int layoutResId;
    private List list;
    private final Context context;
    private Class viewHolderClass;
    private SimpleViewHolder viewholder;
    private PagingListener pagingListener;

    public void setPagingListener(PagingListener pagingListener) {
        this.pagingListener = pagingListener;
    }

    public SimpleAdapter(int layoutResId, List list, Context context, Class<? extends SimpleViewHolder> viewHolder) {
        this.layoutResId = layoutResId;
        this.list = list;
        this.context = context;
        this.viewHolderClass = viewHolder;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflator.inflate(layoutResId, parent, false);
        // get declared constructor of SimpleViewHolder and pass data
        Constructor[] declaredConstructors = viewHolderClass.getDeclaredConstructors();
        try {
//			Gets Simple View Holder's constructor and inserts params at run time using reflection.
//			NOTE : make sure, that SimpleViewHolder constructor params match 'args' passed below.
            viewholder = (SimpleViewHolder) declaredConstructors[0].newInstance(view, this, list, context);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return viewholder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((SimpleViewHolder) holder).setData(list.get(position));
        handlePaging(position);

    }

    /* If User has scrolled to 75% of the data, we communicate it to PagingListener (Could be an activity or fragment)
     * about it. */
    private void handlePaging(int position) {
//		int percentListScrolled = (int)((float)(position + 1) / list.size() * 100);
//		if (percentListScrolled > 70 && percentListScrolled < 75) {
//			if (pagingListener != null && !pagingListener.isPagingCalled()) {
//				pagingListener.callPaging();
//			}
//		}
        // if this is the last item, call Paging.
        if (position == list.size() - 1) {
            if (pagingListener != null && !pagingListener.isPagingCalled()) {
                pagingListener.callPaging();
            }
        }
    }

    @Override
    public int getItemCount() {
        if (list == null) {
            throw new NullPointerException("list is NULL in -> " + context.toString());
        }
        return list.size();
    }

    public void updateList(List updatedList) {
        this.list = updatedList;
    }

    public void addMoreItems(List moreItems) {
        int lastItemPos = list.size();
        list.addAll(moreItems);
        notifyItemRangeInserted(lastItemPos, moreItems.size());
    }

    public List getList() {
        return this.list;
    }
}

interface PagingListener {
    void callPaging();

    boolean isPagingCalled();
}