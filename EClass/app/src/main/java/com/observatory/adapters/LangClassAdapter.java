package com.observatory.adapters;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.observatory.kefcorner.R;

/**
 * Created by rohit on 21/01/15.
 */
public class LangClassAdapter extends BaseAdapter {

    int[] images;
    String[] text;
    int[] colors;
    Typeface typeface;

    public LangClassAdapter(int[] images, String[] text, int[] colors, Typeface typeface) {

        this.images = images;
        this.text = text;
        this.colors = colors;
        this.typeface = typeface;


    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();


            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lang_class, parent, false);

            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.img);
            viewHolder.textView = (TextView) convertView.findViewById(R.id.txt);


            convertView.setTag(viewHolder);


        }


        viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.imageView.setImageResource(images[position]);
        viewHolder.textView.setText(text[position]);

        switch (text[position]) {
            case "CHAPTER":
                viewHolder.textView.setTextColor(parent.getResources().getColor(colors[0]));
                break;
            case "MIND MAP":
                viewHolder.textView.setTextColor(parent.getResources().getColor(colors[1]));
                break;
            case "Q & A":
                viewHolder.textView.setTextColor(parent.getResources().getColor(colors[2]));
                break;
            case "PDF":
                viewHolder.textView.setTextColor(parent.getResources().getColor(colors[3]));
                break;
            case "MCQ":
                viewHolder.textView.setTextColor(parent.getResources().getColor(colors[4]));
                break;
            case "STATISTICS":
            viewHolder.textView.setTextColor(parent.getResources().getColor(colors[5]));
            break;
        }

        viewHolder.textView.setTypeface(typeface);


        return convertView;
    }


    private class ViewHolder {

        TextView textView;
        ImageView imageView;


    }

}
