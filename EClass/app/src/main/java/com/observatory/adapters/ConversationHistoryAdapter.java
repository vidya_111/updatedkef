package com.observatory.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.observatory.kefcorner.R;

import java.util.ArrayList;
import java.util.List;

public class ConversationHistoryAdapter extends RecyclerView.Adapter<ConversationHistoryAdapter.MyViewHolder> {

    private List<chatMessageModel> msgList = null;

    public ConversationHistoryAdapter(List<chatMessageModel> msgList) {
        this.msgList = msgList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout sender_layout, receiver_layout;
        TextView sender_msg_content, sender_time, receiver_name, receiver_message_content, receiver_msg_time;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            sender_layout = itemView.findViewById(R.id.sender_layout_id);
            receiver_layout = itemView.findViewById(R.id.receiver_layout_id);
            sender_msg_content = itemView.findViewById(R.id.send_message_content_id);
            sender_time = itemView.findViewById(R.id.send_message_time_id);
            receiver_message_content = itemView.findViewById(R.id.received_message_content_id);
            receiver_msg_time = itemView.findViewById(R.id.received_message_time_id);
            receiver_name = itemView.findViewById(R.id.receiver_name_id);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_recyclerview, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        chatMessageModel model = this.msgList.get(position);

        if (chatMessageModel.MSG_TYPE_RECEIVED.equals(model.getMsgType())){
            holder.receiver_layout.setVisibility(View.VISIBLE);
            holder.receiver_message_content.setText(model.getMsgContent());
            holder.receiver_msg_time.setText(model.getMsgTime());

            if (model.getUserType().equals("TR")){
                holder.receiver_name.setText("Teacher");
            }else if (model.getUserType().equals("IN")){
                holder.receiver_name.setText("Institute");
            }else if (model.getUserType().equals("SC")){
                holder.receiver_name.setText("School");
            }

        }else if (chatMessageModel.MSG_TYPE_SENT.equals(model.getMsgType())){
            holder.sender_layout.setVisibility(View.VISIBLE);
            holder.sender_msg_content.setText(model.getMsgContent());
            holder.sender_time.setText(model.getMsgTime());
        }
    }

    @Override
    public int getItemCount() {
        if(msgList==null)
        {
            msgList = new ArrayList<chatMessageModel>();
        }
        return msgList.size();
    }


}
