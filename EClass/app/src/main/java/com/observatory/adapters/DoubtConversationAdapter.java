package com.observatory.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.observatory.kefcorner.Doubt_Conversations;
import com.observatory.model.AllDoubts;
import com.observatory.kefcorner.R;

import java.util.ArrayList;
import java.util.List;


public class DoubtConversationAdapter extends RecyclerView.Adapter<DoubtConversationAdapter.MyViewHolder> {

    private List<AllDoubts> msgList = null;
    private Doubt_Conversations activity;
    private int[] background_colors = new int[]
            {R.color.color1, R.color.color2, R.color.color3, R.color.color4, R.color.color5, R.color.color6, R.color.color7, R.color.color8, R.color.color9};

    public DoubtConversationAdapter(List<AllDoubts> msgList, Doubt_Conversations activity) {
        this.msgList = msgList;
        this.activity = activity;
    }

    public void setMsgList(List<AllDoubts> msgList) {
        this.msgList = msgList;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView question_text, standard_text, time_text;
        LinearLayout card;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            question_text = itemView.findViewById(R.id.DC_question);
            standard_text = itemView.findViewById(R.id.DC_standard);
            time_text = itemView.findViewById(R.id.DC_time);
            card = itemView.findViewById(R.id.conversation_card);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.doubt_conversation_headers, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        AllDoubts model = this.msgList.get(position);
        // holder.card.setBackgroundResource(getBackgroundResId(position));
        holder.card.setBackgroundResource(R.color.color1);
        holder.standard_text.setText(model.getSubjectName());
        holder.question_text.setText(model.getDoubtTitle());
        holder.time_text.setText(model.getCreatedOn());
        holder.card.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (activity != null) {
                    activity.deleteDoubtByPosition(holder.getAdapterPosition());
                }
                return true;
            }
        });
    }

    private int getBackgroundResId(int position) {
        int bgPos = position % 9;
        Log.d("colorPosition: ", "::" + bgPos);
        return background_colors[bgPos];
    }

    @Override
    public int getItemCount() {
        if (msgList == null) {
            msgList = new ArrayList<AllDoubts>();
        }
        return msgList.size();
    }


}
