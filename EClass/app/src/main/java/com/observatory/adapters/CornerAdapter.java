package com.observatory.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.observatory.model.KefCornerModel;
import com.observatory.kefcorner.R;

import java.util.ArrayList;
import java.util.List;

public class CornerAdapter extends RecyclerView.Adapter<CornerAdapter.MyViewHolder> {

    private List<KefCornerModel> cornerList = null;

    public CornerAdapter(List<KefCornerModel> cornerList) {
        this.cornerList = cornerList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title, createdDate;
        ImageView fileIcon;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.item_main_text);
            createdDate = itemView.findViewById(R.id.item_text);
            fileIcon = itemView.findViewById(R.id.item_icon);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_card_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        KefCornerModel cornerModel = this.cornerList.get(position);

        holder.title.setText(cornerModel.getTitle());
        holder.createdDate.setText(cornerModel.getCreatedOn());

        if (cornerModel.getMediaTypeName().equals("Image")){
            holder.fileIcon.setImageResource(R.drawable.image_item_icon);
        }else if (cornerModel.getMediaTypeName().equals("PDF")){
            holder.fileIcon.setImageResource(R.drawable.pdf_item_icon);
        }else if (cornerModel.getMediaTypeName().equals("Video")){
            holder.fileIcon.setImageResource(R.drawable.video_item_icon);
        }else if (cornerModel.getMediaTypeName().equals("Audio")){
            holder.fileIcon.setImageResource(R.drawable.audio_item_icon);
        }else if (cornerModel.getMediaTypeName().equals("LINK")){
            holder.fileIcon.setImageResource(R.drawable.ic_baseline_link_24);
        }
    }

    @Override
    public int getItemCount() {
        if(cornerList==null)
        {
            cornerList = new ArrayList<KefCornerModel>();
        }
        return cornerList.size();
    }


}
