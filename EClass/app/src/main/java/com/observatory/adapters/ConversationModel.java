package com.observatory.adapters;

import java.io.Serializable;

public class ConversationModel implements Serializable {

    String doubt_title;
    String standard;
    String time;

    public ConversationModel(String doubt_title, String standard, String time) {
        this.doubt_title = doubt_title;
        this.standard = standard;
        this.time = time;
    }

    public String getDoubt_title() {
        return doubt_title;
    }

    public void setDoubt_title(String doubt_title) {
        this.doubt_title = doubt_title;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
