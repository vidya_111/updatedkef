package com.observatory.adapters;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.observatory.kefcorner.R;

/**
 * Created by rohit on 21/01/15.
 */
public class GridAdapter extends BaseAdapter {

    private String[] subjectName;
    private int[] images;
    private int[] colors;
    private Typeface typeface;


    public GridAdapter(String [] subjectName,int[] images,int[]colors,Typeface typeface) {

        this.subjectName=subjectName;
        this.images=images;
        this.colors=colors;
        this.typeface=typeface;

    }

    @Override
    public int getCount() {
        return subjectName.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView==null){

            viewHolder=new ViewHolder();

            convertView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid,parent,false);

            viewHolder.image= (ImageView) convertView.findViewById(R.id.img);
            viewHolder.text=(TextView) convertView.findViewById(R.id.text);

            convertView.setTag(viewHolder);




        }

        viewHolder= (ViewHolder) convertView.getTag();

        viewHolder.image.setImageResource(images[position]);
        viewHolder.text.setText(subjectName[position]);
        viewHolder.text.setTextColor(parent.getResources().getColor(colors[position]));
        //viewHolder.text.setTypeface(typeface);




        return convertView;
    }

    private class ViewHolder{

        ImageView image;
        TextView text;
    }

}
