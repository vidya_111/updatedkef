package com.observatory.utils;

/**
 * Created by gaurav on 6/1/18.
 */

public class UsbDevice {
    public String getSerialNum() {
        return serialNum;
    }

    public String getDevicePath() {
        return devicePath;
    }

    String serialNum;
    String devicePath;

    public UsbDevice(String serialNum, String devicePath) {
        this.serialNum = serialNum;
        this.devicePath = devicePath;
    }
}
