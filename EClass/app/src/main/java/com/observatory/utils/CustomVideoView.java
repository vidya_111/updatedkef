package com.observatory.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.VideoView;

/**
 * Created by Anuj on 12-06-2015.
 */
public class CustomVideoView extends VideoView {
    private PlayPauseListener mListener;

    public CustomVideoView(Context context) {
        super(context);
    }

    public CustomVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomVideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void seekTo(int timeMillis) {
        Log.e("EclassPlayer", (timeMillis-5000)+"");
        super.seekTo(timeMillis-5000);
        /*if(timeMillis<=5000){
            super.seekTo(timeMillis);
        }
        else
        {
            super.seekTo(timeMillis-5000);
        }*/

    }

    public void setPlayPauseListener(PlayPauseListener listener) {
        mListener = listener;
    }

    @Override
    public void pause() {
        super.pause();
        if (mListener != null) {
            mListener.onPause();
        }
    }

    @Override
    public void start() {
        super.start();
        if (mListener != null) {
            mListener.onPlay();
        }
    }

    public static interface PlayPauseListener {
        void onPlay();

        void onPause();
    }
}
