package com.observatory.utils;

import android.content.Context;

/**
 * Created by anuj on 09/04/16.
 */
public final class AnalyticTracker {

    public enum Target {
        APP,
        // Add more trackers here if you need, and update the code in #get(Target) below
    }

    private static AnalyticTracker sInstance;

    public static synchronized void initialize(Context context) {
        if (sInstance != null) {
            throw new IllegalStateException("Extra call to initialize analytics trackers");
        }

        sInstance = new AnalyticTracker(context);
    }

    public static synchronized AnalyticTracker getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException("Call initialize() before getInstance()");
        }

        return sInstance;
    }

  //  private final Map<Target, Tracker> mTrackers = new HashMap<Target, Tracker>();
    private final Context mContext;

    /**
     * Don't instantiate directly - use {@link #getInstance()} instead.
     */
    private AnalyticTracker(Context context) {
        mContext = context.getApplicationContext();
    }

//    public synchronized Tracker get(Target target) {
//        if (!mTrackers.containsKey(target)) {
//            Tracker tracker;
//            switch (target) {
//                case APP:
//                    tracker = GoogleAnalytics.getInstance(mContext).newTracker(R.xml.app_tracker);
//                    break;
//                default:
//                    throw new IllegalArgumentException("Unhandled analytics target " + target);
//            }
//            mTrackers.put(target, tracker);
//        }
//
//        return mTrackers.get(target);
//    }
}
