package com.observatory.utils;

import io.realm.RealmObject;

/**
 * Created by gaurav on 4/5/17.
 */

public class SecretKeyMaterial extends RealmObject {

    private int id;
    private byte[] iv;
    private byte[] secretKey;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getIv() {
        return iv;
    }

    public void setIv(byte[] iv) {
        this.iv = iv;
    }

    public byte[] getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(byte[] secretKey) {
        this.secretKey = secretKey;
    }

}
