package com.observatory.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.text.SimpleDateFormat;

public class DateUtil {

    public static final String COMMON_DATE_FORMAT1 = "yyyy-MM-dd HH:mm:ss";
    public static final String COMMON_DATE_FORMAT2 = "dd-MM-yyyy";

    public static String getCurrentDateTimeInFormat(String format) {
      /*  DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);*/
      Date myDate=new Date();
      SimpleDateFormat dateformat=new SimpleDateFormat(format);
      String date=dateformat.format(myDate);
      return  date;
    }

    public static String getFormattedDate(Date date, String format) {
        DateFormat dtf = new SimpleDateFormat(format);
        return dtf.format(date);
    }

    public static long getCurrentTimeInMillis() {
        return new Date().getTime();
    }

    public static String getFormattedTimeBySeconds(String format, long seconds) {
        DateFormat dtf = new SimpleDateFormat(format);
        Date date = new Date((seconds * 1000));
        return dtf.format(date);
    }

    public static String getFormattedTimeByMillis(long millis) {
        return String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis)
                        - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The change is in this line
                TimeUnit.MILLISECONDS.toSeconds(millis)
                        - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }

    public static String getFormattedTimeBySec(long seconds) {
        return String.format("%02d:%02d:%02d",
                TimeUnit.SECONDS.toHours(seconds),
                TimeUnit.SECONDS.toMinutes(seconds)
                        - TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(seconds)), // The change is in this line
                TimeUnit.SECONDS.toSeconds(seconds)
                        - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(seconds)));
    }

    public static double getTimeInSeconds(long millis) {
        return TimeUnit.MILLISECONDS.toSeconds(millis);
    }

    public static Date getDateFromFormattedString(String date, String format) {
        try {
            DateFormat dtf = new SimpleDateFormat(format);
            return dtf.parse(date);
        } catch (Exception e) {
            System.err.println(e);
        }
        return new Date();
    }
}
