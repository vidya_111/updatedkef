package com.observatory.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;

import androidx.core.content.ContextCompat;

import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.observatory.kefcorner.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import static com.observatory.utils.Constants.KEF_ContentTypeId_Content;
import static com.observatory.utils.Constants.KEF_ContentTypeId_MCQ;
import static com.observatory.utils.Constants.KEF_ContentTypeId_MindMap;
import static com.observatory.utils.Constants.KEF_ContentTypeId_PDF;
import static com.observatory.utils.Constants.KEF_ContentTypeId_QNA;


/**
 * Created by Anuj on 01-08-2015.
 */
public class CommonFunctions {


    private static Dialog dialog;

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }


    public static String isEmpty(String value) {

        if (TextUtils.isEmpty(value)) {
            return "";
        } else {

            return value;
        }

    }


    public static void hideKeyBoard(Context context, View view) {

        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showDialog(Context context) {
        hideDialog();
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setCancelable(false);
        ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.pb);
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(context, R.color.blue), PorterDuff.Mode.SRC_IN);
        dialog.show();

    }

    public static void showDialog(Context context, String loadingText) {
        hideDialog();
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setCancelable(false);

        TextView tvLoadingText = (TextView) dialog.findViewById(R.id.tv_loading_text);
        tvLoadingText.setText(loadingText);

        ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.pb);
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(context, R.color.blue), PorterDuff.Mode.SRC_IN);

        dialog.show();

    }

    public static void hideDialog() {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception e) {

        }
    }


    public static void sendAnalytics(String screenName, HashMap<String, String> dimensions) {


        if (dimensions == null) {


            //ParseAnalytics.trackEventInBackground(screenName);

        } else {

            //ParseAnalytics.trackEventInBackground(screenName, dimensions);
        }


    }

    public static String getUserId(Context context) {

        if (TextUtils.isEmpty(SettingPreffrences.getUserid(context))) {

            return "0";
        } else {

            return SettingPreffrences.getUserid(context);
        }

    }

    public static void addUserToParse(String userId) {

//        ParseInstallation parseInstallation=ParseInstallation.getCurrentInstallation();
//        parseInstallation.put("User_Id", userId);
//        parseInstallation.saveInBackground(new SaveCallback() {
//            @Override
//            public void done(ParseException e) {
//
//                if (e!=null){
//
//                    Log.d("Done","");
//
//                }
//            }
//        });

    }

    public static void openRateMyApp(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Rate Us");
        builder.setMessage("If you enjoy using EClass, please take a moment to rate the app. Thank You for your support!");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.observatory.sundaram"));
                try {
                    context.startActivity(intent);
                } catch (ActivityNotFoundException e) {

                }
                SettingPreffrences.setIsRated(context, true);

                dialog.dismiss();
            }
        });

        builder.setNeutralButton("Later", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SettingPreffrences.setRateCounter(context, 0);
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("No, Thanks", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                SettingPreffrences.setIsRated(context, true);
                dialog.dismiss();
            }
        });

        builder.create().show();

    }

    public static String getUniqueDeviceId(Context context) {

        String deviceId = "";

        try {

            TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            if (mTelephony.getDeviceId() != null) {

                deviceId = mTelephony.getDeviceId();

            } else {

                deviceId = Settings.Secure.getString(context.getContentResolver(),
                        Settings.Secure.ANDROID_ID);

            }

        } catch (Exception e) {

            e.printStackTrace();
            deviceId = "";

        }


        return deviceId;
    }


    public static String formatDuration(int secs) {

        if (secs < 60)
            return secs + " secs.";
        else {
            int mins = (int) secs / 60;
            int remainderSecs = secs - (mins * 60);
            if (mins < 60) {
                return (mins < 10 ? "0" : "") + mins + " mins, "
                        + (remainderSecs < 10 ? "0" : "") + remainderSecs + " secs.";
            } else {
                int hours = (int) mins / 60;
                int remainderMins = mins - (hours * 60);
                return (hours < 10 ? "0" : "") + hours + " hours, "
                        + (remainderMins < 10 ? "0" : "") + remainderMins + " mins, "
                        + (remainderSecs < 10 ? "0" : "") + remainderSecs + " secs.";
            }
        }
    }

    public static String formatTimeForGenerateReport(int secs) {

        if (secs < 60)
            return String.valueOf(secs);
        else {
            int mins = (int) secs / 60;
            int remainderSecs = secs - (mins * 60);
            if (mins < 60) {
                return ((mins < 10 ? "0" : "") + mins + ":"
                        + (remainderSecs < 10 ? "0" : "") + remainderSecs);
            } else {
                int hours = (int) mins / 60;
                int remainderMins = mins - (hours * 60);
                return ((hours < 10 ? "0" : "") + hours + ":"
                        + (remainderMins < 10 ? "0" : "") + remainderMins + ":"
                        + (remainderSecs < 10 ? "0" : "") + remainderSecs);
            }
        }
    }

    public static String getContentTypeUsedOnServer(String contentType) {

        String contentTypeName;

        switch (contentType) {

            case KEF_ContentTypeId_Content:
                contentTypeName = Constants.LABEL_CONTENT_TYPE_CONTENT;
                break;

            case KEF_ContentTypeId_MindMap:
                contentTypeName = "Mindmap"; /* label slightly different from the one used in app */
                break;

            case KEF_ContentTypeId_QNA:
                contentTypeName = "Question and Answers"; /* label slightly different from the one used in app */
                break;

            case KEF_ContentTypeId_PDF:
                contentTypeName = Constants.LABEL_CONTENT_TYPE_PDF;
                break;

            case KEF_ContentTypeId_MCQ:
                contentTypeName = Constants.LABEL_CONTENT_TYPE_MCQ;
                break;

            // error case
            default:
                contentTypeName = "contentType value is invalid";

        }

        return contentTypeName;
    }

    public static String formatDate(String date, String format) {

        String formattedDate;
        SimpleDateFormat databaseFormat = new SimpleDateFormat(Constants.DATABASE_DATE_FORMAT);
        Date dateObj = null;

        try {

            dateObj = databaseFormat.parse(date);
            SimpleDateFormat reqdFormat = new SimpleDateFormat(format);
            formattedDate = reqdFormat.format(dateObj);

        } catch (ParseException e) {
            formattedDate = "Date formatting Error";
        }
        return formattedDate;
    }

    public static String formatDate(String date, String curFormatPattern, String reqdFormatPattern) {

        String formattedDate;
        SimpleDateFormat dbFormat = new SimpleDateFormat(curFormatPattern);
        Date dateObj = null;

        try {

            dateObj = dbFormat.parse(date);
            SimpleDateFormat reqdFormat = new SimpleDateFormat(reqdFormatPattern);
            formattedDate = reqdFormat.format(dateObj);

        } catch (ParseException e) {
            formattedDate = "Date formatting Error";
        }
        return formattedDate;
    }

    // remove apostrophe is removed to avoid errors in SQL queries.
    public static String removeApostrophe(String text) {
        String aposRemoved;

        aposRemoved = text.replace("'", "");

        return aposRemoved;
    }

    public static boolean isVideoOrPdf(String fileName) {
        boolean isVideoOrPdf = false;

        if (fileName.toLowerCase().contains(".mp4") || fileName.toLowerCase().contains(".pdf")) {
            isVideoOrPdf = true;
        }

        return isVideoOrPdf;
    }

    public static String removeFormatSuffix(String fileName) {

        return fileName.split("\\.")[0];
    }

    public static String removeFormatPrefix(String fileName) {

        if (fileName.contains(" - ")) {
            return fileName.substring(5);
        } else
            return fileName;
    }
}
