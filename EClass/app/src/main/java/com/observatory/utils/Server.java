package com.observatory.utils;


import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;
import android.widget.Toast;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.NoSuchPaddingException;

public class Server extends NanoHTTPD {


    ByteArrayInputStream byteArrayInputStream;
    Context c;

    private static final int PIPE_BUFFER = 25*1024*1024;
    public PipedInputStream inPipe = new PipedInputStream(PIPE_BUFFER);
    public PipedOutputStream outPipe;

    private String tag = "Server";
    File f;
    private FileInputStream fis;

    public Server(Context c) {
        super(8080);
        this.c = c;
        // Log.v(tag,"localhost");




    }

    @Override
    public Response serve(String uri, Method method,
                          Map<String, String> header, Map<String, String> parameters,
                          Map<String, String> files) {


      //  Log.v(tag, "VidS" + uri.toString());




        test2(uri);

      //  NanoHTTPD.Response res = new NanoHTTPD.Response(Response.Status.OK, "video/mp4", inPipe);
        NanoHTTPD.Response res = new NanoHTTPD.Response(Response.Status.OK, "video/mp4", fis);

       // res.addHeader("Transfer-Encoding", "chunked");
        return res;
    }



    public void test2(String url){


        try {
            fis=new FileInputStream(new File("storage/sdcard0/Sundaram/Algebra/2/Content/1.mp4"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }


    public void test() {


        f = new File("storage/sdcard0/Sundaram/Algebra/1/Content/1.mp4");


        try {
            outPipe = new PipedOutputStream(inPipe);
        } catch (IOException e) {
            e.printStackTrace();
        }



        new Thread(
                new Runnable() {
                    public void run() {
                        try {
                            AESCrypt crypt=null;

                            crypt=new AESCrypt(true,"passwd");

                            crypt.decrypt(f.length(),new FileInputStream(f), outPipe);
                          //  Log.v(tag,"byteArrayInputStream:"+inPipe.available());

                        } catch (GeneralSecurityException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).start();
        //data can be read from the pipedInputStream here.
      /*  BufferedReader r = new BufferedReader(new InputStreamReader(inPipe));
        StringBuilder total = new StringBuilder();
        String line;
        try {
            while ((line = r.readLine()) != null) {

                Log.v(tag, "line: " + line);
            }
        } catch (IOException e) {
            e.printStackTrace();

        }
*/

    }
}