package com.observatory.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

/**
 * Created by Anuj on 23-05-2015.
 */
public class SettingPreffrences {

    private static final String PREF_NAME = "Sundaram EClass";
    private static final String LANGUAGE = "Pref_language";
    private static final String SUBJECT = "Pref_subject";
    private static final String SubjectId = "Pref_subject_id";
    private static final String CHAPTER = "Pref_chapter";
    private static final String ChapterId = "Pref_chapter_id";
    private static final String STANDARD = "Pref_dtandard";
    private static final String DISCLAIMER = "Pref_disclaimer";

    public static final String FROM_FB = "Pref_from_fb";

    public static final String FB_ID = "Pref_fb_id";
    public static final String PASSWORD = "Pref_password";

    public static final String NAME = "Pref_name";

    public static final String IS_PARSE_PUSH_DONE = "Pref_parse_done";
    private static final String EMAIL = "pref_email";
    private static final String SchoolId = "pref_school_id";
    private static final String CourseId = "pref_course_id";
    private static final String MOBILE = "pref_mobile";
    private static final String PHOTO = "pref_pic";

    private static final String USERID = "pref_userid";
    private static final String UserType = "pref_usertype";
    private static final String TOKEN = "pref_token";
    private static final String ORIGINALPATH = "pref_originalPath";

    private static final String SIGNUP_DONE = "pref_sign_up_done";
    private static final String LOGIN_DONE = "pref_logindone";

    private static final String PROFILE_EDITED = "pref_profilredited";
    private static final String IS_RATED = "pref_israted";
    private static final String RATE_COUNTER = "pref_rateCounter";
    private static final String SUBJECT_TITLR = "pref_sub_title";

    private static final String ACTIVATION_CODE = "pref_activation_code_nV";
    private static final String ACTIVATION_CODE_COUNT = "pref_activation_code_count_nV";

    public static final String APP_LAUNCHED = "appLaunched";

    public static final String IS_USER_SYNCED = "is_user_sync";

    public static final String IS_MCQ_DB_COPIED = "isMcqDbCopied";

    public static final String MAC_ID = "MAC_ID";
    public static final String Standard_ID = "Standard_ID";

    public static SharedPreferences getPref(Context context) {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    /*
    *used for knowing weather user is using the app for the first time
    */
    public static void setLanguage(Context context, String value) {
        getPref(context).edit().putString(LANGUAGE, value).apply();
    }

    public static String getLanguage(Context context) {
        return getPref(context).getString(LANGUAGE, "");
    }


    /*
    *used for knowing weather user is using the app for the first time
    */
    public static void setSubject(Context context, String value) {
        getPref(context).edit().putString(SUBJECT, value).apply();
    }

    public static String getSubject(Context context) {
        return getPref(context).getString(SUBJECT, "");
    }

    public static void setSubjectId(Context context, String value) {
        getPref(context).edit().putString(SubjectId, value).apply();
    }

    public static String getSubjectId(Context context) {
        return getPref(context).getString(SubjectId, "");
    }

    /*
   *used for knowing weather user is using the app for the first time
   */
    public static void setChapter(Context context, String value) {
        getPref(context).edit().putString(CHAPTER, value).apply();
    }

    public static String getChapter(Context context) {
        return getPref(context).getString(CHAPTER, "");
    }

    public static void setChapterId(Context context, String value) {
        getPref(context).edit().putString(ChapterId, value).apply();
    }

    public static String getChapterId(Context context) {
        return getPref(context).getString(ChapterId, "");
    }

    /*
   *used for knowing weather user is using the app for the first time
   */
    public static void setStandard(Context context, String value) {
        getPref(context).edit().putString(STANDARD, value).apply();
    }

    public static String getStandard(Context context) {
        return getPref(context).getString(STANDARD, "");
    }


    /*
  *used for knowing weather user has seen the disclaimer for the first time
  */
    public static void setDisclaimer(Context context, boolean value) {
        getPref(context).edit().putBoolean(DISCLAIMER, value).apply();
    }

    public static boolean getDisclaimer(Context context) {
        return getPref(context).getBoolean(DISCLAIMER, false);
    }


    /*check if parse push registered*/

    public static void setParsePush(Context context, boolean value) {

        getPref(context).edit().putBoolean(IS_PARSE_PUSH_DONE, value).apply();
    }

    public static boolean getParsePush(Context context) {

        return getPref(context).getBoolean(IS_PARSE_PUSH_DONE, false);
    }

    /*is login done from fb*/

    public static void setLoginViaFB(Context context, boolean value) {

        getPref(context).edit().putBoolean(FROM_FB, value).apply();
    }

    public static boolean getLoginViaFB(Context context) {

        return getPref(context).getBoolean(FROM_FB, false);
    }



    /*set fb id*/

    public static void setFBId(Context context, String value) {

        getPref(context).edit().putString(FB_ID, value).apply();
    }

    public static String getFBId(Context context) {

        return getPref(context).getString(FB_ID, "");
    }

    /*set first name*/

    public static void setName(Context context, String value) {

        getPref(context).edit().putString(NAME, value).apply();
    }

    public static String getName(Context context) {

        return getPref(context).getString(NAME, "");
    }

    /*set MAC_ID*/

    public static void setMacId(Context context, String value) {

        getPref(context).edit().putString(MAC_ID, value).apply();
    }

    public static String getMacId(Context context) {

        return getPref(context).getString(MAC_ID, "");
    }

    /*set Standard_ID*/

    public static void setStandard_ID(Context context, String value) {

        getPref(context).edit().putString(Standard_ID, value).apply();
    }

    public static String getStandard_ID(Context context) {

        return getPref(context).getString(Standard_ID, "");
    }


    /*set email*/

    public static void setEmail(Context context, String value) {

        getPref(context).edit().putString(EMAIL, value).apply();
    }

    public static String getEmail(Context context) {

        return getPref(context).getString(EMAIL, "");
    }

    public static void setSchoolId(Context context, String value) {

        getPref(context).edit().putString(SchoolId, value).apply();
    }

    public static String getSchoolId(Context context) {

        return getPref(context).getString(SchoolId, "");
    }

    public static void setCourseId(Context context, String value) {

        getPref(context).edit().putString(CourseId, value).apply();
    }

    public static String getCourseId(Context context) {

        return getPref(context).getString(CourseId, "");
    }

    public static void setOriginalPath(Context context, String value) {

        getPref(context).edit().putString(ORIGINALPATH, value).apply();
    }

    public static String getOriginalpath(Context context) {

        return getPref(context).getString(ORIGINALPATH, "");
    }

    /*set email*/

    public static void setMobile(Context context, String value) {

        getPref(context).edit().putString(MOBILE, value).apply();
    }

    public static String getMobile(Context context) {

        return getPref(context).getString(MOBILE, "");
    }

    /*set email*/

    public static void setPhoto(Context context, String value) {

        getPref(context).edit().putString(PHOTO, value).apply();
    }

    public static String getPhoto(Context context) {

        return getPref(context).getString(PHOTO, "");
    }

    /*
  *get user id
  */
    public static void setUserid(Context context, String value) {
        getPref(context).edit().putString(USERID, value).apply();
    }

    public static String getUserid(Context context) {
        return getPref(context).getString(USERID, "");
    }

    public static void setUserType(Context context, String value) {
        getPref(context).edit().putString(UserType, value).apply();
    }

    public static String getUserType(Context context) {
        return getPref(context).getString(UserType, "");
    }

    public static void setSubjectTitle(Context context, String value) {
        getPref(context).edit().putString(SUBJECT_TITLR, value).apply();
    }

    public static String getSubjectTitle(Context context) {
        return getPref(context).getString(SUBJECT_TITLR, "");
    }

    public static void setPassword(Context context, String value) {
        getPref(context).edit().putString(PASSWORD, value).apply();
    }

    public static String getPassword(Context context) {
        return getPref(context).getString(PASSWORD, "");
    }

    /*
 *get toekn
 */
    public static void setToken(Context context, String value) {
        getPref(context).edit().putString(TOKEN, value).apply();
    }

    public static String getToken(Context context) {
        return getPref(context).getString(TOKEN, "");
    }

    /*
 *used for knowing weathersign up was done or nor
 */
    public static void setSignupDone(Context context, boolean value) {
        getPref(context).edit().putBoolean(SIGNUP_DONE, value).apply();
    }

    public static boolean getSignupDone(Context context) {
        return getPref(context).getBoolean(SIGNUP_DONE, false);
    }

    /*
*used for knowing weather login up was done or nor
*/
    public static void setLoginDone(Context context, boolean value) {
        getPref(context).edit().putBoolean(LOGIN_DONE, value).apply();
    }

    public static boolean getLoginDone(Context context) {
        return getPref(context).getBoolean(LOGIN_DONE, false);
    }


    /*
*used for knowing weather login up was done or nor
*/
    public static void setContinueAsGuest(Context context, boolean value) {
        getPref(context).edit().putBoolean("cag", value).apply();
    }

    public static boolean getContinueAsGuest(Context context) {
        return getPref(context).getBoolean("cag", false);
    }

    /*
*used for knowing weather login up was done or nor
*/
    public static void setProfileEdited(Context context, boolean value) {
        getPref(context).edit().putBoolean(PROFILE_EDITED, value).apply();
    }

    public static boolean getProfileEdited(Context context) {
        return getPref(context).getBoolean(PROFILE_EDITED, false);
    }

    public static void setIsRated(Context context, boolean value) {
        getPref(context).edit().putBoolean(IS_RATED, value).apply();
    }

    public static boolean getIsRated(Context context) {
        return getPref(context).getBoolean(IS_RATED, false);
    }


    public static void setRateCounter(Context context, int value) {
        getPref(context).edit().putInt(RATE_COUNTER, value).apply();
    }

    public static int getRateCounter(Context context) {
        return getPref(context).getInt(RATE_COUNTER, 0);

    }

    /*setting the activation code*/
    public static void setActivationCode(Context context, Set<String> value) {
        getPref(context).edit().putStringSet(ACTIVATION_CODE, value).apply();
    }

    /*getting activation code*/
    public static Set<String> getActivationCode(Context context) {
        return getPref(context).getStringSet(ACTIVATION_CODE, null);
    }

    /*setting the activation code count*/
    public static void setActivationCodeCount(Context context, int value) {
        getPref(context).edit().putInt(ACTIVATION_CODE_COUNT, value).apply();
    }

    /*getting activation code count*/
    public static int getActivationCodeCount(Context context) {
        return getPref(context).getInt(ACTIVATION_CODE, 0);
    }

    public static void setAppLaunched(Context context, boolean isAppLaunched) {
        getPref(context).edit().putBoolean(APP_LAUNCHED, isAppLaunched).apply();
    }

    public static boolean isAppLaunched(Context context) {
        return getPref(context).getBoolean(APP_LAUNCHED, false);
    }

    public static void setUserSync(Context context, boolean isUserSynced) {
        getPref(context).edit().putBoolean(IS_USER_SYNCED, isUserSynced).apply();
    }

    public static boolean isUserSynced(Context context) {
        return getPref(context).getBoolean(IS_USER_SYNCED, false);
    }

    public static long getLastSyncedMcqId(Context context) {
        return getPref(context).getLong(Constants.LAST_SYNCED_MCQ_ID, -1);
    }

    public static void setLastSyncedMcqId(Context context, Long value) {
        getPref(context).edit().putLong(Constants.LAST_SYNCED_MCQ_ID, value).apply();
    }

    public static long getLastSyncedKefCornerId(Context context) {
        return getPref(context).getLong(Constants.LAST_SYNCED_KefCorner_ID, -1);
    }

    public static void setLastSyncedKefCornerId(Context context, Long value) {
        getPref(context).edit().putLong(Constants.LAST_SYNCED_KefCorner_ID, value).apply();
    }

    public static long getLastSyncedLoginHistoryId(Context context) {
        return getPref(context).getLong(Constants.LAST_SYNCED_LOGIN_HISTORY_ID, -1);
    }

    public static void setLastSyncedLoginHistoryId(Context context, Long value) {
        getPref(context).edit().putLong(Constants.LAST_SYNCED_LOGIN_HISTORY_ID, value).apply();
    }
}
