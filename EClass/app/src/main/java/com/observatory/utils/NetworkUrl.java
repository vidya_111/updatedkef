package com.observatory.utils;

/**
 * Created by Anuj on 06-01-2016.
 */
public class NetworkUrl {

    public static final String KEF_Localhost = "http://eclassapp.com/testkefmobile1.0/web1/"; //http://192.168.1.73/mobile1.0/web/ //http://eclassapp.com/testkefmobile1.0/web1/


    public static final String host = "http://eclassapp.com/testkefmobile1.0/web1/"; //http://eclassapp.com/mobile1.0/web/
    public static final String host1 = "http://eclassapp.com/testkefmobile1.0/web/";
    public static final String login = "login.php";
    //    public static final String signUp = "signUp.php";
    public static final String signUp = "signUp-v1.php";
    public static final String facebook = "continueWithFb.php";
    public static final String changePwd = "changePassword.php";
    public static final String forgotPassword = "forgotPassword.php";
    public static final String editProfile = "editProfile.php";

    //    public static final String statisticsEmailer = host + "statisticsEmailer.php";
    public static final String statisticsEmailer = KEF_Localhost + "statisticsEmailer_v3.php";

    //    public static final String userHistoryEmailer = host + "userHistoryEmailer.php";
//    public static final String userHistoryEmailer = host + "userHistoryEmailer_v2.php";
    public static final String userHistoryEmailer = KEF_Localhost + "userHistoryEmailer_v4.php";
    public static final String activation_code_url = "activation_code.php";
    public static String sundaram_eclass_youtube_videos = "https://www.googleapis.com/youtube/v3/playlistItems";
    public static final String syncStatistics = host + "syncStatistics_v2.php";
    public static final String syncLoginHistory = KEF_Localhost + "userHistoryAutoSync.php";

    // http://www.eclassapp.com/mobile1.0/web/userHistoryEmailer.php

    //statisticsEmailer.php

    // KEF Corner Api //

    public static final String KEF_Open = "https://eclasskef.in/"; //http://eclasskef.in/
    public static final String KEF_Host = "https://eclasskef.in/api/v1/"; //http://203.212.222.191:9544/api/v1/ //http://eclasskef.in/api/v1/
    public static final String KEF_GetSchool = "School/GetSchool";
    public static final String KEF_GetCourse = "Course/GetCourse";
    public static final String KEF_GetMessage = "Message/GetByUserId";
    public static final String KEF_AddDoubt = "Doubtbox/Add";
    public static final String KEF_GetByUserId = "Doubtbox/GetByUserID";
    public static final String KEF_GetByDoubtId = "Doubtbox/GetByDoubtID";
    public static final String KEF_UpdateDoubt = "Doubtbox/Update";
    public static final String KEF_GetGallery = "Gallery/GetByUserID";
    public static final String KEF_AddGallery = "Gallery/AddActivity";
    public static final String KEF_SIgnUp = "Kef_Signupv1.php";
    public static final String KEF_Login = "Kef_Loginv1.php";
    public static final String KEF_SyncStatistics = "Kef_SyncStatisticsv1.php";
    public static final String KEF_insertMcqData = "api/Assessment/InsertUpdateMCQTestResultDetails";
    public static final String KEF_mcqDataHost = "http://eclasswindows.in//";
    public static final String KEF_DeleteDoubt = "Doubtbox/Delete";
    public static final String DownloadAssessment = "http://eclasswindows.in//api/Assessment/GetAssessmentProductDBDetails";
    public static final String DownloadDbPath = "http://eclasswindows.in//";

}
