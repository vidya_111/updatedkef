package com.observatory.utils;

import android.content.Context;
import android.os.Build;
import android.os.Environment;

import androidx.core.os.EnvironmentCompat;
import androidx.multidex.MultiDex;

import android.os.storage.StorageManager;
import android.util.Log;
import android.widget.Toast;

import com.facebook.FacebookSdk;
//import com.onesignal.OneSignal;
import com.observatory.AsyncTaskMain.SyncDataHelper;
import com.orm.SugarApp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.Timer;

import io.realm.Realm;

import static com.observatory.utils.StorageUtils.getStorageList;

/**
 * Created by rohit on 23/01/15.
 */
public class App extends SugarApp implements ActivityLifecycleHandler.LifecycleListener {
    private static final String TAG = "App";

    public static String SDCARD_PATH;
    /**
     * Name of root folder
     */
    public static String BASE_LOCATION = ".Sundaram";
    public static String Card_Path = "";
    /**
     * Path to root folder is present
     */
    public static String BASE_PATH = "";
    public static String pathIssuer = "";
    public static String ORIGINAL_PATH = "";
    public static boolean IS_HAVING_MULTIPLE_STANDARD = false;
    String externalpath = new String();
    String internalpath = new String();
    private String tag = "App";
    List<StorageUtils.StorageInfo> list;
    private static App mInstance;
    public static boolean IS_USER_HISTORY_SYNCED = false;

    private static final String APPLICATION_ID = "9ZjYbJZFOg2qf6axLcSrjMDJEREe56LRIWBYD7zG";
    private String CLIENT_ID = "acjqgt4YZ2wDhrouXT3QIIy7rlcis75jgZJHt1Cc";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static synchronized App getInstance() {
        return mInstance;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        //initialize realm
        Realm.init(this);

        //initializeParse();

        mInstance = this;
        /*OneSignal.startInit(this).init();
        //OneSignal.setLogLevel(OneSignal.LOG_LEVEL.DEBUG, OneSignal.LOG_LEVEL.DEBUG);*/

        // Initiate OneSignal and customizing how Push notifications are handled.
        /*OneSignal.startInit(this).
                setNotificationOpenedHandler(new PushOpenedHandler(this)).
                setNotificationReceivedHandler(new PushReceivedHandler(this)).
                inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification).
                init();*/

        initializeFB();
        getExternalMounts();
        initializeAnalytics();
//        getRemovableStorage();
        list = getStorageList();
//        //Toast.makeText(this, list.toString(), Toast.LENGTH_SHORT).show();
        //SDCARD_PATH = externalStorage1();

        // Toast.makeText(this, SDCARD_PATH, Toast.LENGTH_SHORT).show();

        //BASE_PATH = SDCARD_PATH + BASE_LOCATION + "/";


        Log.d("VersionChecker: ", "CurrentVersion: " + Build.VERSION.SDK_INT);
        //Log.d("externalLocations: ",Arrays.toString(getExternalStorageDirectories()));
            /*final List<String> pathHere =  Arrays.asList(getExternalStorageDirectories());
            Log.d("externalLocations: ",getExternalStoragePath(getApplicationContext(), true));
            for (int i = 0; i < pathHere.size(); i++){
                BASE_PATH = pathHere.get(i)+ "/"+BASE_LOCATION + "/";
                ORIGINAL_PATH = BASE_PATH;
                Card_Path = pathHere.get(i);
                Log.d("PrintValues","App3 "+"sdcard_path: "+BASE_PATH);
            }*/

        BASE_PATH = getExternalStoragePath(getApplicationContext(), true) + "/" + BASE_LOCATION + "/";
        ORIGINAL_PATH = BASE_PATH;
        Card_Path = getExternalStoragePath(getApplicationContext(), true);

        //BASE_PATH = SDCARD_PATH;
        //   Log.v(tag,"PATH: "+BASE_PATH);
        Random random = new Random();

        int i = random.nextInt(100 - 90) + 90;
        i = (i / 45) * 4;
        pathIssuer = String.valueOf(i) + String.valueOf(i) + String.valueOf(i + 1) + "thA";

        // Register a lifecycle handler to track the foreground/background state
        registerActivityLifecycleCallbacks(new ActivityLifecycleHandler(this));

        SyncDataHelper myTask = new SyncDataHelper(this);
        Timer myTimer = new Timer();
        myTimer.schedule(myTask, 900000, 900000);
    }

    public String[] getExternalStorageDirectories() {

        List<String> results = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) { //Method 1 for KitKat & above
            File[] externalDirs = getApplicationContext().getExternalFilesDirs(null);
            String internalRoot = Environment.getExternalStorageDirectory().getAbsolutePath().toLowerCase();

            for (File file : externalDirs) {
                if (file == null) //solved NPE on some Lollipop devices
                    continue;
                String path = file.getPath().split("/Android")[0];

                if (path.toLowerCase().startsWith(internalRoot))
                    continue;

                boolean addPath = false;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    addPath = Environment.isExternalStorageRemovable(file);
                } else {
                    addPath = Environment.MEDIA_MOUNTED.equals(EnvironmentCompat.getStorageState(file));
                }

                if (addPath) {
                    results.add(path);
                }
            }
        }

        if (results.isEmpty()) { //Method 2 for all versions
            String output = "";
            try {
                final Process process = new ProcessBuilder().command("mount | grep /dev/block/vold")
                        .redirectErrorStream(true).start();
                process.waitFor();
                final InputStream is = process.getInputStream();
                final byte[] buffer = new byte[1024];
                while (is.read(buffer) != -1) {
                    output = output + new String(buffer);
                }
                is.close();
            } catch (final Exception e) {
                e.printStackTrace();
            }
            if (!output.trim().isEmpty()) {
                String devicePoints[] = output.split("\n");
                for (String voldPoint : devicePoints) {
                    results.add(voldPoint.split(" ")[2]);
                }
            }
        }

        //Below few lines is to remove paths which may not be external memory card, like OTG (feel free to comment them out)
        /*if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (int i = 0; i < results.size(); i++) {
                if (!results.get(i).toLowerCase().matches(".*[0-9a-f]{4}[-][0-9a-f]{4}")) {
                    Log.d("path1", results.get(i) + " might not be extSDcard");
                    results.remove(i--);
                }
            }
        } else {
            for (int i = 0; i < results.size(); i++) {
                if (!results.get(i).toLowerCase().contains("ext") && !results.get(i).toLowerCase().contains("sdcard")) {
                    Log.d("path1", results.get(i)+" might not be extSDcard");
                    results.remove(i--);
                }
            }
        }*/

        for (int i = 0; i < results.size(); i++) {
            if (!results.get(i).toLowerCase().matches(".*[0-9a-f]{4}[-][0-9a-f]{4}")) {
                Log.d("path1", results.get(i) + " might not be extSDcard");
                results.remove(i--);
            }
        }

        String[] storageDirectories = new String[results.size()];
        if (storageDirectories != null) {
            for (int i = 0; i < results.size(); ++i) storageDirectories[i] = results.get(i);
        } else {
            storageDirectories = new String[]{String.valueOf('/')};
        }
        Log.d("storageDirectories:", Arrays.toString(storageDirectories));
        return storageDirectories;
    }

    protected String externalStorage1() {

        File storage = new File("/storage");
        String path = "";


        //File storage = Environment.getExternalStorageDirectory().getAbsoluteFile();

        File file[] = storage.listFiles();
        //Toast.makeText(this, file.toString(), Toast.LENGTH_SHORT).show();

        if (file != null) {

            for (int i = 0; i < file.length; i++) {
                //  Log.v("Files", "FileName:" + file[i].getName());
                // Toast.makeText(this, "for loop 1", Toast.LENGTH_SHORT).show();

                File file1 = new File("/storage/" + file[i].getName() + "/");

                File files2[] = file1.listFiles();
                // Toast.makeText(this, files2.toString(), Toast.LENGTH_SHORT).show();

                if (files2 != null) {
                    for (int j = 0; j < files2.length; j++) {
                        // Toast.makeText(this, "for loop 2", Toast.LENGTH_SHORT).show();
                        if (files2[j].toString().equals(file1.getAbsolutePath() + "/" + BASE_LOCATION)) {

                            path = file1.getAbsolutePath() + "/" + BASE_LOCATION + "/";
                            return path;
                        }
                    }
                } else {

                    path = "";

                }


            }
        }

        if (path.equals("")) {


            String test[] = externalpath.split("\n");
            String path1 = test[test.length - 1];
            path1 = path1.replace("*", "");


            if (externalpath.contains("fat")) {
                storage = Environment.getExternalStorageDirectory().getAbsoluteFile();
            } else {
                storage = new File(path1);
                //storage = new File(list.get(0).path);
                //storage = new File("/storage");
            }

            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    //  Log.v("Files", "FileName:" + file[i].getName());
                    // Toast.makeText(this, "for loop 3", Toast.LENGTH_SHORT).show();

                    String fileValue;
                    fileValue = files[i].toString();
                    if (externalpath.contains("fat")) {


                        if (fileValue.equals(storage.getPath() + "/" + BASE_LOCATION)) {
                            path = storage.getPath() + "/" + files[i].getName();
                        }
                    } else {
                        if (fileValue.equals(path1 + "/" + BASE_LOCATION)) {
                            path = path1 + "/" + files[i].getName();
                        }
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                }
            } else {

                path = "";

            }


        }

        if (path.equals("")) {

//            String test[] = externalpath.split("\n");
//            String path1 = test[test.length - 1];
//            path1 = path1.replace("*", "");
            String path1;
            if (list.size() > 0) {

                path1 = list.get(list.size() - 1).path;
            } else {
                path1 = "";
            }


            if (externalpath.contains("fat")) {
                storage = Environment.getExternalStorageDirectory().getAbsoluteFile();
            } else {
                storage = new File(path1);
                //storage = new File(list.get(0).path);
                //storage = new File("/storage");
            }

            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    // Toast.makeText(this, "for loop 4", Toast.LENGTH_SHORT).show();
                    //  Log.v("Files", "FileName:" + file[i].getName());

                    String fileValue;
                    fileValue = files[i].toString();
                    if (externalpath.contains("fat")) {


                        if (fileValue.equals(storage.getPath() + "/" + BASE_LOCATION)) {
                            path = storage.getPath() + "/" + files[i].getName();
                        }
                    } else {
                        if (fileValue.equals(path1 + "/" + BASE_LOCATION)) {
                            path = path1 + "/" + files[i].getName();
                        }
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                }
            }

        }

        if (path.equals("")) {

            String path1 = "";


            for (int k = 0; k < list.size(); k++) {

                path1 = list.get(k).path;


                storage = new File(path1);

                File files[] = storage.listFiles();

                if (files != null) {
                    for (int i = 0; i < files.length; i++) {

                        // Toast.makeText(this, "for loop 5", Toast.LENGTH_SHORT).show();
                        //  Log.v("Files", "FileName:" + file[i].getName());

                        String fileValue;
                        fileValue = files[i].toString();

                        if (fileValue.equals(path1 + "/" + BASE_LOCATION)) {
                            path = path1 + "/" + files[i].getName();
                            break;
                        }


                    }

                    if (new File(path).exists()) {

                        return path + "/";

                    }
                } else {

                    path = "";

                }

            }


        }

        if (path.equals("")) {

            String path1 = "mnt/sd-ext/";
            storage = new File(path1);
            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {


                    String fileValue;
                    fileValue = files[i].toString();

                    if (fileValue.equals(path1 + BASE_LOCATION)) {
                        path = path1 + "/" + files[i].getName();
                        break;
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                }
            } else {

                return "/";

            }

        }

        if (path.equalsIgnoreCase("")) {


            String path1 = "mnt/usb_storage/USB_DISK0/udisk0";
            storage = new File(path1);
            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {


                    String fileValue;
                    fileValue = files[i].toString();

                    if (fileValue.equals(path1 + BASE_LOCATION)) {
                        path = path1 + "/" + files[i].getName();
                        break;
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                }
            } else {

                return "/";

            }

        }

        return "/";


    }

    /**
     * To delete all temp files
     * @author Vidya
     */
    public void deleteTemp() {
        try {
            File temp = new File(getExternalFilesDir(null), "temp/");
            File[] fileList = temp.listFiles();
            if (fileList != null) {
                Log.d("VideoLogChecker", "onPause1: " + "fileList: IsNotNull: length: ");
                for (int i = 0; i < fileList.length; i++) {
                    //Log.d("VideoLogChecker","onPause1: "+"fileList: IsNotNull: length: "+fileList[i].getAbsolutePath());
                    if (fileList[i].getAbsolutePath().contains(".mp4")) {
                        fileList[i].delete();
                    }
                }
            }


            File temp1 = new File(Environment.getExternalStorageDirectory(), "temp/");
            File[] fileList1 = temp1.listFiles();

            if (fileList1 != null) {
                Log.d("VideoLogChecker", "onPause2: " + "fileList: IsNotNull: length: ");
                for (int i = 0; i < fileList1.length; i++) {
                    //Log.d("VideoLogChecker","onPause2: "+"videoPath: "+fileList[i].getAbsolutePath());
                    if (fileList1[i].getAbsolutePath().contains(".mp4") || fileList1[i].getAbsolutePath().contains(".xls")) {
                        fileList1[i].delete();
                    }
                }
            }
        } catch (Exception e) {

        }
    }


    public void getExternalMounts() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process proc = runtime.exec("mount");
            InputStream is = proc.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            String line;

            BufferedReader br = new BufferedReader(isr);
            while ((line = br.readLine()) != null) {
                if (line.contains("secure")) continue;
                if (line.contains("asec")) continue;

                if (line.contains("fat")) {//external card
                    String columns[] = line.split(" ");
                    if (columns != null && columns.length > 1) {
                        externalpath = externalpath.concat("*" + columns[1] + "\n");
                    }
                } else if (line.contains("fuse")) {//internal storage
                    String columns[] = line.split(" ");
                    if (columns != null && columns.length > 1) {
                        internalpath = internalpath.concat(columns[1] + "\n");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Path  of sd card external............" + externalpath);
        System.out.println("Path  of internal memory............" + internalpath);
    }

    /**
     * Get external sd card path using reflection
     *
     * @param mContext
     * @param is_removable is external storage removable
     * @return
     */
    private static String getExternalStoragePath(Context mContext, boolean is_removable) {

        StorageManager mStorageManager = (StorageManager) mContext.getSystemService(Context.STORAGE_SERVICE);
        Class<?> storageVolumeClazz = null;
        try {
            storageVolumeClazz = Class.forName("android.os.storage.StorageVolume");
            Method getVolumeList = mStorageManager.getClass().getMethod("getVolumeList");
            Method getPath = storageVolumeClazz.getMethod("getPath");
            Method isRemovable = storageVolumeClazz.getMethod("isRemovable");
            Object result = getVolumeList.invoke(mStorageManager);
            final int length = Array.getLength(result);
            for (int i = 0; i < length; i++) {
                Object storageVolumeElement = Array.get(result, i);
                String path = (String) getPath.invoke(storageVolumeElement);
                boolean removable = (Boolean) isRemovable.invoke(storageVolumeElement);
                if (is_removable == removable) {
                    return path;
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void initializeParse() {

        // Parse.initialize(this, APPLICATION_ID, CLIENT_ID);

    }

    private void initializeFB() {

        FacebookSdk.sdkInitialize(getApplicationContext());
    }

    private void initializeAnalytics() {

        AnalyticTracker.initialize(this);

        // AnalyticTracker.getInstance().get(AnalyticTracker.Target.APP);
    }

//    public synchronized Tracker getGoogleAnalyticsTracker() {
//        AnalyticTracker analyticsTrackers = AnalyticTracker.getInstance();
//        return analyticsTrackers.get(AnalyticTracker.Target.APP);
//    }

    /***
     * Tracking screen view
     *
     * @param screenName screen name to be displayed on GA dashboard
     */
    public void trackScreenView(String screenName) {
//        Tracker t = getGoogleAnalyticsTracker();
//
//        // Set screen name.
//        t.setScreenName(screenName);
//
//        // Send a screen view.
//        t.send(new HitBuilders.ScreenViewBuilder().build());
//
//        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    /***
     * Tracking exception
     *
     * @param e exception to be tracked
     */
    public void trackException(Exception e) {
//        if (e != null) {
//            Tracker t = getGoogleAnalyticsTracker();
//
//            t.send(new HitBuilders.ExceptionBuilder()
//                    .setDescription(
//                            new StandardExceptionParser(this, null)
//                                    .getDescription(Thread.currentThread().getName(), e))
//                    .setFatal(false)
//                    .build()
//            );
//        }
    }

    /***
     * Tracking event
     *
     * @param category event category
     * @param action   action of the event
     * @param label    label
     * @param value    set extra value
     */
    public void trackEvent(String category, String action, String label, String value) {
//        Tracker t = getGoogleAnalyticsTracker();
//        t.set("&uid", SettingPreffrences.getUserid(this));
//        // Build and send an Event.
//        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).set("value", value).build());
    }

    @Override
    public void onApplicationStopped() {
        Log.d(TAG, "onApplicationStopped");
        SettingPreffrences.setAppLaunched(getApplicationContext(), false);
    }


    @Override
    public void onApplicationStarted() {

        Log.d(TAG, "onApplicationStarted");
        SettingPreffrences.setUserSync(getApplicationContext(), false);
        SettingPreffrences.setAppLaunched(getApplicationContext(), true);
    }


    @Override
    public void onApplicationPaused() {
        Log.d(TAG, "onApplicationPaused");
    }

    @Override
    public void onApplicationResumed() {
        Log.d(TAG, "onApplicationResumed");
    }
}


class StorageUtils {

    private static final String TAG = "StorageUtils";

    public static class StorageInfo {

        public final String path;
        public final boolean internal;
        public final boolean readonly;
        public final int display_number;

        StorageInfo(String path, boolean internal, boolean readonly, int display_number) {
            this.path = path;
            this.internal = internal;
            this.readonly = readonly;
            this.display_number = display_number;
        }

        public String getDisplayName() {
            StringBuilder res = new StringBuilder();
            if (internal) {
                res.append("Internal SD card");
            } else if (display_number > 1) {
                res.append("SD card " + display_number);
            } else {
                res.append("SD card");
            }
            if (readonly) {
                res.append(" (Read only)");
            }
            return res.toString();
        }
    }


    public static List<StorageInfo> getStorageList() {

        List<StorageInfo> list = new ArrayList<StorageInfo>();
        String def_path = Environment.getExternalStorageDirectory().getPath();
        boolean def_path_internal = !Environment.isExternalStorageRemovable();
        String def_path_state = Environment.getExternalStorageState();
        boolean def_path_available = def_path_state.equals(Environment.MEDIA_MOUNTED)
                || def_path_state.equals(Environment.MEDIA_MOUNTED_READ_ONLY);
        boolean def_path_readonly = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY);
        BufferedReader buf_reader = null;
        try {
            HashSet<String> paths = new HashSet<String>();
            buf_reader = new BufferedReader(new FileReader("/proc/mounts"));
            String line;
            int cur_display_number = 1;
            Log.d(TAG, "/proc/mounts");
            while ((line = buf_reader.readLine()) != null) {
                Log.d(TAG, line);
                if (line.contains("vfat") || line.contains("/mnt")) {
                    StringTokenizer tokens = new StringTokenizer(line, " ");
                    String unused = tokens.nextToken(); //device
                    String mount_point = tokens.nextToken(); //mount point
                    if (paths.contains(mount_point)) {
                        continue;
                    }
                    unused = tokens.nextToken(); //file system
                    List<String> flags = Arrays.asList(tokens.nextToken().split(",")); //flags
                    boolean readonly = flags.contains("ro");

                    if (mount_point.equals(def_path)) {
                        paths.add(def_path);
                        list.add(0, new StorageInfo(def_path, def_path_internal, readonly, -1));
                    } else if (line.contains("/dev/block/vold")) {
                        if (!line.contains("/mnt/secure")
                                && !line.contains("/mnt/asec")
                                && !line.contains("/mnt/obb")
                                && !line.contains("/dev/mapper")
                                && !line.contains("tmpfs")) {
                            paths.add(mount_point);
                            list.add(new StorageInfo(mount_point, false, readonly, cur_display_number++));
                        }
                    }
                }
            }

            if (!paths.contains(def_path) && def_path_available) {
                list.add(0, new StorageInfo(def_path, def_path_internal, def_path_readonly, -1));
            }

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (buf_reader != null) {
                try {
                    buf_reader.close();
                } catch (IOException ex) {
                }
            }
        }
        return list;
    }



}



