package com.observatory.utils;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.observatory.mcqdatabase.DatabaseHelper;
import com.observatory.mcqdatabase.DatabaseManager;
import com.observatory.kefcorner.Splash;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;

import java.io.File;
import java.util.Random;

/**
 * Created by rohit on 20/01/15.
 */
public abstract class BaseActivity_og extends AppCompatActivity {

    protected androidx.appcompat.app.ActionBar actionBar;
    protected String tag;
    protected Activity activity;
    protected Typeface brandonBold, brandonReg;
    protected DisplayMetrics displayMetrics;


    // protected Master master;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);


        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        activity = this;
        tag = activity.getLocalClassName();
        displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        createFonts();
        initMcqDatabase();

    }

    private void initMcqDatabase() {
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        DatabaseManager.initializeInstance(databaseHelper);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void createFonts() {

        brandonBold = Typeface.createFromAsset(getAssets(), "fonts/Brandon_bld.otf");
        brandonReg = Typeface.createFromAsset(getAssets(), "fonts/Brandon_reg.otf");


    }


    protected String decryptZip(String vidPath, boolean isXl) throws ZipException {

        String tempPath;

        String ebola = "fggd8348a3rfGS4rs!QI";
        Random random = new Random();
        String password = Splash.hexEncoded + ebola.substring(7, random.nextInt(3 - 2) + 12).toLowerCase() + App.pathIssuer;


        File temp = new File(Environment.getExternalStorageDirectory(), "temp/");
        tempPath = temp.getAbsolutePath();
        String unzippedPath;

        ZipFile zipFile = new ZipFile(vidPath);
        FileHeader fileHeader = null;
        try {
            fileHeader = (FileHeader) zipFile.getFileHeaders().get(0);
        } catch (ZipException e) {
            Log.d("decryptZip: ", e.getMessage());
            decryptZip(App.BASE_PATH + "Data-Format.zip", isXl);
        }

        if (zipFile.isEncrypted()) {
            // zipFile.setPassword(Video.password);
            System.out.println("The password is" + password);
            zipFile.setPassword(password);
        }
        System.out.println("I came here - password right");
        zipFile.extractAll(tempPath);
        if (isXl) {
            unzippedPath = tempPath + "/" + fileHeader.getFileName();
        } else {
            unzippedPath = tempPath + "/" + "video.mp4";
        }

        // Log.v(tag,"decryptZip: "+unzippedPath);

        return unzippedPath;


    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

        onLayout();

    }

    protected abstract void onLayout();


    protected void startActivity(Class clazz) {
        Intent i = new Intent(this, clazz);
        startActivity(i);
    }


    protected void alert(String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        })
                .setMessage(message)
                .setTitle("Eclass")
                .show();
    }


    protected void alert(String message, DialogInterface.OnClickListener listener) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).setPositiveButton("Ok",
                listener)
                .setMessage(message)
                .setTitle("Eclass")
                .show();
    }

    protected void alert(String message, DialogInterface.OnClickListener posLis,
                         DialogInterface.OnClickListener negLis) {

        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setPositiveButton("Retry", posLis)
                .setNegativeButton("Exit", negLis)
                .setMessage(message)
                .setTitle("Eclass")
                .show();

    }

    protected void alert(String message, String pBtName, String nBtName, DialogInterface.OnClickListener posLis,
                         DialogInterface.OnClickListener negLis) {

        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setPositiveButton(pBtName, posLis)
                .setNegativeButton(nBtName, negLis)
                .setMessage(message)
                .setTitle("Eclass")
                .show();

    }

    protected int getDeviceHeight() {

        return displayMetrics.heightPixels;

    }

    protected int getDeviceWidth() {

        return displayMetrics.widthPixels;

    }

    protected static String getUserId(Context context) {

        if (TextUtils.isEmpty(SettingPreffrences.getUserid(context))) {

            return "0";
        } else {

            return SettingPreffrences.getUserid(context);
        }

    }
}
