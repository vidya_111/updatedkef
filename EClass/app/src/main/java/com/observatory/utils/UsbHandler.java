package com.observatory.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gaurav on 6/1/18.
 */

public class UsbHandler {

    public static final String PATH_SYS_BUS_USB = "/sys/bus/usb/devices/";

    public static final String SERIAL = "serial";
    public static final String BUSNUM = "busnum";
    public static final String DEVNUM = "devnum";
    private List<UsbDevice> myUsbDevices;

    public UsbHandler() {
        this.myUsbDevices = new ArrayList<>();
    }


    public File[] getListOfChildren(final String path) {

        final File pathAsFile = new File(path);
        final File[] retVal;

        if (pathAsFile.exists()
                && pathAsFile.isDirectory()
                && pathAsFile.listFiles() != null) {
            retVal = pathAsFile.listFiles();
        } else {
            retVal = new File[0];
        }

        return retVal;
    }

    public boolean isValidUsbDevice(final File file) {
        final boolean retVal;

        if (!file.exists()) {
            retVal = false;
        } else if (!file.isDirectory()) {
            retVal = false;
        } else if (".".equals(file.getName()) || "..".equals(file.getName())) {
            retVal = false;
        } else {
            retVal = true;
        }

        return retVal;
    }

    private String read(final File usbDeviceDir, final String propertyName) {
        String basePath = usbDeviceDir.getAbsolutePath() + File.separator;
        final String filePath = basePath + propertyName;

        final File file = new File(filePath);
        final StringBuilder fileContents = new StringBuilder(1000);
        final int bufferSize = 1024;

        if (file.exists() && !file.isDirectory()) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(file));

                char[] buf = new char[bufferSize];
                int numRead = 0;

                while ((numRead = reader.read(buf)) != -1) {
                    String readData = String.valueOf(buf, 0, numRead);
                    fileContents.append(readData);
                    buf = new char[bufferSize];
                }
            } catch (FileNotFoundException e) {
                fileContents.setLength(0);
                e.printStackTrace();
            } catch (IOException e) {
                fileContents.setLength(0);
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return fileContents.toString().trim();
    }


    public List<UsbDevice> getUsbDevices() {
        final File[] children = getListOfChildren(PATH_SYS_BUS_USB);

        UsbDevice usbDevice;
        for (File child : children) {

            if (isValidUsbDevice(child)) {
                File usbDeviceDir = child.getAbsoluteFile();
                usbDevice = new UsbDevice(read(usbDeviceDir, SERIAL), usbDeviceDir.getAbsolutePath());

                myUsbDevices.add(usbDevice);
            }
        }

        return myUsbDevices;
    }
}
