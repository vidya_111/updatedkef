package com.observatory.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.observatory.kefcorner.R;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;


/**
 * Created by Anuj M on 12/11/2014.
 */
public class Network {
    private static String boundary = "*****";
    private static String lineEnd = "\r\n";
    private static String twoHyphens = "--";
    int bytesRead = 0, bytesAvailable = 0, bufferSize;
    int maxBufferSize = 1024;
    byte[] buffer;
    String uploaded_file;
    private HttpURLConnection httpURLConnection = null;
    private URL urlMain;
    private Context context;
    private String response = "";

    private FileInputStream fileInputStream;


    public Network(Context context) {
        this.context = context;
    }

    public String connectToServer(String url, String params, String method) {
        try {

            urlMain = new URL(url);
            httpURLConnection = (HttpURLConnection) urlMain.openConnection();

            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod(method);
            httpURLConnection.setConnectTimeout(context.getResources().getInteger(R.integer.TimeOut));//timeout after waiting for 10 sec


            if (method.equals("POST")) {
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
                if (url.contains("InsertUpdateMCQTestResultDetails")){
                    httpURLConnection.setRequestProperty("Content-Type", "application/json");
                    httpURLConnection.setRequestProperty("Accept", "application/json");
                }else {
                    httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    httpURLConnection.setRequestProperty("charset", "utf-8");
                }
                if(url.contains("Doubtbox") || url.contains("Gallery/AddActivity")){
                    httpURLConnection.setRequestProperty("APIVersion", "1.0.0");
                    httpURLConnection.setRequestProperty("DevicePlatform", "A");
                    httpURLConnection.setRequestProperty("APPVersion", "1.0.0");
                    httpURLConnection.setRequestProperty("APPID", "KEF");
                }

                // get outputStream
                DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
                dataOutputStream.writeBytes(params);
                dataOutputStream.flush();
                dataOutputStream.close();
            }


            if (url.contains("School") || url.contains("Message") || url.contains("Doubtbox/GetByUserID")
                    || url.contains("Doubtbox/GetByDoubtID") || url.contains("GetCourse")
                    || url.contains("Gallery/GetByUserID") || url.contains("Doubtbox/Delete")){

                Log.d("getData","check1"+ "InIf");

                httpURLConnection.setRequestProperty("APIVersion", "1.0.0");
                httpURLConnection.setRequestProperty("DevicePlatform", "A");
                httpURLConnection.setRequestProperty("APPVersion", "1.0.0");
                httpURLConnection.setRequestProperty("APPID", "KEF");
            }

            Log.d("getData","check1"+ "url :"+url+ " responseCode: "+httpURLConnection.getResponseCode()+" :: "
            +httpURLConnection.getResponseMessage()+" "+httpURLConnection.getRequestProperty("APIVersion"));


            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream is = httpURLConnection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuffer sb = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    sb.append(line);
                    sb.append('\r');
                }
                rd.close();
                response = sb.toString();
                Log.d("getData","check2"+ "response :"+response);


            } else {


                response = context.getResources().getString(R.string.Error);

            }

            String cookie = httpURLConnection.getHeaderField("Set-Cookie");

        } catch (MalformedURLException e) {
            e.printStackTrace();
            response = context.getResources().getString(R.string.Error);
        } catch (IOException e) {
            e.printStackTrace();
            response = context.getResources().getString(R.string.Error);

        } catch (IllegalStateException e) {
            Toast.makeText(context, "" + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

        } finally {
            httpURLConnection.disconnect();
        }

        return response;

    }

    public String postRequest(String url, JsonObject params, String method) {
        try {

            urlMain = new URL(url);
            httpURLConnection = (HttpURLConnection) urlMain.openConnection();

            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod(method);
            httpURLConnection.setConnectTimeout(context.getResources().getInteger(R.integer.TimeOut));//timeout after waiting for 10 sec


            if (method.equals("POST")) {
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
                httpURLConnection.setRequestProperty("Content-Type", "application/json");
                httpURLConnection.setRequestProperty("Accept", "application/json");

                // get outputStream
                DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
                dataOutputStream.writeBytes(params.toString());
                dataOutputStream.flush();
                dataOutputStream.close();
            }

            Log.d("getData","check1"+ "url :"+url+ " responseCode: "+httpURLConnection.getResponseCode()+" :: "
                    +httpURLConnection.getResponseMessage());


            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream is = httpURLConnection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuffer sb = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    sb.append(line);
                    sb.append('\r');
                }
                rd.close();
                response = sb.toString();
                Log.d("getData","check3"+ "response :"+response);


            } else {


                response = context.getResources().getString(R.string.Error);

            }

            String cookie = httpURLConnection.getHeaderField("Set-Cookie");

        } catch (MalformedURLException e) {
            e.printStackTrace();
            response = context.getResources().getString(R.string.Error);
        } catch (IOException e) {
            e.printStackTrace();
            response = context.getResources().getString(R.string.Error);

        } catch (IllegalStateException e) {
            Toast.makeText(context, "" + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

        } finally {
            httpURLConnection.disconnect();
        }

        return response;

    }


    public String sendFile(final Context context, final String url, final String params, String fileName, final File f) {
        DataOutputStream dos = null;
        int bytesRead = 0, bytesAvailable = 0, bufferSize;
        int maxBufferSize = 1024;
        byte[] buffer;
        String serverResponseMessage = null;


        URL url1 = null;
        try {

            FileInputStream fileInputStream = new FileInputStream(f);
            url1 = new URL(url + "?" + params);
            HttpURLConnection connection = (HttpURLConnection) url1.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(2000);
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            connection.setRequestProperty("uploaded_file", fileName);
            fileName = f.getName();
            dos = new DataOutputStream(connection.getOutputStream());
            dos.writeBytes(twoHyphens + boundary + lineEnd);



            dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + fileName + "\"" + lineEnd);
            dos.writeBytes(lineEnd);


            bytesAvailable = fileInputStream.available(); // create a buffer of  maximum size

            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // read file and write it into form...
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            // send multipart form data necesssary after file data...
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            dos.close();

            // Responses from the server (code and message)
            int serverResponseCode = connection.getResponseCode();
            serverResponseMessage = connection.getResponseMessage();

            StringBuffer b;
            int ch;
            InputStream is;
            is = connection.getInputStream();
            // retrieve the response from server

            b = new StringBuffer();
            while ((ch = is.read()) != -1) {
                b.append((char) ch);
            }
            response = b.toString();
            Log.d("Response", "" + response);


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("Error message", "" + e);
            response = "error";
        }
        return response;
    }


}
