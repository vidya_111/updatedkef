package com.observatory.mcqui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.observatory.database.Master;
import com.observatory.database.Statistics;
import com.observatory.database.userStatistics;
import com.observatory.mcqmodels.McqContentModel;
import com.observatory.kefcorner.R;
import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.Constants;
import com.observatory.utils.SettingPreffrences;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.observatory.utils.Constants.CONTENT_TYPE_MCQ;
import static com.observatory.utils.Constants.KEF_ContentTypeId_MCQ;
import static com.observatory.utils.Constants.KEF_ContentTypeName_MCQ;


public class MCQResultsActivity extends BaseActivity {
    private final String FORWARD_SLASH = "/";

    private final int ERROR_TIMEOUT_MS = 3000;

    TextView tvYourScore;
    private ArrayList<McqContentModel> attemptedMCQs;

    String englishFont;
    String urduFont;
    String hindiFont;
    String marathiFont;

    private Typeface englishTypeface;
    private Typeface hindiTypeface;
    private Typeface marathiTypeface;
    private Typeface urduTypeface;
    private RecyclerView rvResults;
    private int colorGreen;
    private int colorRed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcq_results);
    }

    @Override
    protected void onLayout() {
        findViews();
        loadColors();
        loadStrings();
        initialize();
    }

    private void loadColors() {
        colorGreen = ContextCompat.getColor(getApplicationContext(), R.color.green1);
        colorRed = ContextCompat.getColor(getApplicationContext(), R.color.red);
    }

    private void findViews() {
        rvResults = ((RecyclerView) findViewById(R.id.rv_results));
        tvYourScore = ((TextView) findViewById(R.id.tv_your_score));
    }

    private void loadStrings() {
        englishFont = "english";
        urduFont = "urdu";
        hindiFont = "krutidev";
        marathiFont = "subak";
    }

    private void initialize() {
        actionBar.setTitle("MCQ Results");
        attemptedMCQs = getIntent().getParcelableArrayListExtra("attemptedMCQs");
        Log.d("AddAssessment","In Initialize");
        calculateScore();
        rvResults.setAdapter(new McqResultsAdapter());
    }

    private void updateStatistics(float score) {
        String userId = SettingPreffrences.getUserid(getApplicationContext()).isEmpty() ? "0" : SettingPreffrences.getUserid(getApplicationContext());
        String imei_number = CommonFunctions.getUniqueDeviceId(getApplicationContext());
        Master master = Master.listAll(Master.class).get(0);
        String medium = master.getMedium();
        String chapter = CommonFunctions.removeApostrophe(SettingPreffrences.getChapter(this));
        String subject = CommonFunctions.removeApostrophe(SettingPreffrences.getSubject(this));
        String date = new SimpleDateFormat(Constants.DATABASE_DATE_FORMAT).format(new Date());
        String mcqName = getIntent().getStringExtra(McqContentModel.MCQ_NAME);
        String formattedScore = String.format("%.2f", score);
        // sending 'content_name' as 'MCQ' as different fonts in Urdu, Hindi and Marathi cause content_name to get jumbled.
        Statistics statistics = new Statistics(userId, imei_number, medium, subject, chapter, "MCQ", CONTENT_TYPE_MCQ, 0, date, formattedScore);
        statistics.save();
    }

    private void updateUserStatistics(float score) {
        String userId = SettingPreffrences.getUserid(getApplicationContext()).isEmpty() ? "0" : SettingPreffrences.getUserid(getApplicationContext());
        String date = new SimpleDateFormat(Constants.DATABASE_DATE_FORMAT).format(new Date());
        String formattedScore = String.format("%.2f", score);

        Log.d("AddAssessment", "userStat: userID: "+userId+" CourseID: "+SettingPreffrences.getCourseId(getApplicationContext())
        +" SubjectID: "+SettingPreffrences.getSubjectId(getApplicationContext())+
                " ChapterID: "+SettingPreffrences.getChapterId(getApplicationContext())
        +" formattedScore: "+formattedScore);

        userStatistics userStatistics = new userStatistics(
                userId,
                SettingPreffrences.getCourseId(getApplicationContext()),
                SettingPreffrences.getSubjectId(getApplicationContext()),
                SettingPreffrences.getChapterId(getApplicationContext()),
                KEF_ContentTypeId_MCQ,
                KEF_ContentTypeName_MCQ,
                "0",
                "0",
                date,
                date,
                formattedScore
        );
        userStatistics.save();
    }

    private void calculateScore() {
        int correctMcqs = 0;
        int totalMcqs = getIntent().getIntExtra("totalMcqs", 0);
        for (Parcelable attemptedMCQ : attemptedMCQs) {
            McqContentModel mcq = (McqContentModel) attemptedMCQ;
            if (mcq.selected_option == mcq.correct_ans_option) {
                correctMcqs++;
            }
        }
        float score = (((float) correctMcqs) / ((float) totalMcqs)) * 100;
        tvYourScore.setText("SCORE - " + String.format("%.2f", score) + "%");
        //updateStatistics(score);
        Log.d("AddAssessment","In calculateScore: "+score);
        updateUserStatistics(score);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setFont(String fontName, TextView textView) {

        if (fontName != null) {

            if (fontName.equals(englishFont)) {
                if (englishTypeface == null) {
                    englishTypeface = Typeface.createFromAsset(this.getAssets(), "fonts/" + "Brandon_bld.otf");
                }
                textView.setTypeface(englishTypeface);
            }

            if (fontName.equals(hindiFont)) {
                if (hindiTypeface == null) {
                    hindiTypeface = Typeface.createFromAsset(this.getAssets(), "fonts/" + "k010.ttf");
                }
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.content_font_name));
                textView.setTypeface(hindiTypeface);
            }

            if (fontName.equals(marathiFont)) {
                if (marathiTypeface == null) {
                    marathiTypeface = Typeface.createFromAsset(this.getAssets(), "fonts/" + "SUBAK0.TTF");
                }
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.content_font_name));
                textView.setTypeface(marathiTypeface);
            }

            if (fontName.equals(urduFont)) {
                if (urduTypeface == null) {
                    urduTypeface = Typeface.createFromAsset(this.getAssets(), "fonts/" + "JameelNooriNastaleeq.TTF");
                }
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.content_font_name));
                textView.setTypeface(urduTypeface);
            }
        }

    }

    private class McqResultsAdapter extends RecyclerView.Adapter {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = getLayoutInflater().inflate(R.layout.item_attempted_mcq, parent, false);
            return new MCQResultsViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            McqContentModel curMCQ = attemptedMCQs.get(position);
            MCQResultsViewHolder resultsViewHolder = (MCQResultsViewHolder) holder;
            setFont(curMCQ.font, resultsViewHolder.tvQuestionText);
            if (curMCQ.isQuesHtml == 1) {
                resultsViewHolder.tvQuestionText.setText(Html.fromHtml(curMCQ.question));
            } else {
                resultsViewHolder.tvQuestionText.setText(curMCQ.question);
            }
            if (curMCQ.isQuesImage != null && !curMCQ.isQuesImage.isEmpty()) {
                String imagePath = App.BASE_PATH + "Images/" + curMCQ.isQuesImage + ".jpg";
                Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                resultsViewHolder.ivQuestionImage.setImageBitmap(bitmap);
                resultsViewHolder.ivQuestionImage.setVisibility(View.VISIBLE);
            } else {
                resultsViewHolder.ivQuestionImage.setVisibility(View.GONE);
            }

            switch (curMCQ.selected_option) {
                case 1:
                    setFont(curMCQ.font1, resultsViewHolder.tvYourAnswerText);
                    if (curMCQ.isOpt1Html == 1) {
                        resultsViewHolder.tvYourAnswerText.setText(Html.fromHtml(curMCQ.option1));
                    } else {
                        resultsViewHolder.tvYourAnswerText.setText(curMCQ.option1);
                    }
                    if (curMCQ.isOpt1Image != null && !curMCQ.isOpt1Image.isEmpty()) {
                        String imagePath = App.BASE_PATH + "Images/" + curMCQ.isOpt1Image + ".jpg";
                        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                        resultsViewHolder.ivYourAnswerImage.setImageBitmap(bitmap);
                        resultsViewHolder.ivYourAnswerImage.setVisibility(View.VISIBLE);
                    } else {
                        resultsViewHolder.ivYourAnswerImage.setVisibility(View.GONE);
                    }
                    break;

                case 2:
                    setFont(curMCQ.font2, resultsViewHolder.tvYourAnswerText);
                    if (curMCQ.isOpt2Html == 1) {
                        resultsViewHolder.tvYourAnswerText.setText(Html.fromHtml(curMCQ.option2));
                    } else {
                        resultsViewHolder.tvYourAnswerText.setText(curMCQ.option2);
                    }
                    if (curMCQ.isOpt2Image != null && !curMCQ.isOpt2Image.isEmpty()) {
                        String imagePath = App.BASE_PATH + "Images/" + curMCQ.isOpt2Image + ".jpg";
                        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                        resultsViewHolder.ivYourAnswerImage.setImageBitmap(bitmap);
                        resultsViewHolder.ivYourAnswerImage.setVisibility(View.VISIBLE);
                    } else {
                        resultsViewHolder.ivYourAnswerImage.setVisibility(View.GONE);
                    }
                    break;

                case 3:
                    setFont(curMCQ.font3, resultsViewHolder.tvYourAnswerText);
                                                                                                                                                       if (curMCQ.isOpt3Html == 1) {
                        resultsViewHolder.tvYourAnswerText.setText(Html.fromHtml(curMCQ.option3));
                    } else {
                        resultsViewHolder.tvYourAnswerText.setText(curMCQ.option3);
                    }
                    if (curMCQ.isOpt3Image != null && !curMCQ.isOpt3Image.isEmpty()) {
                        String imagePath = App.BASE_PATH + "Images/" + curMCQ.isOpt3Image + ".jpg";
                        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                        resultsViewHolder.ivYourAnswerImage.setImageBitmap(bitmap);
                        resultsViewHolder.ivYourAnswerImage.setVisibility(View.VISIBLE);
                    } else {
                        resultsViewHolder.ivYourAnswerImage.setVisibility(View.GONE);
                    }
                    break;

                case 4:
                    setFont(curMCQ.font4, resultsViewHolder.tvYourAnswerText);
                    if (curMCQ.isOpt4Html == 1) {
                        resultsViewHolder.tvYourAnswerText.setText(Html.fromHtml(curMCQ.option4));
                    } else {
                        resultsViewHolder.tvYourAnswerText.setText(curMCQ.option4);
                    }
                    if (curMCQ.isOpt4Image != null && !curMCQ.isOpt4Image.isEmpty()) {
                        String imagePath = App.BASE_PATH + "Images/" + curMCQ.isOpt4Image + ".jpg";
                        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                        resultsViewHolder.ivYourAnswerImage.setImageBitmap(bitmap);
                        resultsViewHolder.ivYourAnswerImage.setVisibility(View.VISIBLE);
                    } else {
                        resultsViewHolder.ivYourAnswerImage.setVisibility(View.GONE);
                    }
                    break;

            }

            switch (curMCQ.correct_ans_option) {
                case 1:
                    setFont(curMCQ.font1, resultsViewHolder.tvRightAnswerText);
                    if (curMCQ.isOpt1Html == 1) {
                        resultsViewHolder.tvRightAnswerText.setText(Html.fromHtml(curMCQ.option1));
                    } else {
                        resultsViewHolder.tvRightAnswerText.setText(curMCQ.option1);
                    }
                    if (curMCQ.isOpt1Image != null && !curMCQ.isOpt1Image.isEmpty()) {
                        String imagePath = App.BASE_PATH + "Images/" + curMCQ.isOpt1Image + ".jpg";
                        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                        resultsViewHolder.ivRightAnswerImage.setImageBitmap(bitmap);
                        resultsViewHolder.ivRightAnswerImage.setVisibility(View.VISIBLE);
                    } else {
                        resultsViewHolder.ivRightAnswerImage.setVisibility(View.GONE);
                    }
                    break;

                case 2:
                    setFont(curMCQ.font2, resultsViewHolder.tvRightAnswerText);
                    if (curMCQ.isOpt2Html == 1) {
                        resultsViewHolder.tvRightAnswerText.setText(Html.fromHtml(curMCQ.option2));
                    } else {
                        resultsViewHolder.tvRightAnswerText.setText(curMCQ.option2);
                    }
                    if (curMCQ.isOpt2Image != null && !curMCQ.isOpt2Image.isEmpty()) {
                        String imagePath = App.BASE_PATH + "Images/" + curMCQ.isOpt2Image + ".jpg";
                        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                        resultsViewHolder.ivRightAnswerImage.setImageBitmap(bitmap);
                        resultsViewHolder.ivRightAnswerImage.setVisibility(View.VISIBLE);
                    } else {
                        resultsViewHolder.ivRightAnswerImage.setVisibility(View.GONE);
                    }
                    break;

                case 3:
                    setFont(curMCQ.font3, resultsViewHolder.tvRightAnswerText);
                    if (curMCQ.isOpt3Html == 1) {
                        resultsViewHolder.tvRightAnswerText.setText(Html.fromHtml(curMCQ.option3));
                    } else {
                        resultsViewHolder.tvRightAnswerText.setText(curMCQ.option3);
                    }
                    if (curMCQ.isOpt3Image != null && !curMCQ.isOpt3Image.isEmpty()) {
                        String imagePath = App.BASE_PATH + "Images/" + curMCQ.isOpt3Image + ".jpg";
                        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                        resultsViewHolder.ivRightAnswerImage.setImageBitmap(bitmap);
                        resultsViewHolder.ivRightAnswerImage.setVisibility(View.VISIBLE);
                    } else {
                        resultsViewHolder.ivRightAnswerImage.setVisibility(View.GONE);
                    }
                    break;

                case 4:
                    setFont(curMCQ.font4, resultsViewHolder.tvRightAnswerText);
                    if (curMCQ.isOpt4Html == 1) {
                        resultsViewHolder.tvRightAnswerText.setText(Html.fromHtml(curMCQ.option4));
                    } else {
                        resultsViewHolder.tvRightAnswerText.setText(curMCQ.option4);
                    }
                    if (curMCQ.isOpt4Image != null && !curMCQ.isOpt4Image.isEmpty()) {
                        String imagePath = App.BASE_PATH + "Images/" + curMCQ.isOpt4Image + ".jpg";
                        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                        resultsViewHolder.ivRightAnswerImage.setImageBitmap(bitmap);
                        resultsViewHolder.ivRightAnswerImage.setVisibility(View.VISIBLE);
                    } else {
                        resultsViewHolder.ivRightAnswerImage.setVisibility(View.GONE);
                    }
                    break;

            }

            if (curMCQ.selected_option == curMCQ.correct_ans_option) {
                resultsViewHolder.tvYourAnswerText.setTextColor(colorGreen);
            } else {
                resultsViewHolder.tvYourAnswerText.setTextColor(colorRed);
            }
            resultsViewHolder.tvQuestionNumber.setText("Q." + (position + 1) + ": ");
        }

        @Override
        public int getItemCount() {
            return attemptedMCQs.size();
        }
    }

    class MCQResultsViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvQuestionText;
        private final TextView tvYourAnswerText;
        private final TextView tvRightAnswerText;
        private final ImageView ivQuestionImage;
        private final ImageView ivYourAnswerImage;
        private final ImageView ivRightAnswerImage;
        private final TextView tvQuestionNumber;

        public MCQResultsViewHolder(View itemView) {
            super(itemView);
            tvQuestionNumber = ((TextView) itemView.findViewById(R.id.tv_question_number));
            tvQuestionText = ((TextView) itemView.findViewById(R.id.tv_question));
            tvYourAnswerText = ((TextView) itemView.findViewById(R.id.tv_your_answer));
            tvRightAnswerText = ((TextView) itemView.findViewById(R.id.tv_right_answer));
            ivQuestionImage = ((ImageView) itemView.findViewById(R.id.iv_question));
            ivYourAnswerImage = ((ImageView) itemView.findViewById(R.id.iv_your_answer));
            ivRightAnswerImage = ((ImageView) itemView.findViewById(R.id.iv_right_answer));
        }
    }
}
