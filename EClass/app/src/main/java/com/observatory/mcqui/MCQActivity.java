package com.observatory.mcqui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.observatory.mcqdatabase.McqTestContentTable;
import com.observatory.mcqmodels.McqContentModel;
import com.observatory.kefcorner.R;
import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.OnSwipeTouchListener;

import java.util.ArrayList;
import java.util.List;


public class MCQActivity extends BaseActivity {

    private final String RESULTS = "Submit Test";
    private final String SUBMIT = "SUBMIT";
    private final String ATTEMPTED = "ATTEMPTED";
    private final String SKIP = "SKIP";

    private View mcqPage;
    private LinearLayout llBottomBtns;
    private ImageView ivBackArrow;
    private TextView tvSubmitBtn;
    private ImageView ivForwardArrow;
    private LinearLayout llMcqQuestionTime;
    private TextView tvQuestionCounter;
    private TextView tvQuestionNo;
    private TextView tvQuestionText;
    private ImageView ivQuestionImage;
    private LinearLayout rlOption1Container;
    private RelativeLayout rlOption1Text;
    private RelativeLayout rlOption1Circle;
    private TextView tvOption1;
    private TextView tvOption1Text;
    private ImageView ivOption1;
    private LinearLayout rlOption2Container;
    private RelativeLayout rlOption2Text;
    private RelativeLayout rlOption2Circle;
    private TextView tvOption2;
    private TextView tvOption2Text;
    private ImageView ivOption2;
    private LinearLayout rlOption3Container;
    private RelativeLayout rlOption3Text;
    private RelativeLayout rlOption3Circle;
    private TextView tvOption3;
    private TextView tvOption3Text;
    private ImageView ivOption3;
    private LinearLayout rlOption4Container;
    private RelativeLayout rlOption4Text;
    private RelativeLayout rlOption4Circle;
    private TextView tvOption4;
    private TextView tvOption4Text;
    private ImageView ivOption4;

    int colorBlue;
    int colorBlack;
    int colorWhite;
    int colorGreen;
    int colorRed;
    int colorGreenTransparent;
    int colorRedTransparent;
    int colorGrey;

    final String ENGLISH_FONT = "english";
    final String URDU_FONT = "urdu";
    final String HINDI_FONT = "krutidev";
    final String MARATHI_FONT = "subak";

    private int curMCQPosition = 0;
    private ArrayList<McqContentModel> mcqList;
    private McqContentModel curMCQ;
    private List<McqContentModel> attemptedMCQs = new ArrayList<>();
    private Typeface englishTypeface;
    private Typeface hindiTypeface;
    private Typeface marathiTypeface;
    private Typeface urduTypeface;
    private boolean submitClicked = false;
    private boolean isLastQuestionAnswered = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcq);
        initialize();
    }

    @Override
    protected void onLayout() {
        findViews();
        loadColors();
        initialize();
    }

    private void loadColors() {
        colorBlue = ContextCompat.getColor(getApplicationContext(), R.color.blue);
        colorBlack = ContextCompat.getColor(getApplicationContext(), R.color.black);
        colorWhite = ContextCompat.getColor(getApplicationContext(), R.color.white);
        colorGreen = ContextCompat.getColor(getApplicationContext(), R.color.green1);
        colorRed = ContextCompat.getColor(getApplicationContext(), R.color.red);
        colorGreenTransparent = ContextCompat.getColor(getApplicationContext(), R.color.transparent_green);
        colorRedTransparent = ContextCompat.getColor(getApplicationContext(), R.color.transparent_red);
        colorGrey = ContextCompat.getColor(getApplicationContext(), R.color.light_grey);
    }

    private void findViews() {
        mcqPage = findViewById( R.id.mcq_page );
        llBottomBtns = (LinearLayout)findViewById( R.id.ll_bottom_btns );
        ivBackArrow = (ImageView)findViewById( R.id.iv_back_arrow );
        tvSubmitBtn = (TextView)findViewById( R.id.tv_submit_btn );
        ivForwardArrow = (ImageView)findViewById( R.id.iv_forward_arrow );
        llMcqQuestionTime = (LinearLayout)findViewById( R.id.ll_mcq_question_time );
        tvQuestionCounter = (TextView)findViewById( R.id.tv_question_counter );
        tvQuestionNo = (TextView)findViewById( R.id.tv_question_number );
        tvQuestionText = (TextView)findViewById( R.id.tv_question );
        ivQuestionImage = (ImageView)findViewById( R.id.iv_question );
        rlOption1Container = (LinearLayout)findViewById( R.id.rl_option1_container );
        rlOption1Text = (RelativeLayout)findViewById( R.id.rl_option1_text );
        rlOption1Circle = (RelativeLayout)findViewById( R.id.rl_option1_circle );
        tvOption1 = (TextView)findViewById( R.id.tv_option_1 );
        tvOption1Text = (TextView)findViewById( R.id.tv_option1_text );
        ivOption1 = (ImageView)findViewById( R.id.iv_option1 );
        rlOption2Container = (LinearLayout)findViewById( R.id.rl_option2_container );
        rlOption2Text = (RelativeLayout)findViewById( R.id.rl_option2_text );
        rlOption2Circle = (RelativeLayout)findViewById( R.id.rl_option2_circle );
        tvOption2 = (TextView)findViewById( R.id.tv_option_2 );
        tvOption2Text = (TextView)findViewById( R.id.tv_option2_text );
        ivOption2 = (ImageView)findViewById( R.id.iv_option2 );
        rlOption3Container = (LinearLayout)findViewById( R.id.rl_option3_container );
        rlOption3Text = (RelativeLayout)findViewById( R.id.rl_option3_text );
        rlOption3Circle = (RelativeLayout)findViewById( R.id.rl_option3_circle );
        tvOption3 = (TextView)findViewById( R.id.tv_option_3 );
        tvOption3Text = (TextView)findViewById( R.id.tv_option3_text );
        ivOption3 = (ImageView)findViewById( R.id.iv_option3 );
        rlOption4Container = (LinearLayout)findViewById( R.id.rl_option4_container );
        rlOption4Text = (RelativeLayout)findViewById( R.id.rl_option4_text );
        rlOption4Circle = (RelativeLayout)findViewById( R.id.rl_option4_circle );
        tvOption4 = (TextView)findViewById( R.id.tv_option_4 );
        tvOption4Text = (TextView)findViewById( R.id.tv_option4_text );
        ivOption4 = (ImageView)findViewById( R.id.iv_option4 );
    }

    private void initialize() {
        actionBar.setTitle("MCQ");
        McqTestContentTable contentTable = new McqTestContentTable();

        mcqList = contentTable.getMcqTest(getIntent().getIntExtra(McqContentModel.MCQ_SERIAL_NO, -1));
        // showing first MCQ
        showMCQ(mcqList.get(0));
        mcqPage.setOnTouchListener(new OnSwipeTouchListener(this) {

            public void onSwipeTop() {
            }

            public void onSwipeRight() {
                // disable navigation if user has not attempted current mcq.
//                if (!curMCQ.questionAlreadyAttempted) {
//                    return;
//                }

                if (curMCQPosition <= 0) {
                    curMCQPosition = 0;
                    return;
                }
//        ivForwardArrow.setImageResource(R.drawable.ic_right_arrow_active);
                curMCQPosition--;
                showMCQ(mcqList.get(curMCQPosition));
                if (curMCQPosition <= 0) {
                    ivBackArrow.setImageResource(R.drawable.ic_left_arrow_inactive);
                }
                swipeRightAnim(AnimationUtils.loadAnimation(MCQActivity.this, R.anim.a_slide_out_right), AnimationUtils.loadAnimation(MCQActivity.this, R.anim.a_slide_in_left));
//                AnimationUtils.loadAnimation(MCQActivity.this,R.anim.a_fade_in);
            }

            public void onSwipeLeft() {
                // disable navigation if user has not attempted current mcq.
                if (!curMCQ.questionAlreadyAttempted) {
                    return;
                }
                if (curMCQPosition >= mcqList.size() - 1) {
                    curMCQPosition = mcqList.size() - 1;
                    return;
                }
//        ivBackArrow.setImageResource(R.drawable.ic_left_arrow_active);
                curMCQPosition++;
                showMCQ(mcqList.get(curMCQPosition));
                // deactivate next button if this is the last question.
                if (curMCQPosition >= mcqList.size() - 1) {
                    ivForwardArrow.setImageResource(R.drawable.ic_right_arrow_inactive);
                }
                swipeRightAnim(AnimationUtils.loadAnimation(MCQActivity.this, R.anim.a_slide_out_left), AnimationUtils.loadAnimation(MCQActivity.this, R.anim.a_slide_in_right));

            }

            public void onSwipeBottom() {
            }

        });
    }

    private void swipeRightAnim(Animation animation, Animation animation2) {
        mcqPage.startAnimation(animation);
        mcqPage.startAnimation(animation2);
    }

    private void showMCQ(McqContentModel curMCQ) {
        // all options will be white in the beginning indicating they are unselected.
        this.curMCQ = curMCQ;
        rlOption1Container.setBackgroundColor(colorWhite);
        rlOption2Container.setBackgroundColor(colorWhite);
        rlOption3Container.setBackgroundColor(colorWhite);
        rlOption4Container.setBackgroundColor(colorWhite);
        ((GradientDrawable) rlOption1Circle.getBackground()).setColor(colorBlack);
        tvOption1Text.setTextColor(colorBlack);
        ((GradientDrawable) rlOption2Circle.getBackground()).setColor(colorBlack);
        tvOption2Text.setTextColor(colorBlack);
        ((GradientDrawable) rlOption3Circle.getBackground()).setColor(colorBlack);
        tvOption3Text.setTextColor(colorBlack);
        ((GradientDrawable) rlOption4Circle.getBackground()).setColor(colorBlack);
        tvOption4Text.setTextColor(colorBlack);
        // Question has been attempted.
        if (curMCQ.questionAlreadyAttempted) {
            if (curMCQ.selected_option == 1) {
                ((GradientDrawable) rlOption1Circle.getBackground()).setColor(colorBlue);
                tvOption1Text.setTextColor(colorBlue);
            }
            if (curMCQ.selected_option == 2) {
                ((GradientDrawable) rlOption2Circle.getBackground()).setColor(colorBlue);
                tvOption2Text.setTextColor(colorBlue);
            }
            if (curMCQ.selected_option == 3) {
                ((GradientDrawable) rlOption3Circle.getBackground()).setColor(colorBlue);
                tvOption3Text.setTextColor(colorBlue);
            }
            if (curMCQ.selected_option == 4) {
                ((GradientDrawable) rlOption4Circle.getBackground()).setColor(colorBlue);
                tvOption4Text.setTextColor(colorBlue);
            }

            enableUserNavigation();
        } else {
            // Not attempted
            disableUserNavigation();
        }

        setQuestionValues(curMCQ);
        setOption1Values(curMCQ);
        setOption2Values(curMCQ);
        setOption3Values(curMCQ);
        setOption4Values(curMCQ);
    }

    private void setOption4Values(McqContentModel curMCQ) {
        // if option4 doesn't have text or image, we hide it.
        if ((curMCQ.option4 == null || curMCQ.option4.isEmpty()) && (curMCQ.isOpt4Image == null ||
                curMCQ.isOpt4Image.isEmpty())) {
            rlOption4Container.setVisibility(View.GONE);
        } else {
            rlOption4Container.setVisibility(View.VISIBLE);
        }
        setFont(curMCQ.font4, tvOption4Text);
        if (curMCQ.isOpt4Html == 1) {
            tvOption4Text.setText(Html.fromHtml(curMCQ.option4));
        } else {
            tvOption4Text.setText(curMCQ.option4);
        }
        if (curMCQ.isOpt4Image != null && !curMCQ.isOpt4Image.isEmpty()) {
            String imagePath = App.BASE_PATH + "Images/" + curMCQ.isOpt4Image + ".jpg";
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            ivOption4.setImageBitmap(bitmap);
            ivOption4.setVisibility(View.VISIBLE);
        } else {
            ivOption4.setVisibility(View.GONE);
        }
    }

    private void setOption3Values(McqContentModel curMCQ) {
        // if option3 doesn't have text or image, we hide it.
        if ((curMCQ.option3 == null || curMCQ.option3.isEmpty()) && (curMCQ.isOpt3Image == null ||
                curMCQ.isOpt3Image.isEmpty())) {
            rlOption3Container.setVisibility(View.GONE);
        } else {
            rlOption3Container.setVisibility(View.VISIBLE);
        }
        setFont(curMCQ.font3, tvOption3Text);
        if (curMCQ.isOpt3Html == 1) {
            tvOption3Text.setText(Html.fromHtml(curMCQ.option3));
        } else {
            tvOption3Text.setText(curMCQ.option3);
        }
        if (curMCQ.isOpt3Image != null && !curMCQ.isOpt3Image.isEmpty()) {
            String imagePath = App.BASE_PATH + "Images/" + curMCQ.isOpt3Image + ".jpg";
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            ivOption3.setImageBitmap(bitmap);
            ivOption3.setVisibility(View.VISIBLE);
        } else {
            ivOption3.setVisibility(View.GONE);
        }
    }

    private void setOption2Values(McqContentModel curMCQ) {
        // if option2 doesn't have text or image, we hide it.
        if ((curMCQ.option2 == null || curMCQ.option2.isEmpty()) && (curMCQ.isOpt2Image == null ||
                curMCQ.isOpt2Image.isEmpty())) {
            rlOption2Container.setVisibility(View.GONE);
        } else {
            rlOption2Container.setVisibility(View.VISIBLE);
        }
        setFont(curMCQ.font2, tvOption2Text);
        if (curMCQ.isOpt2Html == 1) {
            tvOption2Text.setText(Html.fromHtml(curMCQ.option2));
        } else {
            tvOption2Text.setText(curMCQ.option2);
        }
        if (curMCQ.isOpt2Image != null && !curMCQ.isOpt2Image.isEmpty()) {
            String imagePath = App.BASE_PATH + "Images/" + curMCQ.isOpt2Image + ".jpg";
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            ivOption2.setImageBitmap(bitmap);
            ivOption2.setVisibility(View.VISIBLE);
        } else {
            ivOption2.setVisibility(View.GONE);
        }
    }

    private void setOption1Values(McqContentModel curMCQ) {
        // if option1 doesn't have text or image, we hide it.
        if ((curMCQ.option1 == null || curMCQ.option1.isEmpty()) && (curMCQ.isOpt1Image == null ||
                curMCQ.isOpt1Image.isEmpty())) {
            rlOption1Container.setVisibility(View.GONE);
        } else {
            rlOption1Container.setVisibility(View.VISIBLE);
        }
        setFont(curMCQ.font1, tvOption1Text);
        if (curMCQ.isOpt1Html == 1) {
            tvOption1Text.setText(Html.fromHtml(curMCQ.option1));
        } else {
            tvOption1Text.setText(curMCQ.option1);
        }
        if (curMCQ.isOpt1Image != null && !curMCQ.isOpt1Image.isEmpty()) {
            String imagePath = App.BASE_PATH + "Images/" + curMCQ.isOpt1Image + ".jpg";
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            ivOption1.setImageBitmap(bitmap);
            ivOption1.setVisibility(View.VISIBLE);
        } else {
            ivOption1.setVisibility(View.GONE);
        }
    }

    private void setQuestionValues(McqContentModel curMCQ) {
        tvQuestionCounter.setText("Question: " + (curMCQPosition + 1) + "/" + mcqList.size());
        tvQuestionNo.setText("Q." + (curMCQPosition + 1) + ": ");
        setFont(curMCQ.font, tvQuestionText);

        if (curMCQ.isQuesHtml == 1) {
            tvQuestionText.setText(Html.fromHtml(curMCQ.question));
        } else {
            tvQuestionText.setText(curMCQ.question);
        }
        if (curMCQ.isQuesImage != null && !curMCQ.isQuesImage.isEmpty()) {

            String imagePath = App.BASE_PATH + "Images/" + curMCQ.isQuesImage + ".jpg";
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            ivQuestionImage.setImageBitmap(bitmap);
            ivQuestionImage.setVisibility(View.VISIBLE);
        } else {
            ivQuestionImage.setVisibility(View.GONE);
        }
    }

    private void disableUserNavigation() {
        tvSubmitBtn.setBackgroundColor(colorGrey);
        tvSubmitBtn.setText("SUBMIT");
        if (curMCQPosition == 0) {
            ivBackArrow.setImageResource(R.drawable.ic_left_arrow_inactive);
        } else {
            ivBackArrow.setImageResource(R.drawable.ic_left_arrow_active);
        }
        // activate forward button
        if (!curMCQ.questionAlreadyAttempted || curMCQPosition == mcqList.size() - 1) {
            ivForwardArrow.setImageResource(R.drawable.ic_right_arrow_inactive);
        } else {
            ivForwardArrow.setImageResource(R.drawable.ic_right_arrow_active);
        }
//        ivBackArrow.setImageResource(R.drawable.ic_left_arrow_inactive);
//        ivForwardArrow.setImageResource(R.drawable.ic_right_arrow_inactive);
    }

    public void onNextQuestionClicked(View view) {
        // disable navigation if user has not attempted current mcq.
        if (!curMCQ.questionAlreadyAttempted) {
            return;
        }
        if (curMCQPosition >= mcqList.size() - 1) {
            curMCQPosition = mcqList.size() - 1;
            return;
        }
//        ivBackArrow.setImageResource(R.drawable.ic_left_arrow_active);
        curMCQPosition++;
        showMCQ(mcqList.get(curMCQPosition));
        // deactivate next button if this is the last question.
        if (curMCQPosition >= mcqList.size() - 1) {
            ivForwardArrow.setImageResource(R.drawable.ic_right_arrow_inactive);
        }
        swipeRightAnim(AnimationUtils.loadAnimation(MCQActivity.this, R.anim.a_slide_out_left), AnimationUtils.loadAnimation(MCQActivity.this, R.anim.a_slide_in_right));

    }

    public void onPrevQuestionClicked(View view) {
        // disable navigation if user has not attempted current mcq.
//        if (!curMCQ.questionAlreadyAttempted) {
//            return;
//        }
        if (curMCQPosition <= 0) {
            curMCQPosition = 0;
            return;
        }
//        ivForwardArrow.setImageResource(R.drawable.ic_right_arrow_active);
        curMCQPosition--;
        showMCQ(mcqList.get(curMCQPosition));
        if (curMCQPosition <= 0) {
            ivBackArrow.setImageResource(R.drawable.ic_left_arrow_inactive);
        } else {
            ivBackArrow.setImageResource(R.drawable.ic_left_arrow_active);
        }
        swipeRightAnim(AnimationUtils.loadAnimation(MCQActivity.this, R.anim.a_slide_out_right), AnimationUtils.loadAnimation(MCQActivity.this, R.anim.a_slide_in_left));

    }

    public void onOptionsClicked(View view) {
        curMCQ.questionAlreadyAttempted = true;
        enableUserNavigation();
        switch (view.getId()) {
            case R.id.rl_option1_container:
                ((GradientDrawable) rlOption1Circle.getBackground()).setColor(colorBlue);
                tvOption1Text.setTextColor(colorBlue);
                ((GradientDrawable) rlOption2Circle.getBackground()).setColor(colorBlack);
                tvOption2Text.setTextColor(colorBlack);
                ((GradientDrawable) rlOption3Circle.getBackground()).setColor(colorBlack);
                tvOption3Text.setTextColor(colorBlack);
                ((GradientDrawable) rlOption4Circle.getBackground()).setColor(colorBlack);
                tvOption4Text.setTextColor(colorBlack);
                curMCQ.selected_option = 1;
                break;
            case R.id.rl_option2_container:
                ((GradientDrawable) rlOption1Circle.getBackground()).setColor(colorBlack);
                tvOption1Text.setTextColor(colorBlack);
                ((GradientDrawable) rlOption2Circle.getBackground()).setColor(colorBlue);
                tvOption2Text.setTextColor(colorBlue);
                ((GradientDrawable) rlOption3Circle.getBackground()).setColor(colorBlack);
                tvOption3Text.setTextColor(colorBlack);
                ((GradientDrawable) rlOption4Circle.getBackground()).setColor(colorBlack);
                tvOption4Text.setTextColor(colorBlack);
                curMCQ.selected_option = 2;
                break;
            case R.id.rl_option3_container:
                ((GradientDrawable) rlOption1Circle.getBackground()).setColor(colorBlack);
                tvOption1Text.setTextColor(colorBlack);
                ((GradientDrawable) rlOption2Circle.getBackground()).setColor(colorBlack);
                tvOption2Text.setTextColor(colorBlack);
                ((GradientDrawable) rlOption3Circle.getBackground()).setColor(colorBlue);
                tvOption3Text.setTextColor(colorBlue);
                ((GradientDrawable) rlOption4Circle.getBackground()).setColor(colorBlack);
                tvOption4Text.setTextColor(colorBlack);
                curMCQ.selected_option = 3;
                break;
            case R.id.rl_option4_container:
                ((GradientDrawable) rlOption1Circle.getBackground()).setColor(colorBlack);
                tvOption1Text.setTextColor(colorBlack);
                ((GradientDrawable) rlOption2Circle.getBackground()).setColor(colorBlack);
                tvOption2Text.setTextColor(colorBlack);
                ((GradientDrawable) rlOption3Circle.getBackground()).setColor(colorBlack);
                tvOption3Text.setTextColor(colorBlack);
                ((GradientDrawable) rlOption4Circle.getBackground()).setColor(colorBlue);
                tvOption4Text.setTextColor(colorBlue);
                curMCQ.selected_option = 4;
                break;
        }
    }

    private void enableUserNavigation() {

        // enable Submit button only if last question is answered
        if (curMCQPosition == mcqList.size() - 1) {
            tvSubmitBtn.setBackgroundColor(colorBlue);
            isLastQuestionAnswered = true;
        }

        tvSubmitBtn.setText("SUBMIT");
        // activate prev. button
        if (curMCQPosition == 0) {
            ivBackArrow.setImageResource(R.drawable.ic_left_arrow_inactive);
        } else {
            ivBackArrow.setImageResource(R.drawable.ic_left_arrow_active);
        }
        // activate forward button
        if (curMCQPosition == mcqList.size() - 1) {
            ivForwardArrow.setImageResource(R.drawable.ic_right_arrow_inactive);
        } else {
            ivForwardArrow.setImageResource(R.drawable.ic_right_arrow_active);
        }
    }


    public void onSubmitClicked(View view) {

        submitClicked = true;

        submitClicked();


    }

    private void submitClicked() {

        // only when last question is answered, we allow user to view results
        if (!isLastQuestionAnswered) {
            Toast.makeText(getApplicationContext(), "Please answer all questions", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            Log.d("AddAssessment","onSubmitClicked: "+getIntent().getStringExtra(McqContentModel.MCQ_NAME)
            +"  : "+mcqList.size());
            startActivity(new Intent(this, MCQResultsActivity.class)
                    .putExtra(McqContentModel.MCQ_NAME, getIntent().getStringExtra(McqContentModel.MCQ_NAME))
                    .putParcelableArrayListExtra("attemptedMCQs", mcqList)
                    .putExtra("totalMcqs", mcqList.size()));
            finish();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    private Typeface getTypeFace(String fontName) {
        Typeface typeface = null;
        switch (fontName) {
            case ENGLISH_FONT:
                if (englishTypeface == null) {
                    englishTypeface = Typeface.createFromAsset(this.getAssets(), "fonts/" + "Brandon_bld.otf");
                }
                typeface = englishTypeface;
                break;
            case HINDI_FONT:
                if (hindiTypeface == null) {
                    hindiTypeface = Typeface.createFromAsset(this.getAssets(), "fonts/" + "k010.ttf");
                }
                typeface = hindiTypeface;
                break;
            case MARATHI_FONT:
                if (marathiTypeface == null) {
                    marathiTypeface = Typeface.createFromAsset(this.getAssets(), "fonts/" + "SUBAK0.TTF");
                }
                typeface = marathiTypeface;
                break;
            case URDU_FONT:
                if (urduTypeface == null) {
                    urduTypeface = Typeface.createFromAsset(this.getAssets(), "fonts/" + "JameelNooriNastaleeq.TTF");
                }
                typeface = urduTypeface;
                break;
        }

        return typeface;
    }

    private void setFont(String fontName, TextView textView) {

        if (fontName != null) {

            if (fontName.equals(ENGLISH_FONT)) {
                textView.setTypeface(getTypeFace(ENGLISH_FONT));
            }

            if (fontName.equals(HINDI_FONT)) {
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.content_font_name));
                textView.setTypeface(getTypeFace(HINDI_FONT));
            }

            if (fontName.equals(MARATHI_FONT)) {
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.content_font_name));
                textView.setTypeface(getTypeFace(MARATHI_FONT));
            }

            if (fontName.equals(URDU_FONT)) {
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.content_font_name));
                textView.setTypeface(getTypeFace(URDU_FONT));
            }
        }
    }
}
