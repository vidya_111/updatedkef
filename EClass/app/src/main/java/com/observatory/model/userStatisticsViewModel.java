package com.observatory.model;

public class userStatisticsViewModel {

    public String contentTypeID;
    public String contentName;
    public String accessDuration;
    public String accessDatetime;
    public String score;

    public userStatisticsViewModel(String contentTypeID, String contentName, String accessDuration, String accessDatetime) {
        this.contentTypeID = contentTypeID;
        this.contentName = contentName;
        this.accessDuration = accessDuration;
        this.accessDatetime = accessDatetime;

    }
}
