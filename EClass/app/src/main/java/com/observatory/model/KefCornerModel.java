package com.observatory.model;

public class KefCornerModel {
    String GalleryID;
    String Title;
    String MediaTypeID;
    String FileName;
    String arrSchoolID;
    String MediaTypeName;
    String CreatedOn;

    public KefCornerModel(String galleryID, String title, String mediaTypeID, String fileName, String arrSchoolID, String mediaTypeName, String createdOn) {
        GalleryID = galleryID;
        Title = title;
        MediaTypeID = mediaTypeID;
        FileName = fileName;
        this.arrSchoolID = arrSchoolID;
        MediaTypeName = mediaTypeName;
        CreatedOn = createdOn;
    }

    public String getGalleryID() {
        return GalleryID;
    }

    public void setGalleryID(String galleryID) {
        GalleryID = galleryID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getMediaTypeID() {
        return MediaTypeID;
    }

    public void setMediaTypeID(String mediaTypeID) {
        MediaTypeID = mediaTypeID;
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String fileName) {
        FileName = fileName;
    }

    public String getArrSchoolID() {
        return arrSchoolID;
    }

    public void setArrSchoolID(String arrSchoolID) {
        this.arrSchoolID = arrSchoolID;
    }

    public String getMediaTypeName() {
        return MediaTypeName;
    }

    public void setMediaTypeName(String mediaTypeName) {
        MediaTypeName = mediaTypeName;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }
}
