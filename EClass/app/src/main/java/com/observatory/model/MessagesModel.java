package com.observatory.model;

public class MessagesModel {
    String MessageID;
    String MessageBy;
    String MessageText;
    String MessageDateTime;
    String SchoolID;
    String SchoolName;

    public MessagesModel(String messageID, String messageBy, String messageText, String messageDateTime, String schoolID, String schoolName) {
        MessageID = messageID;
        MessageBy = messageBy;
        MessageText = messageText;
        MessageDateTime = messageDateTime;
        SchoolID = schoolID;
        SchoolName = schoolName;
    }

    public String getMessageID() {
        return MessageID;
    }

    public void setMessageID(String messageID) {
        MessageID = messageID;
    }

    public String getMessageBy() {
        return MessageBy;
    }

    public void setMessageBy(String messageBy) {
        MessageBy = messageBy;
    }

    public String getMessageText() {
        return MessageText;
    }

    public void setMessageText(String messageText) {
        MessageText = messageText;
    }

    public String getMessageDateTime() {
        return MessageDateTime;
    }

    public void setMessageDateTime(String messageDateTime) {
        MessageDateTime = messageDateTime;
    }

    public String getSchoolID() {
        return SchoolID;
    }

    public void setSchoolID(String schoolID) {
        SchoolID = schoolID;
    }

    public String getSchoolName() {
        return SchoolName;
    }

    public void setSchoolName(String schoolName) {
        SchoolName = schoolName;
    }

}
