package com.observatory.model;

public class AllDoubts {

    String DoubtBoxID;
    String DoubtCategoryID;
    String SchoolID;
    String CourseID;
    String SubjectID;
    String SubjectName;
    String DoubtTitle;
    String CreatedOn;

    public AllDoubts(String doubtBoxID, String doubtCategoryID, String schoolID, String courseID, String subjectID, String subjectName, String doubtTitle, String createdOn) {
        DoubtBoxID = doubtBoxID;
        DoubtCategoryID = doubtCategoryID;
        SchoolID = schoolID;
        CourseID = courseID;
        SubjectID = subjectID;
        SubjectName = subjectName;
        DoubtTitle = doubtTitle;
        CreatedOn = createdOn;
    }


    public String getDoubtBoxID() {
        return DoubtBoxID;
    }

    public void setDoubtBoxID(String doubtBoxID) {
        DoubtBoxID = doubtBoxID;
    }

    public String getDoubtCategoryID() {
        return DoubtCategoryID;
    }

    public void setDoubtCategoryID(String doubtCategoryID) {
        DoubtCategoryID = doubtCategoryID;
    }

    public String getSchoolID() {
        return SchoolID;
    }

    public void setSchoolID(String schoolID) {
        SchoolID = schoolID;
    }

    public String getCourseID() {
        return CourseID;
    }

    public void setCourseID(String courseID) {
        CourseID = courseID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getDoubtTitle() {
        return DoubtTitle;
    }

    public void setDoubtTitle(String doubtTitle) {
        DoubtTitle = doubtTitle;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

}
