package com.observatory.model;

import java.util.ArrayList;
import java.util.List;

public class SchoolModel {



    List<String> Value = new ArrayList<>();
    String Text = "";
    String DataValue1 ="";
    String DataValue2 = "";
    String DataValue3 ="";


    public List<String> getValue() {
        return Value;
    }

    public void setValue(List<String> value) {
        Value = value;
    }


    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public String getDataValue1() {
        return DataValue1;
    }

    public void setDataValue1(String dataValue1) {
        DataValue1 = dataValue1;
    }

    public String getDataValue2() {
        return DataValue2;
    }

    public void setDataValue2(String dataValue2) {
        DataValue2 = dataValue2;
    }

    public String getDataValue3() {
        return DataValue3;
    }

    public void setDataValue3(String dataValue3) {
        DataValue3 = dataValue3;
    }



}
