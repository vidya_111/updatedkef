package com.observatory.mcqdatabase;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.observatory.mcqmodels.SubjectsModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anuj on 22-05-2015.
 */
public class SubjectsTable {

    private SQLiteDatabase database;
    private static String TableName = "subjects";


    public SubjectsTable(SQLiteDatabase database) {
        this.database = database;
    }


    public List<SubjectsModel> getSubjects() {

        List<SubjectsModel> list = new ArrayList<>();
        database = DatabaseManager.getInstance().openDatabase();
        String query = "Select * from " + TableName;

        Cursor cursor = database.rawQuery(query, null);

        while (cursor != null && cursor.moveToNext()) {

            SubjectsModel model = new SubjectsModel();

            model.subject_id = cursor.getInt(0);
            model.font = cursor.getString(3);
            model.subject_name = cursor.getString(4);
            model.subject_image = cursor.getString(5);

            list.add(model);


        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();

        return list;


    }
}
