package com.observatory.mcqdatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.observatory.mcqmodels.StatisticsModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anuj on 29-08-2015.
 */
public class StatisticTable {

    private SQLiteDatabase database;
    private static String TableName = "statistics";

    private static String MCQ_ID = "mcq_id";
    private static String DATE = "date";
    private static String TIME = "time";
    private static String SCORE = "score";
    private static String QUESTION_ANSWERED = "question_answered";
    private static String MCQ_NAME = "mcq_name";
    private long result;

    public StatisticTable(SQLiteDatabase database) {
        this.database = database;
    }


    public void insertStatistics(StatisticsModel object) {

        if (object != null) {
            database = DatabaseManager.getInstance().openDatabase();
            database = DatabaseManager.getInstance().openDatabase();

            ContentValues values = new ContentValues();
            values.put(MCQ_ID, object.mcq_id);
            values.put(DATE, object.date);
            values.put(TIME, object.time);
            values.put(QUESTION_ANSWERED, object.ques_answered);
            values.put(SCORE, object.score);
            values.put(MCQ_NAME, object.mcq_name);

            try {
                result = database.insert(TableName, null, values);
                Log.e("Statistics", "" + result);
            } catch (SQLiteException e) {

            }
            DatabaseManager.getInstance().closeDatabase();

        }

    }


    public List<StatisticsModel> getStatistics(int mcq_id) {

        List<StatisticsModel> list = new ArrayList<>();
        database = DatabaseManager.getInstance().openDatabase();
        String query = "Select * from " + TableName + " where mcq_id=" + mcq_id;

        Cursor cursor = database.rawQuery(query, null);

        while (cursor != null && cursor.moveToNext()) {

            StatisticsModel model = new StatisticsModel();
            model.mcq_id = cursor.getInt(0);
            model.date = cursor.getString(1);
            model.time = cursor.getString(2);
            model.ques_answered = cursor.getInt(3);
            model.score = cursor.getFloat(4);
            model.mcq_name = cursor.getString(5);

            list.add(model);


        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();

        return list;


    }
}
