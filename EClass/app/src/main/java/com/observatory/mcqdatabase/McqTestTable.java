package com.observatory.mcqdatabase;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.observatory.mcqmodels.McqModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anuj on 10-06-2015.
 */
public class McqTestTable {

    private SQLiteDatabase database;
    private static String TableName = "mcq";


    public McqTestTable() {
        this.database = database;
    }


    public List<McqModel> getMcqTest(int id) {


        List<McqModel> list = new ArrayList<>();
        database = DatabaseManager.getInstance().openDatabase();
        String query = "Select * from " + TableName + " where chapter_serial_no=" + id;

        Cursor cursor = database.rawQuery(query, null);

        while (cursor != null && cursor.moveToNext()) {

            McqModel model = new McqModel();

            model.mcq_id = cursor.getInt(0);
            model.chapter_name = cursor.getString(3);
            model.font = cursor.getString(2);

            list.add(model);


        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();

        return list;


    }

}
