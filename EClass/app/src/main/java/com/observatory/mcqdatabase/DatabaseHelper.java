package com.observatory.mcqdatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.observatory.Assessment.Model.MessageModel;
import com.observatory.utils.App;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Anuj on 22-05-2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static String TAG = "DataBaseHelper"; // Tag just for the LogCat window
    //destination path (location) of our database on device
    private String DB_PATH = "";
    private String DB_NAME = "MCQTABLETDB.sqlite";// Database name
    private SQLiteDatabase mDataBase;
    private final Context mContext;

    //Notification COLUMNS
    private static final String COLUMN_NOTIF_ID = "Notif_ID";
    private static final String COLUMN_NOTIF_MSG = "Notif_Msg";
    private static final String COLUMN_NOTIF_TIME = "Notif_Time";
    private static final String COLUMN_NOTIF_TITLE = "Notif_Title";
    private static final String COLUMN_NOTIF_R1 = "Notif_R1";
    private static final String COLUMN_NOTIF_R2 = "Notif_R2";

    String CREATE_NOTIFICATION_QUERY = "CREATE TABLE 'Notification' (" +
            "'Notif_ID' INTEGER PRIMARY KEY AUTOINCREMENT," +
            "'Notif_Msg' TEXT," +
            "'Notif_Time' INTEGER," +
            "'Notif_R1' TEXT," +
            "'Notif_R2' TEXT," +
            "'Notif_Title' TEXT)";

    private static final String TABLE_NOTIFICATION = "Notification";


    public DatabaseHelper(Context context) {
        super(context, "MCQTABLETDB.sqlite", null, 1);// 1? its Database Version
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        } else {
            DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        }
        this.mContext = context;
    }

    public void createDataBase() throws IOException {
        //If database not exists copy it from the assets

            this.getReadableDatabase();
            this.close();
            try {
                //Copy the database from sd card to App's data directory
                copyDataBase();
                Log.e(TAG, "createDatabase database created");
            } catch (IOException mIOException) {
                Log.e(TAG, "ErrorCopyingDataBase");
            }
    }

    //Check that the database exists here: /data/data/your package/databases/Da Name
    private boolean checkDataBase() {
        File dbFile = new File(DB_PATH + DB_NAME);
        //Log.v("dbFile", dbFile + "   "+ dbFile.exists());
        return dbFile.exists();
    }

    //Copy the database from assets
    private void copyDataBase() throws IOException {
        File dbFile = new File(App.BASE_PATH + DB_NAME);
        InputStream mInput = new FileInputStream(dbFile);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream mOutput = new FileOutputStream(outFileName);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer)) > 0) {
            mOutput.write(mBuffer, 0, mLength);
        }
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

    //Open the database, so we can query it
    public boolean openDataBase() throws SQLException {
        String mPath = DB_PATH + DB_NAME;
        //Log.v("mPath", mPath);
        mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.CREATE_IF_NECESSARY);
        //mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
        return mDataBase != null;
    }

    public boolean InsertNotificationDetail(MessageModel message) {
        boolean result = false;
        try {
            ContentValues values = new ContentValues();
            // values.put(COLUMN_NOTIF_ID, message.id);
            values.put(COLUMN_NOTIF_MSG, message.msg);
            values.put(COLUMN_NOTIF_TIME, (System.currentTimeMillis() / 1000));
            values.put(COLUMN_NOTIF_TITLE, message.title);
            long id = mDataBase.insert(TABLE_NOTIFICATION, null, values);
            Log.e(TABLE_NOTIFICATION, "" + id);
            if (id != -1) {
                result = true;
            } else {
                result = false;
            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
        return result;
    }

    @Override
    public synchronized void close() {
        if (mDataBase != null)
            mDataBase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
