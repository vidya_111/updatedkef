package com.observatory.mcqdatabase;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.observatory.mcqmodels.StandardModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anuj on 22-05-2015.
 */
public class StandardTable {

    private SQLiteDatabase database;
    private DatabaseHelper databaseHelper;
    private Context context;
    private static String TableName = "ma_std";
    private boolean result;


    private static String Standard_Name = "standard_name";
    private static String Standard_Images = "standard_images";


    public StandardTable(SQLiteDatabase database) {
        this.database = database;
    }

//    public static String CreateTable() {
//        String query = "Create table " + TableName + " ( " +
//                Standard_Name + " varchar," +
//                Standard_Images + " int " +
//                ");";
//
//        return query;
//
//    }
//
//    public boolean insertStandards(CommonModel object) {
//
//        long result = 0;
//
//        if (object == null) {
//            return false;
//        } else {
//            database = DatabaseManager.getInstance().openDatabase();
//
//            ContentValues values = new ContentValues();
//            values.put(Standard_Name, object.getContent());
//            values.put(Standard_Images, object.getImage());
//
//            try {
//                result = database.insert(TableName, null, values);
//                Log.e("Standard", "" + result);
//            } catch (SQLiteException e) {
//
//            }
//            DatabaseManager.getInstance().closeDatabase();
//
//            if (result == 1) {
//                return true;
//            } else {
//                return false;
//
//            }
//
//
//        }
//
//
//    }

    public List<StandardModel> getStandards(int id) {

        List<StandardModel> list = new ArrayList<>();
        database = DatabaseManager.getInstance().openDatabase();
        String query = "Select * from " + TableName + " where language_id=" + id;

        Cursor cursor = database.rawQuery(query, null);

        while (cursor != null && cursor.moveToNext()) {

            StandardModel model = new StandardModel();

            model.setStandardId(cursor.getInt(0));
            model.setLanguageId(cursor.getInt(1));
            model.setLanguageName(cursor.getString(2));
            model.setStandardName(cursor.getString(3));
            model.setStandardImages(cursor.getString(4));

            list.add(model);


        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();

        return list;


    }
}
