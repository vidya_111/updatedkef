package com.observatory.mcqdatabase;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.observatory.mcqmodels.McqModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anuj on 22-05-2015.
 */
public class ChapterTable {

    private SQLiteDatabase database;
    private static String TableName = "chapters";


    public ChapterTable(SQLiteDatabase database) {
        this.database = database;
    }


    public List<McqModel> getChapters(int id) {

        List<McqModel> list = new ArrayList<>();
        database = DatabaseManager.getInstance().openDatabase();
        String query = "Select * from " + TableName + " where subject_serial_no=" + id;

        Cursor cursor = database.rawQuery(query, null);

        while (cursor != null && cursor.moveToNext()) {

            McqModel model = new McqModel();

            model.mcq_id = cursor.getInt(0);
            model.font = cursor.getString(3);
            model.chapter_name = cursor.getString(4);
            model.has_mcq = cursor.getInt(5);
            model.has_qb = cursor.getInt(6);

            list.add(model);


        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();

        return list;


    }
}
