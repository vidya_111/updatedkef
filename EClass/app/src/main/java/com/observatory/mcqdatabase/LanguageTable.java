package com.observatory.mcqdatabase;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.observatory.mcqmodels.LanguageModel;

import java.util.ArrayList;

/**
 * Created by Anuj on 22-05-2015.
 */
public class LanguageTable {

    private SQLiteDatabase database;

    private Context context;
    private static String TableName = "ma_lang";
    private boolean result;


    private static String Language_Name = "language_name";
    private static String Language_Image = "language_image";

    public LanguageTable(SQLiteDatabase database) {
        this.database = database;
    }

    public static String CreateTable() {
        String query = "Create table " + TableName + " ( " +
                Language_Name + " varchar," +
                Language_Image + " int " +
                ");";

        return query;

    }

//    public boolean insertLanguages(LanguageModel object) {
//
//        long result = 0;
//
//        if (object == null) {
//            return false;
//        } else {
//            database = DatabaseManager.getInstance().openDatabase();
//
//            ContentValues values = new ContentValues();
//            values.put(Language_Name, object.getContent());
//            values.put(Language_Image, object.getImage());
//
//            try {
//                result = database.insert(TableName, null, values);
//                Log.e("Language", "" + result);
//            } catch (SQLiteException e) {
//
//            }
//            DatabaseManager.getInstance().closeDatabase();
//
//
//            if (result == 1) {
//                return true;
//            } else {
//                return false;
//
//            }
//
//
//        }
//
//
//    }

    public ArrayList<LanguageModel> getLanguages() {

        ArrayList<LanguageModel> list = new ArrayList<>();
        database = DatabaseManager.getInstance().openDatabase();
        String query = "Select * from " + TableName;

        Cursor cursor = database.rawQuery(query, null);

        while (cursor != null && cursor.moveToNext()) {

            LanguageModel model = new LanguageModel();

            model.setLanguageId(cursor.getInt(0));
            model.setLanguageName(cursor.getString(1));
            model.setLanguageImages(cursor.getString(2));

            Log.e("Lang Image", "" + cursor.getString(2));

            list.add(model);


        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();

        return list;


    }
}
