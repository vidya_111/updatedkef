package com.observatory.mcqdatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.observatory.mcqmodels.McqContentModel;

import java.util.ArrayList;

/**
 * Created by Anuj on 23-05-2015.
 */
public class McqTestContentTable {

    private SQLiteDatabase database;
    private DatabaseHelper databaseHelper;
    private Context context;
    private static String TableName = "mcq_data";
    private boolean result;


    private static String Selected_Option = "selected_option";


    public McqTestContentTable() {
        this.database = database;
    }


    public ArrayList<McqContentModel> getMcqTest(int id) {

        ArrayList<McqContentModel> list = new ArrayList<>();
        database = DatabaseManager.getInstance().openDatabase();
        String query = "Select * from " + TableName + " where mcq_serial_no=" + id;

        Cursor cursor = database.rawQuery(query, null);

        while (cursor != null && cursor.moveToNext()) {


            McqContentModel model = new McqContentModel();
            model.mcq_content_id = cursor.getInt(0);
            model.mcq_id = cursor.getInt(1);
            model.mcq_name = cursor.getString(2);
            model.font = cursor.getString(3);
            model.isQuesHtml = cursor.getInt(4);
            model.isQuesImage = cursor.getString(5);
            model.question = cursor.getString(6);
            model.isOpt1Html = cursor.getInt(7);
            model.isOpt1Image = cursor.getString(8);
            model.font1=cursor.getString(9);
            model.option1 = cursor.getString(10);
            model.isOpt2Html = cursor.getInt(11);
            model.isOpt2Image = cursor.getString(12);
            model.font2=cursor.getString(13);
            model.option2 = cursor.getString(14);
            model.isOpt3Html = cursor.getInt(15);
            model.isOpt3Image = cursor.getString(16);
            model.font3=cursor.getString(17);
            model.option3 = cursor.getString(18);
            model.isOpt4Html = cursor.getInt(19);
            model.isOpt4Image = cursor.getString(20);
            model.font4=cursor.getString(21);
            model.option4 = cursor.getString(22);
            model.selected_option = cursor.getInt(23);
            model.correct_ans_option = cursor.getInt(24);



            list.add(model);


        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();

        return list;


    }

    public ArrayList<McqContentModel> getMcqTestResults(int id) {

        ArrayList<McqContentModel> list = new ArrayList<>();
        database = DatabaseManager.getInstance().openDatabase();
        String query = "Select * from " + TableName + " where mcq_serial_no=" + id+" AND selected_option>0";

        Cursor cursor = database.rawQuery(query, null);

        while (cursor != null && cursor.moveToNext()) {


            McqContentModel model = new McqContentModel();
            model.mcq_content_id = cursor.getInt(0);
            model.mcq_id = cursor.getInt(1);
            model.mcq_name = cursor.getString(2);
            model.font = cursor.getString(3);
            model.isQuesHtml = cursor.getInt(4);
            model.isQuesImage = cursor.getString(5);
            model.question = cursor.getString(6);
            model.isOpt1Html = cursor.getInt(7);
            model.isOpt1Image = cursor.getString(8);
            model.font1=cursor.getString(9);
            model.option1 = cursor.getString(10);
            model.isOpt2Html = cursor.getInt(11);
            model.isOpt2Image = cursor.getString(12);
            model.font2=cursor.getString(13);
            model.option2 = cursor.getString(14);
            model.isOpt3Html = cursor.getInt(15);
            model.isOpt3Image = cursor.getString(16);
            model.font3=cursor.getString(17);
            model.option3 = cursor.getString(18);
            model.isOpt4Html = cursor.getInt(19);
            model.isOpt4Image = cursor.getString(20);
            model.font4=cursor.getString(21);
            model.option4 = cursor.getString(22);
            model.selected_option = cursor.getInt(23);
            model.correct_ans_option = cursor.getInt(24);


            list.add(model);


        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();

        return list;


    }

//    public ArrayList<McqContentModel> getMcqTestResults(int id) {
//
//        ArrayList<McqContentModel> list = new ArrayList<>();
//        database = DatabaseManager.getInstance().openDatabase();
//        String query = "Select * from " + TableName + " where mcq_id=" + id;
//
//        Cursor cursor = database.rawQuery(query, null);
//
//        while (cursor != null && cursor.moveToNext()) {
//
//
//            McqContentModel model = new McqContentModel();
//
//            model.setMcq_content_id(cursor.getInt(0));
//            model.setMcq_id(cursor.getInt(1));
//            model.setMcq_name(cursor.getString(2));
//            model.setQuestion(cursor.getString(3));
//            model.setOption1(cursor.getString(4));
//            model.setOption2(cursor.getString(5));
//            model.setOption3(cursor.getString(6));
//            model.setOption4(cursor.getString(7));
//            model.setSelected_option(cursor.getInt(8));
//            model.setCorrect_ans_option(cursor.getInt(9));
//
//
//            list.add(model);
//
//
//        }
//
//        cursor.close();
//        DatabaseManager.getInstance().closeDatabase();
//
//        return list;
//
//
//    }

    public void setSelected_Option(int mcq_id, int selectedOption) {

        long result = 0;
        database = DatabaseManager.getInstance().openDatabase();
        String where = "mcq_content_serial_no" + " =? ";
        String[] whereArgs = new String[]{String.valueOf(mcq_id)};
        ContentValues values = new ContentValues();
        values.put(Selected_Option, selectedOption);

        result = database.update(TableName, values, where, whereArgs);

        Log.e("Update", "" + result);

        DatabaseManager.getInstance().closeDatabase();

    }

    public void resetSelectOptions() {

        database = DatabaseManager.getInstance().openDatabase();
        String query = "Update " + TableName + " SET selected_option=0";

        try {
            database.execSQL(query);
        } catch (SQLiteException e) {

        }
        DatabaseManager.getInstance().closeDatabase();

    }
}
