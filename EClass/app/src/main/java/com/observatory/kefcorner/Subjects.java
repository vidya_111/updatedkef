package com.observatory.kefcorner;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.observatory.Assessment.ConstantManager;
import com.observatory.database.Master;
import com.observatory.database.Statistics;
import com.observatory.database.Subject;
import com.observatory.database.Textbook;
import com.observatory.database.userStatistics;
import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.Constants;
import com.observatory.utils.EmailHandler;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;
import com.orm.query.Condition;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.observatory.kefcorner.R.menu.options;


public class Subjects extends AppCompatActivity implements DatePickerFragment.OnDateSetListener {

    private static final String TAG = "Subjects";

    private GridView grdSubjects;
    private RecyclerView rvSubjects;
    SubjectAdapter subjectAdapter;
    //private GridAdapter gridAdapter;

    //SharedPreferences pref;
    //Boolean isAssessment=false;
    private int[] colors;
    private String[] subjects;
    private String[] displaySubjects;

    private int[] images;


    private HashMap<String, Integer> mapImageID = new HashMap<String, Integer>();
    private HashMap<String, Integer> mapColors = new HashMap<String, Integer>();
    private ArrayList<Subject> subjectsArray;
    private HashMap<String, String> dimensions;
    private TextView tvFromDate;
    private TextView tvToDate;
    private String fromDate;
    private String toDate;
    public static String getAllSubjects = "";
    public static String getFolderName = "";
    public static boolean addSubjectToString = false;

    public DrawerLayout drawerLayout;
    public ActionBarDrawerToggle actionBarDrawerToggle;
    protected androidx.appcompat.app.ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subjects);

        //actionBar.setTitle(SettingPreffrences.getSubjectTitle(getApplicationContext()));
        SettingPreffrences.setStandard(getApplicationContext(), SettingPreffrences.getSubjectTitle(getApplicationContext()));
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(SettingPreffrences.getSubjectTitle(getApplicationContext()));
        setNavigationDrawer();

        // to make the Navigation drawer icon always appear on the action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        grdSubjects = (GridView) findViewById(R.id.grdSubjects);
        rvSubjects = (RecyclerView) findViewById(R.id.rv_test_subjects);

        // by default, From Date is set to two months ago
        fromDate = getDateTwoMonthsAgo();

        // by default, todays date is set to To Date
        String currentDate = new SimpleDateFormat(Constants.UI_DATE_FORMAT_ONLY_DATE).format(new Date());
        toDate = currentDate;

        if (mapImageID.isEmpty()) {
            mapImageID.put("er47sd", R.drawable.eng_science_1);
            mapImageID.put("er48sd", R.drawable.eng_science_2);
            mapImageID.put("uxv35", R.drawable.maths_1);
            mapImageID.put("uxv36", R.drawable.maths_2);
            mapImageID.put("uxv34", R.drawable.ic_maths);
            mapImageID.put("er44sd", R.drawable.ic_science_a);
            mapImageID.put("er45sd", R.drawable.ic_science);
            mapImageID.put("er46sd", R.drawable.ic_science_b);
            mapImageID.put("etc", R.drawable.ic_history);
            mapImageID.put("dfj8sdh", R.drawable.ic_geography);
            mapImageID.put("SAME4", R.drawable.ic_economics);
            mapImageID.put("idfK23", R.drawable.ic_grammar);
            mapImageID.put("9232378v39934734", R.drawable.ic_balbharati);

            mapImageID.put("win32", R.drawable.ic_civics);

            mapImageID.put("misc32sd", R.drawable.ic_politicalsci);
            mapImageID.put("ioSD6zz", R.drawable.ic_algebra);
            mapImageID.put("Sd97cv", R.drawable.ic_geometry);

            mapImageID.put("bin", R.drawable.ic_evs);
            mapImageID.put("abs62ed", R.drawable.ic_evs);
            mapImageID.put("JGD8sdy", R.drawable.ic_evs);

            mapImageID.put("abs62ed", R.drawable.ic_evs);
            mapImageID.put("JGD8sdy", R.drawable.ic_evs);

            mapImageID.put("khdb", R.drawable.ic_my_english_book);
            mapImageID.put("khnur", R.drawable.ic_nursery_rhyms);
            mapImageID.put("khgen", R.drawable.ic_general);

            mapImageID.put("IoAZSm", R.drawable.ic_hindi_grammer);
            mapImageID.put("KsLCEq", R.drawable.ic_marathi_grammer);
            mapImageID.put("IutCUx", R.drawable.ic_maths_trick);
            mapImageID.put("lFDsuN", R.drawable.ic_spoken_eng);

            mapImageID.put("goku4t", R.drawable.ic_marathi_kumarbharti);
            mapImageID.put("kgudbn", R.drawable.ic_english_kumarbharti);
            mapImageID.put("exod3a", R.drawable.ic_english_coursebook);
            mapImageID.put("saei41", R.drawable.ic_hindi_lokbharti);
            mapImageID.put("ie2abd", R.drawable.ic_marathi_aksharbharti);
            mapImageID.put("bxx31a", R.drawable.ic_hindi_lokwani);
            mapImageID.put("oeibgi", R.drawable.ic_marathi_aantarbharti);
            //srj
            mapImageID.put("dsthre5", R.drawable.ic_hindi_sulabhbharati);
            mapImageID.put("mjrr43252", R.drawable.ic_marathi_sulabhbharati);
            //dsthre5 - Hindi Sulab bharti
            //mjrr43252 - Marathi Sulabh bharti

        }


        if (mapColors.isEmpty()) {
            mapColors.put("er47sd", R.color.eng_sci_1);
            mapColors.put("er48sd", R.color.eng_sci_2);
            mapColors.put("uxv35", R.color.maths_1);
            mapColors.put("uxv36", R.color.maths_2);
            mapColors.put("uxv34", R.color.maths);
            mapColors.put("er44sd", R.color.science_a);
            mapColors.put("er45sd", R.color.science);
            mapColors.put("er46sd", R.color.science_b);
            mapColors.put("etc", R.color.history);
            mapColors.put("dfj8sdh", R.color.geography);
            mapColors.put("SAME4", R.color.economics);
            mapColors.put("idfK23", R.color.grammar);
            mapColors.put("win32", R.color.civics);
            mapColors.put("misc32sd", R.color.pol_sci);
            mapColors.put("ioSD6zz", R.color.algebra);
            mapColors.put("Sd97cv", R.color.geometry);

            mapColors.put("bin", R.color.evs);
            mapColors.put("abs62ed", R.color.evs);
            mapColors.put("JGD8sdy", R.color.evs);

            mapColors.put("abs62ed", R.color.evs);
            mapColors.put("JGD8sdy", R.color.evs);

            mapColors.put("khdb", R.color.my_first_english);
            mapColors.put("9232378v39934734", R.color.bal_bharati);
            mapColors.put("khnur", R.color.nursery);
            mapColors.put("khgen", R.color.general);

            mapColors.put("IoAZSm", R.color.IoAZSm);
            mapColors.put("KsLCEq", R.color.KsLCEq);
            mapColors.put("IutCUx", R.color.IutCUx);
            mapColors.put("lFDsuN", R.color.lFDsuN);

            mapColors.put("goku4t", R.color.color_marathi_kumarbharti);
            mapColors.put("kgudbn", R.color.color_english_kumarbharti);
            mapColors.put("exod3a", R.color.color_english_coursebook);
            mapColors.put("saei41", R.color.color_hindi_lokbharti);
            mapColors.put("ie2abd", R.color.color_marathi_aksharbharti);
            mapColors.put("bxx31a", R.color.color_hindi_lokwani);
            mapColors.put("oeibgi", R.color.color_marathi_aantarbharti);
            //srj
            mapColors.put("dsthre5", R.color.color_hindi_sullabh_bharti);
            mapColors.put("mjrr43252", R.color.color_marathi_sullabh_bharti);
            //dsthre5 - Hindi Sulab bharti
            //mjrr43252 - Marathi Sulabh bharti

        }
       /* pref = getSharedPreferences("TypeDetail", MODE_PRIVATE);
        String type=pref.getString("Type","");
        int typeID=pref.getInt("TypeID",0);
        if(type.equals("Assessment") || typeID == 1)
        {
            isAssessment=true;

        }*/
        String whereClause = "SUBJECT_ID != ''";
        subjectsArray = (ArrayList<Subject>) Select.from(Subject.class).where(whereClause).groupBy("SUBJECT_NAME").list();
        subjects = new String[subjectsArray.size()];
        images = new int[subjectsArray.size()];
        colors = new int[subjectsArray.size()];
        displaySubjects = new String[subjectsArray.size()];


        if (getAllSubjects.equals("")) {
            addSubjectToString = true;
        } else {
            addSubjectToString = false;
        }
        Log.d("boolean", "value: " + addSubjectToString);

        for (int i = 0; i < subjectsArray.size(); i++) {
            try {
                Log.d("subjectIDHere", ":: " + subjectsArray.get(i).getSubjectId());
                //Changes
                String subjectName = subjectsArray.get(i).getSubjectName().toString();//.toUpperCase();
                subjects[i] = subjectName;
                images[i] = mapImageID.get(subjectName);
                colors[i] = mapColors.get(subjectName);
                displaySubjects[i] = subjectsArray.get(i).getDisplaySubjectName().toUpperCase();
                getFolderName = subjectsArray.get(i).getFolderName();
                if (addSubjectToString) {
                    getAllSubjects += subjectsArray.get(i).getDisplaySubjectName();
                    if (i < subjectsArray.size() - 1) {
                        getAllSubjects = getAllSubjects + ",";
                    }
                }
            } catch (Exception e) {
                Log.e("Error for " + i, e.getMessage());
            }

        }

        subjectAdapter = new SubjectAdapter(displaySubjects, images, colors);
        rvSubjects.setLayoutManager(new GridLayoutManager(this, 2));
        rvSubjects.setAdapter(subjectAdapter);

        grdSubjects.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i = new Intent(Subjects.this, Chapters.class);
                SettingPreffrences.setSubjectId(Subjects.this, subjectsArray.get(position).getSubjectId());
                SettingPreffrences.setSubject(Subjects.this, subjectsArray.get(position).getDisplaySubjectName());

                i.putExtra(Chapters.SUBJECT_NAME, subjectsArray.get(position).getSubjectName());
                App.getInstance().trackEvent("Chapter", "Screen", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " + "selectedStandard - " + SettingPreffrences.getStandard(Subjects.this) + "" + " ; " + "selectedSubject - " + SettingPreffrences.getSubject(Subjects
                        .this), "");
                startActivity(i);

            }
        });

        Master master = Master.listAll(Master.class).get(0);

        Log.d("CourseID: ", "Pref: " + SettingPreffrences.getCourseId(getApplicationContext()) + " master: " + master.getCourse_id());


        autoSyncStatistics(false, false);
        if (!SettingPreffrences.isUserSynced(this)) {
            updateLastSeen();
            autoSyncLoginHistory();
            SettingPreffrences.setUserSync(getApplicationContext(), true);
        }
    }

    private void setNavigationDrawer() {
        drawerLayout = findViewById(R.id.my_drawer_layout);
        NavigationView navigationView = findViewById(R.id.navigation);

        navigationView.findViewById(R.id.kef_textbook);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.profile_navigation:
                        if (SettingPreffrences.getLoginDone(getApplicationContext())) {
                            startActivity1(Profile.class);
                        } else {
                            showLoginAlert();
                        }
                        drawerLayout.closeDrawers();
                        break;

                    /*case R.id.about_us_navigation:
                        startActivity1(How.class);
                        drawerLayout.closeDrawers();
                        break;*/

                    case R.id.user_history_navigation:
                        startActivity1(UserHistory.class);
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.sync_navigation:
                        boolean loggedIn = isLoggedIn();
                        if (loggedIn) {
                            List<userStatistics> statisticsList = Select.from(userStatistics.class).where("ISSYNCED = 0").list();
                            // there is data to sync
                            if (statisticsList.size() > 0) {
                                String statsJson = formatStatisticsToJsonNew(statisticsList);
                                callSyncStatisticsWebService(statsJson, statisticsList, true, true);
                            } else {
                                // no data to sync
                                showSendReportDialog();
                            }
                        } else {
                            showLoginAlert();
                        }
                        drawerLayout.closeDrawers();
                        break;

                    /*case R.id.share_navigation:
                        try {
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            String sAux = "Hi, I am using free E-class app for my studies and revision. " + "It" + " helped me in my studies and it can help you too.\n";
                            sAux = sAux + "Download the app at : https://play.google" + ".com/store/apps/details?id=com.observatory.sundaram\n";
                            sAux = sAux + "Or visit www.e-class.in";
                            i.putExtra(Intent.EXTRA_TEXT, sAux);
                            startActivity(Intent.createChooser(i, "choose one"));
                        } catch (Exception e) { //e.toString();
                        }
                        drawerLayout.closeDrawers();
                        break;*/

                    case R.id.message_navigation:
                        startActivity1(MessagesActivity.class);
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.doubtbox_navigation:
                        startActivity1(Doubt_Conversations.class);
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.kef_corner_navigation:
                        startActivity1(KefCorner.class);
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.kef_textbook:
                        startActivity1(TextbookActivity.class);
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.kef_other:
                        startActivity1(OtherFilesActivity.class);
                        drawerLayout.closeDrawers();
                        break;
                }
                return true;
            }
        });
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.nav_open, R.string.nav_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }


    private void autoSyncStatistics(boolean isForeground, boolean showReportOption) {
        long lastSyncedId = SettingPreffrences.getLastSyncedMcqId(getApplicationContext());
        List<userStatistics> statisticsList;

        if (lastSyncedId != -1) {
            statisticsList = Select.from(userStatistics.class).where("ID > " + lastSyncedId).list();

        } else {
            statisticsList = Select.from(userStatistics.class).list();
        }

        // there is data to sync
        if (statisticsList.size() > 0) {
            String statsJson = formatStatisticsToJsonNew(statisticsList);
            Log.d("KefSyncStat", "size: " + statisticsList.size() + " json: " + statsJson);
            callSyncStatisticsWebService(statsJson, statisticsList, isForeground, showReportOption);
        } else {
            if (isForeground) {
                alert1("Data is already synced");
            }
        }
    }

    private void autoSyncLoginHistory() {
        long lastSyncedId = SettingPreffrences.getLastSyncedLoginHistoryId(getApplicationContext());
        List<com.observatory.database.UserHistory> userHistoryList;

        if (lastSyncedId != -1) {
            userHistoryList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com.observatory.database.UserHistory.class).where("id > " + lastSyncedId).orderBy("id asc").list(); /* ordering in ascending so that latest appear at the end
					. */

        } else {
//			userHistoryList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com
//					.observatory.database.UserHistory.class)
//					.list();

            userHistoryList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com.observatory.database.UserHistory.class).orderBy("id asc").list(); /* ordering in ascending so that latest appear at the end
					. */
        }

        // there is data to sync
        if (userHistoryList.size() > 0) {
            String statsJson = formatLoginHistoryToJson(userHistoryList);
            callSyncLoginHistoryWebService(statsJson, userHistoryList);
        }
    }


    private void updateLastSeen() {

        if (SettingPreffrences.isAppLaunched(getApplicationContext())) {
            String medium = getMedium();
            String userid = SettingPreffrences.getUserid(this);
            String time = new SimpleDateFormat(Constants.UI_TIME_FORMAT).format(Calendar.getInstance().getTime());
            String date = new SimpleDateFormat(Constants.UI_DATE_FORMAT_ONLY_DATE).format(Calendar.getInstance().getTime());

            com.observatory.database.UserHistory userHistory = new com.observatory.database.UserHistory(date, time, userid.isEmpty() ? "0" : userid, medium);
            userHistory.save();
            Log.d(TAG, "updateLastSeen: " + userid);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.assessment_refresh_menu, menu);
        //  getMenuInflater().inflate(options, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        /*switch (item.getItemId()) {


            case R.id.profile:

                if (SettingPreffrences.getLoginDone(getApplicationContext())) {
                    startActivity1(Profile.class);
                } else {
                    showLoginAlert();
                }
                break;


            case R.id.about_us:


                startActivity1(How.class);

                break;

            case R.id.userProfile:


                startActivity1(UserHistory.class);

                break;

            case R.id.ask_doubt_id:

                startActivity1(Doubt_Conversations.class);

                break;

            case R.id.kef_corner_id:

                startActivity1(KefCorner.class);

                break;

            case R.id.notifications:

                startActivity1(MessagesActivity.class);

                break;

            case R.id.share:
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    String sAux = "Hi, I am using free E-class app for my studies and revision. " + "It" + " helped me in my studies and it can help you too.\n";
                    sAux = sAux + "Download the app at : https://play.google" + ".com/store/apps/details?id=com.observatory.sundaram\n";
                    sAux = sAux + "Or visit www.e-class.in";
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "choose one"));
                } catch (Exception e) { //e.toString();
                }
                break;

            case R.id.feedback:

                try {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@e-class.in"});
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback");

                    startActivity(intent);
                } catch (ActivityNotFoundException e) {

                    Toast.makeText(this, "No Applications were found to handle this action", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.rate_us:

                CommonFunctions.openRateMyApp(this);
                break;

            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.sync:


                boolean loggedIn = isLoggedIn();


                if (loggedIn) {

                    List<userStatistics> statisticsList = Select.from(userStatistics.class).where("ISSYNCED = 0").list();

                    // there is data to sync
                    if (statisticsList.size() > 0) {

                        String statsJson = formatStatisticsToJsonNew(statisticsList);
                        callSyncStatisticsWebService(statsJson, statisticsList, true);

                    } else {
                        // no data to sync
                        showSendReportDialog();
                    }


                } else {
                    showLoginAlert();
                }
                break;

        }*/

        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        int id = item.getItemId();

        if (id == R.id.sync_data) {
            autoSyncStatistics(true, false);
            autoSyncLoginHistory();
        }
        return super.onOptionsItemSelected(item);
    }

    // isForeground : when user initiates it
    private void callSyncStatisticsWebService(String json, final List<userStatistics> statisticsToBeUpdated, final boolean isForeground, final boolean showReportOption) {
try {
    if (CommonFunctions.isNetworkConnected(this)) {

        if (isForeground) {
            CommonFunctions.showDialog(this, "Syncing...");
        }

        String userId = SettingPreffrences.getUserid(getApplicationContext()).isEmpty() ? "0" : SettingPreffrences.getUserid(getApplicationContext());
        String token = SettingPreffrences.getToken(getApplicationContext()).isEmpty() ? "0000" : SettingPreffrences.getToken(getApplicationContext());
        //String deviceId = CommonFunctions.getUniqueDeviceId(getApplicationContext());

        Log.d("KefSyncStat", "Parameters: " + "userID: " + userId + " token: " + token + "Medium: " + getMedium() + " userStat: " + json);

        Ion.with(this).load(NetworkUrl.KEF_Localhost + NetworkUrl.KEF_SyncStatistics)


                /*.setBodyParameter("userId", userId)
                .setBodyParameter("token", token)
                .setBodyParameter("medium", "")
                .setBodyParameter("email", "")
                .setBodyParameter("imeiNo", deviceId)
                .setBodyParameter("startDate", "")
                .setBodyParameter("endDate", "")
                .setBodyParameter("userStat", json)*/

                .setBodyParameter("os", Splash.OS)
                .setBodyParameter("make", Splash.MAKE)
                .setBodyParameter("model", Splash.MODEL)
                .setBodyParameter("UserID", userId)
                .setBodyParameter("token", token)
                .setBodyParameter("medium", getMedium())
                .setBodyParameter("email", "")
                .setBodyParameter("startDate", "")
                .setBodyParameter("endDate", "")
                .setBodyParameter("userStat", json).asString().setCallback(new FutureCallback<String>() {

            @Override
            public void onCompleted(Exception e, String result) {

                if (e == null) {

                    Log.d("userStatResponse", ":: " + result);

                    updateSyncState(statisticsToBeUpdated);

                    if (isForeground && showReportOption) {
                        showSendReportDialog();
                    }

                    if (!showReportOption) {
                        Toast.makeText(getApplicationContext(), "Data Synced Successfully !!", Toast.LENGTH_SHORT).show();
                    }

                } else {

                    if (isForeground) {
                        alert1("Something went wrong, please try again later.");
                    }
                }

                if (isForeground) {
                    CommonFunctions.hideDialog();
                }

            }
        });
    } else {

        if (isForeground) {
            alert1("Internet Connection not available");
        }
    }
}catch (Exception e){

}

    }

    private void callSyncLoginHistoryWebService(String json, final List<com.observatory.database.UserHistory> userHistoryList) {

        if (CommonFunctions.isNetworkConnected(this)) {

            String userId = SettingPreffrences.getUserid(getApplicationContext()).isEmpty() ? "0" : SettingPreffrences.getUserid(getApplicationContext());
            String token = SettingPreffrences.getToken(getApplicationContext()).isEmpty() ? "0000" : SettingPreffrences.getToken(getApplicationContext());
            String deviceId = CommonFunctions.getUniqueDeviceId(getApplicationContext());

            Ion.with(this).load(NetworkUrl.syncLoginHistory)
                    .setBodyParameter("userId", userId)
                    .setBodyParameter("token", token)
                    .setBodyParameter("os", Build.VERSION.RELEASE)
                    .setBodyParameter("make", Build.MANUFACTURER)
                    .setBodyParameter("model", Build.MODEL)
                    .setBodyParameter("historyDetails", json)
                    .asString().setCallback(new FutureCallback<String>() {

                @Override
                public void onCompleted(Exception e, String result) {

                    if (e == null) {
                        // update last synced Id of UserHistory table
                        Log.d("ApiResponse", "UserHistoryAutoSync: " + result);
                        com.observatory.database.UserHistory lastSyncedUserHistory = userHistoryList.get(userHistoryList.size() - 1);
                        SettingPreffrences.setLastSyncedLoginHistoryId(getApplicationContext(), lastSyncedUserHistory.getId());
                    } else {
                        new EmailHandler().sendMail("phone@millicent.in", "KEF Sync", e.getMessage());
                    }
                }
            });
        }

    }

    private void updateSyncState(List<userStatistics> statisticsList) {

        userStatistics lastSyncedStat = statisticsList.get(statisticsList.size() - 1);
        SettingPreffrences.setLastSyncedMcqId(getApplicationContext(), lastSyncedStat.getId());
        //Initially commented uncommented by divyesh 17/05/2019
        for (userStatistics statistics : statisticsList) {
            statistics.setIs_synced(true);
            Log.d("Subjects : ", "Content name before DB update -> " + statistics.getContentName());
            Statistics.save(statistics);  // @vidya changed method to "save" on 28th Dec 2020, earlier it was "update"
        }
        //uncommented by divyesh 17/05/2019
    }

    private void callSendReportWebService(String email) {

        if (CommonFunctions.isNetworkConnected(this)) {

            CommonFunctions.showDialog(this, "Sending report...");

            String userId = SettingPreffrences.getUserid(getApplicationContext()).isEmpty() ? "0" : SettingPreffrences.getUserid(getApplicationContext());
            String token = SettingPreffrences.getToken(getApplicationContext()).isEmpty() ? "0000" : SettingPreffrences.getToken(getApplicationContext());
            String deviceId = CommonFunctions.getUniqueDeviceId(getApplicationContext());

            Ion.with(this).load(NetworkUrl.KEF_Localhost + NetworkUrl.KEF_SyncStatistics)

                    /*.setBodyParameter("userId", userId)
                    .setBodyParameter("token", token)
                    .setBodyParameter("medium", getMedium())
                    .setBodyParameter("email", email)
                    .setBodyParameter("imeiNo", deviceId)
                    .setBodyParameter("startDate", CommonFunctions.formatDate(fromDate, Constants.UI_DATE_FORMAT_ONLY_DATE, Constants.FILTER_DATE_FORMAT))
                    .setBodyParameter("endDate", CommonFunctions.formatDate(toDate, Constants.UI_DATE_FORMAT_ONLY_DATE, Constants.FILTER_DATE_FORMAT))
                    .setBodyParameter("userStat", "")*/

                    .setBodyParameter("os", Splash.OS)
                    .setBodyParameter("make", Splash.MAKE)
                    .setBodyParameter("model", Splash.MODEL)
                    .setBodyParameter("UserID", userId)
                    .setBodyParameter("token", token)
                    .setBodyParameter("medium", getMedium())
                    .setBodyParameter("email", email)
                    .setBodyParameter("startDate", CommonFunctions.formatDate(fromDate, Constants.UI_DATE_FORMAT_ONLY_DATE, Constants.FILTER_DATE_FORMAT))
                    .setBodyParameter("endDate", CommonFunctions.formatDate(toDate, Constants.UI_DATE_FORMAT_ONLY_DATE, Constants.FILTER_DATE_FORMAT))
                    .setBodyParameter("userStat", "")

                    .asString().setCallback(new FutureCallback<String>() {

                @Override
                public void onCompleted(Exception e, String result) {

                    if (e == null) {

                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            alert1(jsonObject.getString("message"));

                        } catch (JSONException e1) {
                            alert1("Some error occurred while sending report");
                        }

                    } else {
                        alert1("Something went wrong, please try again later.");
                    }

                    CommonFunctions.hideDialog();
                }
            });
        } else {

            alert1("Internet Connection not available");
        }


    }

    private boolean isLoggedIn() {
        String userid = SettingPreffrences.getUserid(getApplicationContext());
        return !userid.isEmpty() && !userid.equals("0");
    }

    private void showSendReportDialog() {

        View dialogView = View.inflate(this, R.layout.dialog_send_report, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        final AlertDialog dialog = builder.create();

        tvFromDate = (TextView) dialogView.findViewById(R.id.tv_from_date);
        tvToDate = (TextView) dialogView.findViewById(R.id.tv_to_date);
        final EditText etEmail = (EditText) dialogView.findViewById(R.id.et_email);
        View tvSendReportBtn = dialogView.findViewById(R.id.tv_send_report_btn);
        View tvCancelBtn = dialogView.findViewById(R.id.tv_cancel_btn);

        tvFromDate.setText(fromDate);
        tvToDate.setText(toDate);

        tvFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialogFragment = DatePickerFragment.newInstance(getDateAsLong(fromDate), getDateAsLong(toDate), DatePickerFragment.SELECT_FROM_DATE);
                dialogFragment.show(getSupportFragmentManager(), DatePickerFragment.class.toString());
            }
        });

        tvToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialogFragment = DatePickerFragment.newInstance(getDateAsLong(fromDate), getDateAsLong(toDate), DatePickerFragment.SELECT_TO_DATE);
                dialogFragment.show(getSupportFragmentManager(), DatePickerFragment.class.toString());
            }
        });

        tvSendReportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = etEmail.getText().toString();

                if (CommonFunctions.isValidEmail(email)) {

                    dialog.dismiss();
                    callSendReportWebService(email);

                } else {
                    Toast.makeText(Subjects.this, "Please enter your email id.", Toast.LENGTH_LONG).show();
                }
            }
        });

        tvCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private long getDateAsLong(String date) {

        long dateLong;

        try {

            dateLong = new SimpleDateFormat(Constants.UI_DATE_FORMAT_ONLY_DATE).parse(date).getTime();

        } catch (ParseException e) {

            dateLong = DatePickerFragment.DATE_NOT_SET;

        }
        return dateLong;
    }

    private void showLoginAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Eclass");
        builder.setMessage("Please login to continue");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Login", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Subjects.this, Splash.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("action", "login");
                startActivity(intent);
            }
        });

        builder.create().show();
    }

    private String formatLoginHistoryToJson(List<com.observatory.database.UserHistory> arrayUserHistory) {

        JSONArray mainJsonArr = new JSONArray();

        ArrayList<String> courseList = getCourseList();

        for (int i = 0; i < courseList.size(); i++) {

            JSONObject jsonObject = new JSONObject();

            try {

                jsonObject.put("medium", courseList.get(i));

                JSONArray dataJsonArr = new JSONArray();

                for (com.observatory.database.UserHistory userHistory : arrayUserHistory) {

                    if (userHistory.getMedium().equals(courseList.get(i))) {

                        try {

                            JSONObject dateTimeJson = new JSONObject();
                            dateTimeJson.put("date", userHistory.getDate());
                            dateTimeJson.put("time", userHistory.getTime());
                            dataJsonArr.put(dateTimeJson);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

                // send data only if current medium has login data
                if (dataJsonArr.length() > 0) {
                    jsonObject.put("data", dataJsonArr);
                    mainJsonArr.put(jsonObject);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Log.d("Tag", "formatLoginHistoryToJson: " + mainJsonArr.toString());

        return mainJsonArr.toString();
    }

    private ArrayList<String> getCourseList() {

        String userid = SettingPreffrences.getUserid(getApplicationContext());

        ArrayList<String> courseList = new ArrayList<>();
        ArrayList<com.observatory.database.UserHistory> userHistoryArrayList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com.observatory.database.UserHistory.class).groupBy("medium").orderBy("id asc").where(Condition.prop("userId").eq((userid.isEmpty() ? "0" : userid))).list();

        for (com.observatory.database.UserHistory userHistory : userHistoryArrayList) {
            courseList.add(userHistory.getMedium());
        }

        return courseList;
    }

    @Override
    public void onBackPressed() {
        Log.d("PrintCheckerHere", App.IS_HAVING_MULTIPLE_STANDARD + " :: " + ConstantManager.ISSINGLESTANDARD);
        if (App.IS_HAVING_MULTIPLE_STANDARD) {
            App.BASE_PATH = App.ORIGINAL_PATH;
           /* startActivity(new Intent(this, Splash.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            finish();*/
        } else {
            if (ConstantManager.ISSINGLESTANDARD) {
                App.BASE_PATH = App.ORIGINAL_PATH;
               /* startActivity(new Intent(this, Splash.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();*/
            } else {
                // super.onBackPressed();
            }
        }
        super.onBackPressed();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day, int whichDate) {

        String date = getDay(day) + " " + getMonth(month) + " " + year;

        if (whichDate == DatePickerFragment.SELECT_FROM_DATE) {
            fromDate = date;
            if (tvFromDate != null) tvFromDate.setText(fromDate);
        } else {
            toDate = date;
            if (tvToDate != null) tvToDate.setText(toDate);
        }
    }

    private String getDay(int day) {
        return String.format("%02d", day);
    }


    private String getMonth(int month) {

        String monthStr;

        switch (month) {

            case 0:
                monthStr = "Jan";
                break;

            case 1:
                monthStr = "Feb";
                break;

            case 2:
                monthStr = "Mar";
                break;

            case 3:
                monthStr = "Apr";
                break;

            case 4:
                monthStr = "May";
                break;

            case 5:
                monthStr = "Jun";
                break;

            case 6:
                monthStr = "Jul";
                break;

            case 7:
                monthStr = "Aug";
                break;

            case 8:
                monthStr = "Sep";
                break;

            case 9:
                monthStr = "Oct";
                break;

            case 10:
                monthStr = "Nov";
                break;

            case 11:
                monthStr = "Dec";
                break;

            // error case
            default:
                monthStr = "Month value is invalid";
        }

        return monthStr;
    }

    public String formatStatisticsToJsonNew(List<userStatistics> statisticsList) {

        String statsAsJson;

        try {
            JSONArray statsJsonArr = new JSONArray();

            for (userStatistics statistics : statisticsList) {
                if (!statistics.isIs_synced()) {
                    JSONObject aStatJson = new JSONObject();

                    aStatJson.put("StatUserId", statistics.getUserID());
                    aStatJson.put("courseId", statistics.getCourseID());
                    aStatJson.put("subjectId", statistics.getSubjectID());
                    aStatJson.put("chapterId", statistics.getChapterID());
                    aStatJson.put("chapterId", statistics.getChapterID());
                    aStatJson.put("contentTypeId", statistics.getContentTypeID());
                    aStatJson.put("contentName", statistics.getContentName());
                    aStatJson.put("contentDuration", statistics.getContentDuration());
                    aStatJson.put("AccessDuration", statistics.getAccessDuration());
                    aStatJson.put("AccessDatetime", statistics.getAccessDatetime());

                    statsJsonArr.put(aStatJson);
                }
            }

            statsAsJson = statsJsonArr.toString();

        } catch (JSONException e) {
            statsAsJson = "Error formatting Statistics data into JSON for syncing purposes";
        }

        return statsAsJson;
    }

    private String getCreatedAt(userStatistics statistics) {
        return CommonFunctions.formatDate(statistics.getCreatedOn(), Constants.DATABASE_DATE_FORMAT, Constants.SERVER_DATE_FORMAT);
    }

    public String getDateTwoMonthsAgo() {
        String dateTwoMonthsAgo;

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -2);

        dateTwoMonthsAgo = new SimpleDateFormat(Constants.UI_DATE_FORMAT_ONLY_DATE).format(new Date(calendar.getTimeInMillis()));

        return dateTwoMonthsAgo;
    }

    public String getMedium() {
        String medium;

        List<Master> masterList = Master.listAll(Master.class);
        if (masterList.size() > 0) {
            Master master = masterList.get(0);
            medium = master.getMedium();
        } else {
            medium = "Medium could not be found";
            Toast.makeText(getApplicationContext(), medium, Toast.LENGTH_LONG).show();
        }

        return medium;
    }

    protected void startActivity1(Class clazz) {
        Intent i = new Intent(this, clazz);
        startActivity(i);
    }

    protected void alert1(String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        })
                .setMessage(message)
                .setTitle("KEF")
                .show();
    }


    class SubjectAdapter extends RecyclerView.Adapter<SubjectAdapter.MyViewHolder> {

        private String[] subjectName;
        private int[] images;
        private int[] colors;

        private int selectedPos = RecyclerView.NO_POSITION;

        public SubjectAdapter(String[] subjectName, int[] images, int[] colors) {

            this.subjectName = subjectName;
            this.images = images;
            this.colors = colors;

        }

        @Override
        public SubjectAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid, parent, false);
            return new MyViewHolder(itemView);

        }

        @Override
        public void onBindViewHolder(SubjectAdapter.MyViewHolder holder, int position) {


            holder.itemView.setSelected(selectedPos == position);
            holder.imageView.setImageResource(images[position]);
            holder.textView.setText(subjectName[position]);
            holder.textView.setTextColor(getApplicationContext().getResources().getColor(colors[position]));


        }

        @Override
        public int getItemCount() {
            return subjectName.length;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView textView;
            ImageView imageView;

            public MyViewHolder(final View itemView) {
                super(itemView);

                textView = itemView.findViewById(R.id.text);
                imageView = itemView.findViewById(R.id.img);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //selector
                        notifyItemChanged(selectedPos);
                        selectedPos = getAdapterPosition();
                        notifyItemChanged(selectedPos);

                      /*  if(isAssessment)
                        {
                            Intent i = new Intent(Subjects.this, A_ChapterActivity.class);

                            SettingPreffrences.setSubject(Subjects.this, subjectsArray.get(getAdapterPosition()).getDisplaySubjectName());

                            i.putExtra(Chapters.SUBJECT_NAME, subjectsArray.get(getAdapterPosition()).getSubjectName());
                            App.getInstance().trackEvent("Chapter", "Screen", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " + "selectedStandard - " + SettingPreffrences.getStandard(Subjects.this) + "" + " ; " + "selectedSubject - " + SettingPreffrences.getSubject(Subjects
                                    .this), "");
                            startActivity(i);
                        }
                        else
                        {*/
                        Intent i = new Intent(Subjects.this, Chapters.class);
                        Log.d("SyncSubjectData", "setSubjectID: " + subjectsArray.get(getAdapterPosition()).getSubjectId() + "   " + subjectsArray.get(getAdapterPosition()).getDisplaySubjectName() + "   " + getAdapterPosition());
                        SettingPreffrences.setSubjectId(Subjects.this, subjectsArray.get(getAdapterPosition()).getSubjectId());
                        SettingPreffrences.setSubject(Subjects.this, subjectsArray.get(getAdapterPosition()).getDisplaySubjectName());

                        i.putExtra(Chapters.SUBJECT_NAME, subjectsArray.get(getAdapterPosition()).getSubjectName());
                        App.getInstance().trackEvent("Chapter", "Screen", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " + "selectedStandard - " + SettingPreffrences.getStandard(Subjects.this) + "" + " ; " + "selectedSubject - " + SettingPreffrences.getSubject(Subjects
                                .this), "");
                        startActivity(i);
                        //}

                    }
                });


            }
        }

    }


}
