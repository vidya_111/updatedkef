package com.observatory.kefcorner;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.card.MaterialCardView;
import com.observatory.AsyncTaskMain.AsyncTaskHelper;
import com.observatory.AsyncTaskMain.OnTaskComplete;
import com.observatory.adapters.CornerAdapter;
import com.observatory.adapters.MyRecycleViewClickListener;
import com.observatory.database.KefCornerStatistics;
import com.observatory.model.KefCornerModel;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.DateUtil;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class KefCorner extends BaseActivity {

    public static final int VIDEO_REQUEST_CODE = 101;
    public static final int AUDIO_REQUEST_CODE = 102;
    public static final int IMAGE_REQUEST_CODE = 103;
    public static final int PDF_REQUEST_CODE = 104;
    public static final int LINK_REQUEST_CODE = 105;

    MaterialCardView video, pdf, audio, image, links;
    String getType = "";
    CornerAdapter adapter;
    KefCornerModel model;
    KefCornerStatistics statsModel;
    RecyclerView content_list;
    ArrayList<KefCornerModel> data_list = new ArrayList<>();
    LinearLayout addContentLayout, notFoundLayout;

    boolean isVideoSelected = false, isPdfSelected = false, isAudioSelected = false, isImageSelected = false, isLinkSelected = false;

    public static int imageData = 0, videoData = 0, pdfData = 0;
    public static int pdfValue = 0, imageValue = 0, audioValue = 0;
    int GalleryID = -1;

    /*Webservice*/
    private AsyncTaskHelper asyncTaskHelper;
    private String params;
    private JSONObject jsonObject;

    SimpleDateFormat inputDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat outputDate = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    Date date = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kef_corner);
        actionBar.setTitle("KEF Corner");

    }

    public void syncKefUsage() {
        List<KefCornerStatistics> list = Select.from(KefCornerStatistics.class).where("ISSYNCED = '0'").list();
        Log.d("SizeList:", ":: " + list.size());

        if (list.size() > 0) {
            for (KefCornerStatistics update : list) {
                update.setIs_synced(true);
                update.save();
                //  KefCornerStatistics.save(update);
                addStatisticsData(
                        update.getUserID(),
                        update.getGalleryId(),
                        update.getAccessDuration(),
                        update.getAccessDateTime()
                );
            }
        }
    }

    @Override
    protected void onLayout() {
        video = findViewById(R.id.videos_Card);
        pdf = findViewById(R.id.pdf_Card);
        audio = findViewById(R.id.audio_card);
        image = findViewById(R.id.image_card);
        links = findViewById(R.id.link_card);
        content_list = findViewById(R.id.content_list);

        addContentLayout = findViewById(R.id.add_content_layout);
        notFoundLayout = findViewById(R.id.notFoundLayout);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        content_list.setLayoutManager(linearLayoutManager);


        byDefaultSelection(isVideoSelected = true, "VDO");

        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                video.setCardBackgroundColor(Color.parseColor("#F6CED8"));
                isVideoSelected = true;

                if (adapter != null) {
                    data_list.clear();
                    adapter.notifyDataSetChanged();
                }


                if (isPdfSelected || isAudioSelected || isImageSelected || isLinkSelected) {

                    pdf.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                    audio.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                    image.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                    links.setCardBackgroundColor(Color.parseColor("#FFFFFF"));

                    isPdfSelected = false;
                    isAudioSelected = false;
                    isImageSelected = false;
                    isLinkSelected = false;
                }

                getData(SettingPreffrences.getUserid(getApplicationContext()), "VDO");
            }
        });

        pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdf.setCardBackgroundColor(Color.parseColor("#F6CED8"));
                isPdfSelected = true;

                if (adapter != null) {
                    data_list.clear();
                    adapter.notifyDataSetChanged();
                }

                if (isImageSelected || isAudioSelected || isVideoSelected || isLinkSelected) {

                    video.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                    audio.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                    image.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                    links.setCardBackgroundColor(Color.parseColor("#FFFFFF"));

                    isImageSelected = false;
                    isAudioSelected = false;
                    isVideoSelected = false;
                    isLinkSelected = false;
                }

                getData(SettingPreffrences.getUserid(getApplicationContext()), "PDF");
            }
        });

        audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audio.setCardBackgroundColor(Color.parseColor("#F6CED8"));
                isAudioSelected = true;

                if (adapter != null) {
                    data_list.clear();
                    adapter.notifyDataSetChanged();
                }

                if (isVideoSelected || isPdfSelected || isImageSelected || isLinkSelected) {

                    video.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                    pdf.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                    image.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                    links.setCardBackgroundColor(Color.parseColor("#FFFFFF"));

                    isVideoSelected = false;
                    isPdfSelected = false;
                    isImageSelected = false;
                    isLinkSelected = false;
                }

                getData(SettingPreffrences.getUserid(getApplicationContext()), "ADO");
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image.setCardBackgroundColor(Color.parseColor("#F6CED8"));
                isImageSelected = true;

                if (adapter != null) {
                    data_list.clear();
                    adapter.notifyDataSetChanged();
                }

                if (isVideoSelected || isPdfSelected || isAudioSelected || isLinkSelected) {

                    video.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                    audio.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                    pdf.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                    links.setCardBackgroundColor(Color.parseColor("#FFFFFF"));

                    isVideoSelected = false;
                    isAudioSelected = false;
                    isPdfSelected = false;
                    isLinkSelected = false;
                }
                getData(SettingPreffrences.getUserid(getApplicationContext()), "IMG");
            }
        });

        links.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                links.setCardBackgroundColor(Color.parseColor("#F6CED8"));
                isLinkSelected = true;

                if (adapter != null) {
                    data_list.clear();
                    adapter.notifyDataSetChanged();
                }


                if (isPdfSelected || isAudioSelected || isImageSelected || isVideoSelected) {

                    pdf.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                    audio.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                    image.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                    video.setCardBackgroundColor(Color.parseColor("#FFFFFF"));

                    isPdfSelected = false;
                    isAudioSelected = false;
                    isImageSelected = false;
                    isVideoSelected = false;
                }

                getData(SettingPreffrences.getUserid(getApplicationContext()), "LNK");


            }
        });

        content_list.addOnItemTouchListener(new MyRecycleViewClickListener(getApplicationContext(),
                new MyRecycleViewClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Log.d("CornerData", "getType: " + data_list.get(position).getMediaTypeName());
                        //Video Image PDF
                        getType = data_list.get(position).getMediaTypeName();
                        GalleryID = Integer.parseInt(data_list.get(position).getGalleryID());
                        Log.d("CornerData", "openFile: " + data_list.get(position).getFileName());
                        if (getType.equals("Image")) {
                            openImage(
                                    NetworkUrl.KEF_Open + data_list.get(position).getFileName(),
                                    data_list.get(position).getTitle(),
                                    String.valueOf(GalleryID),
                                    imageValue + 1
                            );
                        }

                        if (getType.equals("PDF")) {
                            openPdf(
                                    NetworkUrl.KEF_Open + data_list.get(position).getFileName(),
                                    String.valueOf(GalleryID),
                                    data_list.get(position).getTitle()
                            );
                        }

                        if (getType.equals("Video")) {
                            openVideo(
                                    NetworkUrl.KEF_Open + data_list.get(position).getFileName(),
                                    String.valueOf(GalleryID)
                            );
                        }

                        if (getType.equals("Audio")) {
                            openAudio(
                                    NetworkUrl.KEF_Open + data_list.get(position).getFileName(),
                                    String.valueOf(GalleryID));
                        }

                        if (getType.equals("LINK")) {
                            openLink(data_list.get(position).getFileName(), String.valueOf(GalleryID));
                        }
                    }
                }));

    }

    private void byDefaultSelection(boolean selected, String mediaTypeId) {
        if (selected) {
            video.setCardBackgroundColor(Color.parseColor("#F6CED8"));
            if (isPdfSelected || isAudioSelected || isImageSelected) {

                pdf.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                audio.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                image.setCardBackgroundColor(Color.parseColor("#FFFFFF"));

                data_list.clear();
                adapter.notifyDataSetChanged();

                isPdfSelected = false;
                isAudioSelected = false;
                isImageSelected = false;
            }
            getData(SettingPreffrences.getUserid(getApplicationContext()), mediaTypeId);
        } else {
            video.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
        }
    }

    private void openLink(String link, String GalleryId) {
        /*Intent intent = new Intent(getApplicationContext(), LinkActivity.class);
        intent.putExtra("url",link);
        startActivityForResult(intent,LINK_REQUEST_CODE);*/

        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.setPackage("com.android.chrome");
        try {
            startActivityForResult(i, LINK_REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(), "unable to open chrome", Toast.LENGTH_SHORT).show();
            i.setPackage(null);
            startActivity(Intent.createChooser(i, "Select Browser"));
        }

        String where = "GALLERY_ID = '" + GalleryId + "' order by ID DESC limit 1";
        List<KefCornerStatistics> listData = Select.from(KefCornerStatistics.class).where(where).list();
        double duration;
       /* if (listData.size() > 0) {
            duration = listData.get(0).getAccessDuration() + 1;
        } else {*/
        duration = 1;
        // }

        Log.d("LastUpdated", "Link:" + duration);

        saveDataLocally(Integer.parseInt(SettingPreffrences.getUserid(getApplicationContext())),
                Integer.parseInt(GalleryId),
                duration,
                DateUtil.getCurrentDateTimeInFormat("YYYY-MM-dd HH:mm:ss"));
    }

    private void openImage(String link, String title, String GalleryId, int imageValue) {

        Intent intent = new Intent(getApplicationContext(), KefImageViewer.class);
        intent.putExtra("image_data", link);
        intent.putExtra("image_title", title);
        intent.putExtra("gallery_id", GalleryId);
        intent.putExtra("image_value", imageValue);
        startActivityForResult(intent, IMAGE_REQUEST_CODE);

        String where = "GALLERY_ID = '" + GalleryId + "' order by ID DESC limit 1";
        List<KefCornerStatistics> listData = Select.from(KefCornerStatistics.class).where(where).list();
        double duration;
       /* if (listData.size() > 0) {
            duration = listData.get(0).getAccessDuration() + 1;
        } else {*/
        duration = 1;
        // }

        Log.d("LastUpdated", "Image:" + duration);

        saveDataLocally(Integer.parseInt(SettingPreffrences.getUserid(getApplicationContext())),
                Integer.parseInt(GalleryId),
                duration,
                DateUtil.getCurrentDateTimeInFormat("YYYY-MM-dd HH:mm:ss"));
    }

    private void openVideo(String link, String GalleryId) {
        Intent intent = new Intent(getApplicationContext(), KefVideoViewer.class);
        intent.putExtra("video_data", link);
        intent.putExtra("gallery_id", GalleryId);
        startActivityForResult(intent, VIDEO_REQUEST_CODE);

    }

    private void openAudio(String link, String GalleryId) {
        Intent view_intent = new Intent(Intent.ACTION_VIEW);
        view_intent.setDataAndType(Uri.parse(link), "audio/*");
        startActivityForResult(view_intent, AUDIO_REQUEST_CODE);

        String where = "GALLERY_ID = '" + GalleryId + "' order by ID DESC limit 1";
        List<KefCornerStatistics> listData = Select.from(KefCornerStatistics.class).where(where).list();

        double duration;
        if (listData.size() > 0) {
            duration = listData.get(0).getAccessDuration() + 1;
        } else {
            duration = 1;
        }
        Log.d("LastUpdated", "Audio:" + duration);
        saveDataLocally(Integer.parseInt(SettingPreffrences.getUserid(getApplicationContext())),
                Integer.parseInt(GalleryId),
                duration,
                DateUtil.getCurrentDateTimeInFormat("YYYY-MM-dd HH:mm:ss"));
    }

    private void openPdf(String link, String GalleryId, String Title) {
        try {
            Intent view_intent = new Intent(Intent.ACTION_VIEW);
            view_intent.setDataAndType(Uri.parse(link), "application/pdf");
            //startActivity(Intent.createChooser(view_intent,"Open with"));
            startActivityForResult(view_intent, PDF_REQUEST_CODE);

            String where = "GALLERY_ID = '" + GalleryId + "' order by ID DESC limit 1";
            List<KefCornerStatistics> listData = Select.from(KefCornerStatistics.class).where(where).list();
            double duration;
            if (listData.size() > 0) {
                duration = listData.get(0).getAccessDuration() + 1;
            } else {
                duration = 1;
            }

            Log.d("LastUpdated", "Pdf:" + duration);

            saveDataLocally(Integer.parseInt(SettingPreffrences.getUserid(getApplicationContext())),
                    Integer.parseInt(GalleryId),
                    duration,
                    DateUtil.getCurrentDateTimeInFormat("YYYY-MM-dd HH:mm:ss"));
        } catch (Exception e) {
            /*Intent pdf = new Intent(this, KefPdfViewer.class);
            pdf.putExtra("title", Title);
            pdf.putExtra("pdf_link", link);
            startActivity(pdf);*/
            Toast toast = Toast.makeText(getApplicationContext(), "No suitable app found in device to view PDF.", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER,0,0);
            toast.show();
        }

    }

    public void saveDataLocally(int userID, int galleryID, double accessDuration, String accessDatetime) {

        Log.d("SavingData", "USerID: " + userID + " GalleryID: " + galleryID + " Duration: " + accessDuration + " Time: " + accessDatetime);

        statsModel = new KefCornerStatistics(
                userID,
                galleryID,
                accessDuration,
                accessDatetime
        );
        statsModel.save();
    }

    public void getData(String userID, String mediaTypeID) {

        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {
            CommonFunctions.showDialog(this);
            try {

                params = "userID=" + URLEncoder.encode(String.valueOf(userID), "UTF-8")
                        + "&mediaTypeID=" + URLEncoder.encode(mediaTypeID, "UTF-8")
                        + "&dtFilter=" + URLEncoder.encode("", "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.KEF_Host + NetworkUrl.KEF_GetGallery, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {
                        CommonFunctions.hideDialog();
                        Log.d("getImageResponse", "response: " + response);
                        if (!response.equals("error")) {
                            addContentLayout.setVisibility(View.VISIBLE);
                            getDataResponse(response);
                        }
                    }
                }, "GET");

                asyncTaskHelper.execute();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getDataResponse(String response) {
        try {

            JSONArray responseArray = new JSONArray(response);
            Log.d("CornerValues", "::" + responseArray.length());

            if (responseArray.length() > 0) {

                notFoundLayout.setVisibility(View.GONE);
                addContentLayout.setVisibility(View.VISIBLE);

                for (int i = 0; i < responseArray.length(); i++) {
                    jsonObject = responseArray.getJSONObject(i);
                    date = inputDate.parse(jsonObject.getString("CreatedOn"));

                    model = new KefCornerModel(
                            jsonObject.getString("GalleryID"),
                            jsonObject.getString("Title"),
                            jsonObject.getString("MediaTypeID"),
                            jsonObject.getString("FileName"),
                            jsonObject.getString("arrSchoolID"),
                            jsonObject.getString("MediaTypeName"),
                            outputDate.format(date)
                    );
                    data_list.add(model);
                }
                Collections.sort(data_list, new Comparator<KefCornerModel>() {
                    @Override
                    public int compare(KefCornerModel o1, KefCornerModel o2) {
                        return o1.getCreatedOn().compareTo(o2.getCreatedOn());
                    }
                });

                adapter = new CornerAdapter(data_list);
                content_list.setAdapter(adapter);
                content_list.scrollToPosition(data_list.size() - 1);
            } else {
                notFoundLayout.setVisibility(View.VISIBLE);
                addContentLayout.setVisibility(View.GONE);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("ActivityResult", "Code: " + requestCode);

        syncKefUsage();

        /*if (requestCode == PDF_REQUEST_CODE){

            Log.d("parameters: 11",
                    SettingPreffrences.getUserid(getApplicationContext())+" : "+GalleryID+" : "+pdfValue+1+" : "+DateUtil.getCurrentDateTimeInFormat("YYYY-mm-dd HH:mm:ss"));

            addStatisticsData(
                    SettingPreffrences.getUserid(getApplicationContext()),
                    GalleryID,
                    pdfValue+1,
                    DateUtil.getCurrentDateTimeInFormat("YYYY-mm-dd HH:mm:ss"));
        }

        if (requestCode == AUDIO_REQUEST_CODE){

            Log.d("parameters: 22",
                    SettingPreffrences.getUserid(getApplicationContext())+" : "+GalleryID+" : "+audioValue+1+" : "+DateUtil.getCurrentDateTimeInFormat("YYYY-mm-dd HH:mm:ss"));


            addStatisticsData(
                    SettingPreffrences.getUserid(getApplicationContext()),
                    GalleryID,
                    audioValue+1,
                    DateUtil.getCurrentDateTimeInFormat("YYYY-mm-dd HH:mm:ss"));
        }*/
    }

    public void addStatisticsData(int UserID, int GalleryID, double AccessDuration, String AccessDatetime) {

        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {

            Log.d("parameters: ", UserID + " : " + GalleryID + " : " + AccessDuration + " : " + AccessDatetime);

            try {
                params = "GalleryID=" + URLEncoder.encode(String.valueOf(GalleryID), "UTF-8")
                        + "&UserID=" + URLEncoder.encode(String.valueOf(UserID), "UTF-8")
                        + "&AccessDuration=" + URLEncoder.encode(String.valueOf(AccessDuration), "UTF-8")
                        + "&AccessDatetime=" + URLEncoder.encode(AccessDatetime, "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.KEF_Host + NetworkUrl.KEF_AddGallery, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {
                        Log.d("ApiResponse", "response: " + response);
                    }
                }, "POST");

                asyncTaskHelper.execute();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}