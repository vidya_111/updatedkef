package com.observatory.kefcorner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.ItemTouchHelper.Callback;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.observatory.AsyncTaskMain.AsyncTaskHelper;
import com.observatory.AsyncTaskMain.OnTaskComplete;
import com.observatory.adapters.DoubtConversationAdapter;
import com.observatory.adapters.MyRecycleViewClickListener;
import com.observatory.adapters.SwipeHelper;
import com.observatory.model.AllDoubts;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;
import com.tsuryo.swipeablerv.SwipeableRecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Doubt_Conversations extends AppCompatActivity {

    RecyclerView conversation_view;
    Toolbar toolbar;
    Button ask_button;
    RelativeLayout no_data_exist;
    String question_intent = "", standard_intent = "", time_intent = "";
    public List<AllDoubts> msgList = new ArrayList<>();
    public DoubtConversationAdapter adapter;
    AllDoubts model = null;
    SimpleDateFormat inputDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat outputDate = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    Date date = null;

    /*Webservice*/
    private AsyncTaskHelper asyncTaskHelper;
    private String params;
    private JSONObject jsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doubt__conversations);

        toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        no_data_exist = findViewById(R.id.no_data_exist);
        ask_button = findViewById(R.id.ask_button_id);
        ask_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AskDoubtsActivity.class);
                startActivity(intent);
                finish();
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        conversation_view = findViewById(R.id.conversation_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        conversation_view.setLayoutManager(linearLayoutManager);

        getAllDoubts(SettingPreffrences.getUserid(getApplicationContext()),"");

        conversation_view.addOnItemTouchListener(new MyRecycleViewClickListener(getApplicationContext(),
                new MyRecycleViewClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(getApplicationContext(), ConversationHistory.class);
                        intent.putExtra("doubt_id",msgList.get(position).getDoubtBoxID());
                        intent.putExtra("doubt_title",msgList.get(position).getDoubtTitle());
                        startActivity(intent);
                    }
                }));

   /*     ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT){

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                //Toast.makeText(getApplicationContext(), "on Move", Toast.LENGTH_SHORT).show();
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                //Toast.makeText(getApplicationContext(), "on Swiped "+msgList.get(viewHolder.getAdapterPosition()).getDoubtBoxID(), Toast.LENGTH_SHORT).show();
                int position = viewHolder.getAdapterPosition();
                deleteDoubt(msgList.get(viewHolder.getAdapterPosition()).getDoubtBoxID(),position);
                *//*androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(Doubt_Conversations.this);
                builder.setTitle("");
                builder.setMessage("Are you sure, you want to delete?");
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                builder.show();*//*

            }


        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(conversation_view);*/

    }

    private void deleteDoubt(String doubtBoxID, int position){
        if (CommonFunctions.isNetworkConnected(getApplicationContext())){

            try {

                params = "doubtBoxID=" + URLEncoder.encode(doubtBoxID, "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.KEF_Host + NetworkUrl.KEF_DeleteDoubt, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {

                        Log.d("getDoubtsByUserId","response: "+response);
                        if (!response.equals("error")){
                            msgList.remove(position);
                            adapter.notifyItemRemoved(position);
                            Toast.makeText(getApplicationContext(),"Delete Successfully",Toast.LENGTH_SHORT).show();
                        }
                    }
                },"GET");

                asyncTaskHelper.execute();

            }catch (Exception e){
                Log.e("Error: GetDoubts: ",e.getMessage());
            }

        }else {
            //alert("Internet Connection not available");
        }
    }

    private void getAllDoubts(String UserID, String dtFilter){
        if (CommonFunctions.isNetworkConnected(getApplicationContext())){

            CommonFunctions.showDialog(this);
            try {

                params = "UserID=" + URLEncoder.encode(UserID, "UTF-8")
                        +"&dtFilter=" + URLEncoder.encode(dtFilter, "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.KEF_Host + NetworkUrl.KEF_GetByUserId, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {
                        CommonFunctions.hideDialog();
                        Log.d("getDoubtsByUserId","response: "+response);
                        if (!response.equals("error")){
                            getAllDoubtsResponse(response);
                        }
                    }
                },"GET");

                asyncTaskHelper.execute();

            }catch (Exception e){
                Log.e("Error: GetDoubts: ",e.getMessage());
            }

        }else {
            //alert("Internet Connection not available");
        }
    }

 /*   @Override
    public void onBackPressed() {

        Intent intent = new Intent(getApplicationContext(), Subjects.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();

        super.onBackPressed();
    }*/

    private void getAllDoubtsResponse(String response){
        try {
            JSONArray responseArray = new JSONArray(response);
            msgList.clear();
            for (int i = 0; i < responseArray.length(); i++){
                jsonObject = responseArray.getJSONObject(i);
                date = inputDate.parse(jsonObject.getString("LastRepliedOn"));
                Log.d("Timings",":: "+date+" :: "+outputDate.format(date));
                model = new AllDoubts(
                        jsonObject.getString("DoubtBoxID"),
                        jsonObject.getString("DoubtCategoryID"),
                        jsonObject.getString("SchoolID"),
                        jsonObject.getString("CourseID"),
                        jsonObject.getString("SubjectID"),
                        jsonObject.getString("SubjectName"),
                        jsonObject.getString("DoubtTitle").substring(0,1).toUpperCase()+
                                jsonObject.getString("DoubtTitle").substring(1).toLowerCase(),
                        outputDate.format(date)
                );
                msgList.add( model);

                //myString.toLowerCase(Locale.getDefault()).capitalize()
            }
            Collections.sort(msgList, new Comparator<AllDoubts>() {
                @Override
                public int compare(AllDoubts o1, AllDoubts o2) {
                    return o2.getCreatedOn().compareTo(o1.getCreatedOn());
                }
            });
            conversation_view.setVisibility(View.VISIBLE);

            adapter = new DoubtConversationAdapter(msgList,this);
            conversation_view.setAdapter(adapter);
            //conversation_view.scrollToPosition(msgList.size()-1);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void deleteDoubtByPosition(int position){
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(Doubt_Conversations.this);
        builder.setTitle("");
        builder.setMessage("Are you sure, you want to delete?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteDoubt(msgList.get(position).getDoubtBoxID(),position);
                adapter.setMsgList(msgList);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
             dialog.dismiss();
            }
        });
        builder.create().show();

    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }

    protected void startActivity(Class clazz) {
        Intent i = new Intent(this, clazz);
        startActivity(i);
    }
}