package com.observatory.kefcorner;

import android.Manifest;
import android.animation.Animator;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.observatory.AsyncTaskMain.AsyncTaskHelper;
import com.observatory.AsyncTaskMain.OnTaskComplete;
import com.observatory.database.ActivationCode;
import com.observatory.database.Master;
import com.observatory.database.OfflineUsers;
import com.observatory.database.RealmDb;
import com.observatory.database.Subject;
import com.observatory.mcqdatabase.DatabaseHelper;
import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.Constants;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;
import com.observatory.utils.UsbDevice;
import com.observatory.utils.UsbHandler;

import net.lingala.zip4j.exception.ZipException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.StringTokenizer;

import io.fabric.sdk.android.Fabric;
import jxl.Cell;
import jxl.CellType;
import jxl.DateCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import static com.observatory.kefcorner.StorageUtils.getStorageList;
import static com.observatory.utils.Constants.COURSE_NAME_EXCEL;


/**
 * Created by Anuj on 05-01-2016.
 */
public class Splash_typeC extends BaseActivity implements View.OnClickListener {


    /*for checking the sd card*/
    public static final String backupPassword = "x4r!sDF/SA0";
    protected static final String SDCARD_IS_WRITEABLE = "SDCARD_IS_WRITEABLE";
    protected static final String IS_SDCARD_EXPIRED = "SDCARD_IS_EXPIRED";
    protected static final String LAST_ACCESS_TIME = "LAST_ACCESS_TIME";
    private static final String TAG = "Splash";
    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final String APPLICATION_ID = "9ZjYbJZFOg2qf6axLcSrjMDJEREe56LRIWBYD7zG";
    private static final int PERMISSION_PHONE_STATE = 34;
    public static String hex = "4rIk.S3!p0-sd";
    public static String key = "p3Qr!-";
    public static String hexEncoded = "4rIk.S3!p0-sd";
    public static String OS;
    public static String MAKE;
    public static String MODEL;
    //for checking the sdcard path
    public static String SDCARD_PATH;
    /**
     * Name of root folder
     */
    public static String BASE_LOCATION = ".Sundaram";
    /**
     * Path to root folder is present
     */
    public static String ORIGINAL_PATH = "";
    public static String BASE_PATH = "";
    public static String pathIssuer = "";
    SharedPreferences ePrefs;
    String externalpath = new String();
    String internalpath = new String();
    List<StorageUtils.StorageInfo> list;
    private LinearLayout ivLogo;
    /*Login Main*/
    private RelativeLayout rlLoginMain;
    private TextView tvLogin;
    private TextView tvSignUp;
   // private TextView tvGuest;
    /*Login*/
    private ScrollView svLogin;
    private TextView tvCancel;
    private TextView tvFP;
    private ImageView ivFacebook;
    private EditText etEmail;
    private EditText etPassword;
    private TextView tvLoginButton;
    /*Offline*/
    private ScrollView svOfline;
    private TextView tvOffline;
    private TextView tvOfflineCancel;
    private EditText etOlName;
    private EditText etOlEmail;
    private EditText etOlPassword;
    private TextView tvOflineLogin;
    /*Forget Password*/
    private RelativeLayout rlFP;
    private TextView tvFPCancel;
    private EditText etFPEmail;
    private TextView tvFPSubmit;
    /*Error*/
    private ScrollView scError;
    private TextView tvError;
    //private TextView tvTakeToEdzam;
    private TextView tvDemoVideos;
    private TextView tvBuySDCard;
    private TextView tvNoMem;
    private Dialog dialog;
    /*Facebook*/
    private CallbackManager callbackManager;
    /*Webservice*/
    private AsyncTaskHelper asyncTaskHelper;
    private String params;
    /*Json*/
    private JSONObject jsonObject;
    private JSONArray jsonArray;
    private String status;
    private String msg;
    private Intent intent;
    private HashMap<String, String> dimensions;
    private String filePath;
    private String tag = "App";
    private String CLIENT_ID = "acjqgt4YZ2wDhrouXT3QIIy7rlcis75jgZJHt1Cc";

    /*activation code view*/ ScrollView sc_act_code;
    EditText etActCode;
    TextView tvCancelActCode;
    TextView tvLogOutActCode;
    TextView tvActivate;
    TextView tvContactUsActCode;
    private TextView tvReenter;
    private boolean ACTIVATION_CODE_ERR_OCC = false;

    /*inform user to login view*/ ScrollView sc_inform_to_login;
    TextView tv_inform_v_Login;
    TextView tv_inform_v_sign_up;

    /*Multiple standard listview*/
    private RelativeLayout rlMultipleStd;
    private RecyclerView rvMediums;
    private Subject subject;
    private int screenWidth;

    final String ENGLISH_FONT = "english";
    final String URDU_FONT = "urdu";
    final String HINDI_FONT = "krutidev";
    final String MARATHI_FONT = "subak";

    private Typeface englishTypeface;
    private Typeface hindiTypeface;
    private Typeface marathiTypeface;
    private Typeface urduTypeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.splash);

        //ParseAnalytics.trackAppOpenedInBackground(getIntent());

//        dimensions = new HashMap<>();
//        dimensions.put("userId", getUserId(context));
//        CommonFunctions.sendAnalytics(Constants.SPLASH_MAIN, null);

        App.getInstance().trackScreenView(Constants.SPLASH_MAIN);

        list = getStorageList();


    }

    @Override
    protected void onLayout() {

        actionBar.hide();

        ivLogo = (LinearLayout) findViewById(R.id.iv_logo);

        /*Login Main*/
        rlLoginMain = (RelativeLayout) findViewById(R.id.rl_login_main);
        tvLogin = (TextView) findViewById(R.id.tv_login);
        tvSignUp = (TextView) findViewById(R.id.tv_signup);
        //tvGuest = (TextView) findViewById(R.id.tv_guest);

        /*Login*/
        svLogin = (ScrollView) findViewById(R.id.sv_login);
        tvCancel = (TextView) findViewById(R.id.tv_cancel);
        tvFP = (TextView) findViewById(R.id.tv_fp);
        ivFacebook = (ImageView) findViewById(R.id.iv_facebook);
        etEmail = (EditText) findViewById(R.id.et_l_email);
        etPassword = (EditText) findViewById(R.id.et_l_password);
        tvLoginButton = (TextView) findViewById(R.id.tv_login_login);

        /*Forget Password*/
        rlFP = (RelativeLayout) findViewById(R.id.rl_fp);
        tvFPCancel = (TextView) findViewById(R.id.tv_fp_cancel);
        etFPEmail = (EditText) findViewById(R.id.et_fp_email);
        tvFPSubmit = (TextView) findViewById(R.id.tv_fp_submit);

        /* offline */
        svOfline = (ScrollView) findViewById(R.id.sv_offline);
        tvOffline = (TextView) findViewById(R.id.tv_offline);
        tvOfflineCancel = (TextView) findViewById(R.id.tv_ol_cancel);
        etOlName = (EditText) findViewById(R.id.et_ol_name);
        etOlEmail = (EditText) findViewById(R.id.et_ol_email);
        etOlPassword = (EditText) findViewById(R.id.et_ol_password);
        tvOflineLogin = (TextView) findViewById(R.id.tv_ol_login);

        /*Error*/
        scError = (ScrollView) findViewById(R.id.sc_error);
        tvError = (TextView) findViewById(R.id.textView11);
        tvReenter = (TextView) findViewById(R.id.tv_re_enter_activation_code);
        tvReenter.setPaintFlags(tvReenter.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvReenter.setOnClickListener(this);
        //tvTakeToEdzam = (TextView) findViewById(R.id.tv_take_to_edzam);
        tvDemoVideos = (TextView) findViewById(R.id.tv_demo_video);
        tvBuySDCard = (TextView) findViewById(R.id.tv_buy_sd_card);
        tvNoMem = (TextView) findViewById(R.id.textView12);

        /*activation code*/
        sc_act_code = (ScrollView) findViewById(R.id.sv_activation_code);
        etActCode = (EditText) findViewById(R.id.et_activation_code);
        tvCancelActCode = (TextView) findViewById(R.id.tv_cancel_activation);
        tvCancelActCode.setOnClickListener(this);
        tvLogOutActCode = (TextView) findViewById(R.id.tv_logOut_activation_view);
        tvLogOutActCode.setOnClickListener(this);
        tvActivate = (TextView) findViewById(R.id.tv_activate);
        tvActivate.setOnClickListener(this);
        tvContactUsActCode = (TextView) findViewById(R.id.tv_contact_act_code);
        tvContactUsActCode.setOnClickListener(this);

        /*inform to login & sign up view*/
        sc_inform_to_login = (ScrollView) findViewById(R.id.sv_inform_to_login);
        tv_inform_v_Login = (TextView) findViewById(R.id.tv_login_inform_to_login);
        tv_inform_v_sign_up = (TextView) findViewById(R.id.tv_sign_up_inform_to_login);
        tv_inform_v_Login.setOnClickListener(this);
        tv_inform_v_sign_up.setOnClickListener(this);

        /*multiple standard list item*/
        rvMediums = (RecyclerView) findViewById(R.id.rv_mediums);
        rlMultipleStd = (RelativeLayout) findViewById(R.id.rl_multiple_standards);
        calcScreenWidth();

        callbackManager = CallbackManager.Factory.create();


        OS = Build.VERSION.RELEASE;
        MAKE = Build.MANUFACTURER;
        MODEL = Build.MODEL;

        if (SettingPreffrences.getEmail(this) != "") {
            //updateLastSeen();
        }


        //Log.d(TAG, "onLayout: " + Statistics.listAll(Statistics.class));


        App.BASE_LOCATION = SettingPreffrences.getOriginalpath(getApplicationContext());

        if (!checkPermission()) {

            requestPermission();

        } else {


            animateLogo();
        }


        tvLogin.setOnClickListener(this);
        ivFacebook.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);
       // tvGuest.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvFP.setOnClickListener(this);
        tvFPCancel.setOnClickListener(this);
        tvLoginButton.setOnClickListener(this);
        tvFPSubmit.setOnClickListener(this);
        tvDemoVideos.setOnClickListener(this);
        //tvTakeToEdzam.setOnClickListener(this);
        tvBuySDCard.setOnClickListener(this);
        tvOffline.setOnClickListener(this);
        tvOfflineCancel.setOnClickListener(this);
        tvOflineLogin.setOnClickListener(this);

    }

    private void calcScreenWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenWidth = displayMetrics.widthPixels;
    }


    private void animateLogo() {

        //this is redundant, can be removed
        SettingPreffrences.setParsePush(getApplicationContext(), false);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                ivLogo.animate().
                        translationY(-(getDeviceHeight() / 4)).setDuration(500).setInterpolator(new AccelerateDecelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {


                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        if (App.IS_HAVING_MULTIPLE_STANDARD = isHavingMultipleStandards()) {

                            //String path = externalStorage1();
                            String path = App.BASE_PATH;

                            boolean isNewMultiStdCard = isNewMultiStdCard();
                            List<Subject> listMediums;

                            if (isNewMultiStdCard) {
                                // new way :- gets Course info from an excel sheet
                                listMediums = getMediumsFromCourseNameExcelSheet(path);
                            } else {
                                // old way :- Course name is basically Folder name inside
                                // .Sundaram folder
                                listMediums = getMediumsFromFolderNames(path);
                            }

                            if (listMediums.size() > 0) {
                                rvMediums.setAdapter(new Splash_typeC.MediumAdapter(brandonBold, brandonReg, (ArrayList<Subject>) listMediums));
                            }

                            rlMultipleStd.setAlpha(0.f);
                            rlMultipleStd.setScaleX(0.f);
                            rlMultipleStd.setScaleY(0.f);
                            rlMultipleStd.setVisibility(View.VISIBLE);
                            rlMultipleStd.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500);

                        } else {

                            if (SettingPreffrences.getLoginDone(getApplicationContext())) {

                                checkForSDCard();

                            } else {

                                if (SettingPreffrences.getContinueAsGuest(getApplicationContext()) && !CommonFunctions.isNetworkConnected(getApplicationContext())) {

                                    checkForSDCard();

                                } else {

                                    rlLoginMain.setAlpha(0.f);
                                    rlLoginMain.setScaleX(0.f);
                                    rlLoginMain.setScaleY(0.f);
                                    rlLoginMain.setVisibility(View.VISIBLE);
                                    rlLoginMain.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500);

                                }

                            }

                        }


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });

            }
        }, 600);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.tv_login:


                // ParseAnalytics.trackAppOpenedInBackground(getIntent());

//                dimensions = new HashMap<>();
//                dimensions.put("userId", getUserId(getApplicationContext()));
//                CommonFunctions.sendAnalytics(Constants.LOGIN, null);

                App.getInstance().trackScreenView(Constants.LOGIN);
                showLogin();

                break;

            case R.id.tv_signup:

//                dimensions = new HashMap<>();
//                dimensions.put("userId", getUserId(getApplicationContext()));
//                CommonFunctions.sendAnalytics(Constants.SIGNUP, null);

                App.getInstance().trackScreenView(Constants.SIGNUP);
                startActivity(SignUp.class);
                break;

/*            case R.id.tv_guest:

//                dimensions = new HashMap<>();
//                dimensions.put("userId", getUserId(getApplicationContext()));
//                CommonFunctions.sendAnalytics("Guest", null);

                App.getInstance().trackScreenView("Guest");
                SettingPreffrences.setContinueAsGuest(getApplicationContext(), true);

                checkForSDCard();

                break;*/


            case R.id.tv_cancel:

                etEmail.setText("");
                etPassword.setText("");
                hideLogin();

                break;

            case R.id.tv_fp:

//                dimensions = new HashMap<>();
//                dimensions.put("userId", getUserId(getApplicationContext()));
//                CommonFunctions.sendAnalytics("ForgetPassword", null);

                App.getInstance().trackScreenView("ForgetPassword");
                showForgetPassword();
                break;

            case R.id.tv_fp_cancel:

                etFPEmail.setText("");
                hideForgetPassword();
                break;

            case R.id.iv_facebook:

//                dimensions = new HashMap<>();
//                dimensions.put("userId", getUserId(getApplicationContext()));
//                CommonFunctions.sendAnalytics("Facebook", null);

                App.getInstance().trackScreenView("Facebook");

                if (CommonFunctions.isNetworkConnected(getApplicationContext())) {
                    loginWithFacebook();
                } else {
                    alert("Internet Connection not available");
                }
                break;

            case R.id.tv_login_login:

                if (TextUtils.isEmpty(etEmail.getText().toString()) || TextUtils.isEmpty(etPassword.getText().toString())) {

                    alert("Fields cannot be empty");
                } else if (!CommonFunctions.isValidEmail(etEmail.getText().toString())) {

                    alert("Please enter valid Email-Id");
                } else if (etPassword.getText().toString().length() < 6) {

                    alert("Password cannot be less then 6 characters");

                } else {

                    callLoginWS();
                }


                break;

            case R.id.tv_fp_submit:

                if (TextUtils.isEmpty(etFPEmail.getText().toString())) {

                    alert("Please enter Email-Id ");
                } else {
                    callForgorPwdWS();
                }

                break;

            /*case R.id.tv_take_to_edzam:

                try {
                    PackageManager pm = getPackageManager();
                    Intent intent = pm.getLaunchIntentForPackage("com.techmorphosis.sundaram" + ".eclassonline");
                    startActivity(intent);
                } catch (Exception e) {
                    // No Edzam app found! Open playstore.
                    String url = "https://play.google.com/store/apps/details?id=com.techmorphosis" + ".sundaram.eclassonline&hl=en";
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }

                break;*/

            case R.id.tv_demo_video:

                startActivity(How.class);

                break;


            case R.id.tv_buy_sd_card:

                startActivity(Standard.class);
                break;

            case R.id.tv_offline:

                showOffline();
                break;

            case R.id.tv_ol_cancel:
                hideOfflineAccess();
                break;

            case R.id.tv_ol_login:

                if (TextUtils.isEmpty(etOlName.getText().toString()) || TextUtils.isEmpty(etOlEmail.getText().toString()) || TextUtils.isEmpty(etOlPassword.getText().toString())) {

                    alert("Fields cannot be empty");
                    return;
                }

                if (etOlPassword.getText().length() < 6) {

                    alert("Password cannot be less then 6 characters");
                    return;
                }

                addOfflineUser();

                break;

            case R.id.tv_activate:
                isActivationCodeValid();
                break;

            case R.id.tv_contact_act_code:
                tvError.setText("Please enter valid activation code");
                tvReenter.setVisibility(View.VISIBLE);
                ACTIVATION_CODE_ERR_OCC = true;
                hideActivationCode_showError();
                break;

            case R.id.tv_cancel_activation:
//                tvError.setText("You don't seem to have a valid Eclass memory card inserted");
//                hideActivationCode_showError();

                alert("Are you sure you want to exit?", "Exit", "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                break;

            case R.id.tv_re_enter_activation_code:
                showActivationCodeView_and_hideErrorView();
                break;

            case R.id.tv_login_inform_to_login:
                hide_inform_to_login_view_ShowLogin();
                break;


            case R.id.tv_sign_up_inform_to_login:
                App.getInstance().trackScreenView(Constants.SIGNUP);
                startActivity(SignUp.class);
                break;


            case R.id.tv_logOut_activation_view:

                alert("Are you sure you want to log out?", "Yes", "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        PreferenceManager.getDefaultSharedPreferences(getBaseContext()).
                                edit().clear().apply();

                        SettingPreffrences.setLoginDone(getApplicationContext(), false);
                        SettingPreffrences.setContinueAsGuest(getApplicationContext(), false);
                        SettingPreffrences.setName(getApplicationContext(), "");
                        SettingPreffrences.setPhoto(getApplicationContext(), "");
                        SettingPreffrences.setUserid(getApplicationContext(), "");
                        SettingPreffrences.setEmail(getApplicationContext(), "");
                        ActivationCode.deleteAll(ActivationCode.class);
                        etEmail.setText("");
                        etPassword.setText("");

                        if (App.IS_HAVING_MULTIPLE_STANDARD = isHavingMultipleStandards()) {
                            hideActivationCode_showMultipleStdOption();
                        } else {
                            hideActivationCode_showLogin();
                        }

                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                break;

            default:
        }

    }

    private boolean isActivationCodeValid() {
        if (etActCode.getText().toString().trim().isEmpty()) {
            alert("Enter the activation code");

        } else {

            String act_code = etActCode.getText().toString().trim();
            Master master = Master.listAll(Master.class).get(0);

            if (master.getActivation_code().equalsIgnoreCase(act_code)) {
                if (checkPhoneStatePermission()) {
                    sendSDcardCredentials_blocking();
                } else {
                    requestPhoneStatePermission();
                }
            } else {
//                alert("Activation code is invalid");
                tvError.setText("Please enter valid activation code");
                tvReenter.setVisibility(View.VISIBLE);
                ACTIVATION_CODE_ERR_OCC = true;
                hideActivationCode_showError();
            }

        }
        return false;
    }

    public boolean checkWhetherActivationCodeIsValid() {

        List<ActivationCode> actList = ActivationCode.listAll(ActivationCode.class);
        Master master = Master.listAll(Master.class).get(0);

        boolean flag = false;

        if (actList != null && !actList.isEmpty()) {

            for (ActivationCode code : actList) {
                if (master.getActivation_code().equalsIgnoreCase(code.getActivationCode())) {
                    flag = true;
                    break;
                }
            }

        }

        return flag;

    }

    public boolean hasPreviouslyEnteredActivationCode() {
        List<ActivationCode> actList = ActivationCode.listAll(ActivationCode.class);
        return actList != null && !actList.isEmpty();
    }

    private void addOfflineUser() {

        //Log.d(TAG, "addOfflineUser() returned: " + OfflineUsers.listAll(OfflineUsers.class));

        List<OfflineUsers> list = OfflineUsers.find(OfflineUsers.class, "email = ?", etOlEmail.getText().toString());

        if (list.size() == 0) {
            OfflineUsers offlineUsers = new OfflineUsers(etOlName.getText().toString(), etOlEmail.getText().toString(), etOlPassword.getText().toString(), new Date().toString());
            offlineUsers.save();
            SettingPreffrences.setUserid(this, "0");
            SettingPreffrences.setEmail(this, etOlEmail.getText().toString());
            SettingPreffrences.setPassword(this, etOlPassword.getText().toString());
            SettingPreffrences.setName(this, etOlName.getText().toString());
            SettingPreffrences.setLoginDone(getApplicationContext(), true);
            //updateLastSeen();
            System.out.print(SettingPreffrences.getEmail(this));

            checkForSDCard();
        } else {

            if (list.get(0).getPassword() != etOlPassword.getText().toString()) {
                SettingPreffrences.setLoginDone(getApplicationContext(), false);
                alert("Email & password combination is incorrect. Please login with correct " + "password or create a new account");
            } else {
                SettingPreffrences.setUserid(this, "0");
                SettingPreffrences.setEmail(this, etOlEmail.getText().toString());
                SettingPreffrences.setName(this, etOlName.getText().toString());
                SettingPreffrences.setPassword(this, etOlPassword.getText().toString());
                SettingPreffrences.setLoginDone(getApplicationContext(), true);
                System.out.print(SettingPreffrences.getEmail(this));
                //updateLastSeen();
                checkForSDCard();
            }


        }


    }

    private void callForgorPwdWS() {

        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {

            showDialog();
            try {
                params = "os=" + URLEncoder.encode(OS, "UTF-8") + "&make=" + URLEncoder.encode(MAKE, "UTF-8") + "&model=" + URLEncoder.encode(MODEL, "UTF-8") + "&email=" + URLEncoder.encode(etFPEmail.getText().toString(), "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.host + NetworkUrl.forgotPassword, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {


                        if (!response.equals("error")) {

                            parseForgotPwdJson(response);

                        } else {

                            //error
                            alert("Something went, please try again later");
                            hideDialog();
                        }


                    }
                }, "POST");

                asyncTaskHelper.execute();


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {

            //no internet
            alert("Internet Connection not available");
        }
    }

    private void parseForgotPwdJson(String response) {


        try {
            jsonObject = new JSONObject(response);

            status = jsonObject.getString("status");
            msg = jsonObject.getString("message");

            if (status.equalsIgnoreCase("failure")) {

                alert(msg);
            } else if (status.equalsIgnoreCase("error")) {
                alert("Something went, please try again later");

            } else {
                alert(msg);
                hideDialog();

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        hideDialog();

    }

    private void callLoginWS() {

        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {

            showDialog();
            try {
                params = "os=" + URLEncoder.encode(OS, "UTF-8") + "&make=" + URLEncoder.encode(MAKE, "UTF-8") + "&model=" + URLEncoder.encode(MODEL, "UTF-8") + "&email=" + URLEncoder.encode(etEmail.getText().toString(), "UTF-8") + "&password=" + URLEncoder.encode(etPassword.getText().toString(), "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.host + NetworkUrl.login, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {


                        if (!response.equals("error")) {

                            parseLoginJson(response);

                        } else {

                            //error
                            alert("Something went wrong, please try again later");
                            hideDialog();

                        }


                    }
                }, "POST");

                asyncTaskHelper.execute();


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {

            //no internet
            alert("Internet Connection not available");
        }
    }

    private void parseLoginJson(String response) {

       // Log.d(TAG, "Login Response-->" + response);

        try {
            jsonObject = new JSONObject(response);

            status = jsonObject.getString("status");
            msg = jsonObject.getString("message");

            if (status.equalsIgnoreCase("failure")) {

                alert(msg);
                hideDialog();
            } else if (status.equalsIgnoreCase("error")) {

                if (msg != null && !msg.isEmpty()) {
                    alert(msg);
                } else {
                    alert("Something went wrong, please try again later");
                }

                hideDialog();

            } else {

                JSONArray res = jsonObject.getJSONArray("response");

                for (int i = 0; i < res.length(); i++) {

                    jsonObject = res.getJSONObject(i);
                    SettingPreffrences.setUserid(getApplicationContext(), jsonObject.getString("userId"));
                    SettingPreffrences.setToken(getApplicationContext(), jsonObject.getString("token"));
                    SettingPreffrences.setName(getApplicationContext(), jsonObject.getString("firstName") + " " + jsonObject.getString("lastName"));
                    SettingPreffrences.setEmail(getApplicationContext(), jsonObject.getString("email"));
                    SettingPreffrences.setMobile(getApplicationContext(), jsonObject.getString("mobile"));
                    SettingPreffrences.setPhoto(getApplicationContext(), jsonObject.getString("photo"));
                    CommonFunctions.addUserToParse(jsonObject.getString("userId"));
                }

                SettingPreffrences.setLoginDone(getApplicationContext(), true);
                hideDialog();
                //updateLastSeen();
                checkForSDCard();

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void loginWithFacebook() {

        //App.BASE_LOCATION = App.ORIGINAL_PATH;
        showDialog();
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email", "user_birthday", "user_location", "user_hometown"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {

                System.out.println("Success");
                final GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject json, GraphResponse response) {
                        if (response.getError() != null) {
                            // handle error hideDialog();
                            alert("Something went wrong, please try again later");
                            System.out.println("ERROR");
                        } else {

                            System.out.println("Success");
                            try {
                               // Log.d(TAG, "onCompleted() called with " + "FACEBOOK_TOKEN " + "ReadPermissions = " + AccessToken.getCurrentAccessToken().getToken());
                                String jsonresult = String.valueOf(json);
                                System.out.println("JSON Result" + jsonresult);

                                String firstName = json.getString("first_name");
                                String LastName = json.getString("last_name");
                                String email;

                                if (json.has("email")) {
                                    email = json.getString("email");
                                } else {
                                    email = "";
                                }


                                SettingPreffrences.setFBId(getApplicationContext(), json.getString("id"));


                                callFBWS(loginResult.getAccessToken(), firstName, LastName, json.getString("email"), json.getString("id"));


                            } catch (Exception e) {
                                e.printStackTrace();
                                hideDialog();
                                alert("Something went wrong, please try again later");
                            }
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "On cancel");
                hideDialog();
                alert("Something went wrong, please try again later");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, " onError " + error.toString());
                hideDialog();
                //alert(error.getLocalizedMessage());
                alert("Something went wrong, please try again later");
            }
        });


    }

    private void callFBWS(AccessToken accessToken, String firstName, String lastName, String email, String id) {


        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {


            try {
                params = "os=" + URLEncoder.encode(OS, "UTF-8") + "&make=" + URLEncoder.encode(MAKE, "UTF-8") + "&model=" + URLEncoder.encode(MODEL, "UTF-8") + "&firstName=" + URLEncoder.encode(firstName, "UTF-8") + "&lastName=" + URLEncoder.encode(lastName, "UTF-8") + "&email=" + URLEncoder.encode(email, "UTF-8") + "&fbId=" + URLEncoder.encode(id, "UTF-8") + "&fbOauth=" + URLEncoder.encode(accessToken.getToken(), "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.host + NetworkUrl.facebook, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {


                        if (!response.equals("error")) {

                            parseFBJson(response);

                        } else {

                            //error
                            alert("Something went, please try again later");
                            hideDialog();
                        }


                    }
                }, "POST");

                asyncTaskHelper.execute();


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {

            //no internet
            alert("Internet Connection not available");
        }


    }

    private void parseFBJson(String response) {


        try {
            jsonObject = new JSONObject(response);

            status = jsonObject.getString("status");
            msg = jsonObject.getString("message");

            if (status.equalsIgnoreCase("error")) {
                alert("Something went wrong, please try again later");

            } else if (status.equalsIgnoreCase("failure")) {
                alert(msg);
            } else {

                JSONArray res = jsonObject.getJSONArray("response");

                for (int i = 0; i < res.length(); i++) {

                    jsonObject = res.getJSONObject(i);
                    SettingPreffrences.setUserid(getApplicationContext(), jsonObject.getString("userId"));
                    SettingPreffrences.setToken(getApplicationContext(), jsonObject.getString("token"));
                    SettingPreffrences.setName(getApplicationContext(), jsonObject.getString("firstName") + " " + jsonObject.getString("lastName"));
                    SettingPreffrences.setEmail(getApplicationContext(), jsonObject.getString("email"));
                    SettingPreffrences.setMobile(getApplicationContext(), jsonObject.getString("mobile"));

                    if (jsonObject.getString("photo").equalsIgnoreCase("")) {
                        SettingPreffrences.setPhoto(getApplicationContext(), "http://graph" + ".facebook.com/" + SettingPreffrences.getFBId(getApplicationContext()) + "/picture?type=large");
                    } else {

                        SettingPreffrences.setPhoto(getApplicationContext(), jsonObject.getString("photo"));
                    }
                    CommonFunctions.addUserToParse(jsonObject.getString("userId"));
                }

                SettingPreffrences.setLoginViaFB(getApplicationContext(), true);
                SettingPreffrences.setLoginDone(getApplicationContext(), true);
                /*if (!SettingPreffrences.getDisclaimer(getApplicationContext())) {
                    intent = new Intent(getApplicationContext(), Disclaimer.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent
                    .FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                } else {
                    intent = new Intent(getApplicationContext(), Language.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent
                    .FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }*/

                hideDialog();
                //updateLastSeen();
                checkForSDCard();


            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        hideDialog();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public boolean isHavingMultipleStandards() {
       // String path = externalStorage1();
        String path = App.BASE_PATH;
        if (new File(path).exists()) {
            if (new File(path + "data-format.zip").exists() || new File(path + "Data-Format.zip").exists()) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /* new SD Cards have Course Name excel sheet which is used to get Course names and folder
	location for those courses */
    public boolean isNewMultiStdCard() {
        //String path = externalStorage1();
        String path = App.BASE_PATH;
        if (new File(path).exists()) {
            if (new File(path + COURSE_NAME_EXCEL).exists()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private void checkForSDCard() {

        showDialog();

       // App.BASE_PATH = externalStorage1();
        SettingPreffrences.setOriginalPath(getApplicationContext(), BASE_PATH);

        if (!new File(App.BASE_PATH).exists()) {

//            dimensions = new HashMap<>();
//            dimensions.put("userId", getUserId(getApplicationContext()));
//            dimensions.put("name", "SD Card not found");
//            CommonFunctions.sendAnalytics("errorscreen", null);

            App.getInstance().trackScreenView("errorscreen");
            showError();

            return;
        }

        try {
            hexEncoded = StringXORer.encode(key, hex);
            hexEncoded = StringXORer.decode(hexEncoded, hex);
        } catch (Exception e) {

        }

        hexEncoded = hexEncoded + Video.randomizer.substring(1);

        if (new File(App.BASE_PATH + "data-format.zip").exists() || new File(App.BASE_PATH + "Data-Format.zip").exists()) {
            createDB();
        } else {

            if (!App.BASE_PATH.equalsIgnoreCase("/")) {

                SettingPreffrences.setSubjectTitle(getApplicationContext(), subject.getChapterName());
                SettingPreffrences.setStandard(getApplicationContext(), subject.getChapterName());
               // App.BASE_PATH = externalStorage1();
                SettingPreffrences.setOriginalPath(getApplicationContext(), App.BASE_PATH);

                App.BASE_PATH = SettingPreffrences.getOriginalpath(getApplicationContext()) + subject.getFolderName() + "/";

                createDB();

            } else {

                tvNoMem.setVisibility(View.GONE);
                tvError.setText("You don't seem to have a valid Eclass memory card inserted");
                showError();
            }
        }

        RealmDb.makeRealmConfiguration();
    }

    private void showError() {

        hideDialog();
        if (svOfline.getVisibility() == View.VISIBLE) {

            svOfline.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                    setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {

                            scError.setAlpha(0.f);
                            scError.setScaleX(0.f);
                            scError.setScaleY(0.f);
                            scError.setVisibility(View.VISIBLE);
                            scError.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animator) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animator) {

                                    svOfline.setVisibility(View.GONE);
                                }

                                @Override
                                public void onAnimationCancel(Animator animator) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animator) {

                                }
                            });


                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    });


        } else {
            rlLoginMain.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                    setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {

                            scError.setAlpha(0.f);
                            scError.setScaleX(0.f);
                            scError.setScaleY(0.f);
                            scError.setVisibility(View.VISIBLE);
                            scError.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animator) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animator) {

                                    rlLoginMain.setVisibility(View.GONE);
                                }

                                @Override
                                public void onAnimationCancel(Animator animator) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animator) {

                                }
                            });


                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    });

        }


    }

    private void hideError() {


        scError.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {


                rlLoginMain.setAlpha(0.f);
                rlLoginMain.setScaleX(0.f);
                rlLoginMain.setScaleY(0.f);
                rlLoginMain.setVisibility(View.VISIBLE);
                rlLoginMain.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        scError.setVisibility(View.GONE);

                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });


            }

            @Override
            public void onAnimationCancel(Animator animator) {


            }

            @Override
            public void onAnimationRepeat(Animator animator) {


            }
        });


    }


    private void hideForgetPassword() {

        rlFP.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        svLogin.setAlpha(0.f);
                        svLogin.setScaleX(0.f);
                        svLogin.setScaleY(0.f);
                        svLogin.setVisibility(View.VISIBLE);
                        svLogin.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {

                                rlFP.setVisibility(View.GONE);
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });


    }

    private void showForgetPassword() {

        svLogin.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {


                        rlFP.setAlpha(0.f);
                        rlFP.setScaleX(0.f);
                        rlFP.setScaleY(0.f);
                        rlFP.setVisibility(View.VISIBLE);
                        rlFP.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {

                                svLogin.setVisibility(View.GONE);

                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {


                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {


                    }
                });


    }

    private void hideLogin() {


        svLogin.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {


                        rlLoginMain.setAlpha(0.f);
                        rlLoginMain.setScaleX(0.f);
                        rlLoginMain.setScaleY(0.f);
                        rlLoginMain.setVisibility(View.VISIBLE);
                        rlLoginMain.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {

                                svLogin.setVisibility(View.GONE);

                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {


                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {


                    }
                });


    }

    private void hideOfflineAccess() {


        svOfline.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {


                        rlLoginMain.setAlpha(0.f);
                        rlLoginMain.setScaleX(0.f);
                        rlLoginMain.setScaleY(0.f);
                        rlLoginMain.setVisibility(View.VISIBLE);
                        rlLoginMain.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {

                                svOfline.setVisibility(View.GONE);

                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {


                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {


                    }
                });

    }

    private void hideActivationCode_showError() {

        sc_act_code.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {


                        scError.setAlpha(0.f);
                        scError.setScaleX(0.f);
                        scError.setScaleY(0.f);
                        scError.setVisibility(View.VISIBLE);
                        scError.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {

                                sc_act_code.setVisibility(View.GONE);

                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {


                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {


                    }
                });

    }

    private void hideActivationCode_showLogin() {
        sc_act_code.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {


                        rlLoginMain.setAlpha(0.f);
                        rlLoginMain.setScaleX(0.f);
                        rlLoginMain.setScaleY(0.f);
                        rlLoginMain.setVisibility(View.VISIBLE);
                        rlLoginMain.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {

                                sc_act_code.setVisibility(View.GONE);

                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {


                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {


                    }
                });

    }


    private void hideActivationCode_showMultipleStdOption() {
        sc_act_code.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {


                        rlMultipleStd.setAlpha(0.f);
                        rlMultipleStd.setScaleX(0.f);
                        rlMultipleStd.setScaleY(0.f);
                        rlMultipleStd.setVisibility(View.VISIBLE);
                        rlMultipleStd.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {

                                sc_act_code.setVisibility(View.GONE);

                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {


                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {


                    }
                });

    }


    private void showLogin() {

        rlLoginMain.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        svLogin.setAlpha(0.f);
                        svLogin.setScaleX(0.f);
                        svLogin.setScaleY(0.f);
                        svLogin.setVisibility(View.VISIBLE);
                        svLogin.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {

                                rlLoginMain.setVisibility(View.GONE);
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
    }


    private void showActivationCodeView() {
        if (rlLoginMain.getVisibility() == View.VISIBLE) {

            rlLoginMain.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                    setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {

                            sc_act_code.setAlpha(0.f);
                            sc_act_code.setScaleX(0.f);
                            sc_act_code.setScaleY(0.f);
                            sc_act_code.setVisibility(View.VISIBLE);
                            sc_act_code.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animator) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animator) {

                                    rlLoginMain.setVisibility(View.GONE);
                                }

                                @Override
                                public void onAnimationCancel(Animator animator) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animator) {

                                }
                            });


                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    });

        } else if (svLogin.getVisibility() == View.VISIBLE) {

            svLogin.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                    setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {

                            sc_act_code.setAlpha(0.f);
                            sc_act_code.setScaleX(0.f);
                            sc_act_code.setScaleY(0.f);
                            sc_act_code.setVisibility(View.VISIBLE);
                            sc_act_code.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animator) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animator) {

                                    svLogin.setVisibility(View.GONE);
                                }

                                @Override
                                public void onAnimationCancel(Animator animator) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animator) {

                                }
                            });


                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    });

        } else {

            sc_act_code.setAlpha(0.f);
            sc_act_code.setScaleX(0.f);
            sc_act_code.setScaleY(0.f);
            sc_act_code.setVisibility(View.VISIBLE);
            sc_act_code.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {

                    svLogin.setVisibility(View.GONE);
                    rlLoginMain.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

        }
    }


    private void showActivationCodeView_and_hideMainView() {


        rlLoginMain.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        sc_act_code.setAlpha(0.f);
                        sc_act_code.setScaleX(0.f);
                        sc_act_code.setScaleY(0.f);
                        sc_act_code.setVisibility(View.VISIBLE);
                        sc_act_code.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {

                                rlLoginMain.setVisibility(View.GONE);
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
    }

    private void showActivationCodeView_and_hideLoginView() {

        svLogin.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        sc_act_code.setAlpha(0.f);
                        sc_act_code.setScaleX(0.f);
                        sc_act_code.setScaleY(0.f);
                        sc_act_code.setVisibility(View.VISIBLE);
                        sc_act_code.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {

                                svLogin.setVisibility(View.GONE);
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });

    }

    private void showActivationCodeView_and_hideErrorView() {

        scError.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        sc_act_code.setAlpha(0.f);
                        sc_act_code.setScaleX(0.f);
                        sc_act_code.setScaleY(0.f);
                        sc_act_code.setVisibility(View.VISIBLE);
                        sc_act_code.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {

                                scError.setVisibility(View.GONE);
                                tvReenter.setVisibility(View.GONE);
                                ACTIVATION_CODE_ERR_OCC = false;
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });

    }

    private void showOffline() {

        rlLoginMain.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        svOfline.setAlpha(0.f);
                        svOfline.setScaleX(0.f);
                        svOfline.setScaleY(0.f);
                        svOfline.setVisibility(View.VISIBLE);
                        svOfline.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {

                                rlLoginMain.setVisibility(View.GONE);
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });


    }

    @Override
    public void onBackPressed() {

        if (svLogin.getVisibility() == View.VISIBLE) {

            hideLogin();
        } else if (rlFP.getVisibility() == View.VISIBLE) {

            hideForgetPassword();

        } else if (scError.getVisibility() == View.VISIBLE) {

            if (ACTIVATION_CODE_ERR_OCC) {

                alert("Are you sure you want to exit?", "Exit", "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

            } else {
                hideError();
            }

        } else if (svOfline.getVisibility() == View.VISIBLE) {

            hideOfflineAccess();
        } else if (sc_act_code.getVisibility() == View.VISIBLE) {

            alert("Are you sure you want to exit?", "Exit", "Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

        } else if (sc_inform_to_login.getVisibility() == View.VISIBLE) {

            hide_inform_to_login_view();

        } else {
            super.onBackPressed();
        }


    }

    public void showDialog() {

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setCancelable(false);
        ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.pb);
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.blue), PorterDuff.Mode.SRC_IN);
        dialog.show();

    }

    public void hideDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

    }

    private void checkValidity() {
        Log.e("Checker","checkValidity_typeC: "+Master.listAll(Master.class).get(0));
        hideDialog();

        String cardId = "";

        try {
            cardId = getSDCARDiD();
        } catch (IOException e) {
            e.printStackTrace();
            cardId = "";
        }

        Master master = Master.listAll(Master.class).get(0);
        //Log.d("checkhere",master.getExpiry().toString());
        boolean isPenDrive = isPenDrive(master);
        if (isPenDrive) {
            // check if Usb device is inserted
            UsbHandler usbHandler = new UsbHandler();
            List<UsbDevice> usbDevices = usbHandler.getUsbDevices();
            //Log.d(TAG, "USB Device Count -> " + usbDevices.size());
            if (!usbDevices.isEmpty()) {
                for (int i = 0; i < usbDevices.size(); i++) {
                    String serialNum = usbDevices.get(i).getSerialNum();
                    // if serial number of USB device matches with serial number stored in Master,
                    // we use that as 'cardId'
                    if (cardIdMatchesWithMasterSheet(serialNum, master, isPenDrive)) {
                        cardId = serialNum;
                        break;
                    }
                    //Log.d(TAG, "USB Device SID -> " + cardId);
                }
            }
        }


//        showSerialNum(cardId);

        if (TextUtils.isEmpty(cardId)) {

//            showError();
            App.getInstance().trackEvent("Error", "click", "Something seems to be wrong", "");
//            tvError.setText("You don't seem to have a valid Eclass memory card inserted");
//            showLogin();


            goForActivationCode("You don't seem to have a valid Eclass memory card inserted");

            return;

        }


        if (master.getSid().equalsIgnoreCase("")) {

            goForActivationCode("SID is empty");
//            alert("SID is empty");
            return;

        } else {

            if (!cardIdMatchesWithMasterSheet(cardId, master, isPenDrive)) {

                App.getInstance().trackEvent("Error", "click", "Wrong Eclass memory card is " + "inserted, please try with Genuine Eclass memory card. Contact us to " + "purchase one", "");

                goForActivationCode("You don't seem to have a valid Eclass memory card inserted");
                return;
            }
        }

//        if (!master.getSid().substring(18, 25).equals(cardId.substring(18, 25))) {
//
//            showError();
//            dimensions = new HashMap<>();
//            dimensions.put("userId", getUserId(context));
//            dimensions.put("name", "Wrong Eclass memory card is inserted, please try with
// Genuine Eclass memory card. Contact us to purchase one");
//            CommonFunctions.sendAnalytics("errorscreen", dimensions);
//            tvError.setText("Wrong Eclass memory card is inserted, please try with Genuine
// Eclass memory card. Contact us to purchase one");
//            tvBuySDCard.setText("click to buy SD Card");
//            return;
//        }
        Log.d("isDateValid","valid");
        Log.e("Checker","1: "+"in typeC:: "+isDateValid(master));
        if (!isDateValid(master)) {
            return;
        }


        if (master.getActivation_code() != null && !master.getActivation_code().isEmpty()) {
            sendSDcardCredentials();
        }


        if (!SettingPreffrences.getDisclaimer(getApplicationContext())) {
            startActivity(Disclaimer.class);
        } else {
            if (new File(App.BASE_PATH + "data-format.zip").exists() || new File(App.BASE_PATH + "Data-Format.zip").exists()) {
                startActivity(Subjects.class);
            } else {
                startActivity(SelectMediumActivity.class);
            }

        }

        finish();


    }

    // Pen drives have SID of 20 character length whereas memory cards have 25 long SIDS.
    private boolean isPenDrive(Master master) {
        return master.getSid() != null && master.getSid().length() == 20;
    }

    private boolean cardIdMatchesWithMasterSheet(String cardId, Master master, boolean isPenDrive) {
        if (isPenDrive) {
            // compare whole Ids for pen drives
            return master.getSid().equals(cardId);
        } else {
            // compare last seven digits for memory cards
            return master.getSid().substring(18, 25).equals(cardId.substring(18, 25));
        }
    }

    private void showSerialNum(final String title, String serialNum) {
        AlertDialog dialog = new AlertDialog.Builder(Splash_typeC.this).setTitle(title).setMessage(serialNum).setCancelable(true).setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create();

        dialog.show();
    }

    public void goForActivationCode(String msg) {

        if (sc_inform_to_login.getVisibility() == View.VISIBLE) {
            hide_inform_to_login_view_S();
        }

        Master master = Master.listAll(Master.class).get(0);

        String activationCode = master.getActivation_code();
        if (activationCode != null && !activationCode.isEmpty()) {
            performActivationCodeChecks();
        } else {
            tvError.setText(msg);
            showError();
        }

    }

    private void performActivationCodeChecks() {
        /* sdcard sid is not available, then check whether logged in or not */
        if (SettingPreffrences.getLoginDone(this)) {

            /* check whether user has previously entered activation code and check whether
             *  that is valid or not*/
            if (hasPreviouslyEnteredActivationCode() && checkWhetherActivationCodeIsValid()) {

                /* update the server if user has internet*/
                if (CommonFunctions.isNetworkConnected(this)) {

                    if (checkPhoneStatePermission()) {
                        sendSDcardCredentials_blocking();
                    } else {
                        requestPhoneStatePermission();
                    }

                } else {

                    /*else forward the user to next screen directly*/
                    forwardUserToNextScreen();

                }

            } else {

                /* If logged in then ask user to enter activation code*/
                showActivationCodeView();

            }

        } else {

            if (SettingPreffrences.getContinueAsGuest(this)) {

                show_inform_to_login_view();

            } else {

            /* If not logged in then check internet and show login if internet available else
            show message*/
                if (CommonFunctions.isNetworkConnected(this)) {
                    showLogin();
                } else {
                    alert("Internet Connection not available, make sure you are connected to " + "internet.", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            showLogin();
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                }

            }
        }
    }

    public void show_inform_to_login_view() {
        rlLoginMain.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        sc_inform_to_login.setAlpha(0.f);
                        sc_inform_to_login.setScaleX(0.f);
                        sc_inform_to_login.setScaleY(0.f);
                        sc_inform_to_login.setVisibility(View.VISIBLE);
                        sc_inform_to_login.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {

                                rlLoginMain.setVisibility(View.GONE);
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
    }

    public void hide_inform_to_login_view_S() {
        sc_inform_to_login.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        sc_inform_to_login.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {


                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {


                    }
                });
    }

    public void hide_inform_to_login_view() {
        sc_inform_to_login.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {


                        rlLoginMain.setAlpha(0.f);
                        rlLoginMain.setScaleX(0.f);
                        rlLoginMain.setScaleY(0.f);
                        rlLoginMain.setVisibility(View.VISIBLE);
                        rlLoginMain.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {

                                sc_inform_to_login.setVisibility(View.GONE);

                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {


                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {


                    }
                });
    }

    public void hide_multiple_std_options_view_show_main_login() {

        rlMultipleStd.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {


                        rlLoginMain.setAlpha(0.f);
                        rlLoginMain.setScaleX(0.f);
                        rlLoginMain.setScaleY(0.f);
                        rlLoginMain.setVisibility(View.VISIBLE);
                        rlLoginMain.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {

                                rlMultipleStd.setVisibility(View.GONE);

                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {


                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {


                    }
                });
    }

    public void hide_multiple_std_options_view() {

        rlMultipleStd.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(600).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        rlMultipleStd.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {
                    }
                });
    }


    public void hide_inform_to_login_view_ShowLogin() {
        sc_inform_to_login.animate().alpha(0.f).scaleX(0.f).scaleY(0.f).setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {


                        svLogin.setAlpha(0.f);
                        svLogin.setScaleX(0.f);
                        svLogin.setScaleY(0.f);
                        svLogin.setVisibility(View.VISIBLE);
                        svLogin.animate().alpha(1.f).scaleX(1.f).scaleY(1.f).setDuration(500).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {

                                sc_inform_to_login.setVisibility(View.GONE);

                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {


                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {


                    }
                });
    }


    public void forwardUserToNextScreen() {

        Master master = Master.listAll(Master.class).get(0);
        Log.e("Checker","2: "+"in typeC:: "+isDateValid(master));
        if (!isDateValid(master)) {
            hideDialog();
            return;
        }

        if (!SettingPreffrences.getDisclaimer(getApplicationContext())) {
            sendSDcardCredentials();
            hideDialog();
            startActivity(Disclaimer.class);
        } else {
            sendSDcardCredentials();
            hideDialog();
            if (new File(App.BASE_PATH + "data-format.zip").exists() || new File(App.BASE_PATH + "Data-Format.zip").exists()) {
                startActivity(Subjects.class);
            } else {
                startActivity(SelectMediumActivity.class);
            }

        }

        finish();
    }


    private boolean isDateValid(Master master) {
        Log.e("Checker","inIsDateValid"+"in type c");
        ePrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String sid = master.getSid();

        // check if isExpired == true, if yes, return false.
        if (ePrefs.getBoolean(master.getSid(), false)) {

            tvError.setText("The Eclass memory card inserted has crossed its validity date and has expired.");
            showError();
            return false;
        }


        //Step 1 : check for network time
        String timeSettings = android.provider.Settings.System.getString(this.getContentResolver(), android.provider.Settings.System.AUTO_TIME);
        if (timeSettings.contentEquals("0")) {

//            dimensions = new HashMap<>();
//            dimensions.put("userId", getUserId(context));
//            dimensions.put("name", "Please set your device date and time to use Automatic Date & Time. You can change this from your device settings");
//            CommonFunctions.sendAnalytics("errorscreen", dimensions);

            App.getInstance().trackEvent("Error", "click", "Please set your device date and time to use Automatic Date & Time. You can change this from your device settings", "");
            tvError.setText("Please set your device date and time to use Automatic Date & Time. You can change this from your device settings");
            //tvBuySDCard.setText("CLICK TO KNOW MORE");

            tvNoMem.setVisibility(View.GONE);
            showError();

            return false;
        }

        long currentSystemTime = 0;
        try {
            currentSystemTime = new Date().getTime();
        } catch (Exception e) {

        }

        Log.v(tag, "Password: " + backupPassword);

        Log.d("MyExpiryCheck_typeC",currentSystemTime+" == "+master.getExpiry().getTime());
        /* Date expired */
        if (currentSystemTime > master.getExpiry().getTime()) {
//            dimensions = new HashMap<>();
//            dimensions.put("userId", getUserId(context));
//            dimensions.put("name", "This Eclass memory card is expired");
//            CommonFunctions.sendAnalytics("errorscreen", dimensions);

            App.getInstance().trackEvent("Error", "click", "This Eclass memory card is expired", "");

            // pavan :write code for deleting file
            // check if sd card is writable. If writabe, delete file
            // if not writable set boolean value isExpired to true

            File tempFile = new File(App.BASE_PATH + "tempText.txt");


            if (!tempFile.exists()) {
                try {
                    tempFile.createNewFile();
                    tempFile.delete();
                    Log.i(TAG, "WRITABLE");
                    deleteDataFormatFile();


                } catch (IOException e) {
                    e.printStackTrace();
                    // txtMessage.setText("Failed due to " + e.getMessage());

                    ePrefs.edit().putBoolean(master.getSid(), true).commit();
                    Log.i(TAG, "NOT WRITABLE");
                    // return false;

                }
            }


            tvError.setText("The Eclass memory card inserted has crossed its validity date and has expired.");
            //tvNoMem.setVisibility(View.VISIBLE);
            //tvBuySDCard.setText("CLICK TO KNOW MORE");

            showError();
            return false;
        }

        return true;
    }//end of function

    private boolean deleteDataFormatFile() {

        boolean isDeleted = false;
        if (new File(App.BASE_PATH + "data-format.zip").exists() || new File(App.BASE_PATH + "Data-Format.zip").exists()) {

            if (new File(App.BASE_PATH + "data-format.zip").exists()) {
                isDeleted = new File(App.BASE_PATH + "data-format.zip").delete();
                if (isDeleted) {
                    Log.i("DELETE", "" + isDeleted);
                }
            } else if (new File(App.BASE_PATH + "Data-Format.zip").exists()) {
                isDeleted = new File(App.BASE_PATH + "Data-Format.zip").delete();
                if (isDeleted) {
                    Log.i("DELETE", "" + isDeleted);
                }
            }
        }

        return isDeleted;
    }

/* srj 12/12
	private boolean isDateValid(Master master) {

		ePrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

		//Step 1 : check for network time
		String timeSettings = android.provider.Settings.System.getString(
				this.getContentResolver(),
				android.provider.Settings.System.AUTO_TIME);
		if (timeSettings.contentEquals("0")) {

//            dimensions = new HashMap<>();
//            dimensions.put("userId", getUserId(context));
//            dimensions.put("name", "Please set your device date and time to use Automatic Date
// & Time. You can change this from your device settings");
//            CommonFunctions.sendAnalytics("errorscreen", dimensions);

			App.getInstance().trackEvent("Error", "click", "Please set your device date and time " +
                    "to use Automatic Date & Time. You can change this from your device " +
                    "settings", "");
			tvError.setText("Please set your device date and time to use Automatic Date & Time. " +
                    "You can change this from your device settings");
			//tvBuySDCard.setText("CLICK TO KNOW MORE");

			tvNoMem.setVisibility(View.GONE);
			showError();

			return false;
		}

		File dateFile = new File(App.BASE_PATH + ".date.zip");
		long currentSystemTime = 0;
		try {
			currentSystemTime = new Date().getTime();
		} catch (Exception e) {

		}


        */
    /*
     *If the file does not exist,memory card is inserted for the 1st time
     * *//*

		if (ePrefs.getBoolean(SDCARD_IS_WRITEABLE, true)) {
			if (!dateFile.exists()) {
				try {
					dateFile.createNewFile();
					writeAccessTime(dateFile);
				} catch (IOException e) {
					e.printStackTrace();
					// txtMessage.setText("Failed due to " + e.getMessage());
					ePrefs.edit().putBoolean(SDCARD_IS_WRITEABLE, false).commit();
					// return false;
				}
			}
		}

		//Now check for it being false - Last Access Time never filled
		if (!ePrefs.getBoolean(SDCARD_IS_WRITEABLE, true)) {
			if (ePrefs.getString(LAST_ACCESS_TIME, "").equals("")) {
				//Put today's time as last accessed time
				String currentTime = new Date().getTime() + "";
				ePrefs.edit().putString(LAST_ACCESS_TIME, currentTime).commit();
			}
		}

		String temp = "";

		*/

    /**
     * Now checking for the actual date.
     *//*

		if (ePrefs.getBoolean(SDCARD_IS_WRITEABLE, true)) {
			try {

				FileInputStream inputStream = new FileInputStream(dateFile);
				int length = (int) dateFile.length();
				byte[] bytes = new byte[length];
				inputStream.read(bytes);
				inputStream.close();
				temp = new String(bytes);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				tvError.setText("You don't seem to have a valid Eclass memory card inserted");
				//tvNoMem.setText(" Please insert the Eclass memory card and try again.");
				//tvBuySDCard.setText("CLICK TO KNOW MORE");
				//tvNoMem.setVisibility(View.VISIBLE);
				showError();

				return false;

			} catch (IOException e) {
				e.printStackTrace();
				tvError.setText("You don't seem to have a valid Eclass memory card inserted");
				//tvNoMem.setText(" Please insert the Eclass memory card and try again.");
				//tvBuySDCard.setText("CLICK TO KNOW MORE");
				// tvNoMem.setVisibility(View.VISIBLE);
				showError();

				return false;
			}

			long dateInCard = 0;

			if (!temp.equals("")) {
				dateInCard = Long.parseLong(temp);
			} else {
				dateInCard = currentSystemTime;
			}

			//  Log.v(tag,"Date in card: "+new Date(dateInCard).toGMTString());
			Log.v(tag, "Password: " + backupPassword);

            */
    /* System date is tampered *//*

			if (currentSystemTime + 10000 < dateInCard) {

//                dimensions = new HashMap<>();
//                dimensions.put("name", "Your system date is not set correctly,please reset it");
//                dimensions.put("userId", getUserId(context));
//                CommonFunctions.sendAnalytics("errorscreen", dimensions);
				App.getInstance().trackEvent("Error", "click", "Your system date is not set " +
                        "correctly,please reset it", "");


				tvError.setText("Your system date is not set correctly,please reset it");
				tvNoMem.setVisibility(View.GONE);
				//tvBuySDCard.setText("CLICK TO KNOW MORE");
				showError();
				return false;
			}
		}//end of SD card is writable

		//Use Network Time & LAST ACCESSED TIME instead

		if (!ePrefs.getBoolean(SDCARD_IS_WRITEABLE, true)) {
			if (!ePrefs.getString(LAST_ACCESS_TIME, "").equals("")) {
				//Do Something here
				//Step 1 : check for network time
               */
/* String timeSettings = android.provider.Settings.System.getString(
                        this.getContentResolver(),
                        android.provider.Settings.System.AUTO_TIME);
              /*  if (timeSettings.contentEquals("0")) {
                    dimensions = new HashMap<>();
                    dimensions.put("userId", getUserId(context));
                    dimensions.put("name", "Please set your device date and time to use Automatic
                     Date & Time. You can change this from your device settings");
                    CommonFunctions.sendAnalytics("errorscreen", dimensions);
                    tvError.setText("Please set your device date and time to use Automatic Date &
                     Time. You can change this from your device settings");
                    tvBuySDCard.setText("Contact Us");
                    showError();

                    return false;
                } *//*
//else {
				//Automatic Data and Time is selected
				long dateInPersistence = Long.parseLong(ePrefs.getString(LAST_ACCESS_TIME, ""));
				long timeNow = new Date().getTime();
				if (dateInPersistence > timeNow) {

//                    dimensions = new HashMap<>();
//                    dimensions.put("userId", getUserId(context));
//                    dimensions.put("name", "Your system date is not set correctly, please reset
// it");
//                    CommonFunctions.sendAnalytics("errorscreen", dimensions);

					App.getInstance().trackEvent("Error", "click", "Your system date is not set " +
                            "correctly, please reset it", "");
					tvError.setText("Your system date is not set correctly, please reset it");
					//tvBuySDCard.setText("CLICK TO KNOW MORE");
					//tvNoMem.setVisibility(View.GONE);

					showError();
					return false;
				}
				// }
				//Step 2: check for last access time
			}
		}

        */
    /* Date expired *//*

		if (currentSystemTime > master.getExpiry().getTime()) {
//            dimensions = new HashMap<>();
//            dimensions.put("userId", getUserId(context));
//            dimensions.put("name", "This Eclass memory card is expired");
//            CommonFunctions.sendAnalytics("errorscreen", dimensions);

			App.getInstance().trackEvent("Error", "click", "This Eclass memory card is expired",
                    "");
			tvError.setText("The Eclass memory card inserted has crossed its validity date and has" +
                    " expired.");
			//tvNoMem.setVisibility(View.VISIBLE);
			//tvBuySDCard.setText("CLICK TO KNOW MORE");

			showError();
			return false;
		}

		if (ePrefs.getBoolean(SDCARD_IS_WRITEABLE, true)) {
			try {
				writeAccessTime(dateFile);
			} catch (IOException e) {
				e.printStackTrace();
				//txtMessage.setText("Failed due to " + e.getMessage());
				//You've caught them - they've changed phones
				//txtMessage.setText("Please re-launch the app");
				ePrefs.edit().putBoolean(SDCARD_IS_WRITEABLE, false).commit();
				//return isDateValid(master);
				//return false;
			}
		}
		//Updating Last Access Time
		if (!ePrefs.getBoolean(SDCARD_IS_WRITEABLE, true)) {
			String currentTime = new Date().getTime() + "";
			ePrefs.edit().putString(LAST_ACCESS_TIME, currentTime).commit();
		}

		return true;
	}//end of function
*/
    private void writeAccessTime(File file) throws IOException {

        FileOutputStream outputStreamWriter = new FileOutputStream(file);
        String currentTime = new Date().getTime() + ""; // We are using new time here. That is why
        // we have to use + 1000 later
        //  Log.v(tag,"currentTime: "+currentTime);

        outputStreamWriter.write(currentTime.getBytes());
        outputStreamWriter.close();

    }


    private String getSDCARDiD() throws IOException {

        String memBlk = "";
        String sd_cid = "";
        String serial_id = "";

        File file = new File("/sys/block/mmcblk1");
        if (file.exists() && file.isDirectory()) {

            memBlk = "mmcblk1";
        } else {
            memBlk = "mmcblk0";
        }

        Process cmd = Runtime.getRuntime().exec("cat /sys/block/" + memBlk + "/device/cid");
        BufferedReader br = new BufferedReader(new InputStreamReader(cmd.getInputStream()));
        sd_cid = br.readLine();
        // sd_cid = sd_cid.substring(0, sd_cid.length() - 1);
        return sd_cid;
        //return serial_id;
    }

    private void createDB() {


        new AsyncTask<Void, Void, String>() {


            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected String doInBackground(Void... params) {

                try {
                    Master.deleteAll(Master.class);
                    Subject.deleteAll(Subject.class);
                } catch (Exception e) {
                    // ignoring errors that might be caused due to non-existence of tables
                    e.printStackTrace();
                }

                try {
                    createDatabaseXL();
                    // copying and initializing MCQ Database
                    DatabaseHelper dataBaseHelper = new DatabaseHelper(Splash_typeC.this);
                    dataBaseHelper.createDataBase();

                } catch (IOException e) {
                    e.printStackTrace();
                    return e.getMessage();
                } catch (BiffException e) {
                    e.printStackTrace();
                    return e.getMessage();
                } catch (ZipException e) {
                    e.printStackTrace();
                    return e.getMessage();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    Log.i("SRJ", "ERROR");
                    return e.getMessage();
                }


                return "success";
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                if (!s.equals("success")) {

                    showError();
                    return;
                }

                checkValidity();

            }
        }.execute();


    }


    private void createDatabaseXL() throws IOException, BiffException, ZipException, NullPointerException {


        filePath = decryptZip(App.BASE_PATH + "data-format.zip", true);


        Workbook workbook = Workbook.getWorkbook(new File(filePath));


        int noOfSheets = workbook.getNumberOfSheets();

        for (int i = 0; i < noOfSheets; i++) {

            if (i == 0) {
                createMasterSheet(workbook.getSheet(0));
            } else {
                createSubject(workbook.getSheet(i));

            }

        }

    }

    private void exportDatabase(String databaseName) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" + getPackageName() + "//databases//" + databaseName + "";
                String backupDBPath = "backupname.db";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }
    }


    private void createSubject(Sheet sheet) {

        boolean isNewSdCard = false;
        String displaySubjectName = "";
        for (int row = 1; row < sheet.getRows(); row++) {
            Cell[] cell = sheet.getRow(row);

            // old sd cards had six columns
            if (cell.length == 6) {
                isNewSdCard = false;
                if (!TextUtils.isEmpty(cell[5].getContents().trim())) {
                    displaySubjectName = cell[5].getContents().trim();
                    break;
                }

                // new sd cards have eight columns
            } else if (cell.length == 8) {
                isNewSdCard = true;
                if (!TextUtils.isEmpty(cell[7].getContents().trim())) {
                    displaySubjectName = cell[7].getContents().trim();
                    break;
                }
            }


        }

        if (!TextUtils.isEmpty(displaySubjectName)) {

            for (int row = 1; row < sheet.getRows(); row++) {

                Cell[] cell = sheet.getRow(row);


                if (cell.length <= 0) {
                    return;
                }


                if (!TextUtils.isEmpty(cell[0].getContents())) {

                    Subject subject = new Subject();

                    subject.setDisplaySubjectName(displaySubjectName);
                    subject.setSubjectName(sheet.getName().trim());
                    subject.setFolderName(cell[0].getContents().trim());
                    subject.setChapterName(cell[1].getContents().trim());
                    subject.setContent(cell[2].getContents().trim());

                    subject.setMindMap(cell[3].getContents().trim());
                    subject.setqNa(cell[4].getContents().trim());

                    /* for new sd cards, we have PDFs and MCQs too. Each Subject sheet has a
                    column for PDF where 'NO' means
                     there are no PDFs and else there is count of PDFs in given chapter. Whereas
                     for MCQs, an integer indicates
                     it is Chapter_serial_number which is used to query MCQ database for number
                     of MCQs in the given chapter*/
                    if (isNewSdCard) {
                        String pdfCount = cell[5].getContents().trim();
                        if (pdfCount.equalsIgnoreCase("NO")) {
                            subject.setPdf("0");
                        } else {
                            subject.setPdf(pdfCount);
                        }

                        String chapSerialNum = cell[6].getContents().trim();
                        if (chapSerialNum.equalsIgnoreCase("NO")) {
                            subject.setMcq("0");
                        } else {
                            subject.setMcq(chapSerialNum);
                        }

                    } else {
                        subject.setPdf("0");
                        subject.setMcq("0");
                    }

                    if (subject != null) {
                        subject.save();
                    }

                }


            }
        }


    }

    private void createMasterSheet(Sheet sheet) {


        for (int row = 1; row < sheet.getRows(); row++) {

            Cell[] cell = sheet.getRow(row);

            Master master = new Master();

            if (!TextUtils.isEmpty(cell[0].getContents())) {

                master.setStd(Integer.parseInt(cell[0].getContents().trim()));
                master.setMedium(cell[1].getContents().trim());

                SettingPreffrences.setSubjectTitle(getApplicationContext(), "Standard " + master.getStd() == null ? "" : master.getStd() + "-" + master.getMedium() == null ? "" : master.getMedium());


                //    Log.v(tag,"createMasterSheet"+cell[2].getContents().trim());

                if (cell[2].getType() == CellType.DATE) {

                    DateCell dateCell = (DateCell) cell[2];
                    master.setExpiry(dateCell.getDate());

                } else {

                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");


                    try {
                        master.setExpiry(dateFormat.parse(cell[2].getContents().trim()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                        // Wrong expiry date
                        master.setExpiry(new Date());

                    }

                }

                try {
                    master.setKey(Integer.parseInt(cell[4].getContents().trim()));
                } catch (Exception e) {
                    master.setKey(-1);
                }

                master.setSid(cell[3].getContents().trim());

                try {
                    master.setActivation_code(cell[5].getContents().trim());
                } catch (Exception e) {
                    master.setActivation_code("");
                }

                master.save();

            }


        }
    }

    @Override
    protected void onPause() {
        super.onPause();


        File temp = new File(Environment.getDataDirectory(), "temp/");
        File[] fileList = temp.listFiles();

        if (fileList != null) {

            for (int i = 0; i < fileList.length; i++) {

                if (fileList[i].getAbsolutePath().contains(".xls")) {
                    fileList[i].delete();
                }
            }
        }

        File temp1 = new File(getExternalFilesDir(null), "temp/");
        File[] fileList1 = temp1.listFiles();

        if (fileList1 != null) {

            for (int i = 0; i < fileList1.length; i++) {

                if (fileList1[i].getAbsolutePath().contains(".xls")) {
                    fileList1[i].delete();
                }
            }
        }


        if (filePath != null) {

            filePath = filePath.replace("Data-Format.xls", "");
            File temp2 = new File(filePath);
            File[] fileList2 = temp2.listFiles();

            if (fileList2 != null) {

                for (int i = 0; i < fileList2.length; i++) {

                    if (fileList2[i].getAbsolutePath().contains(".xls")) {
                        fileList2[i].delete();
                    }
                }
            }
        }


    }

    @Override
    protected void onDestroy() {
        hideDialog();
        super.onDestroy();

        File temp = new File(Environment.getDataDirectory(), "temp/");
        File[] fileList = temp.listFiles();

        if (fileList != null) {

            for (int i = 0; i < fileList.length; i++) {

                if (fileList[i].getAbsolutePath().contains(".xls")) {
                    fileList[i].delete();
                }
            }
        }

        File temp1 = new File(getExternalFilesDir(null), "temp/");
        File[] fileList1 = temp1.listFiles();

        if (fileList1 != null) {

            for (int i = 0; i < fileList1.length; i++) {

                if (fileList1[i].getAbsolutePath().contains(".xls")) {
                    fileList1[i].delete();
                }
            }
        }


        if (filePath != null) {

            filePath = filePath.replace("Data-Format.xls", "");
            File temp2 = new File(filePath);
            File[] fileList2 = temp2.listFiles();

            if (fileList2 != null) {

                for (int i = 0; i < fileList2.length; i++) {

                    if (fileList2[i].getAbsolutePath().contains(".xls")) {
                        fileList2[i].delete();
                    }
                }
            }
        }


    }

    private boolean checkPermission() {

        int read_perm = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int phone_state_perm = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE);

        switch (0) {

            case 0:
                if (read_perm != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }

            case 1:
                if (phone_state_perm != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }

            case 3:
                return true;


        }

        return true;

//        if (read_perm == PackageManager.PERMISSION_GRANTED) {
//            return true;
//        } else {
//            return false;
//        }


    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);

//        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission
// .READ_EXTERNAL_STORAGE)) {
//
//
//        } else {
//
//            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission
// .READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
//        }
    }


    private boolean checkPhoneStatePermission() {

        int phone_state_perm = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE);

        return phone_state_perm == PackageManager.PERMISSION_GRANTED;

    }

    private void requestPhoneStatePermission() {

        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_PHONE_STATE);

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if (SettingPreffrences.getSignupDone(this)) {

            //updateLastSeen();
            checkForSDCard();

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    animateLogo();

                } else {

                    alert("We need access to your SD Card storage to display content and videos. " + "Kindly provide the app with access to SD Card storage by going into " + "your app settings and enabling them.");

                }
                break;

            case PERMISSION_PHONE_STATE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendSDcardCredentials_blocking();
                } else {

                    boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE);

                    if (!showRationale) {

                        showRationaleForPhoneStatePermission();

                    } else {

                        requestPhoneStatePermission();

                    }

                }
                break;
        }
    }

    private void showRationaleForPhoneStatePermission() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(Splash_typeC.this);
        builder.setMessage(getString(R.string.message_for_requesting_phone_state_perm));
        builder.setCancelable(false);
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("Go to settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }

        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    protected String externalStorage1() {

        File storage = new File("/storage");
        String path = "";


        //File storage = Environment.getExternalStorageDirectory().getAbsoluteFile();

        File file[] = storage.listFiles();
        //Toast.makeText(this, file.toString(), Toast.LENGTH_SHORT).show();

//        StringBuilder sf = new StringBuilder("");
//        for (File f : file) {
//            sf.append(f.getName());
//            sf.append("\n");
//        }

//        Toast.makeText(this, sf.toString(), Toast.LENGTH_LONG).show();

        if (file != null) {

            for (int i = 0; i < file.length; i++) {
                //  Log.v("Files", "FileName:" + file[i].getName());
                // Toast.makeText(this, "for loop 1", Toast.LENGTH_SHORT).show();

                File file1 = new File("/storage/" + file[i].getName() + "/");

                File files2[] = file1.listFiles();
                // Toast.makeText(this, files2.toString(), Toast.LENGTH_SHORT).show();

                if (files2 != null) {
                    for (int j = 0; j < files2.length; j++) {
                        // Toast.makeText(this, "for loop 2", Toast.LENGTH_SHORT).show();
                        if (files2[j].toString().equals(file1.getAbsolutePath() + "/" + BASE_LOCATION)) {

                            path = file1.getAbsolutePath() + "/" + BASE_LOCATION + "/";
                            return path;
                        }
                    }
                } else {

                    path = "";

                }


            }
        }


        //for blueStack path is different
        File winShared = new File("/storage/sdcard/windows/BstSharedFolder");

        if (winShared.listFiles() != null) {
            path = winShared.getAbsolutePath() + "/" + BASE_LOCATION + "/";
            Toast.makeText(this, path, Toast.LENGTH_LONG).show();
            return path;
        } else {
            path = "";
        }


        if (path.equals("")) {


            String test[] = externalpath.split("\n");
            String path1 = test[test.length - 1];
            path1 = path1.replace("*", "");


            if (externalpath.contains("fat")) {
                storage = Environment.getExternalStorageDirectory().getAbsoluteFile();
            } else {
                storage = new File(path1);
                //storage = new File(list.get(0).path);
                //storage = new File("/storage");
            }

            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    //  Log.v("Files", "FileName:" + file[i].getName());
                    // Toast.makeText(this, "for loop 3", Toast.LENGTH_SHORT).show();

                    String fileValue;
                    fileValue = files[i].toString();
                    if (externalpath.contains("fat")) {


                        if (fileValue.equals(storage.getPath() + "/" + BASE_LOCATION)) {
                            path = storage.getPath() + "/" + files[i].getName();
                        }
                    } else {
                        if (fileValue.equals(path1 + "/" + BASE_LOCATION)) {
                            path = path1 + "/" + files[i].getName();
                        }
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                }
            } else {

                path = "";

            }


        }

        if (path.equals("")) {

//            String test[] = externalpath.split("\n");
//            String path1 = test[test.length - 1];
//            path1 = path1.replace("*", "");
            String path1;
            if (list.size() > 0) {

                path1 = list.get(list.size() - 1).path;
            } else {
                path1 = "";
            }


            if (externalpath.contains("fat")) {
                storage = Environment.getExternalStorageDirectory().getAbsoluteFile();
            } else {
                storage = new File(path1);
                //storage = new File(list.get(0).path);
                //storage = new File("/storage");
            }

            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    // Toast.makeText(this, "for loop 4", Toast.LENGTH_SHORT).show();
                    //  Log.v("Files", "FileName:" + file[i].getName());

                    String fileValue;
                    fileValue = files[i].toString();
                    if (externalpath.contains("fat")) {


                        if (fileValue.equals(storage.getPath() + "/" + BASE_LOCATION)) {
                            path = storage.getPath() + "/" + files[i].getName();
                        }
                    } else {
                        if (fileValue.equals(path1 + "/" + BASE_LOCATION)) {
                            path = path1 + "/" + files[i].getName();
                        }
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                } else {

                    path = "";
                }
            }

        }

        if (path.equals("")) {

            String path1 = "";


            for (int k = 0; k < list.size(); k++) {

                path1 = list.get(k).path;


                storage = new File(path1);

                File files[] = storage.listFiles();

                if (files != null) {
                    for (int i = 0; i < files.length; i++) {

                        // Toast.makeText(this, "for loop 5", Toast.LENGTH_SHORT).show();
                        //  Log.v("Files", "FileName:" + file[i].getName());

                        String fileValue;
                        fileValue = files[i].toString();

                        if (fileValue.equals(path1 + "/" + BASE_LOCATION)) {
                            path = path1 + "/" + files[i].getName();
                            break;
                        }


                    }

                    if (new File(path).exists()) {

                        return path + "/";

                    }
                } else {

                    path = "";

                }

            }


        }

        if (path.equals("")) {

            String path1 = "mnt/sd-ext/";
            storage = new File(path1);
            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {


                    String fileValue;
                    fileValue = files[i].toString();

                    if (fileValue.equals(path1 + BASE_LOCATION)) {
                        path = path1 + "/" + files[i].getName();
                        break;
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                }
            } else {

                path = "";

            }

        }

        if (path.equalsIgnoreCase("")) {


            String path1 = "/mnt/usb_storage/USB_DISK0/udisk0";
            storage = new File(path1);
            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {


                    String fileValue;
                    fileValue = files[i].toString();

                    if (fileValue.equals(path1 + BASE_LOCATION)) {
                        path = path1 + "/" + files[i].getName();
                        break;
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                }
            } else {

                path = "";

            }

        }

        if (path.equalsIgnoreCase("")) {


            String path1 = "/USB/USB_DISK0/udisk0";
            storage = new File(path1);
            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {


                    String fileValue;
                    fileValue = files[i].toString();

                    if (fileValue.equals(path1 + BASE_LOCATION)) {
                        path = path1 + "/" + files[i].getName();
                        break;
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                }
            } else {

                return "";

            }

        }

        return "/";


    }

    public void sendSDcardCredentials_blocking() {

        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {

            showDialog();

            Master master = Master.listAll(Master.class).get(0);

            String cardIDSystem;
            try {
                cardIDSystem = getSDCARDiD();
            } catch (IOException e) {
                e.printStackTrace();
                cardIDSystem = "";
            }
            if (cardIDSystem == null) {
                cardIDSystem = "";
            }

            String imei = "";

            if (checkPhoneStatePermission()) {
//                TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context
// .TELEPHONY_SERVICE);
//                imei = telephonyManager.getDeviceId();
                imei = getUniqueDeviceId();
            }

            try {
                params = "os=" + URLEncoder.encode(OS, "UTF-8") + "&make=" + URLEncoder.encode(MAKE, "UTF-8") + "&model=" + URLEncoder.encode(MODEL, "UTF-8") + "&imei=" + URLEncoder.encode(imei, "UTF-8") + "&userId=" + URLEncoder.encode(SettingPreffrences.getUserid(this), "UTF-8") + "&activationCode=" + URLEncoder.encode(master.getActivation_code(), "UTF-8") + "&sidExcel=" + URLEncoder.encode(master.getSid(), "UTF-8") + "&sidAndroid=" + URLEncoder.encode(cardIDSystem, "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.host + NetworkUrl.activation_code_url, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {

                        parseActivationResponse(response);

                    }
                }, "POST");

                asyncTaskHelper.execute();


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {

            //no internet
            alert("Internet Connection not available", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sendSDcardCredentials_blocking();
                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });

        }


    }

    private void parseActivationResponse(String response) {
        try {
            jsonObject = new JSONObject(response);

            status = jsonObject.getString("status");
            msg = jsonObject.getString("message");

            if (status.equalsIgnoreCase("failure")) {

                try {
                    ActivationCode.deleteAll(ActivationCode.class);
                } catch (Exception e) {
//                    e.printStackTrace();
                }

                hideDialog();
                tvError.setText(msg);
                tvReenter.setVisibility(View.VISIBLE);
                ACTIVATION_CODE_ERR_OCC = true;
                hideActivationCode_showError();

            } else if (status.equalsIgnoreCase("error")) {

                hideDialog();

            } else {
                ACTIVATION_CODE_ERR_OCC = false;
                tvReenter.setVisibility(View.GONE);

                Master master = Master.listAll(Master.class).get(0);
                ActivationCode code = new ActivationCode();
                code.setActivationCode(master.getActivation_code());
                code.save();

                hideDialog();
                Log.e("Checker","3: "+"in typeC:: "+isDateValid(master));
                if (!isDateValid(master)) {
                    hideDialog();
                    return;
                }

                if (!SettingPreffrences.getDisclaimer(getApplicationContext())) {
                    hideDialog();
                    startActivity(Disclaimer.class);
                } else {
                    hideDialog();
                    if (new File(App.BASE_PATH + "data-format.zip").exists() || new File(App.BASE_PATH + "data-format.zip").exists()) {
                        startActivity(Subjects.class);
                    } else {
                        startActivity(SelectMediumActivity.class);
                    }
                }

                finish();

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void sendSDcardCredentials() {

        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {

            Master master = Master.listAll(Master.class).get(0);

            String cardIDSystem;
            try {
                cardIDSystem = getSDCARDiD();
            } catch (IOException e) {
                e.printStackTrace();
                cardIDSystem = "";
            }
            if (cardIDSystem == null) {
                cardIDSystem = "";
            }

            String userId = SettingPreffrences.getContinueAsGuest(this) ? "0" : SettingPreffrences.getUserid(this);

            String imei = "";

            if (checkPhoneStatePermission()) {
//                TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context
// .TELEPHONY_SERVICE);
//                imei = telephonyManager.getDeviceId();
                imei = getUniqueDeviceId();
            }

            try {
                params = "os=" + URLEncoder.encode(OS, "UTF-8") + "&make=" + URLEncoder.encode(MAKE, "UTF-8") + "&model=" + URLEncoder.encode(MODEL, "UTF-8") + "&imei=" + URLEncoder.encode(imei, "UTF-8") + "&userId=" + URLEncoder.encode(userId, "UTF-8") + "&activationCode=" + URLEncoder.encode(master.getActivation_code(), "UTF-8") + "&sidExcel=" + URLEncoder.encode(master.getSid(), "UTF-8") + "&sidAndroid=" + URLEncoder.encode(cardIDSystem, "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.host + NetworkUrl.activation_code_url, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {
                       // Log.e(TAG, "getResponse: " + response);
                    }
                }, "POST");

                asyncTaskHelper.execute();


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }
    }

    public String getUniqueDeviceId() {

        String deviceId = "";

        try {

            TelephonyManager mTelephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

            if (mTelephony.getDeviceId() != null) {

                deviceId = mTelephony.getDeviceId();

            } else {

                deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
            }
        } catch (Exception e) {
            e.printStackTrace();
            deviceId = "";
        }

        return deviceId;
    }

    public void showRetryCancelDialog() {
        alert("Internet Connection not available", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
    }

    public List<Subject> getMediumsFromFolderNames(String path) {
        List<Subject> mediumsFromFolderNames = new ArrayList<>();

        for (int i = 0; i < new File(path).listFiles().length; i++) {
            subject = new Subject();
            File file = new File(path).listFiles()[i];
            subject.setChapterName(file.getName());
            subject.setFolderName(file.getName());
            subject.setFontName("english");
            mediumsFromFolderNames.add(subject);
        }
        return mediumsFromFolderNames;
    }

    public List<Subject> getMediumsFromCourseNameExcelSheet(String path) {
        List<Subject> mediumsFromCourseNameExcelSheet = new ArrayList<>();

        try {
            Workbook workbook = Workbook.getWorkbook(new File(path + COURSE_NAME_EXCEL));
            Sheet sheet = workbook.getSheets()[0];
            for (int i = 1; i < sheet.getRows(); i++) {
                Cell[] cells = sheet.getRow(i);
                subject = new Subject();
                subject.setChapterName(cells[1].getContents().trim());
                subject.setFolderName(cells[3].getContents().trim());
                subject.setFontName(cells[2].getContents().trim());
                mediumsFromCourseNameExcelSheet.add(subject);
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Something went wrong while loading Multiple " + "Standards", Toast.LENGTH_LONG).show();
        }

        return mediumsFromCourseNameExcelSheet;
    }

    private Typeface getTypeFace(String fontName) {
        Typeface typeface = null;
        switch (fontName) {
            case ENGLISH_FONT:
                if (englishTypeface == null) {
                    englishTypeface = Typeface.createFromAsset(this.getAssets(), "fonts/" + "Brandon_bld.otf");
                }
                typeface = englishTypeface;
                break;
            case HINDI_FONT:
                if (hindiTypeface == null) {
                    hindiTypeface = Typeface.createFromAsset(this.getAssets(), "fonts/" + "k010.ttf");
                }
                typeface = hindiTypeface;
                break;
            case MARATHI_FONT:
                if (marathiTypeface == null) {
                    marathiTypeface = Typeface.createFromAsset(this.getAssets(), "fonts/" + "SUBAK0.TTF");
                }
                typeface = marathiTypeface;
                break;
            case URDU_FONT:
                if (urduTypeface == null) {
                    urduTypeface = Typeface.createFromAsset(this.getAssets(), "fonts/" + "JameelNooriNastaleeq.TTF");
                }
                typeface = urduTypeface;
                break;
        }

        return typeface;
    }

    private void setFont(String fontName, TextView textView) {

        if (fontName != null) {

            if (fontName.equals(ENGLISH_FONT)) {
                textView.setTypeface(getTypeFace(ENGLISH_FONT));
            }

            if (fontName.equals(HINDI_FONT)) {
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.content_font_name));
                textView.setTypeface(getTypeFace(HINDI_FONT));
            }

            if (fontName.equals(MARATHI_FONT)) {
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.content_font_name));
                textView.setTypeface(getTypeFace(MARATHI_FONT));
            }

            if (fontName.equals(URDU_FONT)) {
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.content_font_name));
                textView.setTypeface(getTypeFace(URDU_FONT));
            }
        }
    }

    private class MediumAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


        Typeface bold, reg;
        ArrayList<Subject> subjects;
        private int[] bg = new int[]{R.drawable.one, R.drawable.two, R.drawable.three, R.drawable.four, R.drawable.five, R.drawable.six, R.drawable.seven, R.drawable.eight, R.drawable.nine};


        public MediumAdapter(Typeface bold, Typeface reg, ArrayList<Subject> subjects) {
            this.bold = bold;
            this.reg = reg;
            this.subjects = subjects;

        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.itemView.setBackgroundResource(getBackgroundResId(position));
            viewHolder.tvStandardMedium.setText(subjects.get(position).getChapterName());
            setFont(subjects.get(position).getFontName(), viewHolder.tvStandardMedium);
        }

        /* providing random backgrounds for multiple standards */
        private int getBackgroundResId(int position) {
            int bgPos = position % 9;
//			if (position < 9) {
//				return bg[position];
//			} else {
//				return bg[position - 9];
//			}
            return bg[bgPos];
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_std_and_medium, parent, false);

            return new ViewHolder(view);
        }

        @Override
        public int getItemCount() {
            return subjects.size();
        }


        private class ViewHolder extends RecyclerView.ViewHolder {

            TextView tvStandardMedium;


            public ViewHolder(View itemView) {
                super(itemView);
                tvStandardMedium = (TextView) itemView.findViewById(R.id.tv_standard_medium);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemClicked();
                    }
                });
            }

            private void onItemClicked() {
                subject = MediumAdapter.this.subjects.get(getLayoutPosition());
                if (SettingPreffrences.getLoginDone(getApplicationContext())) {
                    hide_multiple_std_options_view();
                    checkForSDCard();
                } else {
                    if (SettingPreffrences.getContinueAsGuest(getApplicationContext()) && !CommonFunctions.isNetworkConnected(getApplicationContext())) {
                        hide_multiple_std_options_view();
                        checkForSDCard();
                    } else {
                        hide_multiple_std_options_view_show_main_login();
                    }
                }

            }
        }
    }


}

class StorageUtils {

    private static final String TAG = "StorageUtils";

    public static List<StorageInfo> getStorageList() {

        List<StorageInfo> list = new ArrayList<StorageInfo>();
        String def_path = Environment.getExternalStorageDirectory().getPath();
        boolean def_path_internal = !Environment.isExternalStorageRemovable();
        String def_path_state = Environment.getExternalStorageState();
        boolean def_path_available = def_path_state.equals(Environment.MEDIA_MOUNTED) || def_path_state.equals(Environment.MEDIA_MOUNTED_READ_ONLY);
        boolean def_path_readonly = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY);
        BufferedReader buf_reader = null;
        try {
            HashSet<String> paths = new HashSet<String>();
            buf_reader = new BufferedReader(new FileReader("/proc/mounts"));
            String line;
            int cur_display_number = 1;
            //Log.d(TAG, "/proc/mounts");
            while ((line = buf_reader.readLine()) != null) {
                //Log.d(TAG, line);
                if (line.contains("vfat") || line.contains("/mnt")) {
                    StringTokenizer tokens = new StringTokenizer(line, " ");
                    String unused = tokens.nextToken(); //device
                    String mount_point = tokens.nextToken(); //mount point
                    if (paths.contains(mount_point)) {
                        continue;
                    }
                    unused = tokens.nextToken(); //file system
                    List<String> flags = Arrays.asList(tokens.nextToken().split(",")); //flags
                    boolean readonly = flags.contains("ro");

                    if (mount_point.equals(def_path)) {
                        paths.add(def_path);
                        list.add(0, new StorageInfo(def_path, def_path_internal, readonly, -1));
                    } else if (line.contains("/dev/block/vold")) {
                        if (!line.contains("/mnt/secure") && !line.contains("/mnt/asec") && !line.contains("/mnt/obb") && !line.contains("/dev/mapper") && !line.contains("tmpfs")) {
                            paths.add(mount_point);
                            list.add(new StorageInfo(mount_point, false, readonly, cur_display_number++));
                        }
                    }
                }
            }

            if (!paths.contains(def_path) && def_path_available) {
                list.add(0, new StorageInfo(def_path, def_path_internal, def_path_readonly, -1));
            }

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (buf_reader != null) {
                try {
                    buf_reader.close();
                } catch (IOException ex) {
                }
            }
        }
        return list;
    }

    public static class StorageInfo {

        public final String path;
        public final boolean internal;
        public final boolean readonly;
        public final int display_number;

        StorageInfo(String path, boolean internal, boolean readonly, int display_number) {
            this.path = path;
            this.internal = internal;
            this.readonly = readonly;
            this.display_number = display_number;
        }

        public String getDisplayName() {
            StringBuilder res = new StringBuilder();
            if (internal) {
                res.append("Internal SD card");
            } else if (display_number > 1) {
                res.append("SD card " + display_number);
            } else {
                res.append("SD card");
            }
            if (readonly) {
                res.append(" (Read only)");
            }
            return res.toString();
        }
    }
}
