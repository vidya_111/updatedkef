package com.observatory.kefcorner;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.BasicNameValuePair;
import com.koushikdutta.ion.Ion;
import com.observatory.adapters.DemoContentAdapter;
import com.observatory.database.Subject;
import com.observatory.utils.App;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.SettingPreffrences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static com.observatory.kefcorner.R.id.et_search;

public class SearchYouTubeActivity extends AppCompatActivity {

    private ImageView ivBack;
    private EditText etSearch;
    private ListView lstChapter;
    private Button btReport;
    private RelativeLayout rlPb;
    private ProgressBar pbMain;
    private RelativeLayout rlNoInternet;
    private ImageView ivErr;
    private TextView tvErr;
    private TextView tvNoVid;
    private TextView tvSearch;

    private ArrayList<Subject> subjects;
    private HashMap<String, String> dimensions;
    private boolean isLoading = false;

    private String PAGE_TOKEN = "";
    private String TAG = "DemoContent";
    public String sundaram_eclass_youtube_videos_search = "";

    public String page_tok_txt = "&pageToken=";

    public String searchText = "";

    private DemoContentAdapter demoContentAdapter;
    private Future<String> futureNetworkCall;

    Handler handler;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_you_tube);

        byte[] playlist = new byte[0];
        byte[] content = new byte[0];
        String key = "", play = "";
        try {
            playlist = Base64.decode(BuildConfig.PLAY_LIST_ID, Base64.DEFAULT);
            content = Base64.decode(BuildConfig.DEMO_CONTENT_KEY, Base64.DEFAULT);
            key = new String(content, "UTF-8");
            play = new String(playlist, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        sundaram_eclass_youtube_videos_search = "https://www.googleapis.com/youtube/v3/search?" +
                "part=snippet&forUsername=sundarameclass&playlistId="+ play +"&" +
                "key="+ key +"&" +
                "fields=kind,nextPageToken,pageInfo(totalResults,resultsPerPage),items(kind,id(kind,videoId)," +
                "snippet(channelId,title,description,thumbnails(high(url,width,height)),channelTitle))&" +
                "channelId=UCkJOKn8-_k09cFJW5LcVR3w&maxResults=50&q=";
        findViews();
    }

    private void findViews() {

        handler = new Handler();
        ivBack = (ImageView) findViewById(R.id.imageView_back_header_layout);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        lstChapter = (ListView) findViewById(R.id.lstChapter);


        etSearch = (EditText) findViewById(et_search);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (tvSearch.getText().toString().trim().isEmpty()) {
                    tvSearch.setVisibility(View.GONE);
                } else {
                    tvSearch.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (tvSearch.getText().toString().trim().isEmpty()) {
                    tvSearch.setVisibility(View.GONE);
                } else {
                    tvSearch.setVisibility(View.VISIBLE);
                }
            }
        });


        context = this;

        btReport = (Button)

                findViewById(R.id.button2);
        btReport.setVisibility(View.GONE);

        rlPb = (RelativeLayout)

                findViewById(R.id.rl_progress);
        rlPb.setVisibility(View.GONE);

        pbMain = (ProgressBar)

                findViewById(R.id.pb_demo_youtube_content);
        pbMain.setVisibility(View.GONE);

        rlNoInternet = (RelativeLayout)

                findViewById(R.id.rl_no_internet);
        rlNoInternet.setVisibility(View.GONE);

        ivErr = (ImageView)

                findViewById(R.id.iv_err);

        tvErr = (TextView)

                findViewById(R.id.tv_err);

        tvNoVid = (TextView)

                findViewById(R.id.tv_no_vid);
        tvNoVid.setVisibility(View.GONE);

        tvSearch = (TextView) findViewById(R.id.tv_search);
        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search();
            }
        });

        etSearch.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            search();
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

        subjects = new ArrayList<>();

        lstChapter.setOnItemClickListener(new AdapterView.OnItemClickListener()

        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    App.getInstance().trackEvent("Demo Content", "Screen", "name - " + subjects.get(position).getChapterName() + " ; category - CONTENT", "");
                } else if (position == 1) {

                    dimensions = new HashMap<>();
                    dimensions.put("userId", SettingPreffrences.getUserid(context));
                    dimensions.put("category", "MIND MAP");
                    dimensions.put("name", subjects.get(position).getChapterName());
                    CommonFunctions.sendAnalytics("videoDemoContent", dimensions);

                    App.getInstance().trackEvent("Demo Content", "Screen", "name - " + subjects.get(position).getChapterName() + " ; category - MIND-MAP", "");

                } else {

                    App.getInstance().trackEvent("Demo Content", "Screen", "name - " + subjects.get(position).getChapterName() + " ; category - Q&A", "");

                }

                Intent intent = new Intent(context, YoutubeView.class);
                intent.putExtra("id", subjects.get(position).getFolderName());
                startActivity(intent);


            }
        });

        lstChapter.setOnScrollListener(new AbsListView.OnScrollListener()

        {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {

                int lPos = view.getLastVisiblePosition();

                if ((lPos >= (subjects.size() - 14)) && !isLoading && !PAGE_TOKEN.isEmpty()) {
                    Log.e(TAG, "onScroll: page token : - " + PAGE_TOKEN);
                    isLoading = true;
                    getYouTubeVideos();
                }

            }
        });

    }

    private void search() {
        subjects.clear();
        if (demoContentAdapter != null) {
            demoContentAdapter.notifyDataSetChanged();
        }

        PAGE_TOKEN = "";
        getYouTubeVideos();
    }

    private void getYouTubeVideos() {

        if (CommonFunctions.isNetworkConnected(context)) {


            String[] searchTextArr = etSearch.getText().toString().trim().split(" ");

            Log.e(TAG, "getYouTubeVideos: " + Arrays.toString(searchTextArr));

            StringBuilder srchA = new StringBuilder("");

            for (String st : searchTextArr) {
                if (srchA.length() == 0) {
                    srchA.append(st);
                } else {
                    srchA.append("%20");
                    srchA.append(st);
                }
            }

            Log.e(TAG, "getYouTubeVideos: " + srchA);

            searchText = srchA.toString();

            hideErrorMsg();

            if (PAGE_TOKEN.isEmpty()) {
                tvNoVid.setVisibility(View.GONE);
                pbMain.setVisibility(View.VISIBLE);
            } else {
                rlPb.setVisibility(View.VISIBLE);
            }

            BasicNameValuePair[] headerParams = {new BasicNameValuePair("Content-Type", "application/json")};

//            params = new JsonObject();
//            params.addProperty("part", "snippet");
//            params.addProperty("playlistId", PLAY_LIST_ID);
//            params.addProperty("key", KEY);
//            params.addProperty("maxResults", MAX_RESULTS);
//            params.addProperty("fields", FIELDS);
//            params.addProperty("pageToken", PAGE_TOKEN);

            futureNetworkCall = Ion.with(context)
                    .load("GET", sundaram_eclass_youtube_videos_search + searchText + page_tok_txt + PAGE_TOKEN)
                    .setHeader(headerParams)
                    .setTimeout(16000)
                    .setLogging("SundaramEclassRetail", Log.DEBUG)
//                    .setJsonObjectBody(params)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            parseVideoResponse(e, result);
                        }
                    });

        } else {

            // no internet
            isLoading = false;
            pbMain.setVisibility(View.GONE);
            rlPb.setVisibility(View.GONE);
            if (subjects.isEmpty()) {
                showErrorMsg(R.drawable.ic_no_internet, "No internet connection, tap to retry");
            }
        }

    }

    private void parseVideoResponse(Exception e, String response) {
        Log.e(TAG, "parseVideoResponse: --> " + response);

        if (e != null) {

            pbMain.setVisibility(View.GONE);
            rlPb.setVisibility(View.GONE);
            if (subjects.isEmpty()) {
                showErrorMsg(R.drawable.ic_no_internet, "No internet connection, tap to retry");
            }
        } else {

            tvSearch.setVisibility(View.GONE);

            try {
                JSONObject jsonObject = new JSONObject(response);

                if (jsonObject.has("items")) {
                    JSONArray vidArr = jsonObject.getJSONArray("items");
                    int len = vidArr.length();
                    for (int i = 0; i < len; ++i) {
                        JSONObject snipObj = vidArr.getJSONObject(i).getJSONObject("snippet");
                        JSONObject id = vidArr.getJSONObject(i).getJSONObject("id");
                        if (id.has("videoId")) {
                            Subject subject = new Subject();
                            subject.setChapterName(snipObj.getString("title"));
                            subject.setFolderName(id.getString("videoId"));
                            subjects.add(subject);
                        }
                    }

                    if (len == 0) {
                        if (PAGE_TOKEN.isEmpty()) {
                            tvNoVid.setVisibility(View.VISIBLE);
                        }
                    } else {
                        tvNoVid.setVisibility(View.GONE);
                    }

                    if (PAGE_TOKEN.isEmpty()) {
                        pbMain.setVisibility(View.GONE);
                        hideErrorMsg();
                        if (demoContentAdapter != null) {
                            demoContentAdapter.notifyDataSetChanged();
                        } else {
                            demoContentAdapter = new DemoContentAdapter(this, subjects);
                            lstChapter.setAdapter(demoContentAdapter);
                        }
                    } else {
                        rlPb.setVisibility(View.GONE);
                        if (demoContentAdapter != null) {
                            demoContentAdapter.notifyDataSetChanged();
                        }
                    }

                    if (jsonObject.has("nextPageToken")) {
                        PAGE_TOKEN = jsonObject.getString("nextPageToken");
                    } else {
                        PAGE_TOKEN = "";
                    }

                    isLoading = false;

                } else if (jsonObject.has("error")) {

                    if (subjects.isEmpty()) {
                        showErrorMsg(R.drawable.ic_something_wrong, jsonObject.getJSONObject("error").getString("message"));
                    }
                    isLoading = false;
                }


            } catch (JSONException e1) {
                if (subjects.isEmpty()) {
//                    showErrorMsg(R.drawable.ic_something_wrong, "Something went wrong, please try again later");
                }
                isLoading = false;
            }


        }

    }

    public void showErrorMsg(int imgId, String msg) {
        pbMain.setVisibility(View.GONE);
        lstChapter.setVisibility(View.GONE);
        rlNoInternet.setVisibility(View.VISIBLE);
        tvErr.setText(msg);
        ivErr.setImageResource(imgId);
        if (imgId == R.drawable.ic_no_internet) {
            rlNoInternet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getYouTubeVideos();
                }
            });
        } else {
            rlNoInternet.setOnClickListener(null);
        }
    }

    public void hideErrorMsg() {
        lstChapter.setVisibility(View.VISIBLE);
        rlNoInternet.setVisibility(View.GONE);
    }


}
