package com.observatory.kefcorner;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.observatory.adapters.DHMessagesAdapter;
import com.observatory.adapters.MyRecycleViewClickListener;
import com.observatory.utils.BaseActivity;

import java.util.ArrayList;

public class DoubtHistory extends BaseActivity {

    Spinner DH_subject_spinner;
    String subjectData = "";
    ArrayList<String> subject_spinner_list = new ArrayList<>();
    RecyclerView DH_message_recyclerview;
    DHMessagesAdapter messagesAdapter;
    int count;
    String getQuestion = "", getAnswer = "", getTime = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doubt_history);
        actionBar.setTitle("Doubt History");

        getQuestion = getIntent().getExtras().getString("question");
        getAnswer = getIntent().getExtras().getString("answer");
        getTime = getIntent().getExtras().getString("time");
        count = getIntent().getIntExtra("count", 0);

        //subject spinner
        subjectData = "Select Subject,"+ Subjects.getAllSubjects;
        String[] splitData = subjectData.split(",");

        for (int i=0 ; i<splitData.length; i++){
            subject_spinner_list.add(splitData[i]);
        }

        ArrayAdapter spinner_adapter = new ArrayAdapter(getApplicationContext(),
                android.R.layout.simple_spinner_item,subject_spinner_list);
        spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        DH_subject_spinner.setAdapter(spinner_adapter);

        //message recyclerView
        messagesAdapter =new DHMessagesAdapter(getApplicationContext(), new String[]{getQuestion}, new String[]{getAnswer}, new String[]{getTime}, count);
        DH_message_recyclerview.setHasFixedSize(true);
        DH_message_recyclerview.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        DH_message_recyclerview.setAdapter(messagesAdapter);

        DH_message_recyclerview.addOnItemTouchListener(new MyRecycleViewClickListener(getApplicationContext(), new MyRecycleViewClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                showPopUp(view);
            }
        }));
    }

    @Override
    protected void onLayout() {
        DH_subject_spinner = findViewById(R.id.DH_subject_spinner);
        DH_message_recyclerview = findViewById(R.id.DH_message_spinner);
    }

    public void showPopUp(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = getLayoutInflater();

        View customView = layoutInflater.inflate(R.layout.question_answer_popup, null);

        TextView question = (TextView) customView.findViewById(R.id.DH_question_tv_popup);
        TextView answer = (TextView) customView.findViewById(R.id.DH_answer_tv_popup);
        TextView time = (TextView) customView.findViewById(R.id.DH_time_tv_popup);

        question.setText(getQuestion);
        answer.setText(getAnswer);
        time.setText(getTime);

        builder.setView(customView);
        builder.create();
        builder.show();

    }
}