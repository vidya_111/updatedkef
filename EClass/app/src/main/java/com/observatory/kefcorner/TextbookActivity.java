package com.observatory.kefcorner;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.observatory.adapters.CourseBookViewHolder;
import com.observatory.adapters.SimpleAdapter;
import com.observatory.database.Textbook;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.EmailHandler;
import com.orm.query.Select;

import java.io.File;
import java.util.ArrayList;

public class TextbookActivity extends BaseActivity {
    RecyclerView rvMyCourses;
    SimpleAdapter adapter;
    RelativeLayout no_data_layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_textbook);
    }

    @Override
    protected void onLayout() {
        actionBar.setTitle("Textbook");
        rvMyCourses = findViewById(R.id.rv_my_courses);
        no_data_layout = findViewById(R.id.no_data_layout);
        getSubjectList();
    }

    private void getSubjectList(){
        String whereClause = "SUBJECT_ID != ''";

        ArrayList<Textbook> bookArray = (ArrayList<Textbook>) Select.from(Textbook.class).list();

        Log.e("Textbook",  "bookArray = "+bookArray.size());

        if (bookArray.size() > 0){
            rvMyCourses.setVisibility(View.VISIBLE);
            adapter = new SimpleAdapter(R.layout.l_single_my_course_book,bookArray , TextbookActivity.this, CourseBookViewHolder.class);
            rvMyCourses.setAdapter(adapter);
        }else {
            no_data_layout.setVisibility(View.VISIBLE);
            rvMyCourses.setVisibility(View.GONE);
        }
    }

    public void openPdf(String filePath) {
        try {
            File file = new File(filePath);
            Uri fileUri=null;
            //srj
            if (file.exists()) {
                //new EmailHandler().sendMail("phone@millicent.in", "PDF file exist condition : ", "true");

                Intent target = new Intent(Intent.ACTION_VIEW);
                if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT){
                    fileUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", file);
                } else{
                    fileUri = Uri.fromFile(file.getAbsoluteFile());
                }
                target.setDataAndType(fileUri, "application/pdf");
                target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                target.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                //new EmailHandler().sendMail("phone@millicent.in", "Pdf Before createChooser :  ", fileUri.getEncodedPath());
                Intent intent = Intent.createChooser(target, "Open File");
                try {
                    //new EmailHandler().sendMail("phone@millicent.in", "Pdf Before Intent :  ", "true");
                    startActivity(intent);
                    //new EmailHandler().sendMail("phone@millicent.in", "Pdf After Intent :", "true");
                } catch (ActivityNotFoundException e) {
                    // new EmailHandler().sendMail("phone@millicent.in", "Pdf Catch Exception", e.getMessage());
                    Toast.makeText(getApplicationContext(), "Please install a PDF Viewer to view this " + "document", Toast.LENGTH_LONG).show();
                }
            } else {
                //new EmailHandler().sendMail("phone@millicent.in", "File not available  ", "true");
                Toast.makeText(this, "File not available", Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception ex)
        {
            new EmailHandler().sendMail("phone@millicent.in", "OpenPdf Method Exception", ex.getMessage());

        }
    }

/*    @Override
    public void onBackPressed() {

        Intent intent = new Intent(getApplicationContext(), Subjects.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();

        super.onBackPressed();
    }*/

}