package com.observatory.kefcorner;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.common.collect.Multimap;
import com.observatory.Assessment.ConstantManager;
import com.observatory.Assessment.Model.CourseModel;
import com.observatory.AsyncTaskMain.AsyncTaskHelper;
import com.observatory.AsyncTaskMain.OnTaskComplete;
import com.observatory.database.Master;
import com.observatory.model.SchoolModel;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.Constants;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Anuj on 05-01-2016.
 */
public class SignUp extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "SignUp";
    private static final int PERMISSION_SIGNUP_CODE = 100;
    String user_type = "", courseDetailParam = "";
    private EditText etFName;
    private EditText etLName;
    private EditText etMobile;
    private EditText etEmail;
    private EditText etPassword;
    private TextView etStandard;
    private TextView etMedium;
    private TextView etSchool;
    //private EditText etSchool;
    private Spinner userTypeSpinner;
    ArrayList<String> userType_Spinner_list = new ArrayList<>();
    private ArrayList<String> school_list = new ArrayList<>();
    ArrayList<String> standard_list = new ArrayList<>();
    ArrayList<Integer> course_id_list = new ArrayList<>();
    ArrayAdapter spinnerAdapter;
    private TextView etState;
    private TextView etCity;

    private ImageView ivFacebook;
    JSONArray mainJsonArr = new JSONArray();
    private TextView tvSubmit;

    /*Facebook*/
    private CallbackManager callbackManager;


    /*Webservice*/
    private AsyncTaskHelper asyncTaskHelper;
    private String params;

    /*Json*/
    private JSONObject jsonObject;
    private JSONArray jsonArray;
    private String status;
    private String msg;
    SchoolModel model = new SchoolModel();
    CourseModel courseModel = new CourseModel();
    int selectedProductPosition;
    Master master;
    private Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);
    }

    @Override
    protected void onLayout() {

        actionBar.setTitle("Sign Up");

        callbackManager = CallbackManager.Factory.create();

        etFName = (EditText) findViewById(R.id.et_s_fname);
        etLName = (EditText) findViewById(R.id.et_s_lname);
        etMobile = (EditText) findViewById(R.id.et_s_mobile);
        etEmail = (EditText) findViewById(R.id.et_s_email);
        etPassword = (EditText) findViewById(R.id.et_s_password);
        etStandard = (TextView) findViewById(R.id.et_s_standard);
        etMedium = (TextView) findViewById(R.id.et_s_medium);
        etSchool = findViewById(R.id.et_s_school);
        etState = (TextView) findViewById(R.id.et_s_state);
        etCity = (TextView) findViewById(R.id.et_s_city);
        etStandard.setText("Select Standard");
        etMedium.setText("Select Medium");
        etState.setText("Select State");
        etCity.setText("Select City");
        etSchool.setText("Select School");

        userType_Spinner_list.add("Select User Type");
        userType_Spinner_list.add("Teacher");
        userType_Spinner_list.add("Student");

        userTypeSpinner = findViewById(R.id.user_type_spinner);

        ArrayAdapter spinner_adapter = new ArrayAdapter(getApplicationContext(),
                android.R.layout.simple_spinner_item, userType_Spinner_list);
        spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        userTypeSpinner.setAdapter(spinner_adapter);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            selectedProductPosition = bundle.getInt("selectedProductPosition");
            master = Master.listAll(Master.class).get(0);
            Log.d("master_data", "sign up: " + master.getStd() + " :: " + master.getMedium() + " Course_Id: " + master.getCourse_id());
        }

        Log.d("Sign_up", "getData: Standard: " + ConstantManager.Standard_Name);

        ivFacebook = (ImageView) findViewById(R.id.iv_facebook);

        tvSubmit = (TextView) findViewById(R.id.tv_s_submit);

        tvSubmit.setOnClickListener(this);
        ivFacebook.setOnClickListener(this);
        getSchoolData(1);
        setUpSpinner();

    }

    private void getSchoolData(int instituteID) {
        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {

            CommonFunctions.showDialog(this);
            try {

                params = "instituteID=" + URLEncoder.encode(String.valueOf(instituteID), "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.KEF_Host + NetworkUrl.KEF_GetSchool, params, new OnTaskComplete() {

                    @Override
                    public void getResponse(String response) {
                        CommonFunctions.hideDialog();
                        Log.d("getSchoolData", "Response: " + response);
                        if (!response.equals("error")) {
                            getSchoolResponse(response);
                        } else {
                            alert("Something went, please try again later");
                            CommonFunctions.hideDialog();
                        }

                    }
                }, "GET");

                asyncTaskHelper.execute();

            } catch (Exception e) {
                Log.e("Error: GetSchoolData: ", e.getMessage());
            }

        } else {
            alert("Internet Connection not available");
        }

    }

    private void getSchoolResponse(String response) {
        try {

            List<String> values = new ArrayList<>();

            JSONArray res = new JSONArray(response);

            for (int i = 0; i < res.length(); i++) {

                jsonObject = res.getJSONObject(i);

                model.setText(jsonObject.getString("Text"));
                values.add(jsonObject.getString("Value"));

                Log.d("schoolData", "Value: " + jsonObject.getString("Text"));

                school_list.add(jsonObject.getString("Text"));

            }
            model.setValue(values);
            Log.d("schoolData", "Value: " + Arrays.toString(new ArrayList[]{school_list}));


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getCourse(String schoolID) {
        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {

            CommonFunctions.showDialog(this);
            try {

                params = "schoolID=" + URLEncoder.encode(String.valueOf(schoolID), "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.KEF_Host + NetworkUrl.KEF_GetCourse, params, new OnTaskComplete() {

                    @Override
                    public void getResponse(String response) {
                        CommonFunctions.hideDialog();
                        Log.d("getDataCourse", "Response: " + response);
                        if (!response.equals("error")) {
                            getCourseResponse(response);
                        } else {
                            alert("Something went wrong, please try again later");
                            CommonFunctions.hideDialog();
                        }

                    }
                }, "GET");

                asyncTaskHelper.execute();

            } catch (Exception e) {
                Log.e("Error: GetSchoolData: ", e.getMessage());
            }

        } else {
            alert("Internet Connection not available");
        }
    }

    private void getCourseResponse(String response) {
        try {
            standard_list.clear();
            course_id_list.clear();
            List<String> values = new ArrayList<>();
            JSONArray res = new JSONArray(response);

            SchoolModel model = new SchoolModel();
            for (int i = 0; i < res.length(); i++) {

                jsonObject = res.getJSONObject(i);

                JSONObject jsonObject1 = new JSONObject();

                jsonObject1.put("course_id", jsonObject.getString("Value"));
                mainJsonArr.put(jsonObject1);

                model.setText(jsonObject.getString("Text"));
                values.add(jsonObject.getString("Value"));
                Log.d("schoolData", "Value: " + jsonObject.getString("Text") + " :: " + jsonObject.getString("Value"));

                standard_list.add(jsonObject.getString("Text"));
                course_id_list.add(Integer.parseInt(jsonObject.getString("Value")));

            }
            model.setValue(values);


            if (course_id_list.contains(master.getCourse_id())) {
                Master master = Master.listAll(Master.class).get(0);
                ConstantManager.Standard_Name = master.getMedium();
                Log.d("sign_up", "inIf");
                etStandard.setText(String.valueOf(master.getStd()));
                etStandard.setTextColor(getResources().getColor(R.color.black));
                standard = String.valueOf(master.getStd());
                etMedium.setText(ConstantManager.Standard_Name);
                etMedium.setTextColor(getResources().getColor(R.color.black));
                medium = ConstantManager.Standard_Name;
            } else {
                etStandard.setText("");
                etMedium.setText("");
            }

            Log.d("schoolData", "Value: " + Arrays.toString(new ArrayList[]{standard_list}) + " :: " + Arrays.toString(new List[]{course_id_list}));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    boolean isEmpty(EditText input) {
        return TextUtils.isEmpty(input.getText().toString());
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {

            case R.id.tv_s_submit:

                if (isEmpty(etFName) || isEmpty(etLName) || isEmpty(etMobile) || isEmpty(etEmail) || isEmpty(etPassword)
                    /* || isEmpty(etSchool)*/) {
                    alert("Fields cannot be empty");
                } else if (etFName.getText().toString().length() < 2) {
                    alert("First name cannot be less then 2 characters");
                } else if (etLName.getText().toString().length() < 2) {
                    alert("Last name cannot be less then 2 characters");
                } else if (etEmail.getText().toString().length() < 6) {
                    alert("Email-Id cannot be less then 6 characters");
                } else if (!CommonFunctions.isValidEmail(etEmail.getText().toString())) {
                    alert("Please enter valid Email-Id");
                } else if (etPassword.getText().toString().length() < 6) {
                    alert("Password cannot be less then 6 characters");
                } else if (etMobile.getText().toString().length() < 10) {
                    alert("Invalid Mobile Number");
                } else if (stateCode == -1) {
                    alert("Please select state");
                } else if (cityCode == -1) {
                    alert("Please select city");
                } else if (standard == null || standard.isEmpty()) {
                    alert("Please select standard");
                } else if (medium == null || medium.isEmpty()) {
                    alert("Please select medium");
                } else if (userTypeSpinner.getSelectedItem().toString().equals("Select User Type")) {
                    alert("Please select user type");
                } else {
                    //callSignUpWS();

                    if (userTypeSpinner.getSelectedItem().toString().equals("Teacher")) {
                        user_type = "TR";
                    } else if (userTypeSpinner.getSelectedItem().toString().equals("Student")) {
                        user_type = "ST";
                    }

                    if (user_type.equals("ST")) {
                        courseDetailParam = getParamData(String.valueOf(master.getCourse_id()));
                    } else if (user_type.equals("TR")) {
                        courseDetailParam = mainJsonArr.toString();
                    }

                    userRegistration(Build.VERSION.RELEASE, Build.MANUFACTURER, Build.MODEL,
                            etFName.getText().toString(), etLName.getText().toString(), etEmail.getText().toString(),
                            etPassword.getText().toString(), etMobile.getText().toString(), selectedState,
                            selectedCity, school, medium, standard,
                            user_type, courseDetailParam);

                }

                break;

            case R.id.iv_facebook:
                if (CommonFunctions.isNetworkConnected(getApplicationContext())) {
                    loginWithFacebook();
                } else {
                    alert("Internet Connection not available");
                }
                break;
        }

    }

    public String getParamData(String courseID) {
        JSONArray mainJsonArr1 = new JSONArray();
        try {

            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("course_id", courseID);
            mainJsonArr1.put(jsonObject1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJsonArr1.toString();
    }

    private void userRegistration(String os, String make, String model, String firstName, String lastName, String email,
                                  String password, String mobile, String state, String city, String School, String medium,
                                  String standard, String userType, String courseDetails) {

        Log.d("SignUp", "params:" + " os: " + os + " make: " + make + " model: " + model + " fName: " + firstName + " lNAme: " + lastName
                + " email: " + email + " password: " + password + " mobile:" + mobile + " state: " + state + " city: " + city + " school: " + school
                + " medium: " + medium + " standard: " + standard + " userType: " + userType + " courseDetails: " + courseDetails);

        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {

            CommonFunctions.showDialog(this);

            try {
                params = "os=" + URLEncoder.encode(os, "UTF-8")
                        + "&make=" + URLEncoder.encode(make, "UTF-8")
                        + "&model=" + URLEncoder.encode(model, "UTF-8")
                        + "&firstName=" + URLEncoder.encode(firstName, "UTF-8")
                        + "&lastName=" + URLEncoder.encode(lastName, "UTF-8")
                        + "&email=" + URLEncoder.encode(email, "UTF-8")
                        + "&password=" + URLEncoder.encode(password, "UTF-8")
                        + "&mobile=" + URLEncoder.encode(mobile, "UTF-8")
                        + "&medium=" + URLEncoder.encode(medium, "UTF-8")
                        + "&standard=" + URLEncoder.encode(standard, "UTF-8")
                        + "&city=" + URLEncoder.encode(city, "UTF-8")
                        + "&state=" + URLEncoder.encode(state, "UTF-8")
                        + "&school=" + URLEncoder.encode(School, "UTF-8")
                        + "&user_type=" + URLEncoder.encode(userType, "UTF-8")
                        + "&courseDetails=" + URLEncoder.encode(courseDetails, "UTF-8")
                        + "&schoolID=" + URLEncoder.encode(ConstantManager.School_Id, "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.KEF_Localhost + NetworkUrl.KEF_SIgnUp, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {
                        if (!response.equals("error")) {
                            CommonFunctions.hideDialog();
                            Log.d("Sign_up", "api_done: " + response);
                            parseSignUpJson(response);
                        } else {
                            //error
                            alert("Something went, please try again later");
                            CommonFunctions.hideDialog();
                        }
                    }
                }, "POST");

                asyncTaskHelper.execute();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //no internet
            alert("Internet Connection not available");
        }
    }

    private void callSignUpWS() {

        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {

            CommonFunctions.showDialog(this);

            String rel = Build.VERSION.RELEASE;
            String man = Build.MANUFACTURER;
            String mod = Build.MODEL;
            if (cityCode == -1) {
                cityCode = 0;
            }
            if (stateCode == -1) {
                stateCode = 0;
            }


            if (userTypeSpinner.getSelectedItem().toString().equals("Teacher")) {
                user_type = "TR";
            } else if (userTypeSpinner.getSelectedItem().toString().equals("Student")) {
                user_type = "ST";
            }

            Log.d("SignUp_Api", "os: " + rel + " make: " + man + " model: " + mod + " firstName: " + etFName.getText().toString()
                    + " lastName: " + etLName.getText().toString() + " email: " + etEmail.getText().toString()
                    + " password: " + etPassword.getText().toString() + " mobile: " + etMobile.getText().toString()
                    + " state: " + selectedState + " city: " + selectedCity + " School: " + school + " std: " + standard + " medium: " + medium
                    + " user_type: " + user_type + " SchoolId: " + ConstantManager.School_Id
                    + " course_id: " + master.getCourse_id());

            try {
                params = "os=" + URLEncoder.encode((rel != null ? rel : "os"), "UTF-8")
                        + "&make=" + URLEncoder.encode((man != null ? man : "make"), "UTF-8")
                        + "&model=" + URLEncoder.encode((mod != null ? mod : "model"), "UTF-8")
                        + "&firstName=" + URLEncoder.encode(etFName.getText().toString(), "UTF-8")
                        + "&lastName=" + URLEncoder.encode(etLName.getText().toString(), "UTF-8")
                        + "&email=" + URLEncoder.encode(etEmail.getText().toString(), "UTF-8")
                        + "&password=" + URLEncoder.encode(etPassword.getText().toString(), "UTF-8")
                        + "&mobile=" + URLEncoder.encode(etMobile.getText().toString(), "UTF-8")
                        + "&medium=" + URLEncoder.encode(medium, "UTF-8")
                        + "&standard=" + URLEncoder.encode(standard, "UTF-8")
                        + "&city=" + URLEncoder.encode(selectedCity, "UTF-8")
                        + "&state=" + URLEncoder.encode(selectedState, "UTF-8")
                        + "&school=" + URLEncoder.encode(school, "UTF-8")
                        + "&user_type=" + URLEncoder.encode(user_type, "UTF-8")
                        + "&courseID=" + URLEncoder.encode(String.valueOf(master.getCourse_id()), "UTF-8")
                        + "&schoolID=" + URLEncoder.encode(ConstantManager.School_Id, "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.KEF_Localhost + NetworkUrl.KEF_SIgnUp, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {

                        if (!response.equals("error")) {
                            CommonFunctions.hideDialog();
                            Log.d("Sign_up", "api_done: " + response);

                            parseSignUpJson(response);

                        } else {

                            //error
                            alert("Something went, please try again later");
                            CommonFunctions.hideDialog();
                        }


                    }
                }, "POST");

                asyncTaskHelper.execute();

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            //no internet
            alert("Internet Connection not available");
        }

    }

    private void parseSignUpJson(String response) {


        Log.d(TAG, "SignUp Response-->" + response);

        try {
            jsonObject = new JSONObject(response);

            status = jsonObject.getString("status");
            msg = jsonObject.getString("message");

            if (status.equalsIgnoreCase("failure")) {

                alert(msg);
            } else {
                JSONArray res = jsonObject.getJSONArray("response");

                for (int i = 0; i < res.length(); i++) {
                    jsonObject = res.getJSONObject(i);
                    SettingPreffrences.setUserid(getApplicationContext(), jsonObject.getString("user_id"));
                    SettingPreffrences.setToken(getApplicationContext(), jsonObject.getString("token"));
                    SettingPreffrences.setSchoolId(getApplicationContext(), jsonObject.getString("schoolID"));
                    SettingPreffrences.setUserType(getApplicationContext(), jsonObject.getString("user_type"));
                }

                SettingPreffrences.setName(getApplicationContext(), etFName.getText().toString() + " " + etLName.getText().toString());
                SettingPreffrences.setEmail(getApplicationContext(), etEmail.getText().toString());
                SettingPreffrences.setMobile(getApplicationContext(), etMobile.getText().toString());
                SettingPreffrences.setPhoto(getApplicationContext(), "");

                CommonFunctions.addUserToParse(jsonObject.getString("user_id"));

                SettingPreffrences.setSignupDone(getApplicationContext(), true);
                SettingPreffrences.setLoginDone(getApplicationContext(), true);
                CommonFunctions.hideDialog();
                // Activity result code by @vidya
                //  finish();
                Intent intent = new Intent();
                intent.putExtra("is_signup_done", true);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        CommonFunctions.hideDialog();


    }

    private void loginWithFacebook() {


        CommonFunctions.showDialog(this);
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {

                        System.out.println("Success");
                        final GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject json, GraphResponse response) {
                                if (response.getError() != null) {
                                    // handle error
                                    System.out.println("ERROR");
                                } else {

                                    System.out.println("Success");
                                    try {
                                        Log.d(TAG, "onCompleted() called with " + "FACEBOOK_TOKEN ReadPermissions = " + AccessToken.getCurrentAccessToken().getToken());
                                        String jsonresult = String.valueOf(json);
                                        System.out.println("JSON Result" + jsonresult);

                                        String firstName = json.getString("first_name");
                                        String LastName = json.getString("last_name");
                                        String email = "";
                                        if (json.has("email")) {
                                            email = json.getString("email");
                                        } else {
                                            email = "";

                                        }


                                        SettingPreffrences.setLoginViaFB(getApplicationContext(), true);
                                        SettingPreffrences.setFBId(getApplicationContext(), json.getString("id"));
                                        SettingPreffrences.setName(getApplicationContext(), firstName + " " + LastName);
                                        SettingPreffrences.setEmail(getApplicationContext(), email);


                                        callFBWS(loginResult.getAccessToken(), firstName, LastName, email, json.getString("id"));


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,first_name,last_name,email");
                        graphRequest.setParameters(parameters);
                        graphRequest.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG, "On cancel");
                        CommonFunctions.hideDialog();
                        alert("Something went wrong, please try again later");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.d(TAG, " onError " + error.toString());
                        CommonFunctions.hideDialog();
                        alert("Something went wrong, please try again later");
                    }
                });


    }

    private void callFBWS(AccessToken accessToken, String firstName, String lastName, String email, String id) {


        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {


            try {
                params = "os=" + URLEncoder.encode(Splash.OS, "UTF-8")
                        + "&make=" + URLEncoder.encode(Splash.MAKE, "UTF-8")
                        + "&model=" + URLEncoder.encode(Splash.MODEL, "UTF-8")
                        + "&firstName=" + URLEncoder.encode(firstName, "UTF-8")
                        + "&lastName=" + URLEncoder.encode(lastName, "UTF-8")
                        + "&email=" + URLEncoder.encode(email, "UTF-8")
                        + "&fbId=" + URLEncoder.encode(id, "UTF-8")
                        + "&fbOauth=" + URLEncoder.encode(accessToken.getToken(), "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.host + NetworkUrl.facebook, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {


                        if (!response.equals("error")) {

                            parseFBJson(response);

                        } else {

                            //error
                            alert("Something went, please try again later");

                        }


                    }
                }, "POST");

                asyncTaskHelper.execute();


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {

            //no internet
            alert("Internet Connection not available");
        }


    }

    private void parseFBJson(String response) {

        Log.d(TAG, "SignUp Response-->" + response);

        try {
            jsonObject = new JSONObject(response);

            status = jsonObject.getString("status");
            msg = jsonObject.getString("message");

            if (status.equalsIgnoreCase("error")) {
                alert("Something went, please try again later");

            } else if (status.equalsIgnoreCase("failure")) {
                alert(msg);
            } else {

                JSONArray res = jsonObject.getJSONArray("response");

                for (int i = 0; i < res.length(); i++) {

                    jsonObject = res.getJSONObject(i);
                    SettingPreffrences.setUserid(getApplicationContext(), jsonObject.getString("userId"));
                    SettingPreffrences.setToken(getApplicationContext(), jsonObject.getString("token"));
                    CommonFunctions.addUserToParse(jsonObject.getString("userId"));
                    if (jsonObject.getString("photo").equalsIgnoreCase("")) {
                        SettingPreffrences.setPhoto(getApplicationContext(), "http://graph.facebook.com/" + SettingPreffrences.getFBId(getApplicationContext()) + "/picture?type=large");
                    } else {

                        SettingPreffrences.setPhoto(getApplicationContext(), jsonObject.getString("photo"));
                    }
                }
                //SettingPreffrences.setPhoto(getApplicationContext(), "http://graph.facebook.com/" + SettingPreffrences.getFBId(getApplicationContext()) + "/picture?type=large");
                SettingPreffrences.setLoginDone(getApplicationContext(), true);
                SettingPreffrences.setSignupDone(getApplicationContext(), true);

                /*if (!SettingPreffrences.getDisclaimer(getApplicationContext())) {
                    intent = new Intent(getApplicationContext(), Disclaimer.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                } else {
                    intent = new Intent(getApplicationContext(), Language.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
*/
                CommonFunctions.hideDialog();
                //onBackPressed();
                // Activity result code by @vidya
                //  finish();
                Intent intent = new Intent();
                intent.putExtra("is_signup_done", true);
                setResult(Activity.RESULT_OK, intent);
                finish();

            }


        } catch (JSONException e) {
            e.printStackTrace();
            CommonFunctions.hideDialog();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private int cityCode = -1;
    private String selectedState;
    private String selectedCity;
    private ArrayList<String> tempCity;
    private int stateCode = -1;
    private String standard, medium, school;
    private ArrayAdapter<String> cityAdapter;

    private void setUpSpinner() {

        final Multimap<String, String> cityMap = Constants.cityMap();
        final List<String> state = Constants.stateList();
        final List<String> cityNames = Constants.cityName();
        tempCity = new ArrayList<String>();

/*

            for (int i = 0; i < state.size(); i++) {
                if (selectedState.equals(state.get(i))) {
                    stateCode = i;
                }
            }

            for (int i = 0; i < cityNames.size(); i++) {
                if (selectedCity.equals(cityNames.get(i))) {
                    cityCode = i;
                }
            }
            etState.setText(selectedState);
            etCity.setText(selectedCity);
            Log.d(TAG, "onViewCreated: wellhellooo set your city and state here " + cityCode + " " + stateCode);
*/


        if (stateCode != -1) {
            for (String s : cityMap.get(String.valueOf(stateCode))) {
                tempCity.add(s);
            }
        }


        final ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, state);
        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spState.setAdapter(stateAdapter);

        etState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new androidx.appcompat.app.AlertDialog.Builder(SignUp.this)
                        .setTitle("Select State")
                        .setAdapter(stateAdapter, new DialogInterface.OnClickListener() {


                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                etState.setTextColor(getResources().getColor(R.color.black));
                                selectedState = stateAdapter.getItem(i);

                                etState.setText(selectedState);
                                dialogInterface.dismiss();
                                tempCity.clear();

                                for (int i1 = 0; i1 < state.size(); i1++) {
                                    if (selectedState.equals(state.get(i1))) {

                                        stateCode = i1;

                                        if (stateCode >= 26) {

                                            stateCode = stateCode + 1;
                                        }
//
                                        for (String s : cityMap.get(String.valueOf(stateCode))) {

                                            tempCity.add(s);


                                        }


                                    }

                                    if (i1 == state.size() - 1) {
                                        selectedCity = tempCity.get(0);
                                        etCity.setText(tempCity.get(0));
                                        etCity.setTextColor(getResources().getColor(R.color.black));
                                        for (int i2 = 0; i2 < cityNames.size(); i2++) {
                                            if (tempCity.get(0).equals(cityNames.get(i2))) {
                                                Log.d(TAG, "onClick: cityCode " + i2 + " cityName " + tempCity.get(0));
                                                cityCode = i2;
                                            }
                                        }
                                    }
                                }


                            }
                        }).create().show();
            }
        });

        if (tempCity != null) {
            cityAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, tempCity);
            cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        }

        etCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new androidx.appcompat.app.AlertDialog.Builder(SignUp.this)
                        .setTitle("Select City")
                        .setAdapter(cityAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                etCity.setTextColor(getResources().getColor(R.color.black));
                                String cityName = cityAdapter.getItem(i);
                                selectedCity = cityAdapter.getItem(i);
                                for (int i1 = 0; i1 < cityNames.size(); i1++) {
                                    if (cityName.equals(cityNames.get(i1))) {

                                        cityCode = i1;
                                    }
                                }
                                selectedCity = cityName;
                                etCity.setText(cityName);
                                dialogInterface.dismiss();
                            }
                        }).create().show();
            }
        });

        final ArrayAdapter<String> standardAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, standard_list);
        final ArrayAdapter<String> mediumAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Constants.getMediumList());
        final ArrayAdapter<String> schoolAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, school_list);


/*        etStandard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new androidx.appcompat.app.AlertDialog.Builder(SignUp.this)
                        .setTitle("Select Standard")
                        .setAdapter(standardAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                etStandard.setTextColor(getResources().getColor(R.color.black));
                                standard = standardAdapter.getItem(i);
                                etStandard.setText(standard);
                                dialogInterface.dismiss();
                            }
                        }).create().show();
            }
        });

        etMedium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new androidx.appcompat.app.AlertDialog.Builder(SignUp.this)
                        .setTitle("Select Medium")
                        .setAdapter(mediumAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                etMedium.setTextColor(getResources().getColor(R.color.black));
                                medium = mediumAdapter.getItem(i);
                                etMedium.setText(medium);
                                dialogInterface.dismiss();
                            }
                        }).create().show();
            }
        });*/

        etSchool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new androidx.appcompat.app.AlertDialog.Builder(SignUp.this)
                        .setTitle("Select School")
                        .setAdapter(schoolAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                etSchool.setTextColor(getResources().getColor(R.color.black));
                                school = schoolAdapter.getItem(i);
                                etSchool.setText(school);
                                dialogInterface.dismiss();
                                Log.d("getCourseValue", ":: " + model.getValue().get(i));
                                ConstantManager.School_Id = model.getValue().get(i);
                                getCourse(model.getValue().get(i));
                            }
                        }).create().show();
            }
        });


    }
}
