package com.observatory.kefcorner;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.observatory.utils.BaseActivity;

import java.util.HashMap;


public class How extends BaseActivity {

    private TextView txtHow, txtHowIn, txtBuy, txtBuyIn, txtSyllabus, txtSyllabusContent;
    private HashMap<String, String> dimensions;
    private Button btContact;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how);
//        dimensions = new HashMap<>();
//        dimensions.put("userId", getUserId(context));
//        CommonFunctions.sendAnalytics("howToUse", dimensions);
    }

    @Override
    protected void onLayout() {

        btContact = (Button) findViewById(R.id.bt_contact_us);

        txtHow = (TextView) findViewById(R.id.txtHow);
        txtHow.setTypeface(brandonBold);

        txtBuy = (TextView) findViewById(R.id.txtBuy);
        txtBuy.setTypeface(brandonBold);

        txtHowIn = (TextView) findViewById(R.id.txtHowIn);
        txtHowIn.setTypeface(brandonReg);

        txtBuyIn = (TextView) findViewById(R.id.txtBuyIn);
        txtBuyIn.setTypeface(brandonReg);

        txtSyllabus = (TextView) findViewById(R.id.syllabus);
        txtSyllabus.setTypeface(brandonBold);

        txtSyllabusContent = (TextView) findViewById(R.id.sy_content);
        txtSyllabusContent.setTypeface(brandonReg);


//        SpannableString ss = new SpannableString(getString(R.string.buy));
//        ClickableSpan clickableSpan = new ClickableSpan() {
//            @Override
//            public void onClick(View textView) {
//                //startActivity(new Intent(MyActivity.this, NextActivity.class));
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.flipkart.com/search?q=eclass+education&as=off&as-show=on&otracker=start"));
//                startActivity(browserIntent);
//            }
//
//            @Override
//            public void updateDrawState(TextPaint ds) {
//                super.updateDrawState(ds);
//                ds.setUnderlineText(true);
//            }
//        };
//        ss.setSpan(clickableSpan, 125, 133, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//
//        txtBuyIn.setText(ss);
//        txtBuyIn.setMovementMethod(LinkMovementMethod.getInstance());
        //txtBuyIn.setHighlightColor(Color.TRANSPARENT);
        btContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String[] TO = {"info@eclass.com"};
                String[] CC = {""};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);

                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                emailIntent.putExtra(Intent.EXTRA_CC, CC);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "EClass Query");
                //emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    finish();
                    //Log.i("Finished sending email...", "");
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(How.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }


}
