package com.observatory.kefcorner;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.chrisbanes.photoview.PhotoView;
import com.observatory.AsyncTaskMain.AsyncTaskHelper;
import com.observatory.AsyncTaskMain.OnTaskComplete;
import com.observatory.database.KefCornerStatistics;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.DateUtil;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;

import org.json.JSONObject;

import java.net.URLEncoder;

public class KefImageViewer extends BaseActivity {

    String data ="", title ="";
    PhotoView photoView;
    String GalleryID;
    int image_Value;

    /*Webservice*/
    private AsyncTaskHelper asyncTaskHelper;
    private String params, params1;
    private JSONObject jsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);

    }

    @Override
    protected void onLayout() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            data = bundle.getString("image_data");
            title = bundle.getString("image_title");
            GalleryID = bundle.getString("gallery_id");
            image_Value = bundle.getInt("image_value");
            actionBar.setTitle(title);
        }
        photoView = findViewById(R.id.image_data);
        Glide.with(getApplicationContext())
                .load(data)
                .listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
                finish();
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

               /* addStatisticsData(
                        SettingPreffrences.getUserid(getApplicationContext()),
                        Integer.parseInt(GalleryID),
                        image_Value,
                        DateUtil.getCurrentDateTimeInFormat("YYYY-MM-dd HH:mm:ss"));*/

                return false;
            }
        }).into(photoView);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void addStatisticsData(String UserID, int GalleryID, int AccessDuration, String AccessDatetime){

        if (CommonFunctions.isNetworkConnected(getApplicationContext())){

            Log.d("parameters22: ",UserID+" : "+GalleryID+" : "+AccessDuration+" : "+AccessDatetime);

            try {
                params = "GalleryID=" + URLEncoder.encode(String.valueOf(GalleryID), "UTF-8")
                        + "&UserID=" + URLEncoder.encode(UserID, "UTF-8")
                        + "&AccessDuration=" + URLEncoder.encode(String.valueOf(AccessDuration), "UTF-8")
                        + "&AccessDatetime=" + URLEncoder.encode(AccessDatetime, "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.KEF_Host + NetworkUrl.KEF_AddGallery, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {
                        Log.d("getImageResponse","response: "+response);
                    }
                },"POST");

                asyncTaskHelper.execute();

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}