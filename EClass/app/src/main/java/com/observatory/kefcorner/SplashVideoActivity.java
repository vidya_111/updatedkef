package com.observatory.kefcorner;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

public class SplashVideoActivity extends AppCompatActivity {

    VideoView videoView;
    MediaController mMediaController;
    protected androidx.appcompat.app.ActionBar actionBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_video);
        videoView = (VideoView) findViewById(R.id.video_view_splash);

        getSupportActionBar().hide();
        playVideo();



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(SplashVideoActivity.this, Splash.class));
        finish();

    }

    private void playVideo() {

        mMediaController = new MediaController(this);

      //  String videoPath = "android.resource://com.observatory.sundaram/"+R.raw.sequence01_6;

       // Uri u = Uri.parse(videoPath);

    //    videoView.setVideoURI(u);

        videoView.start();

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

              //  Toast.makeText(SplashVideoActivity.this,"Thats it folks",Toast.LENGTH_SHORT);
                //redirect user to splash screen

                startActivity(new Intent(SplashVideoActivity.this, Splash.class));
                finish();

            }
        });

    }
}
