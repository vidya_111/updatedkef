package com.observatory.kefcorner;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.observatory.adapters.LangClassAdapter;
import com.observatory.database.Master;
import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.SettingPreffrences;

import java.util.HashMap;


public class Standard extends BaseActivity {

    private ListView lstClass;
    private LangClassAdapter langClassAdapter;

    private int[] images = new int[]{
            R.drawable.ic_standard1, R.drawable.ic_standard2, R.drawable.ic_standard3,
            R.drawable.ic_standard4, R.drawable.ic_standard5, R.drawable.ic_standard6,
            R.drawable.ic_standard7, R.drawable.ic_standard8, R.drawable.ic_standard9,
            R.drawable.ic_standard10
    };
    private String[] texts = new String[]{"STANDARD 1", "STANDARD 2", "STANDARD 3",
            "STANDARD 4", "STANDARD 5", "STANDARD 6",
            "STANDARD 7", "STANDARD 8", "STANDARD 9",
            "STANDARD 10"};
    private int[] colors = new int[]{
            R.color.std1, R.color.std2, R.color.std3,
            R.color.std4, R.color.std5, R.color.std6,
            R.color.std7, R.color.std8, R.color.std9,
            R.color.std10};
    private HashMap<String, String> dimensions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standard);

        langClassAdapter = new LangClassAdapter(images, texts, colors, brandonBold);
        lstClass.setAdapter(langClassAdapter);

        lstClass.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Master master = Master.listAll(Master.class).get(0);

                SettingPreffrences.setStandard(getApplicationContext(), String.valueOf(master.getStd()));

                /*send std to analytics*/
//                dimensions = new HashMap<>();
//                dimensions.put("userId", getUserId(getApplicationContext()));
//                dimensions.put("selectedLanguage", SettingPreffrences.getLanguage(getApplicationContext()));
//                dimensions.put("selectedStandard", String.valueOf(master.getStd()));
//                CommonFunctions.sendAnalytics("standardScreen", dimensions);

                App.getInstance().trackEvent("Subject", "Screen", "", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " +
                        "selectedStandard - " + SettingPreffrences.getStandard(Standard.this) + " ; ");

                if (master.getStd() != (position + 1)) {

                    alert("This requested feature is not available in this Memory Card, Please buy a " + texts[position] + " memory card");
                    return;
                }

                startActivity(Subjects.class);

            }
        });


    }

    @Override
    protected void onLayout() {

        lstClass = (ListView) findViewById(R.id.lstClass);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.options, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {


            case R.id.profile:

                if (SettingPreffrences.getLoginDone(getApplicationContext())) {
                    startActivity(Profile.class);
                } else {
                    alert("Please login to continue");
                    //showLoginAlert();
                }
                break;

            case R.id.userProfile:

                startActivity(UserHistory.class);
                break;
//                if (SettingPreffrences.getLoginDone(getApplicationContext())) {
//                    startActivity(Profile.class);
//                } else {
//                    alert("Please login to continue");
//                }
//                break;


            case R.id.about_us:
                startActivity(How.class);
                break;

            case R.id.share:
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    String sAux = "Hi, I am using free E-class app for my studies and revision. It helped me in my studies and it can help you too.\n";
                    sAux = sAux + "Download the app at : https://play.google.com/store/apps/details?id=com.observatory.sundaram\n";
                    sAux = sAux + "Or visit www.e-class.in";
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "choose one"));
                } catch (Exception e) { //e.toString();
                }
                break;

            case R.id.feedback:

                try {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@e-class.in"});
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback");

                    startActivity(intent);
                } catch (ActivityNotFoundException e) {

                    Toast.makeText(getApplicationContext(), "No Applications were found to handle this action", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.rate_us:
                CommonFunctions.openRateMyApp(this);
                break;


        }


        return super.onOptionsItemSelected(item);
    }




}
