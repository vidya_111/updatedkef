package com.observatory.kefcorner;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.observatory.Assessment.ConstantManager;
import com.observatory.adapters.ChapterAdapter;
import com.observatory.database.Master;
import com.observatory.database.StatDetails;
import com.observatory.database.Statistics;
import com.observatory.database.Subject;
import com.observatory.database.userStatistics;
import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.Constants;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Chapters extends BaseActivity {

    public static final String SUBJECT_NAME = "subject_name";
    public static final String EMTPY_STRING = "";
    private ListView lstChapter;
    private ArrayList<Subject> subjects;
    private HashMap<String, String> dimensions;

    private Button btGenerateReport;
    private String medium;
    SharedPreferences pref;
    Boolean isAssessment=false;
    private static String getFormattedTime(int secs) {
        // int secs = (int) Math.round((double) milliseconds / 1000); // for millisecs arg instead of secs

        secs = secs;
        if (secs < 60)
            return secs + "s";
        else {
            int mins = (int) secs / 60;
            int remainderSecs = secs - (mins * 60);
            if (mins < 60) {
                return (mins < 10 ? "0" : "") + mins + "m "
                        + (remainderSecs < 10 ? "0" : "") + remainderSecs + "s";
            } else {
                int hours = (int) mins / 60;
                int remainderMins = mins - (hours * 60);
                return (hours < 10 ? "0" : "") + hours + "h "
                        + (remainderMins < 10 ? "0" : "") + remainderMins + "m "
                        + (remainderSecs < 10 ? "0" : "") + remainderSecs + "s";
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapters);

        //   Log.v(tag,":"+getIntent().getStringExtra(SUBJECT_NAME)+":");

        subjects = (ArrayList<Subject>)
                Subject.find(Subject.class, "subject_name = ?",
                        new String[]{getIntent().getStringExtra(SUBJECT_NAME)});



        pref = getSharedPreferences("TypeDetail", MODE_PRIVATE);
        String type=pref.getString("Type","");
        int typeID=pref.getInt("TypeID",0);
        if(type.equals("Assessment") || typeID == 1)
        {
            isAssessment=true;
        }
        lstChapter.setAdapter(new ChapterAdapter(brandonBold, brandonReg, subjects));

        lstChapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.d("AddAssessment","ChapterID: "+subjects.get(position).getChapterId());

                Intent intent;
                SettingPreffrences.setChapter(getApplicationContext(), String.valueOf(subjects.get(position).getChapterName()));
                SettingPreffrences.setChapterId(getApplicationContext(),subjects.get(position).getChapterId());
                    intent = new Intent(Chapters.this, Content.class);
                    intent.putExtra(Content.SUBJECT, getIntent().getStringExtra(SUBJECT_NAME));
                    intent.putExtra(Content.CHAPTER, (position + 1));
                    intent.putExtra(Content.CHAPTER_NAME, subjects.get(position).getChapterName());
                    App.getInstance().trackEvent("Content Selection", "Screen", "", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " + "selectedStandard - " + SettingPreffrences.getStandard(Chapters.this) + " ; " + "selectedSubject - " + SettingPreffrences.getSubject(Chapters.this) + " ; " + "selectedChapter - " + SettingPreffrences.getChapter(Chapters.this));
                    startActivity(intent);
                }

        });


    }

    @Override
    protected void onLayout() {

        lstChapter = (ListView) findViewById(R.id.lstChapter);
        lstChapter.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        lstChapter.setSelector(R.color.light_grey);

        btGenerateReport = (Button) findViewById(R.id.button2);
        btGenerateReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONArray genReportArr = generateReport();

                if (genReportArr.length() > 0) {
                    Log.d("PerformanceReport","json: "+genReportArr.toString());
                    showDialog(genReportArr.toString());
                } else {
                    alert(getString(R.string.msg_generate_report_no_data));
                }

            }
        });

        List<Master> masters = Master.listAll(Master.class);
        medium = masters.get(0).getMedium();


    }

    private JSONArray generateReport() {

        String userId = SettingPreffrences.getUserid(getApplicationContext()).isEmpty() ? "0" : SettingPreffrences.getUserid(getApplicationContext());
        //String deviceId = CommonFunctions.getUniqueDeviceId(getApplicationContext());

        JSONArray mainJsonArr = new JSONArray();

         //loop through all chapters looking for statistics. Finally, sort Chapters containing statistics and format them to JSON.
        for (Subject subject : subjects) {

            JSONObject chapterJson = new JSONObject();

            try {
                //chapterJson.put("chapter_id", subject.getChapterId());
                chapterJson.put("chapter_name", subject.getChapterName());

                //List<Statistics> statisticsList = Statistics.find(Statistics.class, "medium = '" + medium + "' AND subject = '" + getSubjectName() + "' AND chapter = '" + getChapterName(subject) + "' AND userId = '" + userId + "' AND deviceId = '" + deviceId + "'");

                Log.d("PerformanceReport","SubjectId: "+SettingPreffrences.getSubjectId(getApplicationContext())+" ChapterId: "+subject.getChapterId()+ " UserID: "+userId+
                        " CourseId: "+SettingPreffrences.getCourseId(getApplicationContext()));

                List<userStatistics> statisticsList = userStatistics.find(userStatistics.class,
                        "COURSE_ID = '"+SettingPreffrences.getCourseId(getApplicationContext())+"' AND SUBJECT_ID = '"+SettingPreffrences.getSubjectId(getApplicationContext())+"' AND CHAPTER_ID = '"+subject.getChapterId()+"' AND USER_ID = '"+userId+"'");

                JSONArray chapStatsArr = new JSONArray();

                for (userStatistics statistics : statisticsList) {

                    JSONObject chapInfoJson = new JSONObject();
                    chapInfoJson.put("content_name", statistics.getContentName());
                    chapInfoJson.put("content_type", statistics.getContentTypeID());
                    chapInfoJson.put("accessed_at", CommonFunctions.formatDate(statistics.getCreatedOn(), Constants.UI_DATE_FORMAT_ONLY_DATE));

                    // For MCQ, we send score; PDFs don't have duration whereas for others we send duration
                    switch (statistics.getContentTypeID()) {
                        case Constants.KEF_ContentTypeId_MCQ:
                            chapInfoJson.put("duration", statistics.getScore());
                            break;
                        case Constants.KEF_ContentTypeId_PDF:
                            chapInfoJson.put("duration", "");
                            break;
                        default:
                            chapInfoJson.put("duration", statistics.getAccessDuration());
                            break;
                    }

                    chapStatsArr.put(chapInfoJson);
                }

                chapterJson.put("chapter_info", chapStatsArr);

                if (chapStatsArr.length() > 0) {
                    mainJsonArr.put(chapterJson);
                } else {
                    // ignoring chapters which dont have statistics recorded yet.
                }

            } catch (JSONException e) {
                String msg = "Error occurred while parsing JSON when generating report";
                Log.d("Chapters", msg);
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            }
        }

        return mainJsonArr;
    }

    private String getChapterName(Subject subject) {
        String chapterName = subject.getChapterName();
        chapterName = CommonFunctions.removeApostrophe(chapterName);
        return chapterName;
    }

    public void showDialog(final String json) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.generate_report_dialog);
        dialog.setCancelable(false);
        final EditText etName = (EditText) dialog.findViewById(R.id.et_name);
        final EditText etEmail = (EditText) dialog.findViewById(R.id.et_email);
        Button btCancel = (Button) dialog.findViewById(R.id.bt_cancel);
        Button btSubmit = (Button) dialog.findViewById(R.id.bt_submit);

        if (SettingPreffrences.getName(getApplicationContext()) == "") {
            etName.setVisibility(View.VISIBLE);
        } else {
            etName.setVisibility(View.GONE);
        }

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = etEmail.getText().toString();

                if (etName.getVisibility() == View.VISIBLE) {

                    String name = etName.getText().toString();

                    if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(name)) {

                        if (CommonFunctions.isValidEmail(email)) {
                            Log.d("PerformanceReport","finalData: name: "+name+" email: "+email+" json: "+json);
                            callWS(name, email, json);
                        } else {
                            Toast.makeText(Chapters.this, "Please enter your email id.", Toast.LENGTH_LONG).show();
                        }

                        dialog.dismiss();

                    } else {

                        Toast.makeText(Chapters.this, "Please fill in all the details.", Toast.LENGTH_LONG).show();
                    }

                } else {

                    if (CommonFunctions.isValidEmail(email)) {
                        callWS(SettingPreffrences.getName(getApplicationContext()), email, json);
                        dialog.dismiss();
                    } else {
                        Toast.makeText(Chapters.this, "Please enter your email id.", Toast.LENGTH_LONG).show();
                    }

                }

            }
        });

        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();


    }

    @Override
    public void onBackPressed() {
        Log.d("PrintCheckerHere",App.IS_HAVING_MULTIPLE_STANDARD+" :: "+ ConstantManager.ISSINGLESTANDARD);
        if (App.IS_HAVING_MULTIPLE_STANDARD) {
            startActivity(new Intent(this, Subjects.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            finish();
        } else {
            if(ConstantManager.ISSINGLESTANDARD)
            {
                startActivity(new Intent(this, Subjects.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
            }
            else
            {
                super.onBackPressed();
            }
        }

    }


    public void callWS(String userName, String email, String json) {

        if (CommonFunctions.isNetworkConnected(this)) {

            Log.d("StatisticsMailer","Name: "+userName+" email: "+email+" json: "+json);

            CommonFunctions.showDialog(this);

            Ion.with(this).load(NetworkUrl.statisticsEmailer)
                    .setBodyParameter("os", Build.VERSION.RELEASE)
                    .setBodyParameter("make",Build.MANUFACTURER)
                    .setBodyParameter("model",Build.MODEL)
                    .setBodyParameter("userId", SettingPreffrences.getUserid(getApplicationContext()))
                    .setBodyParameter("userName", userName)
                    .setBodyParameter("email", email)
                    .setBodyParameter("subjectId",SettingPreffrences.getSubjectId(getApplicationContext()))
                    .setBodyParameter("subjectName", SettingPreffrences.getSubject(getApplicationContext()))
                    .setBodyParameter("medium", medium)
                    .setBodyParameter("stdValue", SettingPreffrences.getStandard(getApplicationContext()))
                    .setBodyParameter("isEdulife","")
                    .setBodyParameter("statDetails", json)
                    .asString()
                    .setCallback(new FutureCallback<String>() {

                @Override
                public void onCompleted(Exception e, String result) {

                    if (e == null) {
                        Log.d("ApiResponse","statisticsEmailer: "+result);
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            alert(jsonObject.getString("message"));
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }


                    } else {

                        alert("Something went wrong, please try again later.");
                    }

                    CommonFunctions.hideDialog();

                }
            });
        } else {

            alert("Internet Connection not available");
        }


    }

    public List<StatDetails> getInnerData(List<Statistics> listInner) {

        String name = "header";
        StatDetails model;
        List<StatDetails> listInnerMain = new ArrayList<>();
        for (int i = 0; i < listInner.size(); i++) {

            model = new StatDetails();

            if (name.trim().equalsIgnoreCase(listInner.get(i).getContent_name())) {
                model.isHeader = false;
            } else {
                model.isHeader = true;
                model.header = "VIDEO " + listInner.get(i).getContent_name().split("\\.")[0];
                listInnerMain.add(model);
            }

            model = new StatDetails();
            model.isHeader = false;
            model.duration = "" + getFormattedTime(listInner.get(i).getDuration());
            model.lastSeen = "" + listInner.get(i).getCreated_at();
            listInnerMain.add(model);
            name = listInner.get(i).getContent_name();

        }

        return listInnerMain;


    }

    public String getSubjectName() {
        String subject = SettingPreffrences.getSubject(getApplicationContext());
        subject = CommonFunctions.removeApostrophe(subject);
        return subject;
    }
}
