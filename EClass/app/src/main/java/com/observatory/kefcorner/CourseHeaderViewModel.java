package com.observatory.kefcorner;

/**
 * Created by gaurav on 15/1/18.
 */

public class CourseHeaderViewModel extends UserViewModel {
    public String course;

    public CourseHeaderViewModel(String course) {
        this.course = course;
        itemViewType = UserViewModel.VIEW_COURSE_HEADER;
    }
}
