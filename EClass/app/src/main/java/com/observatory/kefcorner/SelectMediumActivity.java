package com.observatory.kefcorner;

import android.animation.Animator;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.observatory.adapters.MediumAdapter;
import com.observatory.database.Master;
import com.observatory.database.Subject;
import com.observatory.mcqdatabase.DatabaseHelper;
import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.SettingPreffrences;
import com.observatory.utils.UsbDevice;
import com.observatory.utils.UsbHandler;

import net.lingala.zip4j.exception.ZipException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import jxl.Cell;
import jxl.CellType;
import jxl.DateCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import static com.observatory.kefcorner.StorageUtils.getStorageList;

/**
 * Created by anuj on 04/08/16.
 */
public class SelectMediumActivity extends BaseActivity {

    private static final String TAG = "SelectMediumActivity";


    private ListView lvMediums;
    private List<Subject> listMediums;
    private SharedPreferences ePrefs;
    private HashMap<String, String> dimensions;
    protected static final String SDCARD_IS_WRITEABLE = "SDCARD_IS_WRITEABLE";
    protected static final String LAST_ACCESS_TIME = "LAST_ACCESS_TIME";
    private LinearLayout llProgress;

    /*Error*/
    private RelativeLayout rlError;
    private TextView tvError;
    private TextView tvDemoVideos;
    private TextView tvBuySDCard;
    private Dialog dialog;
    private String filePath;
    private TextView tvNoMem;

    //for checking the sdcard path
    public static String SDCARD_PATH;
    /**
     * Name of root folder
     */
    public static String BASE_LOCATION = ".Sundaram";
    /**
     * Path to root folder is present
     */
    public static String ORIGINAL_PATH = "";
    public static String BASE_PATH = "";
    public static String pathIssuer = "";
    String externalpath = new String();
    String internalpath = new String();
    private String tag = "App";
    List<StorageUtils.StorageInfo> list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_medium);

        init();

    }

    private void init() {

        listMediums = new ArrayList<>();
        list = new ArrayList<>();
        list = getStorageList();


        if (new File(App.BASE_PATH).exists()) {

            for (int i = 0; i < new File(App.BASE_PATH).listFiles().length; i++) {
                Subject subject = new Subject();
                subject.setChapterName(new File(App.BASE_PATH).listFiles()[i].getName());
                listMediums.add(subject);
            }

            if (listMediums.size() > 0) {
                lvMediums.setAdapter(new MediumAdapter(brandonBold, brandonReg, (ArrayList<Subject>) listMediums));
            }
        }

        lvMediums.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // Toast.makeText(SelectMediumActivity.this, "" + listMediums.get(position), Toast.LENGTH_SHORT).show();

                showDialog();


                Subject subject = listMediums.get(position);
                SettingPreffrences.setSubjectTitle(getApplicationContext(), subject.getChapterName());
                SettingPreffrences.setStandard(getApplicationContext(), subject.getChapterName());
                //App.BASE_PATH = externalStorage1();
                SettingPreffrences.setOriginalPath(getApplicationContext(), App.BASE_PATH);
                App.BASE_PATH = SettingPreffrences.getOriginalpath(getApplicationContext()) + subject.getChapterName() + "/";

                createDB();
            }
        });

        tvDemoVideos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(SelectMediumActivity.this, How.class));

            }
        });

        tvBuySDCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("DemoVideoLogs","onClickOfDemoVideo");
                startActivity(new Intent(SelectMediumActivity.this, DemoContent.class));
            }
        });

    }

    @Override
    protected void onLayout() {

        lvMediums = (ListView) findViewById(R.id.lv_mediums);
        llProgress = (LinearLayout) findViewById(R.id.ll_progress);
        /*Error*/
        rlError = (RelativeLayout) findViewById(R.id.rl_error);
        tvError = (TextView) findViewById(R.id.textView11);
        tvDemoVideos = (TextView) findViewById(R.id.tv_demo_video);
        tvBuySDCard = (TextView) findViewById(R.id.tv_buy_sd_card);
        tvNoMem = (TextView) findViewById(R.id.textView12);


    }

    private void createDB() {


        new AsyncTask<Void, Void, String>() {


            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected String doInBackground(Void... params) {

                Master.deleteAll(Master.class);
                Subject.deleteAll(Subject.class);


                try {
                    createDatabaseXL();
                    // copying and initializing MCQ Database
                    DatabaseHelper dataBaseHelper = new DatabaseHelper(SelectMediumActivity.this);
                    dataBaseHelper.createDataBase();

                } catch (IOException e) {
                    e.printStackTrace();
                    return e.getMessage();
                } catch (BiffException e) {
                    e.printStackTrace();
                    return e.getMessage();
                } catch (ZipException e) {
                    e.printStackTrace();
                    showError();
                    hideDialog();
                    return e.getMessage();
                }


                return "success";
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                if (!s.equals("success")) {

                    //showError();
                    return;
                }


                checkValidity();


            }
        }.execute();


    }


    private void createDatabaseXL() throws IOException, BiffException, ZipException {


        filePath = decryptZip(App.BASE_PATH + "data-format.zip", true);

        Workbook workbook = Workbook.getWorkbook(new File(filePath));

        int noOfSheets = workbook.getNumberOfSheets();

        for (int i = 0; i < noOfSheets; i++) {

            if (i == 0) {
                createMasterSheet(workbook.getSheet(0));
            } else {
                createSubject(workbook.getSheet(i));

            }

        }


    }

    private void createSubject(Sheet sheet) {

        boolean isNewSdCard = false;
        String displaySubjectName = "";
        for (int row = 1; row < sheet.getRows(); row++) {
            Cell[] cell = sheet.getRow(row);
            Log.d("cell_Details","cell_length: "+cell.length);
            // old sd cards had six columns
            if (cell.length == 6) {
                isNewSdCard = false;
                if (!TextUtils.isEmpty(cell[5].getContents().trim())) {
                    displaySubjectName = cell[5].getContents().trim();
                    break;
                }

                // new sd cards have eight columns
            } else if (cell.length == 8) {
                //isNewSdCard = true;
                if (!TextUtils.isEmpty(cell[5].getContents().trim())) {
                    displaySubjectName = cell[5].getContents().trim();
                    break;
                }
            }

        }

        if (!TextUtils.isEmpty(displaySubjectName)) {

            for (int row = 1; row < sheet.getRows(); row++) {

                Cell[] cell = sheet.getRow(row);


                if (cell.length <= 0) {
                    return;
                }


                if (!TextUtils.isEmpty(cell[0].getContents())) {

                    Subject subject = new Subject();

                    subject.setDisplaySubjectName(displaySubjectName);
                    subject.setSubjectName(sheet.getName().trim());
                    subject.setFolderName(cell[0].getContents().trim());
                    subject.setChapterName(cell[1].getContents().trim());
                    subject.setContent(cell[2].getContents().trim());

                    subject.setMindMap(cell[3].getContents().trim());
                    subject.setqNa(cell[4].getContents().trim());

                    subject.setSubjectId(cell[6].getContents().trim());
                    subject.setChapterId(cell[7].getContents().trim());

                    /* for new sd cards, we have PDFs and MCQs too. Each Subject sheet has a column for PDF where 'NO' means
                     there are no PDFs and else there is count of PDFs in given chapter. Whereas for MCQs, an integer indicates
                     it is Chapter_serial_number which is used to query MCQ database for number of MCQs in the given chapter*/
                    if (isNewSdCard) {
                        String pdfCount = cell[5].getContents().trim();
                        if (pdfCount.equalsIgnoreCase("NO")) {
                            subject.setPdf("0");
                        } else {
                            subject.setPdf(pdfCount);
                        }

                        String chapSerialNum = cell[6].getContents().trim();
                        if (chapSerialNum.equalsIgnoreCase("NO")) {
                            subject.setMcq("0");
                        } else {
                            subject.setMcq(chapSerialNum);
                        }

                    } else {
                        subject.setPdf("0");
                        subject.setMcq("0");
                    }

                    if (subject != null) {
                        subject.save();
                    }

                }


            }
        }


    }

    private void createMasterSheet(Sheet sheet) {


        for (int row = 1; row < sheet.getRows(); row++) {

            Cell[] cell = sheet.getRow(row);

            Master master = new Master();

            if (!TextUtils.isEmpty(cell[0].getContents())) {

                master.setStd(Integer.parseInt(cell[0].getContents().trim()));
                master.setMedium(cell[1].getContents().trim());


                //    Log.v(tag,"createMasterSheet"+cell[2].getContents().trim());

                if (cell[2].getType() == CellType.DATE) {

                    DateCell dateCell = (DateCell) cell[2];
                    master.setExpiry(dateCell.getDate());

                } else {

                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");


                    try {
                        master.setExpiry(dateFormat.parse(cell[2].getContents().trim()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                        // Wrong expiry date
                        master.setExpiry(new Date());

                    }

                }

                try {
                    master.setKey(Integer.parseInt(cell[4].getContents().trim()));
                } catch (Exception e) {
                    master.setKey(-1);
                }
                try {

                    master.setSid(cell[3].getContents().trim());
                } catch (Exception e) {
                    master.setSid("");
                }

                master.save();
            }


        }
    }

    private void checkValidity() {
        Log.e("Checker","checkValidity_medium: "+Master.listAll(Master.class).get(0));
        hideDialog();

        String cardId = "";

        try {
            cardId = getSDCARDiD();
        } catch (IOException e) {
            e.printStackTrace();
            cardId = "";
        }

        Master master = Master.listAll(Master.class).get(0);
        Log.d("printHereVal", String.valueOf(master.getExpiry().getTime()));
        boolean isPenDrive = isPenDrive(master);

        if (isPenDrive) {
            // check if Usb device is inserted
            UsbHandler usbHandler = new UsbHandler();
            List<UsbDevice> usbDevices = usbHandler.getUsbDevices();
            Log.d(TAG, "USB Device Count -> " + usbDevices.size());
            if (!usbDevices.isEmpty()) {
                for (int i = 0; i < usbDevices.size(); i++) {
                    String serialNum = usbDevices.get(i).getSerialNum();
                    // if serial number of USB device matches with serial number stored in Master, we use that as 'cardId'
                    if (cardIdMatchesWithMasterSheet(serialNum, master, isPenDrive)) {
                        cardId = serialNum;
                        break;
                    }
                    Log.d(TAG, "USB Device SID -> " + cardId);
                }
            }
        }

        if (TextUtils.isEmpty(cardId)) {
            showError();
            App.getInstance().trackScreenView("errorscreen");
//            CommonFunctions.sendAnalytics("errorscreen", dimensions);
            tvError.setText("You don't seem to have a valid Eclass memory card inserted.");
            //tvNoMem.setText(" Please insert the Eclass memory card and try again.");
            //tvBuySDCard.setText("click to buy SD Card");
            tvNoMem.setVisibility(View.GONE);
            // tvBuySDCard.setText("Contact Us");
            return;

        }


        if (master.getSid().equalsIgnoreCase("")) {
            alert("SID is empty");
            return;

        } else {

            if (!cardIdMatchesWithMasterSheet(cardId, master, isPenDrive)) {

                App.getInstance().trackEvent("Error", "click", "Wrong Eclass memory card is inserted, please try with Genuine Eclass memory card. Contact us to purchase one", "");

                tvError.setText("You don't seem to have a valid Eclass memory card inserted");
                return;
            }
        }
        Log.e("Checker","1: "+"in medium:: "+isDateValid(master));
        if (!isDateValid(master)) {
            return;
        }


        startActivity(new Intent(this, Subjects.class));
        //finish();


    }

    // Pen drives have SID of 20 character length whereas memory cards have 25 long SIDS.
    private boolean isPenDrive(Master master) {
        return master.getSid() != null && master.getSid().length() == 20;
    }

    private boolean cardIdMatchesWithMasterSheet(String cardId, Master master, boolean isPenDrive) {
        if (isPenDrive) {
            // compare whole Ids for pen drives
            return master.getSid().equals(cardId);
        } else {
            // compare last seven digits for memory cards
            return master.getSid().substring(18, 25).equals(cardId.substring(18, 25));
        }
    }

    private boolean isDateValid(Master master) {
        Log.e("Checker","inIsDateValid"+"medium");
        ePrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        //Step 1 : check for network time
        String timeSettings = android.provider.Settings.System.getString(
                this.getContentResolver(),
                android.provider.Settings.System.AUTO_TIME);
        if (timeSettings.contentEquals("0")) {
//            dimensions = new HashMap<>();
//            dimensions.put("userId", getUserId(context));
//            dimensions.put("name", "Please set your device date and time to use Automatic Date & Time. You can change this from your device settings");
//            CommonFunctions.sendAnalytics("errorscreen", dimensions);

            App.getInstance().trackEvent("Error", "click", "Please set your device date and time to use Automatic Date & Time. You can change this from your device settings", "");
            tvError.setText("Please set your device date and time to use Automatic Date & Time. You can change this from your device settings");
            //tvBuySDCard.setText("Contact Us");
            //tvNoMem.setVisibility(View.GONE);
            showError();
            hideDialog();
            return false;
        }

        File dateFile = new File(App.BASE_PATH + ".date.zip");
        long currentSystemTime = 0;
        try {
            currentSystemTime = new Date().getTime();
        } catch (Exception e) {

        }


        /*
         *If the file does not exist,memory card is inserted for the 1st time
         * */
        if (ePrefs.getBoolean(SDCARD_IS_WRITEABLE, true)) {
            if (!dateFile.exists()) {
                try {
                    dateFile.createNewFile();
                    writeAccessTime(dateFile);
                } catch (IOException e) {
                    e.printStackTrace();
                    // txtMessage.setText("Failed due to " + e.getMessage());
                    ePrefs.edit().putBoolean(SDCARD_IS_WRITEABLE, false).commit();
                    // return false;
                }
            }
        }

        //Now check for it being false - Last Access Time never filled
        if (!ePrefs.getBoolean(SDCARD_IS_WRITEABLE, true)) {
            if (ePrefs.getString(LAST_ACCESS_TIME, "").equals("")) {
                //Put today's time as last accessed time
                String currentTime = new Date().getTime() + "";
                ePrefs.edit().putString(LAST_ACCESS_TIME, currentTime).commit();
            }
        }

        String temp = "";

        /**
         * Now checking for the actual date.
         */
        if (ePrefs.getBoolean(SDCARD_IS_WRITEABLE, true)) {
            try {

                FileInputStream inputStream = new FileInputStream(dateFile);
                int length = (int) dateFile.length();
                byte[] bytes = new byte[length];
                inputStream.read(bytes);
                inputStream.close();
                temp = new String(bytes);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                tvError.setText("You don't seem to have a valid Eclass memory card inserted");
                //tvNoMem.setText(" Please insert the Eclass memory card and try again.");
                //tvNoMem.setVisibility(View.VISIBLE);
                //tvBuySDCard.setText("Contact Us");
                showError();
                hideDialog();

                return false;

            } catch (IOException e) {
                e.printStackTrace();
                tvError.setText("You don't seem to have a valid Eclass memory card inserted");
                //tvNoMem.setText(" Please insert the Eclass memory card and try again.");
                //tvNoMem.setVisibility(View.VISIBLE);
                //tvBuySDCard.setText("Contact Us");
                showError();
                hideDialog();

                return false;
            }

            long dateInCard = 0;

            if (!temp.equals("")) {
                dateInCard = Long.parseLong(temp);
            } else {
                dateInCard = currentSystemTime;
            }

            //  Log.v(tag,"Date in card: "+new Date(dateInCard).toGMTString());
            //Log.v(tag, "Password: " + backupPassword);

            /* System date is tampered */
            if (currentSystemTime + 10000 < dateInCard) {

//                dimensions = new HashMap<>();
//                dimensions.put("name", "Your system date is not set correctly,please reset it");
//                dimensions.put("userId", getUserId(context));
//                CommonFunctions.sendAnalytics("errorscreen", dimensions);

                App.getInstance().trackEvent("Error", "click", "Your system date is not set correctly,please reset it", "");
                tvError.setText("Your system date is not set correctly,please reset it");
                //////tvBuySDCard.setText("Contact Us");
                // tvNoMem.setVisibility(View.GONE);
                showError();
                hideDialog();
                return false;
            }
        }//end of SD card is writable

        //Use Network Time & LAST ACCESSED TIME instead

        if (!ePrefs.getBoolean(SDCARD_IS_WRITEABLE, true)) {
            if (!ePrefs.getString(LAST_ACCESS_TIME, "").equals("")) {
                //Do Something here
                //Step 1 : check for network time
               /* String timeSettings = android.provider.Settings.System.getString(
                        this.getContentResolver(),
                        android.provider.Settings.System.AUTO_TIME);
              /*  if (timeSettings.contentEquals("0")) {
                    dimensions = new HashMap<>();
                    dimensions.put("userId", getUserId(context));
                    dimensions.put("name", "Please set your device date and time to use Automatic Date & Time. You can change this from your device settings");
                    CommonFunctions.sendAnalytics("errorscreen", dimensions);
                    tvError.setText("Please set your device date and time to use Automatic Date & Time. You can change this from your device settings");
                    ////tvBuySDCard.setText("Contact Us");
                    showError();

                    return false;
                } *///else {
                //Automatic Data and Time is selected
                long dateInPersistence = Long.parseLong(ePrefs.getString(LAST_ACCESS_TIME, ""));
                long timeNow = new Date().getTime();
                if (dateInPersistence > timeNow) {

//                    dimensions = new HashMap<>();
//                    dimensions.put("userId", getUserId(context));
//                    dimensions.put("name", "Your system date is not set correctly, please reset it");
//                    CommonFunctions.sendAnalytics("errorscreen", dimensions);

                    App.getInstance().trackEvent("Error", "click", "Your system date is not set correctly, please reset it", "");
                    tvError.setText("Your system date is not set correctly, please reset it");
                    ////tvBuySDCard.setText("Contact Us");
                    //tvNoMem.setVisibility(View.GONE);
                    showError();
                    hideDialog();
                    return false;
                }
                // }
                //Step 2: check for last access time
            }
        }

        /* Date expired */
        Log.d("MyExpiryCheck_medium",currentSystemTime+" == "+master.getExpiry().getTime());
        if (currentSystemTime > master.getExpiry().getTime()) {
//            dimensions = new HashMap<>();
//            dimensions.put("userId", getUserId(context));
//            dimensions.put("name", "This Eclass memory card is expired");
//            CommonFunctions.sendAnalytics("errorscreen", dimensions);

            App.getInstance().trackEvent("Error", "click", "This Eclass memory card is expired", "");
            tvError.setText("The Eclass memory card inserted has crossed its validity date and has expired.");
            //tvBuySDCard.setText("click to buy SD Card");
            //tvNoMem.setVisibility(View.VISIBLE);
            showError();
            hideDialog();
            return false;
        }

        if (ePrefs.getBoolean(SDCARD_IS_WRITEABLE, true)) {
            try {
                writeAccessTime(dateFile);
            } catch (IOException e) {
                e.printStackTrace();
                //txtMessage.setText("Failed due to " + e.getMessage());
                //You've caught them - they've changed phones
                //txtMessage.setText("Please re-launch the app");
                ePrefs.edit().putBoolean(SDCARD_IS_WRITEABLE, false).commit();
                //return isDateValid(master);
                //return false;
            }
        }
        //Updating Last Access Time
        if (!ePrefs.getBoolean(SDCARD_IS_WRITEABLE, true)) {
            String currentTime = new Date().getTime() + "";
            ePrefs.edit().putString(LAST_ACCESS_TIME, currentTime).commit();
        }

        return true;
    }//end of function

    private void writeAccessTime(File file) throws IOException {

        FileOutputStream outputStreamWriter = new FileOutputStream(file);
        String currentTime = new Date().getTime() + ""; // We are using new time here. That is why we have to use + 1000 later
        //  Log.v(tag,"currentTime: "+currentTime);

        outputStreamWriter.write(currentTime.getBytes());
        outputStreamWriter.close();

    }


    private String getSDCARDiD() throws IOException {

        String memBlk = "";
        String sd_cid = "";
        String serial_id = "";

        File file = new File("/sys/block/mmcblk1");
        if (file.exists() && file.isDirectory()) {

            memBlk = "mmcblk1";
        } else {
            memBlk = "mmcblk0";
        }

        Process cmd = Runtime.getRuntime().exec("cat /sys/block/" + memBlk + "/device/cid");
        BufferedReader br = new BufferedReader(new InputStreamReader(cmd.getInputStream()));
        sd_cid = br.readLine();
        // sd_cid = sd_cid.substring(0, sd_cid.length() - 1);
        return sd_cid;
        //return serial_id;
    }

    private void showError() {


        lvMediums.animate()
                .alpha(0.f)
                .scaleX(0.f).scaleY(0.f)
                .setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {


                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        rlError.setAlpha(0.f);
                        rlError.setScaleX(0.f);
                        rlError.setScaleY(0.f);
                        rlError.setVisibility(View.VISIBLE);
                        rlError.animate()
                                .alpha(1.f)
                                .scaleX(1.f).scaleY(1.f)
                                .setDuration(500)
                                .setListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animator) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animator) {

                                        lvMediums.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animator) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animator) {

                                    }
                                });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });

    }

    public void showDialog() {

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setCancelable(false);
        ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.pb);
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.blue), PorterDuff.Mode.SRC_IN);
        dialog.show();

    }

    public void hideDialog() {

        dialog.dismiss();
    }

    @Override
    protected void onPause() {
        super.onPause();


        File temp = new File(Environment.getDataDirectory(), "temp/");
        File[] fileList = temp.listFiles();

        if (fileList != null) {

            for (int i = 0; i < fileList.length; i++) {

                if (fileList[i].getAbsolutePath().contains(".xls")) {
                    fileList[i].delete();
                }
            }
        }

        File temp1 = new File(getExternalFilesDir(null), "temp/");
        File[] fileList1 = temp1.listFiles();

        if (fileList1 != null) {

            for (int i = 0; i < fileList1.length; i++) {

                if (fileList1[i].getAbsolutePath().contains(".xls")) {
                    fileList1[i].delete();
                }
            }
        }


        if (filePath != null) {

            filePath = filePath.replace("Data-Format.xls", "");
            File temp2 = new File(filePath);
            File[] fileList2 = temp2.listFiles();

            if (fileList2 != null) {

                for (int i = 0; i < fileList2.length; i++) {

                    if (fileList2[i].getAbsolutePath().contains(".xls")) {
                        fileList2[i].delete();
                    }
                }
            }
        }


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        File temp = new File(Environment.getDataDirectory(), "temp/");
        File[] fileList = temp.listFiles();

        if (fileList != null) {

            for (int i = 0; i < fileList.length; i++) {

                if (fileList[i].getAbsolutePath().contains(".xls")) {
                    fileList[i].delete();
                }
            }
        }

        File temp1 = new File(getExternalFilesDir(null), "temp/");
        File[] fileList1 = temp1.listFiles();

        if (fileList1 != null) {

            for (int i = 0; i < fileList1.length; i++) {

                if (fileList1[i].getAbsolutePath().contains(".xls")) {
                    fileList1[i].delete();
                }
            }
        }


        if (filePath != null) {

            filePath = filePath.replace("Data-Format.xls", "");
            File temp2 = new File(filePath);
            File[] fileList2 = temp2.listFiles();

            if (fileList2 != null) {

                for (int i = 0; i < fileList2.length; i++) {

                    if (fileList2[i].getAbsolutePath().contains(".xls")) {
                        fileList2[i].delete();
                    }
                }
            }
        }


    }

    @Override
    public void onBackPressed() {

        if (rlError.getVisibility() == View.VISIBLE) {

            hideError();

        } else {

            super.onBackPressed();
        }

    }

    private void hideError() {


        rlError.animate()
                .alpha(0.f)
                .scaleX(0.f).scaleY(0.f)
                .setDuration(500).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {


                        lvMediums.setAlpha(0.f);
                        lvMediums.setScaleX(0.f);
                        lvMediums.setScaleY(0.f);
                        lvMediums.setVisibility(View.VISIBLE);
                        lvMediums.animate()
                                .alpha(1.f)
                                .scaleX(1.f).scaleY(1.f)
                                .setDuration(500)
                                .setListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animator) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animator) {

                                        rlError.setVisibility(View.GONE);

                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animator) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animator) {

                                    }
                                });


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {


                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {


                    }
                });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {

            if (rlError.getVisibility() == View.VISIBLE) {

                hideError();

            } else {

                finish();
            }

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected String externalStorage1() {

        File storage = new File("/storage");
        String path = "";


        //File storage = Environment.getExternalStorageDirectory().getAbsoluteFile();

        File file[] = storage.listFiles();
        //Toast.makeText(this, file.toString(), Toast.LENGTH_SHORT).show();

        if (file != null) {

            for (int i = 0; i < file.length; i++) {
                //  Log.v("Files", "FileName:" + file[i].getName());
                // Toast.makeText(this, "for loop 1", Toast.LENGTH_SHORT).show();

                File file1 = new File("/storage/" + file[i].getName() + "/");

                File files2[] = file1.listFiles();
                // Toast.makeText(this, files2.toString(), Toast.LENGTH_SHORT).show();

                if (files2 != null) {
                    for (int j = 0; j < files2.length; j++) {
                        // Toast.makeText(this, "for loop 2", Toast.LENGTH_SHORT).show();
                        if (files2[j].toString().equals(file1.getAbsolutePath() + "/" + BASE_LOCATION)) {

                            path = file1.getAbsolutePath() + "/" + BASE_LOCATION + "/";
                            return path;
                        }
                    }
                } else {

                    path = "";

                }


            }
        }

        if (path.equals("")) {


            String test[] = externalpath.split("\n");
            String path1 = test[test.length - 1];
            path1 = path1.replace("*", "");


            if (externalpath.contains("fat")) {
                storage = Environment.getExternalStorageDirectory().getAbsoluteFile();
            } else {
                storage = new File(path1);
                //storage = new File(list.get(0).path);
                //storage = new File("/storage");
            }

            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    //  Log.v("Files", "FileName:" + file[i].getName());
                    // Toast.makeText(this, "for loop 3", Toast.LENGTH_SHORT).show();

                    String fileValue;
                    fileValue = files[i].toString();
                    if (externalpath.contains("fat")) {


                        if (fileValue.equals(storage.getPath() + "/" + BASE_LOCATION)) {
                            path = storage.getPath() + "/" + files[i].getName();
                        }
                    } else {
                        if (fileValue.equals(path1 + "/" + BASE_LOCATION)) {
                            path = path1 + "/" + files[i].getName();
                        }
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                }
            } else {

                path = "";

            }


        }

        if (path.equals("")) {

//            String test[] = externalpath.split("\n");
//            String path1 = test[test.length - 1];
//            path1 = path1.replace("*", "");
            String path1;
            if (list.size() > 0) {

                path1 = list.get(list.size() - 1).path;
            } else {
                path1 = "";
            }


            if (externalpath.contains("fat")) {
                storage = Environment.getExternalStorageDirectory().getAbsoluteFile();
            } else {
                storage = new File(path1);
                //storage = new File(list.get(0).path);
                //storage = new File("/storage");
            }

            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    // Toast.makeText(this, "for loop 4", Toast.LENGTH_SHORT).show();
                    //  Log.v("Files", "FileName:" + file[i].getName());

                    String fileValue;
                    fileValue = files[i].toString();
                    if (externalpath.contains("fat")) {


                        if (fileValue.equals(storage.getPath() + "/" + BASE_LOCATION)) {
                            path = storage.getPath() + "/" + files[i].getName();
                        }
                    } else {
                        if (fileValue.equals(path1 + "/" + BASE_LOCATION)) {
                            path = path1 + "/" + files[i].getName();
                        }
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                } else {

                    path = "";
                }
            }

        }

        if (path.equals("")) {

            String path1 = "";


            for (int k = 0; k < list.size(); k++) {

                path1 = list.get(k).path;


                storage = new File(path1);

                File files[] = storage.listFiles();

                if (files != null) {
                    for (int i = 0; i < files.length; i++) {

                        // Toast.makeText(this, "for loop 5", Toast.LENGTH_SHORT).show();
                        //  Log.v("Files", "FileName:" + file[i].getName());

                        String fileValue;
                        fileValue = files[i].toString();

                        if (fileValue.equals(path1 + "/" + BASE_LOCATION)) {
                            path = path1 + "/" + files[i].getName();
                            break;
                        }


                    }

                    if (new File(path).exists()) {

                        return path + "/";

                    }
                } else {

                    path = "";

                }

            }


        }

        if (path.equals("")) {

            String path1 = "mnt/sd-ext/";
            storage = new File(path1);
            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {


                    String fileValue;
                    fileValue = files[i].toString();

                    if (fileValue.equals(path1 + BASE_LOCATION)) {
                        path = path1 + "/" + files[i].getName();
                        break;
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                }
            } else {

                path = "";

            }

        }

        if (path.equalsIgnoreCase("")) {


            String path1 = "/mnt/usb_storage/USB_DISK0/udisk0";
            storage = new File(path1);
            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {


                    String fileValue;
                    fileValue = files[i].toString();

                    if (fileValue.equals(path1 + BASE_LOCATION)) {
                        path = path1 + "/" + files[i].getName();
                        break;
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                }
            } else {

                path = "";

            }

        }

        if (path.equalsIgnoreCase("")) {


            String path1 = "/USB/USB_DISK0/udisk0";
            storage = new File(path1);
            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {


                    String fileValue;
                    fileValue = files[i].toString();

                    if (fileValue.equals(path1 + BASE_LOCATION)) {
                        path = path1 + "/" + files[i].getName();
                        break;
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                }
            } else {

                return "";

            }

        }

        return "/";


    }


}


