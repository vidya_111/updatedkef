package com.observatory.kefcorner;

/**
 * Created by gaurav on 15/1/18.
 */

/* This ViewModel is created to handle diff. view types. It replaces 'UserHistory' from 'database' package in UserProfileAdapter for aforementioned reason. */

public abstract class UserViewModel {

    public static final int VIEW_COURSE_HEADER = 1;
    public static final int VIEW_USER_HISTORY = 2;

    protected int itemViewType = -1; // default value. Actual value will be assigned by children.

    public int getItemViewType() {
        return itemViewType;
    }
}
