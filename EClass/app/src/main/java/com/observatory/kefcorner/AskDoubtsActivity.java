package com.observatory.kefcorner;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.Toolbar;

import com.observatory.AsyncTaskMain.AsyncTaskHelper;
import com.observatory.AsyncTaskMain.OnTaskComplete;
import com.observatory.database.Subject;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;
import com.orm.query.Select;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class AskDoubtsActivity extends BaseActivity implements View.OnClickListener {

    EditText questionInput, doubtTitleInput;
    Button send_question_button;

    Spinner category_spinner;
    ArrayList<String> category_spinner_list = new ArrayList<>();
    String selectedCategorySpinnerData = "";

    Spinner subject_spinner;
    ArrayList<String> subject_spinner_list = new ArrayList<>();
    String selectedSubjectSpinnerData = "";

    LinearLayout subject_spinner_layout;

    /*Webservice*/
    private AsyncTaskHelper asyncTaskHelper;
    private String params;

    String SubjectId = "";

    ArrayList<Subject> getSubjectId = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_doubts);

        actionBar.setTitle("Ask Doubt");

        category_spinner_list.add("Select Category");
        category_spinner_list.add("Subject");
        category_spinner_list.add("Other");

        ArrayAdapter category_spinner_adapter = new ArrayAdapter(getApplicationContext(),
                android.R.layout.simple_spinner_item, category_spinner_list);
        category_spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category_spinner.setAdapter(category_spinner_adapter);

        subject_spinner_list.add("Select Subject");

        String[] splitData = Subjects.getAllSubjects.split(",");

        for (int i = 0; i < splitData.length; i++) {
            subject_spinner_list.add(splitData[i]);
            Log.d("doubtLogs", "subjectData: " + splitData[i]);
        }

        ArrayAdapter subject_spinner_adapter = new ArrayAdapter(getApplicationContext(),
                android.R.layout.simple_spinner_item, subject_spinner_list);
        subject_spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subject_spinner.setAdapter(subject_spinner_adapter);
    }

    @Override
    protected void onLayout() {

        questionInput = findViewById(R.id.doubt_question_id);
        doubtTitleInput = findViewById(R.id.doubt_title_id);

        send_question_button = findViewById(R.id.doubt_send_btn);
        category_spinner = findViewById(R.id.category_spinner);
        subject_spinner = findViewById(R.id.subject_spinner);

        subject_spinner_layout = findViewById(R.id.subject_spinner_layout);

        send_question_button.setOnClickListener(this);


        category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (category_spinner.getSelectedItem().toString().equals("Subject")) {
                    subject_spinner_layout.setVisibility(View.VISIBLE);
                } else {
                    subject_spinner_layout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @Override
    public void onClick(View v) {
        String whereClause = "";
        switch (v.getId()) {

            case R.id.doubt_send_btn:

                if (!selectedCategorySpinnerData.equals("Select Category") && !questionInput.getText().toString().equals("")
                        && !doubtTitleInput.getText().toString().equals("")) {

                    selectedCategorySpinnerData = category_spinner.getSelectedItem().toString();
                    if (selectedCategorySpinnerData.equals("Subject")) {

                        selectedSubjectSpinnerData = subject_spinner.getSelectedItem().toString();

                        Log.d("printValues", "SelectedValue" + "1::" + selectedSubjectSpinnerData);

                        if (!selectedSubjectSpinnerData.equals("Select Subject")) {

                            Log.d("printValues", "SelectedValue" + "2::" + selectedSubjectSpinnerData);

                            whereClause = "DISPLAY_SUBJECT_NAME = '" + selectedSubjectSpinnerData + "' and SUBJECT_ID != ''";

                            List<Subject> idList = Select.from(Subject.class)
                                    .where(whereClause).list();

                            Log.d("printValues", "ListSize: " + idList.size()
                                    + " SelectedSubject: " + selectedSubjectSpinnerData
                                    + " :: " + whereClause);

                            if (idList.size() > 0) {
                                for (int i = 0; i < idList.size(); i++) {
                                    Log.d("printValues", "SubjectID: " + idList.get(i).getSubjectId());
                                    SubjectId = idList.get(i).getSubjectId();
                                }
                                Log.d("printValues", "SubjectID2: " + SubjectId);
                                if (!SubjectId.equals("")) {
                                    addDoubtDetails("SUB",
                                            SubjectId,
                                            doubtTitleInput.getText().toString(),
                                            questionInput.getText().toString(),
                                            SettingPreffrences.getUserid(getApplicationContext()));
                                } else {
                                    alert("Something went wrong, Please try again");
                                }
                            }
                        } else {
                            alert("Please select Subject");
                        }

                    } else {

                        addDoubtDetails("OTH",
                                "",
                                doubtTitleInput.getText().toString(),
                                questionInput.getText().toString(),
                                SettingPreffrences.getUserid(getApplicationContext()));
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Fields are empty", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(getApplicationContext(), Doubt_Conversations.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();

        super.onBackPressed();
    }

    private void addDoubtDetails(String DoubtCategoryID, String SubjectID, String DoubtTitle, String Message, String UserID) {
        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {

            CommonFunctions.showDialog(this);
            try {

                Log.d("AskDoubtApi", "Params: " + DoubtCategoryID + " :: " + SubjectID + " :: " + DoubtTitle
                        + " :: " + Message + " :: " + UserID);

                params = "DoubtCategoryID=" + URLEncoder.encode(DoubtCategoryID, "UTF-8")
                        + "&SubjectID=" + URLEncoder.encode(SubjectID, "UTF-8")
                        + "&DoubtTitle=" + URLEncoder.encode(DoubtTitle, "UTF-8")
                        + "&Message=" + URLEncoder.encode(Message, "UTF-8")
                        + "&UserID=" + URLEncoder.encode(UserID, "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.KEF_Host + NetworkUrl.KEF_AddDoubt, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {
                        CommonFunctions.hideDialog();
                        Log.d("addDoubt", "response: " + response);
                        if (!response.equals("error")) {
                            Toast.makeText(getApplicationContext(), "Doubt send successfully!!", Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        }
                    }
                }, "POST");

                asyncTaskHelper.execute();

            } catch (Exception e) {
                Log.e("Error: AddDoubt: ", e.getMessage());
            }

        } else {
            alert("Internet Connection not available");
        }
    }
}