package com.observatory.kefcorner;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;

import com.observatory.database.LastContent;
import com.observatory.database.Master;
import com.observatory.database.RealmDb;
import com.observatory.database.Statistics;
import com.observatory.database.userStatistics;
import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.Constants;
import com.observatory.utils.CustomVideoView;
import com.observatory.utils.SecretKeyMaterial;
import com.observatory.utils.SettingPreffrences;

import net.lingala.zip4j.exception.ZipException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import io.realm.Realm;

import static com.observatory.utils.Constants.CONTENT_TYPE_CONTENT;
import static com.observatory.utils.Constants.CONTENT_TYPE_MINDMAP;
import static com.observatory.utils.Constants.CONTENT_TYPE_QANDA;
import static com.observatory.utils.Constants.KEF_ContentTypeId_Content;
import static com.observatory.utils.Constants.KEF_ContentTypeId_MindMap;
import static com.observatory.utils.Constants.KEF_ContentTypeId_QNA;

public class Video extends BaseActivity implements Animation.AnimationListener {

    public static final String password = "x4r!sd678cX0";
    //    public static String VIDEO_PATH="http://localhost:8080";
    public static final String randomizer = "-sE";
    public static String VIDEO_PATH = "video_path";
    //http://192.168.1.20:8080
    String tempPath;
    ProgressDialog progressDialog;
    private CustomVideoView videoView;
    private String fromWhichContent;
    private int time = 0;
    private Handler handler = new Handler();
    private String chapter;
    private String subject;
    private String date;
    private String vidPath;
    private String videoName = "";
    private Runnable updateTime = new Runnable() {
        @Override
        public void run() {
            time++;
            handler.postDelayed(this, 1000);
        }
    };
    private int key = -1;
    private boolean isVideoPauseDataInserted = false;
    private boolean notVideo = false;
    private boolean isComplete = false;
    private boolean isActivityStoped = false;
    private String decryptedVidPath;
    File decryptedVidFile;
    private String medium;
    private String standard;
    private String userId;


    LinearLayout lnr_backward_skip, lnr_forward_skip;
    ImageView img_back, img_forwardl;
    Animation animateForward, animateBackwrd;
    private AlertDialog alertDialog;
    public int playBackPosition = 0;

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {
        byte[] buffer = new byte[1024]; // Adjust if you want
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //   Log.v(tag,""+savedInstanceState);
        Log.v(tag, "Password " + password);

        actionBar.hide();
        setContentView(R.layout.activity_video);
        //    Log.v(tag,getIntent().getStringExtra(VIDEO_PATH));

//        statistics.setLanguage("marathi");
//        statistics.save();

        progressDialog = new ProgressDialog(this);
        vidPath = getIntent().getStringExtra(VIDEO_PATH);
        fromWhichContent = getIntent().getStringExtra(getString(R.string.from));
        Log.d("VideoPlay", "onCreate");
        video_play();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("VideoPlay", "onResume");
        if (videoView != null) {
            Log.d("VideoStatus", "Video Not null  - InResume" + time + "::" + playBackPosition);
            videoView.seekTo(playBackPosition);
        }

    }

    public void video_play() {

        Master master = Master.listAll(Master.class).get(0);

        boolean islastvideoContent = getIntent().getBooleanExtra("isLastcontent", false);
        if (islastvideoContent == true) {
            key = getIntent().getIntExtra("key", -1);
            medium = getIntent().getStringExtra("medium");
        } else {
            key = Master.listAll(Master.class).get(0).getKey();
            medium = master.getMedium();
        }

        /*
         *  Decrypted video file and its path
         */
        Log.d("VideoLogChecker", "key: " + key + " medium: " + medium);
        decryptedVidFile = new File(Environment.getExternalStorageDirectory(), "temp/decryptVid.mp4");
        decryptedVidPath = decryptedVidFile.getAbsolutePath();
        Log.d("VideoLogChecker", "decryptedVidPath: " + decryptedVidPath);
        standard = SettingPreffrences.getStandard(this);
        chapter = CommonFunctions.removeApostrophe(SettingPreffrences.getChapter(this));
        subject = CommonFunctions.removeApostrophe(SettingPreffrences.getSubject(this));

        date = new SimpleDateFormat(Constants.DATABASE_DATE_FORMAT).format(new Date());

        if (key != -1) {
            Log.d("VideoLogChecker", "keyIsNot-1: " + key + " vidPath: " + vidPath);
            vidPath = vidPath.replace(".zip", ".mp4");
        } else {

        }

        if (!new File(vidPath).exists()) {
            alert("The Video requested does not exist or is not playable", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    notVideo = true;
                    dialog.dismiss();
                    finish();
                }
            });

            return;
        }

        App.getInstance().trackEvent("Video", "Screen", "", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " +
                "selectedStandard - " + SettingPreffrences.getStandard(this) + " ; "
                + "selectedSubject - " + SettingPreffrences.getSubject(this) + " ; " +
                "selectedChapter - " + chapter);

        playBackPosition = getIntent().getIntExtra("position", 0);
        Log.d("VideoLogChecker", "keyChecker: " + key);
        if (key != -1) {
            Log.d("VideoLogChecker", "playBackPositionChecker_ INIF: " + playBackPosition);
            if (playBackPosition > 0) {
                AlertDialog dialog = new AlertDialog.Builder(this).create();
                dialog.setMessage("Do you wish to resume from where you stopped?");
                dialog.setCancelable(false);
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Resume", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        playBackPosition = getIntent().getIntExtra("position", 0);
                        decryptAES();
                        dialog.dismiss();
                    }
                });
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Start Over", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        playBackPosition = 0;
                        decryptAES();
                        dialog.dismiss();
                    }
                });
                dialog.show();

            } else {
                decryptAES();
            }

        } else {
            Log.d("VideoLogChecker", "playBackPositionChecker_ INELSE: " + playBackPosition);
            if (playBackPosition > 0) {
                AlertDialog dialog = new AlertDialog.Builder(this).create();
                dialog.setMessage("Do you wish to resume from where you stopped?");
                dialog.setCancelable(false);
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Resume", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        playBackPosition = getIntent().getIntExtra("position", 0);
                        decryptZipAndPlay(vidPath);
                        dialog.dismiss();
                    }
                });
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Start Over", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        playBackPosition = 0;
                        decryptZipAndPlay(vidPath);
                        dialog.dismiss();
                    }
                });
                dialog.show();

            } else {
                decryptZipAndPlay(vidPath);
            }

        }


        //10sec fastforward and backward
        lnr_forward_skip.setOnTouchListener(new View.OnTouchListener() {

            private GestureDetector gestureDetector = new GestureDetector(Video.this, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    // load the animation
                    animateForward = AnimationUtils.loadAnimation(getApplicationContext(),
                            R.anim.move_right);
                    img_forwardl.startAnimation(animateForward);
                    videoView.seekTo(videoView.getCurrentPosition() + 15000);
                    return super.onDoubleTap(e);
                }
            });

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                gestureDetector.onTouchEvent(motionEvent);
                return true;
            }
        });


        lnr_backward_skip.setOnTouchListener(new View.OnTouchListener() {

            private GestureDetector gestureDetector = new GestureDetector(Video.this, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {

                    animateBackwrd = AnimationUtils.loadAnimation(getApplicationContext(),
                            R.anim.move_left);
                    img_back.startAnimation(animateBackwrd);
                    videoView.seekTo(videoView.getCurrentPosition() - 5000);
                    return super.onDoubleTap(e);
                }
            });

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                gestureDetector.onTouchEvent(motionEvent);
                return true;
            }
        });
    }

    private void addFakeStatisticsForTesting() {

        String imei_number = CommonFunctions.getUniqueDeviceId(getApplicationContext());
        Statistics statistics1 = new Statistics(userId, imei_number, medium, "Urdu", "ریاضی -1", videoName, CONTENT_TYPE_CONTENT, time, date);
        statistics1.save();
        Statistics statistics2 = new Statistics(userId, imei_number, medium, "Urdu", "ریاضی -2", videoName, CONTENT_TYPE_MINDMAP, time, date);
        statistics2.save();
        Statistics statistics3 = new Statistics(userId, imei_number, medium, "Urdu", "كمار   بھارتی", videoName, CONTENT_TYPE_QANDA, time, date);
        statistics3.save();

    }

    private void decryptZipAndPlay(final String vidPath) {
        Log.d("VideoLogChecker", "decryptZipAndPlay: " + "videoPath: " + vidPath);
        progressDialog.setTitle("KEF");
        // progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMessage("Please wait while we prepare the video..");
        //progressDialog.setProgress(0);
        // progressDialog.setMax(100);
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {

                progressDialog.dismiss();
                onBackPressed();
                finish();


            }
        });

        new AsyncTask<Void, String, String>() {


            @Override
            protected String doInBackground(Void... params) {


                try {
                    return decryptZip(vidPath, true);
                } catch (ZipException e) {
                    e.printStackTrace();
                }

                return "Failed";
            }


            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (TextUtils.isEmpty(s)) {
                    // Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
                }

                // Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                videoName = CommonFunctions.removeFormatSuffix(new File(vidPath).getName());
                if (s.equals("Failed")) {

                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    if (!isActivityStoped) {
                        alert("Cannot Play video");
                    }


                    return;

                }
                videoView.setVideoPath(s);
                videoView.setMediaController(new MediaController(Video.this));
                //Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        videoView.start();
                        videoView.requestFocus();
                        if (playBackPosition > 0) {
                            videoView.seekTo(playBackPosition);
                        }
                        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mediaPlayer) {

                                isComplete = true;
                                //recordStatistics();
                                recordUserStatistics();
                                time = 0;

                            }
                        });
                        videoView.setPlayPauseListener(new CustomVideoView.PlayPauseListener() {
                            @Override
                            public void onPlay() {
                                // Toast.makeText(Video.this, "Playing", Toast.LENGTH_SHORT).show();
                                handler.postDelayed(updateTime, 1000);

                            }

                            @Override
                            public void onPause() {

                                handler.removeCallbacks(updateTime);
                                isVideoPauseDataInserted = false;

                                //time = 0;
                            }
                        });
                    }
                });


            }
        }.execute();

    }

    private void recordStatistics() {
        try {
            String imei_number = CommonFunctions.getUniqueDeviceId(getApplicationContext());
            Statistics statistics = new Statistics(userId, imei_number, medium, subject, chapter, videoName, getContentType(), time, date);
            statistics.save();
            LastContent lastContent = new LastContent(userId, imei_number, medium, subject, chapter, videoName, getContentType(), time, date, vidPath, playBackPosition, key);
            lastContent.save();
        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
    }

    userStatistics userStatistics;

    private void recordUserStatistics() {
        try {
            String imei_number = CommonFunctions.getUniqueDeviceId(getApplicationContext());
            int minutes, seconds;

            Log.d("USerStatistics", "Video: " + "userID: " + userId + " CourseID: " + SettingPreffrences.getCourseId(getApplicationContext())
                    + " SubjectID: " + SettingPreffrences.getSubjectId(getApplicationContext()) + " ChapterID: " + SettingPreffrences.getChapterId(getApplicationContext())
                    + " ContentTypeID: " + String.valueOf(getContentTypeName_Kef()) + " ContentTypeName: " + videoName + " ContentDuration: " + conversionValue(videoView.getDuration()) + " AccessDuration: " + conversionValue(videoView.getCurrentPosition())
                    + " AccessDatetime: " + date + " CreatedOn: " + date);

            Log.d("SyncSubjectData", "video: " + SettingPreffrences.getSubjectId(getApplicationContext()));
            if (userStatistics == null) {
                userStatistics = new userStatistics(
                        userId,
                        SettingPreffrences.getCourseId(getApplicationContext()),
                        SettingPreffrences.getSubjectId(getApplicationContext()),
                        SettingPreffrences.getChapterId(getApplicationContext()),
                        String.valueOf(getContentTypeName_Kef()),
                        videoName,
                        conversionValue(videoView.getDuration()),
                        conversionValue(videoView.getCurrentPosition()),
                        date,
                        date);
                userStatistics.save();
            } else {
                userStatistics.delete();
                userStatistics = new userStatistics(
                        userId,
                        SettingPreffrences.getCourseId(getApplicationContext()),
                        SettingPreffrences.getSubjectId(getApplicationContext()),
                        SettingPreffrences.getChapterId(getApplicationContext()),
                        String.valueOf(getContentTypeName_Kef()),
                        videoName,
                        conversionValue(videoView.getDuration()),
                        conversionValue(videoView.getCurrentPosition()),
                        date,
                        date);
                userStatistics.save();
            }
            LastContent lastContent = new LastContent(userId, imei_number, medium, subject, chapter, videoName, getContentType(), time, date, vidPath, playBackPosition, key);
            lastContent.save();
        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
    }

    public String conversionValue(long millis) {
        long minutes = 0, seconds = 0;
        String sec, result;
        //  minutes = (int)(millis/1000)/60;
        seconds = (millis / 1000);
        if (seconds > 59) {
            minutes = seconds / 60;
            seconds = seconds % 60;
        }
        if (seconds < 10) {
            sec = "0" + seconds;
        } else {
            sec = seconds + "";
        }
        result = minutes + "." + sec;
        return result;
    }

    @SuppressWarnings("unchecked")
    private void decryptAES() {
        Log.d("VideoLogChecker", "In decryptAES: ");
        progressDialog.setTitle("Eclass");
        // progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMessage("Please wait while we prepare the video..");
        //progressDialog.setProgress(0);
        // progressDialog.setMax(100);
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {

                progressDialog.dismiss();
                onBackPressed();
                finish();


            }
        });


        new AsyncTask<Void, String, String>() {

            @Override
            protected String doInBackground(Void... params) {

                try {
                    performDecryption();
                    return "";
                } catch (Exception e) {
                    return "Failed";
                }

            }

            @Override
            protected void onPostExecute(String s) {

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }


                videoName = CommonFunctions.removeFormatSuffix(new File(vidPath).getName());

                if (s.equals("Failed")) {

                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (!isActivityStoped) {
                        alert("Cannot Play video");
                    }

                    return;

                }
                Log.d("VideoLogs", "decryptedVidPath: " + decryptedVidPath);
                videoView.setVideoPath(decryptedVidPath);
                videoView.setMediaController(new MediaController(videoView.getContext()));
                //Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        videoView.start();
                        videoView.requestFocus();
                        if (playBackPosition > 0) {
                            videoView.seekTo(playBackPosition);
                        }
                        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mediaPlayer) {

                                isComplete = true;
                                //recordStatistics();
                                recordUserStatistics();
                                time = 0;

                                boolean shouldPlayAll = getIntent().getBooleanExtra(ListSelectionActivity.SHOULD_PLAY_ALL, false);
                                if (shouldPlayAll) {
                                    Intent intent = new Intent();
                                    intent.putExtra(ListSelectionActivity.SHOULD_PLAY_ALL, true);
                                    setResult(Activity.RESULT_OK, intent);
                                    finish();
                                }

                            }
                        });
                        videoView.setPlayPauseListener(new CustomVideoView.PlayPauseListener() {
                            @Override
                            public void onPlay() {
                                // Toast.makeText(Video.this, "Playing", Toast.LENGTH_SHORT).show();
                                handler.postDelayed(updateTime, 1000);

                            }

                            @Override
                            public void onPause() {

                                handler.removeCallbacks(updateTime);
                                isVideoPauseDataInserted = false;
                                Log.d("VideoLogChecker", "videoPaused:");
                                //time = 0;
                            }
                        });
                        mediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                            @Override
                            public void onSeekComplete(MediaPlayer mediaPlayer) {
                                int currentPosition = mediaPlayer.getCurrentPosition();
                                int duration = mediaPlayer.getDuration();
                                if (currentPosition == duration) {
                                    boolean shouldPlayAll = getIntent().getBooleanExtra(ListSelectionActivity.SHOULD_PLAY_ALL, false);
                                    if (shouldPlayAll) {
                                        Intent intent = new Intent();
                                        intent.putExtra(ListSelectionActivity.SHOULD_PLAY_ALL, true);
                                        setResult(Activity.RESULT_OK, intent);
                                        finish();
                                    }
                                }
                            }
                        });
                    }
                });

            }
        }.execute();

    }


    public void performDecryption() throws Exception {

        FileInputStream fis = new FileInputStream(new File(vidPath));
        FileOutputStream fos = new FileOutputStream(decryptedVidFile);
        decrypt(fis, fos);

    }

    private void decrypt(InputStream is, OutputStream os) throws Exception {

        Realm realm = RealmDb.getRealm();

        SecretKeyMaterial sk = realm.where(SecretKeyMaterial.class).equalTo("id", key).findFirst();
        byte[] iv = sk.getIv();
        byte[] secretKey = sk.getSecretKey();

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(secretKey, "AES"), new IvParameterSpec(iv));

        CipherOutputStream cos = new CipherOutputStream(os, cipher);

        doCopy(is, cos);

    }


    public void doCopy(InputStream is, OutputStream os) throws Exception {

        byte[] bytes = new byte[4096];
        int numBytes;

//        try {

        while ((numBytes = is.read(bytes)) != -1) {
            os.write(bytes, 0, numBytes);
        }

        os.flush();
        os.close();
        is.close();

//        } catch (Exception e) {
//
//            e.printStackTrace();
//            Toast.makeText(getApplicationContext(), "No Space left in device", Toast.LENGTH_LONG).show();
//
//        }

    }


    @Override
    protected void onDestroy() {
        Log.d("VideoPlay", "onDestroy");
        handler.removeCallbacks(updateTime);
        Log.d("time", "onDestroy: " + time);
        try{
            boolean shouldPlayAll = getIntent().getBooleanExtra(ListSelectionActivity.SHOULD_PLAY_ALL, false);
            if(!shouldPlayAll) {
                ((App) getApplicationContext()).deleteTemp();
            }
        }catch(Exception e){

        }
        super.onDestroy();

    }



    private int getContentType() {
        int contentType;
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey("type")) {
            contentType = getIntent().getIntExtra("type", 1);
        } else {
            switch (fromWhichContent) {
                case "mm":
                    contentType = CONTENT_TYPE_MINDMAP;
                    break;
                case "content":
                    contentType = CONTENT_TYPE_CONTENT;
                    break;
                case "qb":
                    contentType = CONTENT_TYPE_QANDA;
                    break;
                default:
                    contentType = CONTENT_TYPE_CONTENT;
                    break;
            }
        }
        return contentType;
    }

    private int getContentTypeName_Kef() {
        int contentType;
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey("type")) {
            contentType = getIntent().getIntExtra("type", 1);
        } else {
            switch (fromWhichContent) {
                case "mm":
                    contentType = Integer.parseInt(KEF_ContentTypeId_MindMap);
                    break;
                case "content":
                    contentType = Integer.parseInt(KEF_ContentTypeId_Content);
                    break;
                case "qb":
                    contentType = Integer.parseInt(KEF_ContentTypeId_QNA);
                    break;
                default:
                    contentType = Integer.parseInt(KEF_ContentTypeId_Content);
                    break;
            }
        }
        return contentType;
    }


    @Override
    protected void onPause() {
        Log.d("VideoPlay", "onPause");
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        super.onPause();
        playBackPosition = videoView.getCurrentPosition();
        if (videoView != null) {
            Log.d("VideoStatus", "Video Not null  - InPause" + time + "::" + playBackPosition);
            videoView.pause();
        } else {

            File temp = new File(getExternalFilesDir(null), "temp/");
            File[] fileList = temp.listFiles();
            if (fileList != null) {
                Log.d("VideoLogChecker", "onPause1: " + "fileList: IsNotNull: length: ");
                for (int i = 0; i < fileList.length; i++) {
                    //Log.d("VideoLogChecker","onPause1: "+"fileList: IsNotNull: length: "+fileList[i].getAbsolutePath());
                    if (fileList[i].getAbsolutePath().contains(".mp4")) {
                        fileList[i].delete();
                    }
                }
            }


            File temp1 = new File(Environment.getExternalStorageDirectory(), "temp/");
            File[] fileList1 = temp1.listFiles();

            if (fileList1 != null) {
                Log.d("VideoLogChecker", "onPause2: " + "fileList: IsNotNull: length: ");
                for (int i = 0; i < fileList1.length; i++) {
                    //Log.d("VideoLogChecker","onPause2: "+"videoPath: "+fileList[i].getAbsolutePath());
                    if (fileList1[i].getAbsolutePath().contains(".mp4") || fileList1[i].getAbsolutePath().contains(".xls")) {
                        fileList1[i].delete();
                    }
                }
            }
        }
        if (!notVideo && !isVideoPauseDataInserted && !isComplete && time > 0) {
            //recordStatistics();
            recordUserStatistics();
            time = 0;
        }
        //finish();

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("VideoPlay", "onRestart");
    }

    @Override
    protected void onLayout() {

        videoView = (CustomVideoView) findViewById(R.id.videoView);
        lnr_backward_skip = (LinearLayout) findViewById(R.id.lnr_backward_skip);
        lnr_forward_skip = (LinearLayout) findViewById(R.id.lnr_forward_skip);
        img_back = (ImageView) findViewById(R.id.img_back);
        img_forwardl = (ImageView) findViewById(R.id.img_forward);

        userId = SettingPreffrences.getUserid(getApplicationContext()).isEmpty() ? "0" : SettingPreffrences.getUserid(getApplicationContext());

    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.d("VideoPlay", "onStop");
        if (videoView != null && videoView.canPause()) {
            Log.d("VideoStatus", "Video Not null  - InStop" + "::" + playBackPosition);
            videoView.pause();
        } else {
            isActivityStoped = true;
        }

    }

    @Override
    public void onAnimationStart(Animation animation) {
        if (animation == animateForward) {
            img_forwardl.setVisibility(View.VISIBLE);
        }
        if (animation == animateBackwrd) {
            img_back.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {

        if (animation == animateForward) {
            img_forwardl.setVisibility(View.INVISIBLE);
        }
        if (animation == animateBackwrd) {
            img_back.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
