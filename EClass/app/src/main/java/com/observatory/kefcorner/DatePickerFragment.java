package com.observatory.kefcorner;// Created by kamlesh on 18/6/16, 1:01 PM


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.util.Log;
import android.widget.DatePicker;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {
    private static final String TAG = "DatePickerFragment";

    public static final long DATE_NOT_SET = 0;

    public static final int SELECT_FROM_DATE = 1;
    public static final int SELECT_TO_DATE = 2;


    public static DialogFragment newInstance(long fromDate, long toDate, int whichDate) {
        Bundle args = new Bundle();
        args.putLong("fromDate", fromDate);
        args.putLong("toDate", toDate);
        args.putInt("whichDate", whichDate);
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        int whichDate = getArguments().getInt("whichDate");

        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);

        long fromDate = getArguments().getLong("fromDate");

        long toDate = getArguments().getLong("toDate");

        // user is coming to select From Date
        if (whichDate == SELECT_FROM_DATE) {

                // when To Date is set, it should be the max. date otherwise today's date will be max.
                if (toDate != DATE_NOT_SET) {
                    datePickerDialog.getDatePicker().setMaxDate(toDate);
                } else {
                    datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                }

            // user is coming to select To Date
        } else {

            // when From Date is set, it serves at min. date otherwise no need to set any min. date. Today's date is always max. date.
            if (fromDate != DATE_NOT_SET) {
                datePickerDialog.getDatePicker().setMinDate(fromDate);
                datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
            } else {
                datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
            }
        }


        try {

            Calendar selDate = Calendar.getInstance();

            long selectedDate = getSelectedDate(whichDate, fromDate, toDate, selDate);

                selDate.setTimeInMillis(selectedDate);

            datePickerDialog.getDatePicker()
                    .updateDate(selDate.get(Calendar.YEAR), selDate.get(Calendar.MONTH), selDate
                            .get(Calendar.DATE));

        } catch (Exception e) {
            Log.d(TAG, "onCreateDialog: Selected date not found");
        }
        return datePickerDialog;
    }

    private long getSelectedDate(int whichDate, long fromDate, long toDate, Calendar selDate) {
        long selectedDate;
        if (whichDate == SELECT_FROM_DATE) {

            if (fromDate != DATE_NOT_SET) {
                selectedDate = fromDate;
            } else {
                selectedDate = selDate.getTimeInMillis();
            }
        }
        else {

            if (toDate != DATE_NOT_SET) {
                selectedDate = toDate;
            } else {
                selectedDate = selDate.getTimeInMillis();
            }
        }
        return selectedDate;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        // NOTE : month starts from 0 so we +1 before passing it.
        ((DatePickerFragment.OnDateSetListener) getActivity()).onDateSet(view, year, month, day, getArguments()
                .getInt("whichDate"));
    }


    public interface OnDateSetListener {
        void onDateSet(DatePicker view, int year, int month, int day, int whichDate);
    }
}