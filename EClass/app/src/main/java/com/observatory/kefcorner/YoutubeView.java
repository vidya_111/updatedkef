package com.observatory.kefcorner;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

/**
 * Created by Anuj on 08-01-2016.
 */
public class YoutubeView extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    private static final String TAG = "YoutubeView";
    // YouTube player view
    private YouTubePlayerView youTubeView;
    private String id;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.youtube_view);

        try {

            if (getIntent().getExtras() != null) {
                id = getIntent().getStringExtra("id");
            }
            youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
            Log.e(TAG, "onCreate: " + youTubeView);

            byte[] data1 = Base64.decode(BuildConfig.YOUTUBE_APIKEY, Base64.DEFAULT);
            String value = new String(data1, "UTF-8");
            // Initializing video player with developer key
            youTubeView.initialize(value, this);
            //Intent intent = YouTubeStandalonePlayer.createVideoIntent(this, BuildConfig.YOUTUBE_APIKEY, id);
            //startActivity(intent);
        } catch (Exception e) {

        }


    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (youTubeView != null) {
            youTubeView = null;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

        youTubePlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE);
        youTubePlayer.loadVideo(id);

        // Hiding player controls
        Log.d("DemoVideoLogs","InitializeYouTubePlayer");
        youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (youTubeView != null) {
            youTubeView = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d("DemoVideoLogs", "onBackPressed");
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {}
}
