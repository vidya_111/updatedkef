package com.observatory.kefcorner;

/**
 * Created by gaurav on 15/1/18.
 */

public class UserHistoryViewModel extends UserViewModel {

    public String date;
    public String time;

    public UserHistoryViewModel(String date, String time) {
        this.date = date;
        this.time = time;
        itemViewType = UserViewModel.VIEW_USER_HISTORY;
    }
}
