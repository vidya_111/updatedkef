package com.observatory.kefcorner;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.core.content.FileProvider;

import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.observatory.Assessment.ConstantManager;
import com.observatory.adapters.LangClassAdapter;
import com.observatory.database.Master;
import com.observatory.database.Subject;
import com.observatory.database.userStatistics;
import com.observatory.mcqdatabase.McqTestTable;
import com.observatory.mcqmodels.McqContentModel;
import com.observatory.mcqmodels.McqModel;
import com.observatory.mcqui.MCQActivity;
import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.Constants;
import com.observatory.utils.FilenameComparator;
import com.observatory.utils.SettingPreffrences;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.observatory.utils.Constants.KEF_ContentTypeId_PDF;


public class Content extends BaseActivity {

    public static final String SUBJECT = "subject";
    public static final String CHAPTER = "chapter";
    public static final String CHAPTER_NAME = "name";

    private ListView lstContent;
    private LangClassAdapter langClassAdapter;
    private ArrayList<Integer> images = new ArrayList<Integer>();
    private ArrayList<String> text = new ArrayList<String>();
    /*new int[]{R.drawable.ic_content_chapter,R.drawable.ic_mind_map,R.drawable.ic_q_n_a};*/
    //private String[] texts = new String[]{"CHAPTER", "MIND MAP", "Q & A", "STATISTICS"};
    private int[] colors = new int[]{R.color.option_chapter, R.color.option_mindmap, R.color.option_qna, R.color.option_pdf, R.color.option_mcq, R.color.option_statistics};
    private Subject subject;
    private String VIDEO_PATH = "";
    private HashMap<String, String> dimensions;
    private String[] texts;
    private String[] mArray;
    private List<McqModel> mcqList;
    private TextView tvContentNotFound;
    Boolean isHavingMultipleStandards = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        isHavingMultipleStandards = App.IS_HAVING_MULTIPLE_STANDARD;
        Log.d("PrintData ", getIntent().getStringExtra(SUBJECT) + ":: " + getIntent().getStringExtra(CHAPTER_NAME) + "::" + App.IS_HAVING_MULTIPLE_STANDARD);
        subject = Subject.find(Subject.class, "subject_name = ? and chapter_name = ?", new String[]{getIntent().getStringExtra(SUBJECT), getIntent().getStringExtra(CHAPTER_NAME)}).get(0);
        //subject.setMindMap(false);

        images.clear();

        int contentCount = Integer.parseInt(subject.getContent());
        if (contentCount > 0) {
            images.add(R.drawable.ic_content_chapter);
            text.add("CHAPTER");
        }

        String mindMap = subject.getMindMap();
        // if Mindmap is present; show it as an option
        if (!mindMap.equalsIgnoreCase("NO")) {
            images.add(R.drawable.ic_mind_map);
            text.add("MIND MAP");

        }

        String qAndA = subject.getqNa();
        // if Q & A is present; show it as an option
        if (!qAndA.equalsIgnoreCase("NO")) {
            images.add(R.drawable.ic_q_n_a);
            text.add("Q & A");

        }

        int pdfCount = Integer.parseInt(subject.getPdf());
        if (pdfCount > 0) {
            images.add(R.drawable.ic_pdf);
            text.add("PDF");
        }

        int chapSerialNo = Integer.parseInt(subject.getMcq());
        if (chapSerialNo > 0) {
            images.add(R.drawable.ic_mcq);
            text.add("MCQ");
        }

        images.add(R.drawable.ic_statistics);
        text.add("STATISTICS");

        if (hasNoContent(contentCount, pdfCount, mindMap, qAndA, chapSerialNo)) {
            tvContentNotFound.setVisibility(View.VISIBLE);
            lstContent.setVisibility(View.GONE);
        }

        int[] imagesArray = new int[images.size()];


        for (int i = 0; i < images.size(); i++) {
            imagesArray[i] = images.get(i);

        }

        texts = new String[text.size()];
        texts = text.toArray(texts);

        langClassAdapter = new LangClassAdapter(imagesArray, texts, colors, brandonBold);
        lstContent.setAdapter(langClassAdapter);
        lstContent.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        lstContent.setSelector(R.color.light_grey);

        lstContent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                VIDEO_PATH = App.BASE_PATH + getIntent().getStringExtra(SUBJECT) + "/" + getIntent().getIntExtra(CHAPTER, 1) + "/";


                parent.getAdapter().getItem(position);

                if (text.get(position).equals("CHAPTER")) {
                    openContent();
                } else if (text.get(position).equals("PDF")) {
                    openPdf();
                } else if (text.get(position).equals("MCQ")) {
                    openMcq();
                } else if (text.get(position).equals("MIND MAP")) {
                    openMindmap();
                } else if (text.get(position).equals("Q & A")) {
                    openQAndA();
                } else {
                    openStatistics();
                }

            }
        });


    }


    private void openStatistics() {
        App.getInstance().trackEvent("Type Selection", "Screen", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " + "selectedStandard - " + SettingPreffrences.getStandard(Content.this) + " ; " + "selectedSubject - " + SettingPreffrences.getSubject(Content.this) + " ; " + "selectedChapter - " + getIntent().getStringExtra(CHAPTER_NAME) + " ; " + "" + "category - statistics", "");
        Intent intent = new Intent(getApplicationContext(), StatisticsActivity.class);
        startActivity(intent);
    }

    private void openQAndA() {

        String qAndA = subject.getqNa();

        String qAndADirPath;

        // old SD Cards had a single Q & A file in current Chapters folder
        if (qAndA.equalsIgnoreCase("YES")) {
            openQAndA(VIDEO_PATH + "qna.zip");
        } else if (qAndA.equals("1")) {
            // new cards : if there is a single Q & A video, open it directly
            qAndADirPath = VIDEO_PATH + "Q and A/";
            File qAndADir = new File(qAndADirPath);
            String[] fileNames = qAndADir.list();
            String fileToOpen = null;

            for (String fileName : fileNames) {
                if (CommonFunctions.isVideoOrPdf(fileName)) {
                    fileToOpen = fileName;
                    break;
                }
            }

            String qAndAPath = qAndADirPath + fileToOpen;
            openQAndA(qAndAPath);
        } else {
            // new cards : when there are multiple videos, we show selection screen
            openQAndADialog();
        }
    }

    private void openQAndADialog() {
        String qAndADirPath;
        qAndADirPath = VIDEO_PATH + "Q and A/";
        final File qAndADir = new File(qAndADirPath);
        mArray = removeFormatSuffixAndOtherFiles(qAndADir);
        if (mArray != null && mArray.length > 0) {
            Intent intent = new Intent(this, ListSelectionActivity.class);
            intent.putExtra(ListSelectionActivity.TITLE, "Select Q and A");
            intent.putExtra(ListSelectionActivity.VIDEO_PATH, VIDEO_PATH);
            intent.putExtra(ListSelectionActivity.LIST, mArray);
            intent.putExtra(ListSelectionActivity.CONTENT_TYPE, ListSelectionActivity.RQ_QANDA);
            startActivity(intent);
        } else {
            alert("Content not available.");
        }
    }

    private void openMindmap() {

        String mindMap = subject.getMindMap();

        String mindmapDirPath;

        // old SD Cards had a single mindmap file in current Chapters folder
        if (mindMap.equalsIgnoreCase("YES")) {
            openMindmap(VIDEO_PATH + "mindmap.zip");
        } else if (mindMap.equals("1")) {
            // new cards : if there is a single mindmap video, open it directly
            mindmapDirPath = VIDEO_PATH + "Mind Map/";
            File mindmapDir = new File(mindmapDirPath);
            String[] fileNames = mindmapDir.list();
            String fileToOpen = null;

            for (String fileName : fileNames) {
                if (CommonFunctions.isVideoOrPdf(fileName)) {
                    fileToOpen = fileName;
                    break;
                }
            }

            String mindmapPath = mindmapDirPath + fileToOpen;
            openMindmap(mindmapPath);
        } else {
            // new cards : when there are multiple videos, we show selection screen
            openMindmapDialog();
        }

    }

    private void openMindmapDialog() {
        String mindmapDirPath;
        mindmapDirPath = VIDEO_PATH + "Mind Map/";
        final File mindmapDir = new File(mindmapDirPath);
        mArray = removeFormatSuffixAndOtherFiles(mindmapDir);
        if (mArray != null && mArray.length > 0) {
            Intent intent = new Intent(this, ListSelectionActivity.class);
            intent.putExtra(ListSelectionActivity.TITLE, "Select Mind Map");
            intent.putExtra(ListSelectionActivity.VIDEO_PATH, VIDEO_PATH);
            intent.putExtra(ListSelectionActivity.LIST, mArray);
            intent.putExtra(ListSelectionActivity.CONTENT_TYPE, ListSelectionActivity.RQ_MINDMAP);
            startActivity(intent);
        } else {
            alert("Content not available.");
        }
    }

    private void openMindmap(String mindmapPath) {
        System.out.println("Mind Map Path for 1 " + mindmapPath);
        Intent videoIntent = new Intent(this, Video.class);
        videoIntent.putExtra(getString(R.string.from), "mm");
        videoIntent.putExtra(Video.VIDEO_PATH, mindmapPath);
        startActivity(videoIntent);

        App.getInstance().trackEvent("Type Selection", "Screen", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " + "selectedStandard - " + SettingPreffrences.getStandard(Content.this) + " ; " + "selectedSubject - " + SettingPreffrences.getSubject(Content.this) + " ; " + "selectedChapter - " + getIntent().getStringExtra(CHAPTER_NAME) + " ; " + "" + "category - mindmap", mindmapPath);
    }

    private void openQAndA(String qAndAPath) {
        System.out.println("Q and A Path for 1 " + qAndAPath);
        Intent videoIntent = new Intent(this, Video.class);
        videoIntent.putExtra(getString(R.string.from), "qb");
        videoIntent.putExtra(Video.VIDEO_PATH, qAndAPath);
        startActivity(videoIntent);

        App.getInstance().trackEvent("Type Selection", "Screen", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " + "selectedStandard - " + SettingPreffrences.getStandard(Content.this) + " ; " + "selectedSubject - " + SettingPreffrences.getSubject(Content.this) + " ; " + "selectedChapter - " + getIntent().getStringExtra(CHAPTER_NAME) + " ; " + "" + "category - q&a", qAndAPath);
    }

    private void openMcq() {
        int chapSerialNo = Integer.parseInt(subject.getMcq());
        McqTestTable mcqTestTable = new McqTestTable();
        mcqList = mcqTestTable.getMcqTest(chapSerialNo);
        int listSize = mcqList.size();
        if (listSize > 1) {
            // open dialog for multiple mcq
            openMcqDialog();

            // open single mcq
        } else if (listSize == 1) {
            // open single mcq
            openMcq(mcqList.get(0));

            // no mcqs found
        } else {
            Toast.makeText(getApplicationContext(), "Could not find any MCQs for this chapter!", Toast.LENGTH_LONG).show();
        }
    }

    private void openPdf() {
        if (Integer.parseInt(subject.getPdf()) > 1) {
            openPdfDialog();

        } else {

            String pdfDirPath = App.BASE_PATH + getIntent().getStringExtra(SUBJECT) + "/" + getIntent().getIntExtra(CHAPTER, 1) + "/PDF/";
            File pdfDir = new File(pdfDirPath);
            String[] fileNames = pdfDir.list();
            String fileToOpen = null;

            for (String fileName : fileNames) {
                if (CommonFunctions.isVideoOrPdf(fileName)) {
                    fileToOpen = fileName;
                    break;
                }
            }

            String pdfPath = pdfDirPath + fileToOpen;
            openPdf(pdfPath);
        }
    }

    private void openContent() {

        Log.d("openContent", "::" + subject.getContent());

        if (Integer.parseInt(subject.getContent()) > 1) {
            openContentDialog();
        } else {
            String videoDirPath = VIDEO_PATH + "Content/";
            File videoDir = new File(videoDirPath);
            String[] fileNames = videoDir.list();
            if (fileNames == null || fileNames.length == 0) {
                alert("Content not available.");

            } else {
                String fileToOpen = null;
                Log.d("DemoVideoLogs", "InContent: VideoPath: " + videoDirPath);
                for (String fileName : fileNames) {
                    if (CommonFunctions.isVideoOrPdf(fileName)) {
                        fileToOpen = fileName;
                        break;
                    }
                }

                String videoPath = videoDirPath + fileToOpen;
                openContent(videoPath);
            }
        }
    }

    private boolean hasNoContent(int contentCount, int pdfCount, String mindMap, String qAndA, int chapSerialNo) {

        return contentCount == 0 && pdfCount == 0 && mindMap.equalsIgnoreCase("NO") && qAndA.equalsIgnoreCase("NO") && chapSerialNo == 0;
    }

    private void openMcq(McqModel mcq) {
        Intent intent = new Intent(Content.this, MCQActivity.class);
        intent.putExtra(McqContentModel.MCQ_SERIAL_NO, mcq.mcq_id);
        intent.putExtra(McqContentModel.MCQ_NAME, mcq.chapter_name);
        intent.putExtra(McqContentModel.MCQ_FONT, mcq.font);
        startActivity(intent);
    }


    /*    private void openPdf(String pdfPath) {

        File file = new File(pdfPath);
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(file), "application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        Intent intent = Intent.createChooser(target, "Open File");
        startActivity(intent);
    }*/


    private void openPdf(String filePath) {
        File file = new File(filePath);
        Uri fileUri = null;
        //srj

        //
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            fileUri = FileProvider.getUriForFile(Content.this, BuildConfig.APPLICATION_ID + ".provider", file);
        } else {
            fileUri = Uri.fromFile(file.getAbsoluteFile());
        }

        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(fileUri, "application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        target.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
            String userId = SettingPreffrences.getUserid(getApplicationContext()).isEmpty() ? "0" : SettingPreffrences.getUserid(getApplicationContext());
            String imei_number = CommonFunctions.getUniqueDeviceId(getApplicationContext());
            Master master = Master.listAll(Master.class).get(0);
            String medium = master.getMedium();
            String chapter = CommonFunctions.removeApostrophe(SettingPreffrences.getChapter(this));
            String subject = CommonFunctions.removeApostrophe(SettingPreffrences.getSubject(this));
            String date = new SimpleDateFormat(Constants.DATABASE_DATE_FORMAT).format(new Date());
            /*Statistics statistics = new Statistics(userId, imei_number, medium, subject, chapter, CommonFunctions.removeFormatSuffix(file.getName()), CONTENT_TYPE_PDF, 0, date);
            statistics.save();*/
            Log.d("SyncSubjectData", "AddContent: " + SettingPreffrences.getSubjectId(getApplicationContext()));
            userStatistics userStatistics = new userStatistics(
                    userId,
                    SettingPreffrences.getCourseId(getApplicationContext()),
                    SettingPreffrences.getSubjectId(getApplicationContext()),
                    SettingPreffrences.getChapterId(getApplicationContext()),
                    KEF_ContentTypeId_PDF,
                    CommonFunctions.removeFormatSuffix(file.getName()),
                    "0",
                    "0",
                    date,
                    date);
            userStatistics.save();

        } catch (ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(), "Please install a PDF Viewer to view this " + "document", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Select video");


        for (int i = 0; i < Integer.parseInt(subject.getContent()); i++) {

            menu.add(Menu.NONE, (i + 1), Menu.NONE, "Part " + (i + 1));

        }

    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {

        // Log.v(tag,"id: "+item.getItemId());

        // VIDEO_PATH=VIDEO_PATH+"Content/"+item.getItemId()+".mp4";
        VIDEO_PATH = VIDEO_PATH + "Content/" + item.getItemId() + ".zip";
        Intent videoIntent = new Intent(this, Video.class);
        videoIntent.putExtra(Video.VIDEO_PATH, VIDEO_PATH);
        startActivity(videoIntent);

        return super.onContextItemSelected(item);

    }


    @Override
    protected void onLayout() {

        lstContent = (ListView) findViewById(R.id.lstContent);
        tvContentNotFound = (TextView) findViewById(R.id.tvNoContentFound);


    }

    private void openPdfDialog() {
        final String pdfDirPath = App.BASE_PATH + getIntent().getStringExtra(SUBJECT) + "/" + getIntent().getIntExtra(CHAPTER, 1) + "/PDF/";
        File pdfDir = new File(pdfDirPath);
        mArray = removeFormatSuffixAndOtherFiles(pdfDir);
        if (mArray != null && mArray.length > 0) {
            Intent intent = new Intent(this, ListSelectionActivity.class);
            intent.putExtra(ListSelectionActivity.TITLE, "Select PDF");
            intent.putExtra(ListSelectionActivity.VIDEO_PATH, VIDEO_PATH);
            intent.putExtra(ListSelectionActivity.LIST, mArray);
            intent.putExtra(ListSelectionActivity.CONTENT_TYPE, ListSelectionActivity.RQ_PDF);
            startActivity(intent);
        } else {
            alert("Content not available.");
        }
    }

    private void openMcqDialog() {
        mArray = new String[mcqList.size()];

        for (int i = 0; i < mcqList.size(); i++) {
            mArray[i] = mcqList.get(i).chapter_name;
        }

        Intent intent = new Intent(this, ListSelectionActivity.class);
        String subName = getIntent().getStringExtra(SUBJECT);
        String chapName = getIntent().getStringExtra(CHAPTER_NAME);
        intent.putExtra(ListSelectionActivity.SUBJECT_NAME_FOR_MCQ, subName);
        intent.putExtra(ListSelectionActivity.CHAPTER_NAME_FOR_MCQ, chapName);
        intent.putExtra(ListSelectionActivity.TITLE, "Select MCQ");
        intent.putExtra(ListSelectionActivity.VIDEO_PATH, VIDEO_PATH);
        intent.putExtra(ListSelectionActivity.LIST, mArray);
        intent.putExtra(ListSelectionActivity.CONTENT_TYPE, ListSelectionActivity.RQ_MCQ);
        startActivity(intent);
    }

    private void openContentDialog() {
        final String videoDirPath = VIDEO_PATH + "Content/";
        final File videoDir = new File(videoDirPath);
        mArray = removeFormatSuffixAndOtherFiles(videoDir);
        if (mArray != null && mArray.length > 0) {
            Intent intent = new Intent(this, ListSelectionActivity.class);
            intent.putExtra(ListSelectionActivity.TITLE, "Select Content");
            intent.putExtra(ListSelectionActivity.VIDEO_PATH, VIDEO_PATH);
            intent.putExtra(ListSelectionActivity.LIST, mArray);
            intent.putExtra(ListSelectionActivity.CONTENT_TYPE, ListSelectionActivity.RQ_CONTENT);
            startActivity(intent);
        } else {
            alert("Content not available.");
        }

    }

    private String[] removeFormatSuffixAndOtherFiles(File dir) {
        try {
            String[] fileNameList = dir.list();
            Arrays.sort(fileNameList, new FilenameComparator());
            List<String> fileList = new ArrayList<>();

            for (String fileName : fileNameList) {
                if (CommonFunctions.isVideoOrPdf(fileName)) {


                    fileName = CommonFunctions.removeFormatSuffix(fileName);
                    fileList.add(fileName);
                }
            }

            return fileList.toArray(new String[fileList.size()]);
        } catch (Exception e) {

        }
        return null;
    }

    @Override
    public void onBackPressed() {
        Log.d("PrintCheckerHere", App.IS_HAVING_MULTIPLE_STANDARD + " :: " + ConstantManager.ISSINGLESTANDARD);
        if (isHavingMultipleStandards) {

            Intent intent = new Intent(getApplicationContext(), Chapters.class);
            intent.putExtra(Chapters.SUBJECT_NAME, getIntent().getStringExtra(SUBJECT));
            intent.putExtra(Content.CHAPTER_NAME, getIntent().getStringExtra(CHAPTER_NAME));
            intent.putExtra(Content.CHAPTER, getIntent().getStringExtra(CHAPTER));
            startActivity(intent);
            finish();
        } else {
            if (ConstantManager.ISSINGLESTANDARD) {
                Intent intent = new Intent(getApplicationContext(), Chapters.class);
                intent.putExtra(Content.SUBJECT, getIntent().getStringExtra(SUBJECT));
                intent.putExtra(Content.CHAPTER_NAME, getIntent().getStringExtra(CHAPTER_NAME));
                intent.putExtra(Content.CHAPTER, getIntent().getStringExtra(CHAPTER));
                startActivity(intent);
                finish();
            } else {
                super.onBackPressed();
            }
        }

    }

    private void openContent(String videoPath) {
        System.out.println("Video Path for 1 " + videoPath);
        Log.d("videoPath", videoPath + "standard: " + SettingPreffrences.getStandard(Content.this) + " Chapter: " + SettingPreffrences.getChapter(Content.this) + " selectedChapter: " + getIntent().getStringExtra(CHAPTER_NAME));
        Intent videoIntent = new Intent(this, Video.class);
        videoIntent.putExtra(getString(R.string.from), "content");
        videoIntent.putExtra(Video.VIDEO_PATH, videoPath);
        startActivity(videoIntent);

        App.getInstance().trackEvent("Video", "Screen", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " + "selectedStandard - " + SettingPreffrences.getStandard(Content.this) + "" + " ; " + "selectedSubject - " + SettingPreffrences.getSubject(Content.this) + " ;" + " " + "selectedChapter - " + getIntent().getStringExtra(CHAPTER_NAME), videoPath);
    }

}
