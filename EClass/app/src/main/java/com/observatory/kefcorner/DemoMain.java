package com.observatory.kefcorner;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.observatory.adapters.LangClassAdapter;
import com.observatory.database.Subject;
import com.observatory.utils.BaseActivity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Anuj on 08-01-2016.
 */
public class DemoMain extends BaseActivity {


    private ListView lstContent;

    private LangClassAdapter langClassAdapter;

    private ArrayList<Integer> images = new ArrayList<Integer>();
    private ArrayList<String> text = new ArrayList<String>();
    private ArrayList<String> colr = new ArrayList<String>();
    private int[] colors = new int[]{R.color.option_chapter, R.color.option_mindmap, R.color.option_qna, R.color.std6};

    private Subject subject;


    private String VIDEO_PATH = "";

    Intent videoIntent;
    private HashMap<String, String> dimensions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);


//        dimensions = new HashMap<>();
//        dimensions.put("userId", getUserId(context));
//        CommonFunctions.sendAnalytics("videoDemoMain", dimensions);


        images.clear();
        images.add(R.drawable.ic_content_chapter);
        text.add("CONTENT");
        colr.add(String.valueOf(R.color.option_chapter));


        images.add(R.drawable.ic_mind_map);
        text.add("MIND MAP");
        colr.add(String.valueOf(R.color.option_mindmap));


        images.add(R.drawable.ic_q_n_a);
        text.add("Q & A");
        colr.add(String.valueOf(R.color.option_qna));


        int[] imagesArray = new int[images.size()];


        for (int i = 0; i < images.size(); i++) {
            imagesArray[i] = images.get(i);

        }

        String[] texts = new String[text.size()];
        texts = text.toArray(texts);


        langClassAdapter = new LangClassAdapter(imagesArray, texts, colors, brandonBold);
        lstContent.setAdapter(langClassAdapter);
        // registerForContextMenu(lstContent);


        lstContent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(DemoMain.this, DemoContent.class);
                if (position == 0) {

                    intent.putExtra("type", 0);

                } else if (position == 1) {

                    intent.putExtra("type", 1);
                } else {
                    intent.putExtra("type", 2);
                }

                startActivity(intent);


            }
        });


    }


    @Override
    protected void onLayout() {

        lstContent = (ListView) findViewById(R.id.lstContent);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.demo_main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {


            case R.id.profile:

                startActivity(How.class);
                break;
        }


        return super.onOptionsItemSelected(item);
    }


}

