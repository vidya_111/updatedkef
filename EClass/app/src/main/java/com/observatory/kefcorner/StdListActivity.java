package com.observatory.kefcorner;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.observatory.utils.BaseActivity;

import java.util.ArrayList;

public class StdListActivity extends BaseActivity {

    private ListView lstChapter;

    private ArrayList<StandardItem> standard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_std_list);
    }

    @Override
    protected void onLayout() {
        lstChapter = (ListView) findViewById(R.id.lstChapter);
        generateStandardList();
        SimpleArrayAdapter adapter = new SimpleArrayAdapter(this, standard);
        lstChapter.setAdapter(adapter);
        lstChapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent demo = new Intent(StdListActivity.this, DemoContent.class);
                demo.putExtra("playlist_id", standard.get(position).listID);
                startActivity(demo);
            }
        });
    }

    private void generateStandardList() {
        standard = new ArrayList<>();
        standard.add(new StandardItem("STANDARD 1", "PLWabNnw-zhrywh3FikMdf7oC7lPERFzSu", R.drawable.ic_standard1));
        standard.add(new StandardItem("STANDARD 2", "PLWabNnw-zhrx3mx6fYCgTeAH0tPkOIx8d", R.drawable.ic_standard2));
        standard.add(new StandardItem("STANDARD 3", "PLWabNnw-zhrxiMGXegWIzifMZg6n9Jbxt", R.drawable.ic_standard3));
        standard.add(new StandardItem("STANDARD 4", "PLWabNnw-zhrymDjz7fvKuf7A2avhOr_yZ", R.drawable.ic_standard4));
        standard.add(new StandardItem("STANDARD 5", "PLWabNnw-zhrzlt6lsMxw0gnN8NqWy115d", R.drawable.ic_standard5));
        standard.add(new StandardItem("STANDARD 6", "PLWabNnw-zhrwthGvoTXT-HnEw43Iw5Ilp", R.drawable.ic_standard6));
        standard.add(new StandardItem("STANDARD 7", "PLWabNnw-zhrwNRv2KcfOYlRpPq9hTmny7", R.drawable.ic_standard7));
        standard.add(new StandardItem("STANDARD 8", "PLWabNnw-zhrxuykhgpuqVpOs0Wi15QAmW", R.drawable.ic_standard8));
        standard.add(new StandardItem("STANDARD 9", "PLWabNnw-zhryC9-oTtJoEWdXSrrusjymS", R.drawable.ic_standard9));
        standard.add(new StandardItem("STANDARD 10", "PLWabNnw-zhrx7mLDBaZXx9aym0isNMLeg", R.drawable.ic_standard10));

    }

    private class StandardItem {
        String name;
        String listID;
        int drawableID;

        StandardItem(String name, String listID, int drawableID) {
            this.name = name;
            this.listID = listID;
            this.drawableID = drawableID;
        }
    }

    private class SimpleArrayAdapter extends BaseAdapter {
        ArrayList<StandardItem> subjects;
        private Context context;

        public SimpleArrayAdapter(Context context, ArrayList<StandardItem> subjects) {
            this.context = context;
            this.subjects = subjects;

        }

        @Override
        public int getCount() {
            return subjects.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder;

            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lang_class, parent, false);


                viewHolder.txtChapterName = (TextView) convertView.findViewById(R.id.txt);
                viewHolder.thumbNail = ((ImageView) convertView.findViewById(R.id.img));


                convertView.setTag(viewHolder);
            }

            viewHolder = (ViewHolder) convertView.getTag();


            viewHolder.txtChapterName.setText(subjects.get(position).name);
            viewHolder.thumbNail.setImageDrawable(getResources().getDrawable(subjects.get(position).drawableID));
            //Glide.with(context).load("http://img.youtube.com/vi/"+subjects.get(position).getFolderName()+"/0.jpg").into(viewHolder.thumbnail);


            return convertView;
        }


        private class ViewHolder {
            TextView txtChapterName;
            ImageView thumbNail;
        }
    }


}