package com.observatory.kefcorner;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.observatory.Assessment.Reports.ReportsActivity;
import com.observatory.adapters.UserProfileAdapter;
import com.observatory.database.LastContent;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.Constants;
import com.observatory.utils.EmailHandler;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;
import com.orm.query.Condition;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

/**
 * Created by anuj on 09/03/17.
 */

public class UserHistory extends BaseActivity {

    private RecyclerView rvUserHistory;
    private ArrayList<com.observatory.database.UserHistory> arrayUserHistory;
    private ArrayList<LastContent> arraysta;
    private UserProfileAdapter adapter;
    private TextView tvName, tv_standardName, tv_subjectName, tv_chapterName, tv_contentTitle, tv_content_lastSeen;
    private ImageView ivPP;
    private Button btEmail;
    int playBackPosition = 0;
    String userid = "", videopath = "";
    RelativeLayout rl_lastSeenContent;
    LinearLayout date_picker_layout;
    TextView start_date, end_Date;
    DateRangeCalendarView calendar;
    Button btn_Done, btn_Cancel;
    boolean isCheckDate = false;
    DateFormat dateFormat, df;
    String startdatetime = "", enddatetime = "", display_startdate = "", display_enddate = "", current_date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_history);

        rl_lastSeenContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent videoIntent = new Intent(UserHistory.this, Video.class);
                videoIntent.putExtra(getString(R.string.from), "content");
                videoIntent.putExtra(Video.VIDEO_PATH, videopath);
                videoIntent.putExtra("position", playBackPosition);
                startActivity(videoIntent);
            }
        });
    }

    @Override
    protected void onLayout() {

        actionBar.setTitle("User History");
        rvUserHistory = (RecyclerView) findViewById(R.id.rv_user_history);
        tvName = (TextView) findViewById(R.id.tv_userName);
        ivPP = (ImageView) findViewById(R.id.iv_profilepic);
        btEmail = (Button) findViewById(R.id.button2);
        tv_standardName = (TextView) findViewById(R.id.tv_standardName);
        tv_subjectName = (TextView) findViewById(R.id.tv_subjectName);
        tv_chapterName = (TextView) findViewById(R.id.tv_chapterName);
        tv_contentTitle = (TextView) findViewById(R.id.tv_contentTitle);
        rl_lastSeenContent = (RelativeLayout) findViewById(R.id.rl_lastSeenContent);
        tv_content_lastSeen = (TextView) findViewById(R.id.tv_content_lastSeen);
        btEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSendLoginHistoryDialog();
            }
        });

        Calendar current_calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd-MMMM-yyyy ");
        df = new SimpleDateFormat("yyyy-MM-dd");
        startdatetime = df.format(current_calendar.getTime());
        enddatetime = df.format(current_calendar.getTime());
        date_picker_layout = findViewById(R.id.date_picker_id);
        start_date = findViewById(R.id.start_date_tv);
        end_Date = findViewById(R.id.end_date_tv);
        current_date = dateFormat.format(current_calendar.getTime());
        start_date.setText(current_date);
        end_Date.setText(current_date);

        date_picker_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View dialogView = View.inflate(UserHistory.this, R.layout.dialog_datepicker, null);
                calendar = dialogView.findViewById(R.id.calendar);
                calendar.resetAllSelectedViews();
                AlertDialog.Builder builder = new AlertDialog.Builder(UserHistory.this);
                builder.setView(dialogView);
                final AlertDialog dialog = builder.create();
                btn_Done = (Button) dialogView.findViewById(R.id.btn_done);
                btn_Cancel = (Button) dialogView.findViewById(R.id.btn_cancle);
                btn_Done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isCheckDate) {
                            if (startdatetime.equals("") && enddatetime.equals("")) {
                                Toast.makeText(UserHistory.this, "Please select date range", Toast.LENGTH_SHORT).show();
                            } else {
                                if (startdatetime.equals("")) {
                                    Toast.makeText(UserHistory.this, "Please select start date", Toast.LENGTH_SHORT).show();
                                } else if (enddatetime.equals("")) {
                                    Toast.makeText(UserHistory.this, "Please select end date", Toast.LENGTH_SHORT).show();
                                } else {
                                    start_date.setText(display_startdate);
                                    end_Date.setText(display_enddate);
                                    dialog.dismiss();
                                    Log.d("DateTimeLog", "StartDate: " + display_startdate + " EndDate: " + display_enddate);
                                    getNewDetails(display_startdate, display_enddate);
                                }
                            }
                        } else {
                            Toast.makeText(UserHistory.this, "Selected date range is incorrect", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                btn_Cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                calendar.setCalendarListener(new DateRangeCalendarView.CalendarListener() {
                    @Override
                    public void onFirstDateSelected(Calendar startDate) {
                        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        startdatetime = dateFormat.format(startDate.getTime());
                    }

                    @Override
                    public void onDateRangeSelected(Calendar startDate, Calendar endDate) {
                        if (startDate.after(endDate)) {
                            Toast.makeText(UserHistory.this, "StartDate cannot be grater than EndDate", Toast.LENGTH_LONG).show();
                        } else if (endDate.before(startDate)) {
                            Toast.makeText(UserHistory.this, "EndDate cannot be less than StartDate", Toast.LENGTH_LONG).show();
                        } else {
                            dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            startdatetime = dateFormat.format(startDate.getTime());
                            dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            enddatetime = dateFormat.format(endDate.getTime());
                            dateFormat = new SimpleDateFormat("dd-MMMM-yyyy");
                            display_enddate = new SimpleDateFormat(Constants.UI_DATE_FORMAT_ONLY_DATE).format(endDate.getTime());
                            display_startdate = new SimpleDateFormat(Constants.UI_DATE_FORMAT_ONLY_DATE).format(startDate.getTime());
                            isCheckDate = true;
                        }
                    }
                });
                dialog.show();
            }
        });
    }

    private void getNewDetails(String startDate, String endDate) {
        String where = "USERID = '" + userid + "' and DATE BETWEEN '" + startDate + "' and '" + endDate + "'";
        arrayUserHistory = (ArrayList<com.observatory.database.UserHistory>) Select.from(com.observatory.database.UserHistory.class)
                .where(where)
                .list();

        HashSet<String> courseList = new HashSet<>();

        for (com.observatory.database.UserHistory userHistory : arrayUserHistory) {
            courseList.add(userHistory.getMedium());
        }

        List<UserViewModel> userViewModels = mapToUserViewModel(arrayUserHistory, courseList);
        adapter = new UserProfileAdapter(userViewModels);
        rvUserHistory.setAdapter(adapter);
    }

    private void getData() {

        if (SettingPreffrences.getName(this) == "") {
            tvName.setText("Guest User");
        } else {
            tvName.setText(SettingPreffrences.getName(getApplicationContext()));
        }

//        Log.d("", "getData: "+SettingPreffrences.getPhoto(getApplicationContext()));
//        Glide.with(getApplicationContext()).load(SettingPreffrences.getPhoto(getApplicationContext())).signature(new StringSignature(UUID.randomUUID().toString())).placeholder(R.drawable.im_default_profile).into(ivPP);

        Glide.with(getApplicationContext()).load(SettingPreffrences.getPhoto(getApplicationContext())).asBitmap().placeholder(R.drawable.im_default_profile).centerCrop().into(new BitmapImageViewTarget(ivPP) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(getApplicationContext().getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                ivPP.setImageDrawable(circularBitmapDrawable);
            }
        });


        //arrayUserHistory = (ArrayList<com.observatory.database.UserHistory>) com.observatory.database.UserHistory.listAll(com.observatory.database.UserHistory.class);
        /*list = (ArrayList<Statistics>) Select.from(Statistics.class).where(Condition.prop("language").eq(language),
                Condition.prop("standard").eq(standard),
                Condition.prop("subject").eq(subject),
                Condition.prop("chapter").eq(chapter)).list();*/

        System.out.print(com.observatory.database.UserHistory.listAll(com.observatory.database.UserHistory.class));

        userid = SettingPreffrences.getUserid(this);

        arrayUserHistory = (ArrayList<com.observatory.database.UserHistory>) Select.from(com.observatory.database.UserHistory.class).where(Condition.prop("userId").eq(userid.isEmpty() ? "0" : userid))
                .orderBy("id desc") /* ordering in descending so that latest appear at the top. */
                .list();


        HashSet<String> courseList = new HashSet<>();

        for (com.observatory.database.UserHistory userHistory : arrayUserHistory) {
            courseList.add(userHistory.getMedium());
        }

        List<UserViewModel> userViewModels = mapToUserViewModel(arrayUserHistory, courseList);

        adapter = new UserProfileAdapter(userViewModels);
        rvUserHistory.setAdapter(adapter);

    }

    private void getlastcontent() {

        arraysta = (ArrayList<LastContent>) Select.from(com.observatory.database.LastContent.class).where(Condition.prop("userId").eq(userid.isEmpty() ? "0" : userid))
                .orderBy("id desc") /* ordering in descending so that latest appear at the top. */
                .list();

        if (arraysta != null && arraysta.size() > 0) {
            tv_standardName.setText(arraysta.get(0).getMedium());
            tv_subjectName.setText(arraysta.get(0).getSubject());
            tv_chapterName.setText(arraysta.get(0).getChapter());
            tv_contentTitle.setText(arraysta.get(0).getContent_name());
            videopath = arraysta.get(0).getvideoPath();
            playBackPosition = arraysta.get(0).getlastDuration();
            tv_content_lastSeen.setVisibility(View.VISIBLE);
            rl_lastSeenContent.setVisibility(View.VISIBLE);
        } else {
            tv_content_lastSeen.setVisibility(View.GONE);
            rl_lastSeenContent.setVisibility(View.GONE);
        }
    }

    private List<UserViewModel> mapToUserViewModel(ArrayList<com.observatory.database.UserHistory> arrayUserHistory, HashSet<String> courseList) {
        ArrayList<UserViewModel> userHistoryViewModels = new ArrayList<>();

        for (String course : courseList) {

            CourseHeaderViewModel courseHeaderViewModel = new CourseHeaderViewModel(course);
            userHistoryViewModels.add(courseHeaderViewModel);

            for (int i = 0; i < arrayUserHistory.size(); i++) {

                com.observatory.database.UserHistory userHistory = arrayUserHistory.get(i);

                if (userHistory.getMedium().equals(course)) {
                    UserHistoryViewModel userHistoryViewModel = new UserHistoryViewModel(userHistory.getDate(), userHistory.getTime());
                    userHistoryViewModels.add(userHistoryViewModel);
                }

            }
        }

        return userHistoryViewModels;
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(user_history_menu, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.email) {


            generateData();


        }

        return super.onOptionsItemSelected(item);
    }*/


    public void showSendLoginHistoryDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.generate_report_dialog);
        dialog.setCancelable(false);
        final EditText etName = (EditText) dialog.findViewById(R.id.et_name);
        final EditText etEmail = (EditText) dialog.findViewById(R.id.et_email);
        Button btCancel = (Button) dialog.findViewById(R.id.bt_cancel);
        Button btSumit = (Button) dialog.findViewById(R.id.bt_submit);

        if (SettingPreffrences.getName(getApplicationContext()) == "") {
            etName.setVisibility(View.VISIBLE);
        } else {
            etName.setVisibility(View.GONE);
        }

        btSumit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = etEmail.getText().toString();

                if (etName.getVisibility() == View.VISIBLE) {

                    String name = etName.getText().toString();

                    if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(name)) {

                        if (CommonFunctions.isValidEmail(email)) {
                            callWS(name, email, startdatetime, enddatetime);
                        } else {
                            Toast.makeText(UserHistory.this, "Please enter your email id.", Toast.LENGTH_LONG).show();
                        }

                        dialog.dismiss();

                    } else {

                        Toast.makeText(UserHistory.this, "Please fill in all the details.", Toast.LENGTH_LONG).show();
                    }

                } else {

                    if (CommonFunctions.isValidEmail(email)) {
                        callWS(SettingPreffrences.getName(getApplicationContext()), email, startdatetime, enddatetime);
                        dialog.dismiss();
                    } else {
                        Toast.makeText(UserHistory.this, "Please enter your email id.", Toast.LENGTH_LONG).show();
                    }

                }

            }
        });

        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();


    }

    public void callWS(String userName, String email, String startDate, String endDate) {

        Log.d("SendingData", "startDate: " + startDate + " endDate: " + endDate);

        if (CommonFunctions.isNetworkConnected(this)) {
            CommonFunctions.showDialog(this);
            autoSyncLoginHistory(false);
            String userId = SettingPreffrences.getUserid(getApplicationContext()).isEmpty() ? "0"
                    : SettingPreffrences.getUserid(getApplicationContext());
            String token = SettingPreffrences.getToken(getApplicationContext()).isEmpty() ? "0000"
                    : SettingPreffrences.getToken(getApplicationContext());
            String OS = Build.VERSION.RELEASE;
            String MAKE = Build.MANUFACTURER;
            String MODEL = Build.MODEL;
            Ion.with(this).load(NetworkUrl.userHistoryEmailer)
                    .setBodyParameter("userId", userId)
                    .setBodyParameter("token", token)
                    .setBodyParameter("os", OS)
                    .setBodyParameter("make", MAKE)
                    .setBodyParameter("model", MODEL)
                    .setBodyParameter("userName", userName)
                    .setBodyParameter("isEdulife", "")
                    .setBodyParameter("startDate", startDate)
                    .setBodyParameter("endDate", endDate)
                    .setBodyParameter("email", email).asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {

                            if (e == null) {
                                Log.d("ApiResponse", "userHistoryEmailer: " + result);
                                try {
                                    JSONObject jsonObject = new JSONObject(result);
                                    alert(jsonObject.getString("message"));
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }


                            } else {

                                alert("Something went wrong, please try again later.");
                            }

                            CommonFunctions.hideDialog();

                        }
                    });
        } else {

            alert("Internet Connection not available");
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        autoSyncLoginHistory(true);
    }


    ///sync history before showing in app @vidya

    private void autoSyncLoginHistory(boolean isForeground) {
        if (isForeground) {
            CommonFunctions.showDialog(this);
        }
        long lastSyncedId = SettingPreffrences.getLastSyncedLoginHistoryId(getApplicationContext());
        List<com.observatory.database.UserHistory> userHistoryList;

        if (lastSyncedId != -1) {
            userHistoryList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com.observatory.database.UserHistory.class).where("id > " + lastSyncedId).orderBy("id asc").list(); /* ordering in ascending so that latest appear at the end
					. */

        } else {
            userHistoryList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com.observatory.database.UserHistory.class).orderBy("id asc").list(); /* ordering in ascending so that latest appear at the end
					. */
        }

        // there is data to sync
        if (userHistoryList.size() > 0) {
            String statsJson = formatLoginHistoryToJson(userHistoryList);
            callSyncLoginHistoryWebService(statsJson, userHistoryList, isForeground);
        } else {
            if (isForeground) {
                CommonFunctions.hideDialog();
                getData();
            }
        }
    }

    private String formatLoginHistoryToJson(List<com.observatory.database.UserHistory> arrayUserHistory) {

        JSONArray mainJsonArr = new JSONArray();

        ArrayList<String> courseList = getCourseList();

        for (int i = 0; i < courseList.size(); i++) {

            JSONObject jsonObject = new JSONObject();

            try {

                jsonObject.put("medium", courseList.get(i));

                JSONArray dataJsonArr = new JSONArray();

                for (com.observatory.database.UserHistory userHistory : arrayUserHistory) {

                    if (userHistory.getMedium().equals(courseList.get(i))) {

                        try {

                            JSONObject dateTimeJson = new JSONObject();
                            dateTimeJson.put("date", userHistory.getDate());
                            dateTimeJson.put("time", userHistory.getTime());
                            dataJsonArr.put(dateTimeJson);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

                // send data only if current medium has login data
                if (dataJsonArr.length() > 0) {
                    jsonObject.put("data", dataJsonArr);
                    mainJsonArr.put(jsonObject);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Log.d("Tag", "formatLoginHistoryToJson: " + mainJsonArr.toString());

        return mainJsonArr.toString();
    }

    private ArrayList<String> getCourseList() {

        String userid = SettingPreffrences.getUserid(getApplicationContext());

        ArrayList<String> courseList = new ArrayList<>();
        ArrayList<com.observatory.database.UserHistory> userHistoryArrayList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com.observatory.database.UserHistory.class).groupBy("medium").orderBy("id asc").where(Condition.prop("userId").eq((userid.isEmpty() ? "0" : userid))).list();

        for (com.observatory.database.UserHistory userHistory : userHistoryArrayList) {
            courseList.add(userHistory.getMedium());
        }

        return courseList;
    }

    private void callSyncLoginHistoryWebService(String json, final List<com.observatory.database.UserHistory> userHistoryList, boolean isForeground) {

        if (CommonFunctions.isNetworkConnected(this)) {

            String userId = SettingPreffrences.getUserid(getApplicationContext()).isEmpty() ? "0" : SettingPreffrences.getUserid(getApplicationContext());
            String token = SettingPreffrences.getToken(getApplicationContext()).isEmpty() ? "0000" : SettingPreffrences.getToken(getApplicationContext());
            String deviceId = CommonFunctions.getUniqueDeviceId(getApplicationContext());

            Ion.with(this).load(NetworkUrl.syncLoginHistory)
                    .setBodyParameter("userId", userId)
                    .setBodyParameter("token", token)
                    .setBodyParameter("os", Build.VERSION.RELEASE)
                    .setBodyParameter("make", Build.MANUFACTURER)
                    .setBodyParameter("model", Build.MODEL)
                    .setBodyParameter("historyDetails", json)
                    .asString().setCallback(new FutureCallback<String>() {

                @Override
                public void onCompleted(Exception e, String result) {
                    if (isForeground) {
                        CommonFunctions.hideDialog();
                        getData();
                    }
                    if (e == null) {
                        // update last synced Id of UserHistory table
                        Log.d("ApiResponse", "UserHistoryAutoSync: " + result);
                        com.observatory.database.UserHistory lastSyncedUserHistory = userHistoryList.get(userHistoryList.size() - 1);
                        SettingPreffrences.setLastSyncedLoginHistoryId(getApplicationContext(), lastSyncedUserHistory.getId());
                    } else {
                        new EmailHandler().sendMail("phone@millicent.in", "KEF Sync", e.getMessage());
                    }
                }
            });
        } else {
            if (isForeground) {
                CommonFunctions.hideDialog();
                getData();
            }
        }

    }

}
