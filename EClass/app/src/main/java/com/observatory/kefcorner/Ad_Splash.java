package com.observatory.kefcorner;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;

import java.io.File;
import java.util.List;

import static com.observatory.kefcorner.StorageUtils.getStorageList;

public class Ad_Splash extends BaseActivity {


    public static String BASE_LOCATION = ".Splash";
    String externalpath = new String();
    List<StorageUtils.StorageInfo> list;
    private static final int PERMISSION_REQUEST_CODE = 1;
    ImageView img_splash;
    String imagename = "splash.jpg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad__splash);
        if (!checkPermission()) {
            requestPermission();
        } else {
            getSplashImage();
        }
    }

    @Override
    protected void onLayout() {
        actionBar.hide();
        img_splash = (ImageView) findViewById(R.id.img_splash);

    }

    private boolean checkPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
            //Toast.makeText(getApplicationContext(),"permission_checking", Toast.LENGTH_SHORT).show();
            return Environment.isExternalStorageManager();
        }else {
            int read_perm = ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE);
            int phone_state_perm = ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.READ_PHONE_STATE);

            switch (0) {

                case 0:
                    if (read_perm != PackageManager.PERMISSION_GRANTED) {
                        return false;
                    }

                case 1:
                    if (phone_state_perm != PackageManager.PERMISSION_GRANTED) {
                        return false;
                    }

                case 3:
                    return true;
            }
            return true;
        }
    }

    private void requestPermission() {
        Log.d("printval", "sdk:"+Build.VERSION.SDK_INT+"::"+Build.VERSION_CODES.R);
       // Toast.makeText(getApplicationContext(),"sdk:"+Build.VERSION.SDK_INT+"::"+Build.VERSION_CODES.R, Toast.LENGTH_SHORT).show();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
            try {
                //Toast.makeText(getApplicationContext(),"permission_requesting", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                intent.addCategory("android.intent.category.DEFAULT");
                intent.setData(Uri.parse(String.format("package:%s", new Object[]{getApplicationContext().getPackageName()})));
                startActivityForResult(intent,2296);
            }catch (Exception e){
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivityForResult(intent, 2296);
            }
        }else {
            //Toast.makeText(getApplicationContext(),"requesting", Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getSplashImage();
                } else {
                    alert("We need access to your SD Card storage to display content and videos. " + "Kindly provide the app with access to SD Card storage by going into " + "your app settings and enabling them.");
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2296){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
                if (Environment.isExternalStorageManager()){
                   // Toast.makeText(getApplicationContext(),"result", Toast.LENGTH_SHORT).show();
                    getSplashImage();
                }else {
                    alert("We need access to your SD Card storage to display content and videos. " + "Kindly provide the app with access to SD Card storage by going into " + "your app settings and enabling them.");
                }
            }
        }
    }

    protected void alert(String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    getSplashImage();
                }

            }
        })
                .setMessage(message)
                .setTitle("Eclass")
                .show();
    }

    private void getSplashImage() {
        try {
            String imagepath = "";
            boolean issplashexist=false;
            list = getStorageList();
            Log.d("DataLog", "pathNow: "+App.BASE_PATH+"  pathmy: "+App.Card_Path+"/"+BASE_LOCATION);
            //String path = externalStorage1();
            String path = App.Card_Path+"/"+BASE_LOCATION+"/";
            File directory = new File(path);
            if (directory != null && directory.exists()) {
                File[] files = directory.listFiles();
                if (files != null) {
                    for (int i = 0; i < files.length; i++) {
                        String img = path + files[i].getName();
                        Log.d("SplashImage","1: "+path+"  "+files[i].getName());
                        if (files[i].getName().equals(imagename)) {
                            imagepath = img;
                            issplashexist=true;
                            Bitmap bmp = BitmapFactory.decodeFile(imagepath);
                            img_splash.setImageBitmap(bmp);
                            img_splash.setVisibility(View.VISIBLE);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent(Ad_Splash.this, Splash.class);
                                    startActivity(i);
                                    finish();
                                }
                            }, 4000);
                        }
                    }
                    if(!issplashexist){
                        img_splash.setVisibility(View.GONE);
                        redirect();
                    }
                } else {
                    img_splash.setVisibility(View.GONE);
                    redirect();
                }
            } else {
                img_splash.setVisibility(View.GONE);
                redirect();
            }
        } catch (Exception e) {
            Log.e("Ad_splash", e.getMessage());
            redirect();
        }
    }

    public void redirect(){
        Intent i = new Intent(Ad_Splash.this, Splash.class);
        startActivity(i);
        finish();

    }

    protected String externalStorage1() {

        File storage = new File("/storage");
        String path = "";


        //File storage = Environment.getExternalStorageDirectory().getAbsoluteFile();

        File file[] = storage.listFiles();

        if (file != null) {

            for (int i = 0; i < file.length; i++) {
                //  Log.v("Files", "FileName:" + file[i].getName());
                // Toast.makeText(this, "for loop 1", Toast.LENGTH_SHORT).show();

                File file1 = new File("/storage/" + file[i].getName() + "/");

                File files2[] = file1.listFiles();
                // Toast.makeText(this, files2.toString(), Toast.LENGTH_SHORT).show();

                if (files2 != null) {
                    for (int j = 0; j < files2.length; j++) {
                        // Toast.makeText(this, "for loop 2", Toast.LENGTH_SHORT).show();
                        if (files2[j].toString().equals(file1.getAbsolutePath() + "/" + BASE_LOCATION)) {

                            path = file1.getAbsolutePath() + "/" + BASE_LOCATION + "/";
                            return path;
                        }
                    }
                } else {

                    path = "";

                }


            }
        }


        //for blueStack path is different
        File winShared = new File("/storage/sdcard/windows/BstSharedFolder");

        if (winShared.listFiles() != null) {
            path = winShared.getAbsolutePath() + "/" + BASE_LOCATION + "/";
            Toast.makeText(this, path, Toast.LENGTH_LONG).show();
            return path;
        } else {
            path = "";
        }


        if (path.equals("")) {


            String test[] = externalpath.split("\n");
            String path1 = test[test.length - 1];
            path1 = path1.replace("*", "");


            if (externalpath.contains("fat")) {
                storage = Environment.getExternalStorageDirectory().getAbsoluteFile();
            } else {
                storage = new File(path1);
                //storage = new File(list.get(0).path);
                //storage = new File("/storage");
            }

            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    //  Log.v("Files", "FileName:" + file[i].getName());
                    // Toast.makeText(this, "for loop 3", Toast.LENGTH_SHORT).show();

                    String fileValue;
                    fileValue = files[i].toString();
                    if (externalpath.contains("fat")) {


                        if (fileValue.equals(storage.getPath() + "/" + BASE_LOCATION)) {
                            path = storage.getPath() + "/" + files[i].getName();
                        }
                    } else {
                        if (fileValue.equals(path1 + "/" + BASE_LOCATION)) {
                            path = path1 + "/" + files[i].getName();
                        }
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                }
            } else {

                path = "";

            }


        }

        if (path.equals("")) {

//            String test[] = externalpath.split("\n");
//            String path1 = test[test.length - 1];
//            path1 = path1.replace("*", "");
            String path1;
            if (list.size() > 0) {

                path1 = list.get(list.size() - 1).path;
            } else {
                path1 = "";
            }


            if (externalpath.contains("fat")) {
                storage = Environment.getExternalStorageDirectory().getAbsoluteFile();
            } else {
                storage = new File(path1);
                //storage = new File(list.get(0).path);
                //storage = new File("/storage");
            }

            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    // Toast.makeText(this, "for loop 4", Toast.LENGTH_SHORT).show();
                    //  Log.v("Files", "FileName:" + file[i].getName());

                    String fileValue;
                    fileValue = files[i].toString();
                    if (externalpath.contains("fat")) {


                        if (fileValue.equals(storage.getPath() + "/" + BASE_LOCATION)) {
                            path = storage.getPath() + "/" + files[i].getName();
                        }
                    } else {
                        if (fileValue.equals(path1 + "/" + BASE_LOCATION)) {
                            path = path1 + "/" + files[i].getName();
                        }
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                } else {

                    path = "";
                }
            }

        }

        if (path.equals("")) {

            String path1 = "";


            for (int k = 0; k < list.size(); k++) {

                path1 = list.get(k).path;


                storage = new File(path1);

                File files[] = storage.listFiles();

                if (files != null) {
                    for (int i = 0; i < files.length; i++) {

                        // Toast.makeText(this, "for loop 5", Toast.LENGTH_SHORT).show();
                        //  Log.v("Files", "FileName:" + file[i].getName());

                        String fileValue;
                        fileValue = files[i].toString();

                        if (fileValue.equals(path1 + "/" + BASE_LOCATION)) {
                            path = path1 + "/" + files[i].getName();
                            break;
                        }


                    }

                    if (new File(path).exists()) {

                        return path + "/";

                    }
                } else {

                    path = "";

                }

            }


        }

        if (path.equals("")) {

            String path1 = "mnt/sd-ext/";
            storage = new File(path1);
            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {


                    String fileValue;
                    fileValue = files[i].toString();

                    if (fileValue.equals(path1 + BASE_LOCATION)) {
                        path = path1 + "/" + files[i].getName();
                        break;
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                }
            } else {

                path = "";

            }

        }

        if (path.equalsIgnoreCase("")) {


            String path1 = "/mnt/usb_storage/USB_DISK0/udisk0";
            storage = new File(path1);
            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {


                    String fileValue;
                    fileValue = files[i].toString();

                    if (fileValue.equals(path1 + BASE_LOCATION)) {
                        path = path1 + "/" + files[i].getName();
                        break;
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                }
            } else {

                path = "";

            }

        }

        if (path.equalsIgnoreCase("")) {


            String path1 = "/USB/USB_DISK0/udisk0";
            storage = new File(path1);
            File files[] = storage.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {


                    String fileValue;
                    fileValue = files[i].toString();

                    if (fileValue.equals(path1 + BASE_LOCATION)) {
                        path = path1 + "/" + files[i].getName();
                        break;
                    }


                }

                if (new File(path).exists()) {

                    return path + "/";

                }
            } else {

                return "";

            }

        }

        return "/";


    }


}
