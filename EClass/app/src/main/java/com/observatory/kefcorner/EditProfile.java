package com.observatory.kefcorner;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.observatory.AsyncTaskMain.AsyncTaskHelper;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.Image;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

/**
 * Created by Anuj on 06-01-2016.
 */
public class EditProfile extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "EditProfile";

    private static final int GALLERY_CODE = 200;
    private static final int PERMISSION_REQUEST_CODE = 1;

    private RelativeLayout rlImage;
    private ImageView ivImage;

    private EditText etFName;
    private EditText etLName;
    private EditText etMobile;

    private TextView tvEdit;
    private Intent intent;

    private Image getScaledImage;

    private AsyncTaskHelper asyncTaskHelper;

    /*Json*/
    private JSONObject jsonObject;
    private JSONArray jsonArray;
    private String status;
    private String msg;


    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";
    public static String filePath = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.edit_profile);
    }

    @Override
    protected void onLayout() {


        rlImage = (RelativeLayout) findViewById(R.id.rl_ep_image);
        ivImage = (ImageView) findViewById(R.id.iv_ep_image);

        etFName = (EditText) findViewById(R.id.et_ep_fname);
        etLName = (EditText) findViewById(R.id.et_ep_lname);
        etMobile = (EditText) findViewById(R.id.et_ep_mobile);

        tvEdit = (TextView) findViewById(R.id.tv_ep_edit);

        getScaledImage = new Image(getApplicationContext());

        String[] name = SettingPreffrences.getName(getApplicationContext()).split(" ");
        etFName.setText(name[0]);
        etLName.setText(name[1]);
        etMobile.setText(SettingPreffrences.getMobile(getApplicationContext()));

        Glide.with(getApplicationContext()).load(SettingPreffrences.getPhoto(getApplicationContext())).signature(new StringSignature(UUID.randomUUID().toString())).placeholder(R.drawable.ic_profile_default).into(ivImage);

        tvEdit.setOnClickListener(this);
        rlImage.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.rl_ep_image:

                if (!checkPermission()) {
                    requestPermission();
                } else {
                    showImageSelectionDialog();
                }
                // showImageSelectionDialog();


                break;

            case R.id.tv_ep_edit:

                callEditProfileWS();
                break;
        }

    }

    private void callEditProfileWS() {

        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {

            if (TextUtils.isEmpty(etFName.getText().toString()) || TextUtils.isEmpty(etLName.getText().toString())
                    || TextUtils.isEmpty(etMobile.getText().toString())) {

                alert("Fields cannot be empty");

            } else if (etMobile.getText().toString().length() != 10) {
                alert("Please enter valid mobile number.");
            } else {

                /*UploadImageToServer uploadImageToServer = new UploadImageToServer(etFName.getText().toString(), etLName.getText().toString());
                uploadImageToServer.execute();
*/
                try {
                    test();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        } else {

            alert("Internet Connection not available");
        }


    }

    private void test() throws UnsupportedEncodingException {


        /*String params = "userId=" + URLEncoder.encode(SettingPreffrences.getUserid(context), "UTF-8")
                + "&token=" + URLEncoder.encode(SettingPreffrences.getToken(context), "UTF-8")
                + "&firstName=" + URLEncoder.encode(etFName.getText().toString(), "UTF-8")
                + "&mobile=" + URLEncoder.encode("", "UTF-8")
                + "&lastName=" + URLEncoder.encode(etLName.getText().toString(), "UTF-8");
        asyncTaskHelper = new AsyncTaskHelper(context, );*/

        CommonFunctions.showDialog(this);

        if (TextUtils.isEmpty(filePath)) {

            Ion.with(getApplicationContext()).load(NetworkUrl.host + NetworkUrl.editProfile)
                    .setMultipartParameter("userId", SettingPreffrences.getUserid(getApplicationContext()))
                    .setMultipartParameter("token", SettingPreffrences.getToken(getApplicationContext()))
                    .setMultipartParameter("firstName", etFName.getText().toString())
                    .setMultipartParameter("lastName", etLName.getText().toString())
                    .setMultipartParameter("mobile", etMobile.getText().toString())
                    .asString().setCallback(new FutureCallback<String>() {
                                                @Override
                                                public void onCompleted(Exception e, String result) {


                                                    try {

                                                        Log.d("ApiResponse", "EditProfile: " + result);

                                                        jsonObject = new JSONObject(result);
                                                        status = jsonObject.getString("status");
                                                        msg = jsonObject.getString("message");

                                                        if (status.equalsIgnoreCase("failure")) {

                                                            alert(msg);

                                                        } else if (status.equalsIgnoreCase("error")) {

                                                            alert("Something went wrong, please try again later");

                                                        } else {

                                                            JSONArray res = jsonObject.getJSONArray("response");

                                                            for (int i = 0; i < res.length(); i++) {

                                                                jsonObject = res.getJSONObject(i);
                                                                SettingPreffrences.setName(getApplicationContext(), etFName.getText().toString() + " " + etLName.getText().toString());
                                                                SettingPreffrences.setPhoto(getApplicationContext(), jsonObject.getString("photo"));
                                                                SettingPreffrences.setMobile(getApplicationContext(), etMobile.getText().toString());
                                                            }


                                                            AlertDialog.Builder builder = new AlertDialog.Builder(EditProfile.this);
                                                            builder.setTitle("SUCCESS");
                                                            builder.setMessage("Your profile was edited successfully");
                                                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(DialogInterface dialog, int which) {

                                                                            SettingPreffrences.setProfileEdited(getApplicationContext(), true);
                                                                            onBackPressed();
                                                                            finish();

                                                                        }
                                                                    }

                                                            );
                                                            builder.create().show();
                                                        }
                                                    } catch (
                                                            JSONException e1
                                                    ) {
                                                        e1.printStackTrace();
                                                    }


                                                    CommonFunctions.hideDialog();

                                                }
                                            }

            );

        } else {


            Ion.with(getApplicationContext()).load(NetworkUrl.host + NetworkUrl.editProfile)
                    .setMultipartParameter("userId", SettingPreffrences.getUserid(getApplicationContext()))
                    .setMultipartParameter("token", SettingPreffrences.getToken(getApplicationContext()))
                    .setMultipartParameter("firstName", etFName.getText().toString())
                    .setMultipartParameter("lastName", etLName.getText().toString())
                    .setMultipartParameter("mobile", etMobile.getText().toString())
                    .setMultipartFile("photo", "image/jpeg", new File(filePath)).
                    asString().setCallback(new FutureCallback<String>() {
                                               @Override
                                               public void onCompleted(Exception e, String result) {

                                                   Log.d("UpdateProfile: ", ":: " + result);

                                                   try {
                                                       jsonObject = new JSONObject(result);
                                                       status = jsonObject.getString("status");
                                                       msg = jsonObject.getString("message");

                                                       if (status.equalsIgnoreCase("failure")) {

                                                           alert(msg);

                                                       } else if (status.equalsIgnoreCase("error")) {

                                                           alert("Something went wrong, please try again later");

                                                       } else {

                                                           JSONArray res = jsonObject.getJSONArray("response");

                                                           for (int i = 0; i < res.length(); i++) {

                                                               jsonObject = res.getJSONObject(i);
                                                               SettingPreffrences.setName(getApplicationContext(), etFName.getText().toString() + " " + etLName.getText().toString());
                                                               SettingPreffrences.setPhoto(getApplicationContext(), jsonObject.getString("photo"));
                                                           }


                                                           AlertDialog.Builder builder = new AlertDialog.Builder(EditProfile.this);
                                                           builder.setTitle("SUCCESS");
                                                           builder.setMessage("Your profile was edited successfully");
                                                           builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                       @Override
                                                                       public void onClick(DialogInterface dialog, int which) {

                                                                           SettingPreffrences.setProfileEdited(getApplicationContext(), true);
                                                                           onBackPressed();
                                                                           finish();

                                                                       }
                                                                   }

                                                           );
                                                           builder.create().show();
                                                       }
                                                   } catch (
                                                           JSONException e1
                                                   ) {
                                                       e1.printStackTrace();
                                                   }


                                                   CommonFunctions.hideDialog();

                                               }
                                           }

            );
        }


    }

    private void showImageSelectionDialog() {
        openGallery();

    }

    private void openGallery() {

        intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 0);
        intent.putExtra("aspectY", 0);
        intent.putExtra("scale", true);
        intent.putExtra("outputX", 500);
        intent.putExtra("outputY", 500);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri());
        intent.putExtra("outputFormat", Bitmap.CompressFormat.PNG.toString());
        startActivityForResult(intent, GALLERY_CODE);

    }


    private Uri getTempUri() {
        return Uri.fromFile(getTempFile());
    }

    private File getTempFile() {

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {

            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), TEMP_PHOTO_FILE);
            try {
                file.createNewFile();
            } catch (IOException e) {
            }

            return file;
        } else {

            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == GALLERY_CODE && resultCode == RESULT_OK) {

            File tempFile = getTempFile();
            filePath = Environment.getExternalStorageDirectory()
                    + "/" + TEMP_PHOTO_FILE;

            Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
            byte byteArray[] = new byte[0];
            try {
                byteArray = bitmapToByte(MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                FileOutputStream fos = new FileOutputStream(filePath); // writing for temporary reason for creating compressedBitmap;
                fos.write(byteArray);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //ivImage.setImageBitmap(selectedImage);

//            data.getData();
//
            try {
                ivImage.setImageBitmap(MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData()));
            } catch (IOException e) {
                e.printStackTrace();
            }

            //if (tempFile.exists()) tempFile.delete();


        }


    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;

        }
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

//        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//
//
//        } else {
//
//            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
//        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    showImageSelectionDialog();

                } else {

                    alert("We need access to your SD Card storage to display content and videos. Kindly provide the app with access to SD Card storage by going into your app settings and enabling them.");

                }
                break;
        }
    }

    public byte[] bitmapToByte(Bitmap bitmap) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }


}
