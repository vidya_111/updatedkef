package com.observatory.kefcorner;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.observatory.model.userStatisticsViewModel;
import com.observatory.utils.CommonFunctions;

import java.util.List;

import static com.observatory.utils.Constants.CONTENT_TYPE_CONTENT;
import static com.observatory.utils.Constants.CONTENT_TYPE_MCQ;
import static com.observatory.utils.Constants.CONTENT_TYPE_MINDMAP;
import static com.observatory.utils.Constants.CONTENT_TYPE_PDF;
import static com.observatory.utils.Constants.CONTENT_TYPE_QANDA;
import static com.observatory.utils.Constants.KEF_ContentTypeId_Content;
import static com.observatory.utils.Constants.KEF_ContentTypeId_MCQ;
import static com.observatory.utils.Constants.KEF_ContentTypeId_MindMap;
import static com.observatory.utils.Constants.KEF_ContentTypeId_PDF;
import static com.observatory.utils.Constants.KEF_ContentTypeId_QNA;
import static com.observatory.utils.Constants.LABEL_CONTENT_TYPE_CONTENT;
import static com.observatory.utils.Constants.LABEL_CONTENT_TYPE_MCQ;
import static com.observatory.utils.Constants.LABEL_CONTENT_TYPE_MINDMAP;
import static com.observatory.utils.Constants.LABEL_CONTENT_TYPE_PDF;
import static com.observatory.utils.Constants.LABEL_CONTENT_TYPE_QANDA;

/**
 * Created by gaurav on 16/1/18.
 */

class StatisticsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

	private final Context context;
	private final List<userStatisticsViewModel> statisticsViewModelList;
	private String chapter;

	public StatisticsAdapter(Context context, List<userStatisticsViewModel> statisticsViewModelList,String chapter) {
		this.context = context;
		this.statisticsViewModelList = statisticsViewModelList;
		this.chapter=chapter;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context
				.LAYOUT_INFLATER_SERVICE);
		return new StatisticsViewHolder(inflator.inflate(R.layout.item_statistics, parent, false));
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

		StatisticsViewHolder viewHolder = (StatisticsViewHolder) holder;
		userStatisticsViewModel model = statisticsViewModelList.get(position);

		switch (model.contentTypeID) {

			case KEF_ContentTypeId_Content: {
				viewHolder.tvContentType.setCompoundDrawablesWithIntrinsicBounds(R.drawable
                        .ic_statistics_content, 0, 0, 0);

				//viewHolder.tvContentType.setText(chapter + " - " +LABEL_CONTENT_TYPE_CONTENT + " - " + CommonFunctions.removeFormatSuffix(model.contentName));
				viewHolder.tvContentType.setText(chapter + " - " +LABEL_CONTENT_TYPE_CONTENT);
				//String duration = CommonFunctions.formatDuration(Integer.parseInt(model.accessDuration));
				String duration = model.accessDuration;
				viewHolder.tvDuration.setText(duration);
				viewHolder.tvDuration.setVisibility(View.VISIBLE);
			}
			break;

			case KEF_ContentTypeId_MindMap: {
				viewHolder.tvContentType.setCompoundDrawablesWithIntrinsicBounds(R.drawable
                        .ic_statistics_mindmap, 0, 0, 0);

				//viewHolder.tvContentType.setText(chapter + " - "+LABEL_CONTENT_TYPE_MINDMAP + " - " + CommonFunctions.removeFormatSuffix(model.contentName));
				viewHolder.tvContentType.setText(chapter + " - "+LABEL_CONTENT_TYPE_MINDMAP);
				//String duration = CommonFunctions.formatDuration(Integer.parseInt(model.accessDuration));
				String duration = model.accessDuration;
				viewHolder.tvDuration.setText(duration);
				viewHolder.tvDuration.setVisibility(View.VISIBLE);
			}
			break;

			case KEF_ContentTypeId_QNA: {
				viewHolder.tvContentType.setCompoundDrawablesWithIntrinsicBounds(R.drawable
                        .ic_statistics_questions_answers, 0, 0, 0);

				//viewHolder.tvContentType.setText(chapter + " - "+LABEL_CONTENT_TYPE_QANDA + " - " + CommonFunctions.removeFormatSuffix(model.contentName));
				viewHolder.tvContentType.setText(chapter + " - "+LABEL_CONTENT_TYPE_QANDA);
				//String duration = CommonFunctions.formatDuration(Integer.parseInt(model.accessDuration));
				String duration = model.accessDuration;
				viewHolder.tvDuration.setText(duration);
				viewHolder.tvDuration.setVisibility(View.VISIBLE);
			}
			break;

			case KEF_ContentTypeId_PDF: {
				viewHolder.tvContentType.setCompoundDrawablesWithIntrinsicBounds(R.drawable
                        .ic_statistics_pdf, 0, 0, 0);
				//viewHolder.tvContentType.setText(chapter + " - "+LABEL_CONTENT_TYPE_PDF + " - " + CommonFunctions.removeFormatSuffix(model.contentName));
				viewHolder.tvContentType.setText(chapter + " - "+LABEL_CONTENT_TYPE_PDF);
				// for PDFs, no need to show duration
				viewHolder.tvDuration.setVisibility(View.GONE);
			}

			break;

			case KEF_ContentTypeId_MCQ: {
				viewHolder.tvContentType.setCompoundDrawablesWithIntrinsicBounds(R.drawable
                        .ic_statistics_mcq, 0, 0, 0);
				viewHolder.tvContentType.setText(chapter + " - "+LABEL_CONTENT_TYPE_MCQ);
				// for MCQs, show score instead of duration
				viewHolder.tvDuration.setVisibility(View.VISIBLE);
				viewHolder.tvDuration.setCompoundDrawablesWithIntrinsicBounds(R.drawable
						.ic_statistics_score, 0, 0, 0);
				viewHolder.tvDuration.setText(model.score + "%");
			}

			break;

		}

		viewHolder.tvDate.setText(model.accessDatetime);

	}

	@Override
	public int getItemCount() {
		return statisticsViewModelList.size();
	}

	class StatisticsViewHolder extends RecyclerView.ViewHolder {
		TextView tvContentType;
		TextView tvDuration;
		TextView tvDate;

		public StatisticsViewHolder(View itemView) {
			super(itemView);
			tvContentType = (TextView) itemView.findViewById(R.id.tv_content_type);
			tvDuration = (TextView) itemView.findViewById(R.id.tv_duration);
			tvDate = (TextView) itemView.findViewById(R.id.tv_date);
		}
	}
}
