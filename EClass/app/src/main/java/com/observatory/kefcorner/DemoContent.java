package com.observatory.kefcorner;

import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.BasicNameValuePair;
import com.koushikdutta.ion.Ion;
import com.observatory.AsyncTaskMain.AsyncTaskHelper;
import com.observatory.adapters.DemoContentAdapter;
import com.observatory.database.Subject;
import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Anuj on 08-01-2016.
 */
public class DemoContent extends BaseActivity {

    private ListView lstChapter;

    private ArrayList<Subject> subjects;

    private Subject subject;

    private Button btReport;
    private RelativeLayout rlPb;
    private ProgressBar pbMain;
    private RelativeLayout rlNoInternet;
    private ImageView ivErr;
    private TextView tvErr;


    private int position;
    private HashMap<String, String> dimensions;

    private AsyncTaskHelper asyncTaskHelper;
    private JsonObject params = null;
    private Future<String> futureNetworkCall;
    private boolean isLoading = false;

    // this parameters specify's fields required in response from youtube
    private String FIELDS = "kind,nextPageToken,pageInfo(totalResults,resultsPerPage)," +
            "items(kind,snippet(channelId,title,thumbnails(standard(url,width,height))," +
            "channelTitle,playlistId,resourceId(kind,videoId)))";


    private String PAGE_TOKEN = "";
    private String TAG = "DemoContent";
    public String sundaram_eclass_youtube_videos_temp = "";

    private DemoContentAdapter demoContentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_demo_content);
        byte[] playlist = new byte[0];
        byte[] content = new byte[0];
        String content_demo = "", play = "";
        try {
            playlist = Base64.decode(BuildConfig.PLAY_LIST_ID, Base64.DEFAULT);
            content = Base64.decode(BuildConfig.DEMO_CONTENT_KEY, Base64.DEFAULT);
            content_demo = new String(content, "UTF-8");
            //play = new String(playlist, "UTF-8");
            play = getIntent().getExtras().getString("playlist_id");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        sundaram_eclass_youtube_videos_temp = "https://www.googleapis.com/youtube/v3/playlistItems"
                + "?part=snippet&playlistId=" + play + "&key=" + content_demo + "&" +
                "maxResults=5&fields=kind,nextPageToken,pageInfo(totalResults,resultsPerPage)," +
                "items(kind,snippet(channelId,title,thumbnails(standard(url,width,height))," +
                "channelTitle,playlistId,resourceId(kind,videoId)))&pageToken=";
        Log.e("List Link", sundaram_eclass_youtube_videos_temp);

        btReport = (Button) findViewById(R.id.button2);
        btReport.setVisibility(View.GONE);

        rlPb = (RelativeLayout) findViewById(R.id.rl_progress);
        rlPb.setVisibility(View.GONE);

        pbMain = (ProgressBar) findViewById(R.id.pb_demo_youtube_content);
        pbMain.setVisibility(View.VISIBLE);

        rlNoInternet = (RelativeLayout) findViewById(R.id.rl_no_internet);
        rlNoInternet.setVisibility(View.GONE);

        ivErr = (ImageView) findViewById(R.id.iv_err);
        tvErr = (TextView) findViewById(R.id.tv_err);

        actionBar.setTitle("Eclass");

        subjects = new ArrayList<>();

        lstChapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {

//                    dimensions = new HashMap<>();
//                    dimensions.put("userId", getUserId(context));
//                    dimensions.put("category", "CONTENT");
//                    dimensions.put("name", subjects.get(position).getChapterName());
//                    CommonFunctions.sendAnalytics("videoDemoContent", dimensions);

                    App.getInstance().trackEvent("Demo Content", "Screen", "name - " + subjects.get(position).getChapterName() + " ; category - CONTENT", "");


                } else if (position == 1) {

                    dimensions = new HashMap<>();
                    dimensions.put("userId", getUserId(getApplicationContext()));
                    dimensions.put("category", "MIND MAP");
                    dimensions.put("name", subjects.get(position).getChapterName());
                    CommonFunctions.sendAnalytics("videoDemoContent", dimensions);

                    App.getInstance().trackEvent("Demo Content", "Screen", "name - " + subjects.get(position).getChapterName() + " ; category - MIND-MAP", "");

                } else {

//                    dimensions = new HashMap<>();
//                    dimensions.put("userId", getUserId(context));
//                    dimensions.put("category", "Q&A");
//                    dimensions.put("name", subjects.get(position).getChapterName());
//                    CommonFunctions.sendAnalytics("videoDemoContent", dimensions);

                    App.getInstance().trackEvent("Demo Content", "Screen", "name - " + subjects.get(position).getChapterName() + " ; category - Q&A", "");

                }
                Log.d("DemoVideoLogs","IntentToYouTube");
                Intent intent = new Intent(DemoContent.this, YoutubeView.class);
                intent.putExtra("id", subjects.get(position).getFolderName());
                startActivity(intent);


            }
        });

        lstChapter.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

//                Log.e(TAG, "onScroll: " + view.getLastVisiblePosition());
                int lPos = view.getLastVisiblePosition();

                Log.e(TAG, "\nonScroll: lPos" + lPos);
                Log.e(TAG, "\nonScroll size: " + (subjects.size() + 35));
                Log.e(TAG, "\nonScroll:(lPos >= (subjects.size() + 35) " + (lPos >= (subjects.size() + 35)));

                Log.e(TAG, "onScroll: page token : - " + PAGE_TOKEN);

                if ((lPos >= (subjects.size() - 14)) && !isLoading && !PAGE_TOKEN.isEmpty()) {
                    isLoading = true;
                    getYouTubeVideos();
                }

            }
        });

        getYouTubeVideos();

    }

    private void getYouTubeVideos() {

        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {

            hideErrorMsg();

            if (PAGE_TOKEN.isEmpty()) {
                pbMain.setVisibility(View.VISIBLE);
            } else {
                rlPb.setVisibility(View.VISIBLE);
            }

            BasicNameValuePair[] headerParams = {new BasicNameValuePair("Content-Type", "application/json")};

//            params = new JsonObject();
//            params.addProperty("part", "snippet");
//            params.addProperty("playlistId", PLAY_LIST_ID);
//            params.addProperty("key", KEY);
//            params.addProperty("maxResults", MAX_RESULTS);
//            params.addProperty("fields", FIELDS);
//            params.addProperty("pageToken", PAGE_TOKEN);

            futureNetworkCall = Ion.with(getApplicationContext())
                    .load("GET", sundaram_eclass_youtube_videos_temp + PAGE_TOKEN)
                    .setHeader(headerParams)
                    .setTimeout(16000)
                    .setLogging("SundaramEclassRetail", Log.DEBUG)
//                    .setJsonObjectBody(params)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            parseVideoResponse(e, result);
                        }
                    });

        } else {

            // no internet
            isLoading = false;
            pbMain.setVisibility(View.GONE);
            rlPb.setVisibility(View.GONE);
            if (subjects.isEmpty()) {
                showErrorMsg(R.drawable.ic_no_internet, "No internet connection, tap to retry");
            }
        }

    }

    private void parseVideoResponse(Exception e, String response) {
        Log.e(TAG, "parseVideoResponse: --> " + response);

        if (e != null) {

            pbMain.setVisibility(View.GONE);
            rlPb.setVisibility(View.GONE);
            if (subjects.isEmpty()) {
                showErrorMsg(R.drawable.ic_no_internet, "No internet connection, tap to retry");
            }
        } else {

            try {
                JSONObject jsonObject = new JSONObject(response);

                if (jsonObject.has("items")) {
                    JSONArray vidArr = jsonObject.getJSONArray("items");
                    int len = vidArr.length();
                    for (int i = 0; i < len; ++i) {
                        JSONObject snipObj = vidArr.getJSONObject(i).getJSONObject("snippet");
                        Subject subject = new Subject();
                        subject.setChapterName(snipObj.getString("title"));
                        subject.setFolderName(snipObj.getJSONObject("resourceId").getString("videoId"));
                        subjects.add(subject);
                    }

                    if (PAGE_TOKEN.isEmpty()) {
                        pbMain.setVisibility(View.GONE);
                        hideErrorMsg();
                        demoContentAdapter = new DemoContentAdapter(this, subjects);
                        lstChapter.setAdapter(demoContentAdapter);
                    } else {
                        rlPb.setVisibility(View.GONE);
                        if (demoContentAdapter != null) {
                            demoContentAdapter.notifyDataSetChanged();
                        }
                    }

                    if (jsonObject.has("nextPageToken")) {
                        PAGE_TOKEN = jsonObject.getString("nextPageToken");
                    } else {
                        PAGE_TOKEN = "";
                    }

                    isLoading = false;

                } else if (jsonObject.has("error")) {

                    if (subjects.isEmpty()) {
                        showErrorMsg(R.drawable.ic_something_wrong, jsonObject.getJSONObject("error").getString("message"));
                    }
                    isLoading = false;
                }


            } catch (JSONException e1) {
                if (subjects.isEmpty()) {
                    showErrorMsg(R.drawable.ic_something_wrong, "Something went wrong, please try again later");
                }
                isLoading = false;
            }


        }

    }

    public void showErrorMsg(int imgId, String msg) {
        pbMain.setVisibility(View.GONE);
        lstChapter.setVisibility(View.GONE);
        rlNoInternet.setVisibility(View.VISIBLE);
        tvErr.setText(msg);
        ivErr.setImageResource(imgId);
        if (imgId == R.drawable.ic_no_internet) {
            rlNoInternet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getYouTubeVideos();
                }
            });
        } else {
            rlNoInternet.setOnClickListener(null);
        }
    }

    public void hideErrorMsg() {
        lstChapter.setVisibility(View.VISIBLE);
        rlNoInternet.setVisibility(View.GONE);
    }

    @Override
    protected void onLayout() {
        lstChapter = (ListView) findViewById(R.id.lstChapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.demo_content_search, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {

            case R.id.action_search:
                startActivity(new Intent(this, SearchYouTubeActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }

    }

}
