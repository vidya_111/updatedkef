package com.observatory.kefcorner;


import android.util.Base64;

public class StringXORer {

    public static String encode(String s, String key) throws Exception {
        return base64Encode(xorWithKey(s.getBytes("UTF-8"), key.getBytes("UTF-8")));
    }

    public static String decode(String s, String key) throws Exception {
        return new String(xorWithKey(base64Decode(s), key.getBytes("UTF-8")), "UTF-8");
    }

    private static byte[] xorWithKey(byte[] a, byte[] key) {
        byte[] out = new byte[a.length];
        for (int i = 0; i < a.length; i++) {
            out[i] = (byte) (a[i] ^ key[i%key.length]);
        }
        return out;
    }

    private static byte[] base64Decode(String s) {
        return Base64.decode(s, Base64.DEFAULT);
    }

    private static String base64Encode(byte[] bytes) {
        String base64 = Base64.encodeToString(bytes, Base64.DEFAULT);
        return base64;
    }
}
