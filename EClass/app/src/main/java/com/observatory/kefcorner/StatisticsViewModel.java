package com.observatory.kefcorner;

/**
 * Created by gaurav on 16/1/18.
 */

public class StatisticsViewModel {

    public String contentName;
    public int contentType;
    public String duration;
    public String date;
    public String score;

    public StatisticsViewModel(int contentType, String contentName, String duration, String date) {
        this.contentType = contentType;
        this.contentName = contentName;
        this.duration = duration;
        this.date = date;
    }


}
