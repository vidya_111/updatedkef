package com.observatory.kefcorner;

import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.observatory.utils.BaseActivity;


public class KefPdfViewer extends BaseActivity {

    WebView pdfView;
    String link, title, gallery_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kef_pdf_viewer);
    }

    @Override
    protected void onLayout() {

        pdfView = findViewById(R.id.pdfView);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            link = bundle.getString("pdf_link");
            title = bundle.getString("title");
            gallery_id = bundle.getString("gallery_id");

            actionBar.setTitle(title);
        }

        pdfView.getSettings().setJavaScriptEnabled(true);
        pdfView.setWebViewClient(new WebViewClient());
        pdfView.getSettings().setAllowFileAccess(true);
        pdfView.loadUrl(link);
    }

}