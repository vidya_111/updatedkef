package com.observatory.kefcorner;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.observatory.Assessment.ConstantManager;
import com.observatory.AsyncTaskMain.AsyncTaskHelper;
import com.observatory.AsyncTaskMain.OnTaskComplete;
import com.observatory.adapters.MessagesAdapter;
import com.observatory.model.MessagesModel;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class MessagesActivity extends BaseActivity {

    /*Webservice*/
    private AsyncTaskHelper asyncTaskHelper;
    private String params;
    private JSONObject jsonObject;

    RelativeLayout no_data_layout;
    RecyclerView message_recycler_view;
    String UserType ="";

    List<MessagesModel> messageList = new ArrayList<>();
    MessagesAdapter adapter;
    MessagesModel model;

    SimpleDateFormat inputDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat outputDate = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    Date date = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        actionBar.setTitle("Messages");
    }

    @Override
    protected void onLayout() {
        no_data_layout = findViewById(R.id.no_data_layout);
        message_recycler_view = findViewById(R.id.message_recycler_view);
        Log.d("userId",":: "+SettingPreffrences.getUserid(getApplicationContext()));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        message_recycler_view.setLayoutManager(linearLayoutManager);

        getMessageData(SettingPreffrences.getUserid(getApplicationContext()));
    }

    private void getMessageData(String userID){
            if (CommonFunctions.isNetworkConnected(getApplicationContext())){

            CommonFunctions.showDialog(this);
            try {

                params = "userID=" + URLEncoder.encode(String.valueOf(userID), "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.KEF_Host + NetworkUrl.KEF_GetMessage, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {
                        CommonFunctions.hideDialog();
                        Log.d("getMessage","response: "+response);
                        if (!response.equals("error")){
                            getMessageResponse(response);
                        }
                    }
                },"GET");

                asyncTaskHelper.execute();

            }catch (Exception e){
                Log.e("Error: GetMessages: ",e.getMessage());
            }

        }else {
            alert("Internet Connection not available");
        }
    }

    private void getMessageResponse(String response){
        try {
            messageList.clear();
            JSONArray responseArray = new JSONArray(response);
            if (responseArray.length() == 0){
                no_data_layout.setVisibility(View.VISIBLE);
            }else {
                no_data_layout.setVisibility(View.GONE);
                for (int i = 0; i < responseArray.length(); i++){
                    jsonObject = responseArray.getJSONObject(i);
                    date = inputDate.parse(jsonObject.getString("MessageDateTime"));

                    Log.d("SchoolData","IdStored: "+ ConstantManager.School_Id+" IdResponse: "+
                            jsonObject.getString("SchoolID")+" sharedSchoolId: "+
                            SettingPreffrences.getSchoolId(getApplicationContext()));



                    if (SettingPreffrences.getUserType(getApplicationContext()).equals("ST")){
                        UserType = "Student";
                    }else if (SettingPreffrences.getUserType(getApplicationContext()).equals("TR")){
                        UserType = "Teacher";
                    }

                    Log.d("UserType","Pref:"+SettingPreffrences.getUserType(getApplicationContext())
                    +"json: "+jsonObject.getString("UserType")+" String: "+UserType);

                    if (SettingPreffrences.getSchoolId(getApplicationContext()).
                            equals(jsonObject.getString("SchoolID"))
                            && jsonObject.getString("UserType").equalsIgnoreCase(UserType)){
                        model = new MessagesModel(
                                jsonObject.getString("MessageID"),
                                jsonObject.getString("MessageBy"),
                                jsonObject.getString("MessageText"),
                                outputDate.format(date),
                                jsonObject.getString("SchoolID"),
                                jsonObject.getString("SchoolName")
                        );
                        messageList.add(model);
                    }
                }
                Collections.sort(messageList, new Comparator<MessagesModel>() {
                    @Override
                    public int compare(MessagesModel o1, MessagesModel o2) {
                        return o2.getMessageDateTime().compareTo(o1.getMessageDateTime());
                    }
                });

                adapter = new MessagesAdapter(messageList);
                message_recycler_view.setAdapter(adapter);
                //message_recycler_view.scrollToPosition(messageList.size()-1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}