package com.observatory.kefcorner;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.observatory.Assessment.AssessmentTypesActivity;
import com.observatory.Assessment.ConstantManager;
import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.SettingPreffrences;

/**
 * Created by Anuj on 27-07-2015.
 */
public class Disclaimer extends BaseActivity {

    private Button btOk;
    private CheckBox agree;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disclaimer);
        actionBar.setDisplayHomeAsUpEnabled(false);
    }

    @Override
    protected void onLayout() {

        btOk = (Button) findViewById(R.id.bt_next);
        btOk.setEnabled(false);
        btOk.setBackgroundColor(getResources().getColor(R.color.disable_date_color));
        agree = findViewById(R.id.agree_checkbox);

        agree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    btOk.setBackgroundColor(getResources().getColor(R.color.green));
                    btOk.setEnabled(true);
                }else {
                    btOk.setEnabled(false);
                    btOk.setBackgroundColor(getResources().getColor(R.color.disable_date_color));
                }
            }
        });

        btOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Agree_Checkbox","::"+agree.isChecked());
                if (agree.isChecked()){
                    SettingPreffrences.setDisclaimer(Disclaimer.this, true);

//                if (new File(App.BASE_PATH + "data-format.zip").exists()){
                    if(ConstantManager.isAssessment)
                    {
                        startActivity(AssessmentTypesActivity.class);
                    }
                    else
                    {
                        startActivity(Subjects.class);
                    }

//                }else{
//                startActivity(SelectMediumActivity.class);
//                }
                    //startActivity(SelectMediumActivity.class);
                    finish();
                }else {
                    Toast.makeText(getApplicationContext(),"Please select checkbox to continue..",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (App.IS_HAVING_MULTIPLE_STANDARD) {
          /*  startActivity(new Intent(this, Splash.class));
            finish();*/
        } else {
            if(ConstantManager.ISSINGLESTANDARD)
            {
             /*   startActivity(new Intent(this, Splash.class));
                finish();*/
            }
            else
            {
            //    super.onBackPressed();
            }
        }
        super.onBackPressed();
    }
}
