package com.observatory.kefcorner;

import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.observatory.Assessment.Model.OtherFilesModel;
import com.observatory.adapters.MyRecycleViewClickListener;
import com.observatory.adapters.OtherFileAdapter;
import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class OtherFilesActivity extends BaseActivity {

    RecyclerView r_view;
    OtherFileAdapter adapter;
    OtherFilesModel model;
    ArrayList<OtherFilesModel> dataList = new ArrayList<>();
    RelativeLayout no_data_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_files);
        actionBar.setTitle("Other Files");
    }

    @Override
    protected void onLayout() {

        r_view = findViewById(R.id.recycler_view_others);
        no_data_layout = findViewById(R.id.no_data_layout);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        r_view.setLayoutManager(linearLayoutManager);

        getData(App.BASE_PATH + "other");

        r_view.addOnItemTouchListener(new MyRecycleViewClickListener(getApplicationContext(),
                new MyRecycleViewClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        String filePath = App.BASE_PATH + "Other/" + dataList.get(position).getFileName();
                        Log.d("Details", ":: " + filePath);
                        Uri fileUri = null;
                        File file = new File(filePath);
                        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT){
                            fileUri = FileProvider.getUriForFile(OtherFilesActivity.this, BuildConfig.APPLICATION_ID + ".provider", file);
                        } else{
                            fileUri = Uri.fromFile(file);
                        }
                        openMedia(fileUri, filePath);
                    }
                }));
    }

    public void getData(String fileName) {
        File file = new File(fileName);
        File[] data = file.listFiles();

        if (data != null) {
            if (data.length > 0) {
                r_view.setVisibility(View.VISIBLE);
                for (int i = 0; i < data.length; i++) {

                    String date = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date(data[i].lastModified()));

                    model = new OtherFilesModel(data[i].getName(), date);
                    dataList.add(model);
                }
                adapter = new OtherFileAdapter(dataList);
                r_view.setAdapter(adapter);
            } else {
                r_view.setVisibility(View.GONE);
                no_data_layout.setVisibility(View.VISIBLE);
            }
        } else {
            r_view.setVisibility(View.GONE);
            no_data_layout.setVisibility(View.VISIBLE);
        }


    }

    public void openMedia(Uri uri, String url) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.toString().contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.toString().contains(".zip")) {
                // ZIP file
                intent.setDataAndType(uri, "application/zip");
            } else if (url.toString().contains(".rar")) {
                // RAR file
                intent.setDataAndType(uri, "application/x-rar-compressed");
            } else if (url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.toString().contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/*");
            } else if (url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") ||
                    url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                intent.setDataAndType(uri, "*/*");
            }
             startActivity(Intent.createChooser(intent,"Open with"));
            //startActivity(intent);
        } catch (Exception e) {
            Log.d("Error", e.getMessage());
        }
    }
}