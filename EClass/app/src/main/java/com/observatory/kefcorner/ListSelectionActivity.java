package com.observatory.kefcorner;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.observatory.database.Master;
import com.observatory.database.Subject;
import com.observatory.database.userStatistics;
import com.observatory.mcqdatabase.McqTestTable;
import com.observatory.mcqmodels.McqContentModel;
import com.observatory.mcqmodels.McqModel;
import com.observatory.mcqui.MCQActivity;
import com.observatory.utils.App;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.Constants;
import com.observatory.utils.SettingPreffrences;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.observatory.kefcorner.Content.CHAPTER_NAME;
import static com.observatory.utils.Constants.KEF_ContentTypeId_PDF;

/**
 * Created by kamlesh on 7/5/18.
 */

public class ListSelectionActivity extends BaseActivity {
    public static final int RQ_CONTENT = 11;
    public static final int RQ_CONTENT_ALL = 111;
    public static final int RQ_PDF = 12;
    public static final int RQ_MCQ = 13;
    public static final int RQ_MINDMAP = 14;
    public static final int RQ_MINDMAP_ALL = 114;
    public static final int RQ_QANDA = 15;
    public static final int RQ_QANDA_ALL = 115;
    public static final String TITLE = "List";
    public static final String LIST = "listOfItems";
    public static final String VIDEO_PATH = "videoPath";
    public static final String SELECTED_POSITION = "selPos";
    public static final String CONTENT_TYPE = "contentType";
    public static final String SHOULD_PLAY_ALL = "shouldPlayAll";
    public static final String SUBJECT_NAME_FOR_MCQ = "subjectName";
    public static final String CHAPTER_NAME_FOR_MCQ = "chapterName";

    private RecyclerView rvList;
    private View llPlayAllBtn;
    private String[] mList;

    private int currentlySelected = -1;
    private int currentlyPlaying = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_selection);
        rvList = ((RecyclerView) findViewById(R.id.rv_list));
        llPlayAllBtn = findViewById(R.id.ll_play_all_btn);
        int contentType = getIntent().getIntExtra(CONTENT_TYPE, -1);
        switch (contentType) {
            case RQ_CONTENT:
            case RQ_QANDA:
            case RQ_MINDMAP:
                llPlayAllBtn.setVisibility(View.VISIBLE);
                break;
            case RQ_MCQ:
            case RQ_PDF:
                llPlayAllBtn.setVisibility(View.GONE);
                break;
        }
        llPlayAllBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent();
//                intent.putExtra(SHOULD_PLAY_ALL, true);
//                setResult(Activity.RESULT_OK, intent);
//                finish();

                playAllBtnClicked();
            }
        });
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        currentlyPlaying = savedInstanceState.getInt("currentlyPlaying");
    }


    private void playAllBtnClicked() {
        int contentType = getIntent().getIntExtra(CONTENT_TYPE, -1);
        String videoPath = getIntent().getStringExtra(VIDEO_PATH);
        switch (contentType) {
            case RQ_CONTENT: {
                currentlyPlaying = 0;
                String filePath = videoPath + "Content/" + mList[currentlyPlaying] + ".mp4";
                System.out.println("Video Path for 1 " + filePath);
                Intent videoIntent = new Intent(this, Video.class);
                videoIntent.putExtra(getString(R.string.from), "content");
                videoIntent.putExtra(Video.VIDEO_PATH, filePath);
                videoIntent.putExtra(ListSelectionActivity.SHOULD_PLAY_ALL, true);
                startActivityForResult(videoIntent, RQ_CONTENT_ALL);
                currentlyPlaying = currentlyPlaying + 1;
            }
            break;
            case RQ_MINDMAP: {
                currentlyPlaying = 0;
                String filePath = videoPath + "Mind Map/" + mList[currentlyPlaying] + ".mp4";
                System.out.println("Video Path for 1 " + filePath);
                Intent videoIntent = new Intent(this, Video.class);
                videoIntent.putExtra(getString(R.string.from), "mm");
                videoIntent.putExtra(Video.VIDEO_PATH, filePath);
                videoIntent.putExtra(ListSelectionActivity.SHOULD_PLAY_ALL, true);
                startActivityForResult(videoIntent, RQ_MINDMAP_ALL);
                currentlyPlaying = currentlyPlaying + 1;
            }
            break;
            case RQ_QANDA: {
                currentlyPlaying = 0;
                String filePath = videoPath + "Q and A/" + mList[currentlyPlaying] + ".mp4";
                System.out.println("Video Path for 1 " + filePath);
                Intent videoIntent = new Intent(this, Video.class);
                videoIntent.putExtra(getString(R.string.from), "qb");
                videoIntent.putExtra(Video.VIDEO_PATH, filePath);
                videoIntent.putExtra(ListSelectionActivity.SHOULD_PLAY_ALL, true);
                startActivityForResult(videoIntent, RQ_QANDA_ALL);
                currentlyPlaying = currentlyPlaying + 1;
            }
        }
    }

    @Override
    protected void onLayout() {
        actionBar.setTitle(getIntent().getStringExtra(TITLE));
        mList = getIntent().getStringArrayExtra(LIST);
    }

    @Override
    protected void onResume() {
        super.onResume();

        rvList.setAdapter(new ListAdapter());
    }

    //    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (resultCode == Activity.RESULT_OK) {
//            int selPosition = data.getIntExtra(ListSelectionActivity.SELECTED_POSITION, 0);
//            boolean shouldPlayAll = data.getBooleanExtra(ListSelectionActivity.SHOULD_PLAY_ALL, false);
//            String filePath;
//
//            switch (requestCode) {
//                case RQ_CONTENT:
//                    if (shouldPlayAll) {
//                        // should play all for content click
//                        Toast.makeText(getApplicationContext(), "Play all content videos!", Toast.LENGTH_LONG).show();
//                        currentlyPlaying = 0;
//                        filePath = VIDEO_PATH + "Content/" + mList[currentlyPlaying] + ".mp4";
//                        System.out.println("Video Path for 1 " + filePath);
//                        Intent videoIntent = new Intent(this, Video.class);
//                        videoIntent.putExtra(getString(R.string.from), "content");
//                        videoIntent.putExtra(Video.VIDEO_PATH, filePath);
//                        videoIntent.putExtra(ListSelectionActivity.SHOULD_PLAY_ALL, true);
//                        startActivityForResult(videoIntent, RQ_CONTENT_ALL);
//                        currentlyPlaying = currentlyPlaying + 1;
//                    } else {
////                        // play only clicked content
////                        filePath = VIDEO_PATH + "Content/" + mList[selPosition] + ".mp4";
////                        openContent(filePath);
//                    }
//                    break;
//                case RQ_CONTENT_ALL: {
//                    // continue play all for content
//                    if (currentlyPlaying <= (mList.length - 1)) {
//                        filePath = VIDEO_PATH + "Content/" + mList[currentlyPlaying] + ".mp4";
//                        System.out.println("Video Path for 1 " + filePath);
//                        Intent videoIntent = new Intent(this, Video.class);
//                        videoIntent.putExtra(getString(R.string.from), "content");
//                        videoIntent.putExtra(Video.VIDEO_PATH, filePath);
//                        if (currentlyPlaying < mList.length - 1) {
//                            videoIntent.putExtra(ListSelectionActivity.SHOULD_PLAY_ALL, true);
//                        } else {
//                            videoIntent.putExtra(ListSelectionActivity.SHOULD_PLAY_ALL, false);
//                        }
//                        startActivityForResult(videoIntent, RQ_CONTENT_ALL);
//                        currentlyPlaying = currentlyPlaying + 1;
//                    } else {
//                        Toast.makeText(getApplicationContext(), "All videos of content have been played!", Toast.LENGTH_LONG).show();
//                    }
//                }
//                break;
//                case RQ_PDF:
//                    filePath = VIDEO_PATH + "PDF/" + mList[selPosition] + ".pdf";
//                    openPdf(filePath);
//                    break;
//                case RQ_MINDMAP:
//                    if (shouldPlayAll) {
//                        Toast.makeText(getApplicationContext(), "Play all mindmap videos!", Toast.LENGTH_LONG).show();
//                        currentlyPlaying = 0;
//                        filePath = VIDEO_PATH + "Mind Map/" + mList[currentlyPlaying] + ".mp4";
//                        System.out.println("Video Path for 1 " + filePath);
//                        Intent videoIntent = new Intent(this, Video.class);
//                        videoIntent.putExtra(getString(R.string.from), "mm");
//                        videoIntent.putExtra(Video.VIDEO_PATH, filePath);
//                        videoIntent.putExtra(ListSelectionActivity.SHOULD_PLAY_ALL, true);
//                        startActivity(videoIntent);
//                    } else {
//                        filePath = VIDEO_PATH + "Mind Map/" + mList[selPosition] + ".mp4";
//                        openMindmap(filePath);
//                    }
//                    break;
//                case RQ_MINDMAP_ALL: {
//                    if (currentlyPlaying <= (mList.length - 1)) {
//                        filePath = VIDEO_PATH + "Mind Map/" + mList[currentlyPlaying] + ".mp4";
//                        System.out.println("Video Path for 1 " + filePath);
//                        Intent videoIntent = new Intent(this, Video.class);
//                        videoIntent.putExtra(getString(R.string.from), "content");
//                        videoIntent.putExtra(Video.VIDEO_PATH, filePath);
//                        if (currentlyPlaying < mList.length - 1) {
//                            videoIntent.putExtra(ListSelectionActivity.SHOULD_PLAY_ALL, true);
//                        } else {
//                            videoIntent.putExtra(ListSelectionActivity.SHOULD_PLAY_ALL, false);
//                        }
//                        startActivityForResult(videoIntent, RQ_MINDMAP_ALL);
//                        currentlyPlaying = currentlyPlaying + 1;
//                    } else {
//                        Toast.makeText(getApplicationContext(), "All videos of mindmap have been played!", Toast.LENGTH_LONG).show();
//                    }
//                }
//                break;
//                case RQ_QANDA:
//                    if (shouldPlayAll) {
//                        Toast.makeText(getApplicationContext(), "Play all Q&A videos!", Toast.LENGTH_LONG).show();
//                        currentlyPlaying = 0;
//                        filePath = VIDEO_PATH + "Q and A/" + mList[currentlyPlaying] + ".mp4";
//                        System.out.println("Video Path for 1 " + filePath);
//                        Intent videoIntent = new Intent(this, Video.class);
//                        videoIntent.putExtra(getString(R.string.from), "qb");
//                        videoIntent.putExtra(Video.VIDEO_PATH, filePath);
//                        videoIntent.putExtra(ListSelectionActivity.SHOULD_PLAY_ALL, true);
//                        startActivity(videoIntent);
//                    } else {
//                        filePath = VIDEO_PATH + "Q and A/" + mList[selPosition] + ".mp4";
//                        openQAndA(filePath);
//                    }
//                    break;
//                case RQ_QANDA_ALL: {
//                    if (currentlyPlaying <= (mList.length - 1)) {
//                        filePath = VIDEO_PATH + "Q and A/" + mList[currentlyPlaying] + ".mp4";
//                        System.out.println("Video Path for 1 " + filePath);
//                        Intent videoIntent = new Intent(this, Video.class);
//                        videoIntent.putExtra(getString(R.string.from), "qb");
//                        videoIntent.putExtra(Video.VIDEO_PATH, filePath);
//                        if (currentlyPlaying < mList.length - 1) {
//                            videoIntent.putExtra(ListSelectionActivity.SHOULD_PLAY_ALL, true);
//                        } else {
//                            videoIntent.putExtra(ListSelectionActivity.SHOULD_PLAY_ALL, false);
//                        }
//                        startActivityForResult(videoIntent, RQ_QANDA_ALL);
//                        currentlyPlaying = currentlyPlaying + 1;
//                    } else {
//                        Toast.makeText(getApplicationContext(), "All videos of Q&A have been played!", Toast.LENGTH_LONG).show();
//                    }
//                }
//                break;
//                case RQ_MCQ: {
//                    String subName = getIntent().getStringExtra(SUBJECT_NAME_FOR_MCQ);
//                    String chapName = getIntent().getStringExtra(CHAPTER_NAME_FOR_MCQ);
//                    Subject subject = Subject.find(Subject.class, "subject_name = ? and chapter_name = ?", new String[]{subName, chapName}).get(0);
//                    int chapSerialNo = Integer.parseInt(subject.getMcq());
//                    McqTestTable mcqTestTable = new McqTestTable();
//                    List<McqModel> mcqList = mcqTestTable.getMcqTest(chapSerialNo);
//                    openMcq(mcqList.get(selPosition));
//                }
//                break;
//            }
//        }
//    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            String videoPath = getIntent().getStringExtra(VIDEO_PATH);
            switch (requestCode) {
                case RQ_CONTENT_ALL: {
                    if (currentlyPlaying <= (mList.length - 1)) {
                        String filePath = videoPath + "Content/" + mList[currentlyPlaying] + ".mp4";
                        System.out.println("Video Path for 1 " + filePath);
                        Intent videoIntent = new Intent(this, Video.class);
                        videoIntent.putExtra(getString(R.string.from), "content");
                        videoIntent.putExtra(Video.VIDEO_PATH, filePath);
                        videoIntent.putExtra(ListSelectionActivity.SHOULD_PLAY_ALL, true);
                        startActivityForResult(videoIntent, RQ_CONTENT_ALL);
                        currentlyPlaying = currentlyPlaying + 1;
                    }else{
                        try{
                            ((App)getApplicationContext()).deleteTemp();
                        }catch(Exception e){

                        }
                    }
                }
                break;
                case RQ_MINDMAP_ALL: {
                    if (currentlyPlaying <= (mList.length - 1)) {
                        String filePath = videoPath + "Mind Map/" + mList[currentlyPlaying] + ".mp4";
                        System.out.println("Video Path for 1 " + filePath);
                        Intent videoIntent = new Intent(this, Video.class);
                        videoIntent.putExtra(getString(R.string.from), "content");
                        videoIntent.putExtra(Video.VIDEO_PATH, filePath);
                        videoIntent.putExtra(ListSelectionActivity.SHOULD_PLAY_ALL, true);
                        startActivityForResult(videoIntent, RQ_MINDMAP_ALL);
                        currentlyPlaying = currentlyPlaying + 1;
                    }else{
                        try{
                            ((App)getApplicationContext()).deleteTemp();
                        }catch(Exception e){

                        }
                    }
                }
                break;
                case RQ_QANDA_ALL: {
                    if (currentlyPlaying <= (mList.length - 1)) {
                        String filePath = videoPath + "Q and A/" + mList[currentlyPlaying] + ".mp4";
                        System.out.println("Video Path for 1 " + filePath);
                        Intent videoIntent = new Intent(this, Video.class);
                        videoIntent.putExtra(getString(R.string.from), "qb");
                        videoIntent.putExtra(Video.VIDEO_PATH, filePath);
                        videoIntent.putExtra(ListSelectionActivity.SHOULD_PLAY_ALL, true);
                        startActivityForResult(videoIntent, RQ_QANDA_ALL);
                        currentlyPlaying = currentlyPlaying + 1;
                    }else{
                        try{
                            ((App)getApplicationContext()).deleteTemp();
                        }catch(Exception e){

                        }
                    }
                }
                break;
            }
        }
    }

    private void openMcq(McqModel mcq) {
        Intent intent = new Intent(ListSelectionActivity.this, MCQActivity.class);
        intent.putExtra(McqContentModel.MCQ_SERIAL_NO, mcq.mcq_id);
        intent.putExtra(McqContentModel.MCQ_NAME, mcq.chapter_name);
        intent.putExtra(McqContentModel.MCQ_FONT, mcq.font);
        startActivity(intent);
    }

    private void openPdf(String filePath) {
        File file = new File(filePath);

        Uri uri = FileProvider.getUriForFile(getApplicationContext(),
                BuildConfig.APPLICATION_ID + ".provider",
                file);




        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(uri, "application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        target.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
            String userId = SettingPreffrences.getUserid(getApplicationContext()).isEmpty() ? "0" : SettingPreffrences.getUserid(getApplicationContext());
            String imei_number = CommonFunctions.getUniqueDeviceId(getApplicationContext());
            Master master = Master.listAll(Master.class).get(0);
            String medium = master.getMedium();
            String chapter = CommonFunctions.removeApostrophe(SettingPreffrences.getChapter(this));
            String subject = CommonFunctions.removeApostrophe(SettingPreffrences.getSubject(this));
            String date = new SimpleDateFormat(Constants.DATABASE_DATE_FORMAT).format(new Date());
            /*Statistics statistics = new Statistics(userId, imei_number, medium, subject, chapter, CommonFunctions.removeFormatSuffix(file.getName()), CONTENT_TYPE_PDF, 0, date);
            statistics.save();*/
            Log.d("SyncSubjectData","listSelection:"+SettingPreffrences.getSubjectId(getApplicationContext()));
            userStatistics userStatistics = new userStatistics(
                    userId,
                    SettingPreffrences.getCourseId(getApplicationContext()),
                    SettingPreffrences.getSubjectId(getApplicationContext()),
                    SettingPreffrences.getChapterId(getApplicationContext()),
                    KEF_ContentTypeId_PDF,
                    CommonFunctions.removeFormatSuffix(file.getName()),
                    "0",
                    "0",
                    date,
                    date);

            userStatistics.save();

        } catch (ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(), "Please install a PDF Viewer to view this " + "document", Toast.LENGTH_LONG).show();
        }
    }

    private void openVideo(String videoPath, String from) {
        System.out.println("Video Path for 1 " + videoPath);
        Intent videoIntent = new Intent(this, Video.class);
        videoIntent.putExtra(getString(R.string.from), from);
        videoIntent.putExtra(Video.VIDEO_PATH, videoPath);
        startActivity(videoIntent);

        App.getInstance().trackEvent("Video", "Screen", "selectedLanguage - " + SettingPreffrences.getLanguage(getApplicationContext()) + " ; " + "selectedStandard - " + SettingPreffrences.getStandard(ListSelectionActivity.this) + "" + " ; " + "selectedSubject - " + SettingPreffrences.getSubject(ListSelectionActivity.this) + " ;" + " " + "selectedChapter - " + getIntent().getStringExtra(CHAPTER_NAME), videoPath);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("currentlyPlaying", currentlyPlaying);
        super.onSaveInstanceState(outState);
    }

    class ListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // reusing 'item_chapter'
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chapter, parent, false);

            return new ListViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ListViewHolder listViewHolder = (ListViewHolder) holder;
            listViewHolder.tvItemName.setText(CommonFunctions.removeFormatPrefix(mList[position]));
            listViewHolder.tvItemNumber.setText(String.valueOf(position + 1));

            if (currentlySelected == position) {
                holder.itemView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.light_grey));
            } else {
                holder.itemView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            }
        }

        @Override
        public int getItemCount() {
            return mList.length;
        }


        class ListViewHolder extends RecyclerView.ViewHolder {
            private final TextView tvItemName;
            private final TextView tvItemNumber;

            public ListViewHolder(View itemView) {
                super(itemView);
                tvItemName = (TextView) itemView.findViewById(R.id.chapterName);
                tvItemNumber = (TextView) itemView.findViewById(R.id.chapterNumber);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        currentlySelected = getLayoutPosition();
//                        Intent data = new Intent();
//                        data.putExtra(SELECTED_POSITION, getLayoutPosition());
//                        setResult(Activity.RESULT_OK, data);
//                        finish();

                        // play only clicked content
                        String videoPath = getIntent().getStringExtra(VIDEO_PATH);
                        int contentType = getIntent().getIntExtra(CONTENT_TYPE, -1);
                        switch (contentType) {
                            case RQ_CONTENT: {
                                String filePath = videoPath + "Content/" + mList[getLayoutPosition()] + ".mp4";
                                openVideo(filePath, "content");
                            }
                            break;
                            case RQ_QANDA: {
                                String filePath = videoPath + "Q and A/" + mList[getLayoutPosition()] + ".mp4";
                                openVideo(filePath, "qb");
                            }
                            break;
                            case RQ_MINDMAP: {
                                String filePath = videoPath + "Mind Map/" + mList[getLayoutPosition()] + ".mp4";
                                openVideo(filePath, "mm");
                            }
                            break;
                            case RQ_MCQ: {
                                String subName = getIntent().getStringExtra(SUBJECT_NAME_FOR_MCQ);
                                String chapName = getIntent().getStringExtra(CHAPTER_NAME_FOR_MCQ);
                                Subject subject = Subject.find(Subject.class, "subject_name = ? and chapter_name = ?", new String[]{subName, chapName}).get(0);
                                int chapSerialNo = Integer.parseInt(subject.getMcq());
                                McqTestTable mcqTestTable = new McqTestTable();
                                List<McqModel> mcqList = mcqTestTable.getMcqTest(chapSerialNo);
                                openMcq(mcqList.get(getLayoutPosition()));
                            }
                            break;
                            case RQ_PDF: {
                                String filePath = videoPath + "PDF/" + mList[getLayoutPosition()] + ".pdf";
                                openPdf(filePath);
                            }
                            break;
                        }
                    }
                });
            }
        }
    }
}
