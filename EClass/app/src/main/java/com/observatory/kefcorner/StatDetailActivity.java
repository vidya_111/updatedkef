package com.observatory.kefcorner;

import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;

import com.observatory.adapters.StatisticDetailAdapter;
import com.observatory.database.StatDetails;
import com.observatory.database.Statistics;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.SettingPreffrences;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anuj on 04/04/17.
 */

public class StatDetailActivity extends BaseActivity {

    RecyclerView rvStats;
    String chapter;
    String subject;
    int typeId;
    List<StatDetails> listStat = new ArrayList<>();
    StatDetails model;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stat_details);
    }

    @Override
    protected void onLayout() {

        rvStats = (RecyclerView) findViewById(R.id.rv_common);

        chapter = getIntent().getExtras().getString("chapter", "");
        subject = getIntent().getExtras().getString("subject", "");
        typeId = getIntent().getExtras().getInt("type", 0);

        if (typeId == 1) {
            actionBar.setTitle("CONTENT");
        } else if (typeId == 2) {
            actionBar.setTitle("MINDMAP");
        } else if (typeId == 3) {
            actionBar.setTitle("Q&A");
        }


        getData();

    }

    private void getData() {

        List<Statistics> list = Select.from(Statistics.class).where(
                Condition.prop("contentType").eq(typeId),
                Condition.prop("chapter").eq(chapter),
                Condition.prop("subject").eq(subject),
                Condition.prop("userId").eq(SettingPreffrences.getUserid(this)),
                Condition.prop("deviceId").eq(CommonFunctions.getUniqueDeviceId(getApplicationContext())))
                        .orderBy("video").list();

        String name = "header";

//        List<StatisticsNew> list = new ArrayList<>();
//        StatisticsNew s = new StatisticsNew();
//        s.setVideo("1.m");
//        s.setDuration(80);
//        s.setCreated_at("");
//        list.add(s);
//
//        s = new StatisticsNew();
//        s.setVideo("1.m");
//        s.setDuration(60);
//        s.setCreated_at("");
//        list.add(s);
//
//        s = new StatisticsNew();
//        s.setVideo("2.m");
//        s.setDuration(40);
//        s.setCreated_at("");
//        list.add(s);
//
//        s = new StatisticsNew();
//        s.setVideo("2.m");
//        s.setDuration(20);
//        s.setCreated_at("");
//        list.add(s);

        for (int i = 0; i < list.size(); i++) {

            model = new StatDetails();

            if (name.equalsIgnoreCase(list.get(i).getContent_name())) {
                model.isHeader = false;
            } else {
                model.isHeader = true;
                model.header = "VIDEO " + list.get(i).getContent_name().split("\\.")[0];
                listStat.add(model);
            }

            model = new StatDetails();
            model.isHeader = false;
            model.duration = "" + getFormattedTime(list.get(i).getDuration());
            model.lastSeen = "" + list.get(i).getCreated_at();
            listStat.add(model);

            name = list.get(i).getContent_name();

        }


        if (listStat.size() > 0) {
            rvStats.setAdapter(new StatisticDetailAdapter(this, listStat));
        }

    }

    private static String getFormattedTime(int secs) {
        // int secs = (int) Math.round((double) milliseconds / 1000); // for millisecs arg instead of secs

        secs = secs;
        if (secs < 60)
            return secs + "s";
        else {
            int mins = (int) secs / 60;
            int remainderSecs = secs - (mins * 60);
            if (mins < 60) {
                return (mins < 10 ? "0" : "") + mins + "m "
                        + (remainderSecs < 10 ? "0" : "") + remainderSecs + "s";
            } else {
                int hours = (int) mins / 60;
                int remainderMins = mins - (hours * 60);
                return (hours < 10 ? "0" : "") + hours + "h "
                        + (remainderMins < 10 ? "0" : "") + remainderMins + "m "
                        + (remainderSecs < 10 ? "0" : "") + remainderSecs + "s";
            }
        }
    }


}
