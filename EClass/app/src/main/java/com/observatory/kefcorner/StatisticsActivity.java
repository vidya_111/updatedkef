package com.observatory.kefcorner;

import android.app.AlertDialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.observatory.database.Master;
import com.observatory.database.userStatistics;
import com.observatory.model.userStatisticsViewModel;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.Constants;
import com.observatory.utils.SettingPreffrences;
import com.orm.query.Select;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.observatory.kefcorner.R.menu.statistics;

/**
 * Created by Anuj on 13-06-2015.
 */
public class StatisticsActivity extends BaseActivity implements DatePickerFragment.OnDateSetListener {


    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private TextView tvStatisticsNotFound;
    private String fromDate;
    private String toDate;

    public final String START_HR_MIN = "00:00";
    public final String END_HR_MIN = "23:59";
    private String medium;
    private String chapter;
    private String subject;
    private String userId;
    private TextView tvFromDate;
    private TextView tvToDate;

    public static final String CONTENT_TYPE_ALL = "All";

    private String selContentType = CONTENT_TYPE_ALL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
    }


    @Override
    protected void onLayout() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        tvStatisticsNotFound = (TextView) findViewById(R.id.tv_no_statistics_found);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        List<Master> masterList = Master.listAll(Master.class);
        medium = masterList.get(0).getMedium();
        chapter = CommonFunctions.removeApostrophe(SettingPreffrences.getChapter(getApplicationContext()));
        subject = CommonFunctions.removeApostrophe(SettingPreffrences.getSubject(getApplicationContext()));
        userId = SettingPreffrences.getUserid(getApplicationContext()).isEmpty() ? "0" : SettingPreffrences.getUserid(getApplicationContext());


        getStatistics(false);

        // initializing 'fromDate' and 'toDate' to todays date
        String currentDate = new SimpleDateFormat(Constants.UI_DATE_FORMAT_ONLY_DATE).format(new Date());
        fromDate = currentDate;
        toDate = currentDate;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(statistics, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {

            case R.id.action_filter:

                showFilterDialog();

                break;
        }


        return super.onOptionsItemSelected(item);
    }


    private void getStatistics(boolean isFilterApplied) {

        String whereClause;

        /*if (isFilterApplied) {
            whereClause = "medium = '" + medium + "' and subject = '" + subject + "' and chapter = '" + chapter + "' and userId = '" + userId + filterString();
        } else {
            whereClause = "medium = '" + medium + "' and subject = '" + subject + "' and chapter = '" + chapter + "' and userId = '" + userId + "'";
        }*/

        if (isFilterApplied) {
            whereClause = "SUBJECT_ID = '" + SettingPreffrences.getSubjectId(getApplicationContext()) + "' and CHAPTER_ID = '" + SettingPreffrences.getChapterId(getApplicationContext()) + "' and USER_ID = '" + userId + filterString();
        } else {
            whereClause = "SUBJECT_ID = '" + SettingPreffrences.getSubjectId(getApplicationContext()) + "' and CHAPTER_ID = '" + SettingPreffrences.getChapterId(getApplicationContext()) + "' and USER_ID = '" + userId + "'";
        }

        List<userStatistics> statisticsList = Select.from(userStatistics.class).where(whereClause).orderBy("CREATED_ON desc").list();

        List<userStatisticsViewModel> statisticsViewModelList = mapToStatsViewModel(statisticsList);


        if (statisticsViewModelList.size() > 0) {

            recyclerView.setVisibility(View.VISIBLE);
            tvStatisticsNotFound.setVisibility(View.GONE);

            StatisticsAdapter statisticsAdapter = new StatisticsAdapter(this, statisticsViewModelList, chapter);
            recyclerView.setAdapter(statisticsAdapter);

        } else {

            recyclerView.setVisibility(View.GONE);
            tvStatisticsNotFound.setVisibility(View.VISIBLE);

        }


    }

    private String getSelContentType() {
        String contentTypeStr = null;

        switch (selContentType) {
            case Constants.KEF_ContentTypeName_Content:
                contentTypeStr = Constants.KEF_ContentTypeId_Content;
                break;
            case Constants.KEF_ContentTypeName_MindMap:
                contentTypeStr = Constants.KEF_ContentTypeId_MindMap;
                break;
            case Constants.KEF_ContentTypeName_QNA:
                contentTypeStr = Constants.KEF_ContentTypeId_QNA;
                break;
            case Constants.KEF_ContentTypeName_PDF:
                contentTypeStr = Constants.KEF_ContentTypeId_PDF;
                break;

            case Constants.KEF_ContentTypeName_MCQ:
                contentTypeStr = Constants.KEF_ContentTypeName_MCQ;
                break;
        }

        return contentTypeStr;
    }

    @NonNull
    private String filterString() {

        // here we tweak From and To Date for filtering purposes.
        // From Date - we add '00:00' to get data from the beginning.
        // To Date - we add '23:59' to get data till the end.
        String fromDateDb = CommonFunctions.formatDate(fromDate, Constants.UI_DATE_FORMAT_ONLY_DATE, Constants.FILTER_DATE_FORMAT) + ", " + START_HR_MIN;
        String toDateDb = CommonFunctions.formatDate(toDate, Constants.UI_DATE_FORMAT_ONLY_DATE, Constants.FILTER_DATE_FORMAT) + ", " + END_HR_MIN;


        String filterString = "' and CREATED_ON BETWEEN '"
                + fromDateDb
                + "' AND '"
                + toDateDb
                + "'";

        if (getSelContentType() != null) filterString +=  " and CONTENT_TYPE_ID = '" + getSelContentType() +"'";

        return filterString;
    }

    private void showFilterDialog() {

        View dialogView = View.inflate(this, R.layout.dialog_filter_statistics, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        final AlertDialog dialog = builder.create();

        tvFromDate = (TextView) dialogView.findViewById(R.id.tv_from_date);
        tvToDate = (TextView) dialogView.findViewById(R.id.tv_to_date);
        Spinner spContentType = (Spinner) dialogView.findViewById(R.id.sp_content);
        View tvApplyBtn = dialogView.findViewById(R.id.tv_apply_btn);
        View tvCancelBtn = dialogView.findViewById(R.id.tv_cancel_btn);

        tvFromDate.setText(fromDate);
        tvToDate.setText(toDate);
        final List<String> spinnerItems = getSpinnerItems();
        spContentType.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_dropdown_item, spinnerItems));
        spContentType.setSelection(0);

        tvFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialogFragment = DatePickerFragment.newInstance(getDateAsLong(fromDate),
                        getDateAsLong(toDate), DatePickerFragment.SELECT_FROM_DATE);
                dialogFragment.show(getSupportFragmentManager(), DatePickerFragment.class.toString());
            }
        });

        tvToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialogFragment = DatePickerFragment.newInstance(getDateAsLong(fromDate),
                        getDateAsLong(toDate), DatePickerFragment.SELECT_TO_DATE);
                dialogFragment.show(getSupportFragmentManager(), DatePickerFragment.class.toString());
            }
        });

        spContentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selContentType = spinnerItems.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tvApplyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getStatistics(true);
            }
        });

        tvCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private ArrayList<String> getSpinnerItems() {
        ArrayList<String> strings = new ArrayList<>();
        strings.add(CONTENT_TYPE_ALL);
        strings.add(Constants.KEF_ContentTypeName_Content);
        strings.add(Constants.KEF_ContentTypeName_MindMap);
        strings.add(Constants.KEF_ContentTypeName_QNA);
        strings.add(Constants.KEF_ContentTypeName_PDF);
        strings.add(Constants.KEF_ContentTypeName_MCQ);
        return strings;
    }

    private long getDateAsLong(String date) {

        long dateLong;

        try {

            dateLong = new SimpleDateFormat(Constants.UI_DATE_FORMAT_ONLY_DATE).parse(date).getTime();

        } catch (ParseException e) {

            dateLong = DatePickerFragment.DATE_NOT_SET;

        }
        return dateLong;
    }

    private void addFakeDataForTesting() {
//        Statistics statistics1 = new Statistics("0", "358956060964336", "2nd STD English", "Balbharti", "Introduction 2", "1.mp4", 2, 20, "2018-01-18, 10:31");
//        statistics1.save();
//        Statistics statistics2 = new Statistics("0", "358956060964336", "2nd STD English", "Balbharti", "Introduction 2", "1.mp4", 3, 20, "2018-01-17, 10:31");
//        statistics2.save();
//        Statistics statistics3 = new Statistics("0", "358956060964336", "2nd STD English", "Balbharti", "Introduction 2", "1.mp4", 1, 20, "2018-01-16, 10:31");
//        statistics3.save();
//        Statistics statistics4 = new Statistics("0", "358956060964336", "2nd STD English", "Balbharti", "Introduction 2", "1.mp4", 2, 20, "2018-01-15, 10:31");
//        statistics4.save();
//        Statistics statistics5 = new Statistics("0", "358956060964336", "2nd STD English", "Balbharti", "Introduction 2", "1.mp4", 3, 20, "2018-01-14, 10:31");
//        statistics5.save();
    }

    private List<userStatisticsViewModel> mapToStatsViewModel(List<userStatistics> statisticsList) {

        List<userStatisticsViewModel> statisticsViewModelList = new ArrayList<>();

        for (userStatistics statistic : statisticsList) {
            String contentName = statistic.getContentName();
           // String duration = formatDuration(Integer.parseInt(statistic.getAccessDuration()));
            String duration = statistic.getAccessDuration();
            String date = formatDate(statistic);
            //StatisticsViewModel viewModel = new StatisticsViewModel(statistic.getContentTypeID(), contentName, duration, date);
            userStatisticsViewModel viewModel = new userStatisticsViewModel(statistic.getContentTypeID(), contentName, duration, date);
            if (statistic.getScore() != null && !statistic.getScore().isEmpty()) {
                viewModel.score = statistic.getScore();
            }
            statisticsViewModelList.add(viewModel);
        }

        return statisticsViewModelList;
    }

    private String formatDate(userStatistics statistics) {
        String date = CommonFunctions.formatDate(statistics.getCreatedOn(), Constants.DATABASE_DATE_FORMAT, Constants.UI_DATE_FORMAT);
        return date ;
    }

    private String formatDuration(int duration) {
        return String.valueOf(duration);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day, int whichDate) {

        String date = getDay(day) + " " + getMonth(month) + " " + year;

        if (whichDate == DatePickerFragment.SELECT_FROM_DATE) {
            fromDate = date;
            if (tvFromDate != null) tvFromDate.setText(fromDate);
        } else {
            toDate = date;
            if (tvToDate != null) tvToDate.setText(toDate);
        }
    }

    private String getDay(int day) {
        return String.format("%02d", day);
    }


    private String getMonth(int month) {

        String monthStr;

        switch (month) {

            case 0:
                monthStr = "Jan";
                break;

            case 1:
                monthStr = "Feb";
                break;

            case 2:
                monthStr = "Mar";
                break;

            case 3:
                monthStr = "Apr";
                break;

            case 4:
                monthStr = "May";
                break;

            case 5:
                monthStr = "Jun";
                break;

            case 6:
                monthStr = "Jul";
                break;

            case 7:
                monthStr = "Aug";
                break;

            case 8:
                monthStr = "Sep";
                break;

            case 9:
                monthStr = "Oct";
                break;

            case 10:
                monthStr = "Nov";
                break;

            case 11:
                monthStr = "Dec";
                break;

            // error case
            default:
                monthStr = "Month value is invalid";
        }

        return monthStr;
    }
}
