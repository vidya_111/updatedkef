package com.observatory.kefcorner;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.observatory.AsyncTaskMain.AsyncTaskHelper;
import com.observatory.AsyncTaskMain.OnTaskComplete;
import com.observatory.database.OfflineUsers;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created by Anuj on 06-01-2016.
 */
public class ChangePassword extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "ChangePassword";

    private EditText etOldPwd;
    private EditText etNewPwd;
    private EditText etConfirmPwd;

    private TextView tvChangePwd;

    /*Webservice*/
    private AsyncTaskHelper asyncTaskHelper;
    private String params;

    /*Json*/
    private JSONObject jsonObject;
    private JSONArray jsonArray;
    private String status;
    private String msg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.change_password);
    }

    @Override
    protected void onLayout() {

        etOldPwd = (EditText) findViewById(R.id.et_old_pass);
        etNewPwd = (EditText) findViewById(R.id.et_new_pass);
        etConfirmPwd = (EditText) findViewById(R.id.et_confirm_pass);

        tvChangePwd = (TextView) findViewById(R.id.tv_p_changepwd);


        tvChangePwd.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.tv_p_changepwd:

                if (TextUtils.isEmpty(etOldPwd.getText().toString()) || TextUtils.isEmpty(etNewPwd.getText().toString()) || TextUtils.isEmpty(etConfirmPwd.getText().toString())) {

                    alert("Fields cannot be empty");
                } else if (etOldPwd.getText().toString().length() < 6 || etNewPwd.getText().toString().length() < 6 || etConfirmPwd.getText().toString().length() < 6) {
                    alert("Password cannot be less then 6 characters");

                } else if (!etNewPwd.getText().toString().equals(etConfirmPwd.getText().toString())) {
                    alert("Passwords do not match");
                } else {

                    if (SettingPreffrences.getUserid(this) == "0") {

                        if (etNewPwd.getText().toString() != SettingPreffrences.getPassword(this)) {

                            alert("Old password do not match");

                        } else {

                            List<OfflineUsers> list = OfflineUsers.find(OfflineUsers.class, "email = ?", SettingPreffrences.getEmail(getApplicationContext()));
                            OfflineUsers offlineUsers = list.get(0);
                            offlineUsers.setPassword(etNewPwd.getText().toString());
                            offlineUsers.save();
                            alert("Password changed successfully");
                            this.onBackPressed();
                            finish();
                        }

                        return;

                    }
                    callChangePwsWS();

                }


                break;
        }
    }

    private void callChangePwsWS() {


        if (CommonFunctions.isNetworkConnected(getApplicationContext())) {

            CommonFunctions.showDialog(this);
            try {
                params = "os=" + URLEncoder.encode(Build.VERSION.RELEASE, "UTF-8")
                        + "&make=" + URLEncoder.encode(Build.MANUFACTURER, "UTF-8")
                        + "&model=" + URLEncoder.encode(Build.MODEL, "UTF-8")
                        + "&userId=" + URLEncoder.encode(SettingPreffrences.getUserid(getApplicationContext()), "UTF-8")
                        + "&token=" + URLEncoder.encode(SettingPreffrences.getToken(getApplicationContext()), "UTF-8")
                        + "&currentPassword=" + URLEncoder.encode(etOldPwd.getText().toString(), "UTF-8")
                        + "&newPassword=" + URLEncoder.encode(etNewPwd.getText().toString(), "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.host + NetworkUrl.changePwd, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {

                        Log.d("ApiResponse","ChangePassword: "+response);

                        if (!response.equals("error")) {

                            parseChangePwdJson(response);

                        } else {

                            //error
                            alert("Something went, please try again later");
                            CommonFunctions.hideDialog();

                        }


                    }
                }, "POST");

                asyncTaskHelper.execute();


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {

            //no internet
            alert("Internet Connection not available");
        }

    }

    private void parseChangePwdJson(String response) {


        try {
            jsonObject = new JSONObject(response);

            status = jsonObject.getString("status");
            msg = jsonObject.getString("message");

            if (status.equalsIgnoreCase("failure")) {

                alert(msg);
            } else if (status.equals("error")) {

                alert("Something went, please try again later");

            } else {
                JSONArray res = jsonObject.getJSONArray("response");

                for (int i = 0; i < res.length(); i++) {

                    jsonObject = res.getJSONObject(i);
                    SettingPreffrences.setToken(getApplicationContext(), jsonObject.getString("token"));

                }
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("SUCCESS");
                builder.setMessage(msg);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener()

                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                onBackPressed();
                                finish();

                            }
                        }

                );
                builder.create().show();

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        CommonFunctions.hideDialog();
    }
}
