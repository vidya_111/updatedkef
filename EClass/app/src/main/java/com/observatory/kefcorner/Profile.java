package com.observatory.kefcorner;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.target.ViewTarget;
import com.bumptech.glide.signature.StringSignature;
import com.observatory.AsyncTaskMain.AsyncTaskHelper;
import com.observatory.database.ActivationCode;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.SettingPreffrences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.UUID;

/**
 * Created by Anuj on 06-01-2016.
 */
public class Profile extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "Profile";
    Target mCurrentTarget;
    private ImageView ivPic;
    private TextView tvName;
    private TextView tvMobile;
    private TextView tvEmail;
    private TextView tvEdit;
    private TextView tvChangePassword;
    /*Webservice*/
    private AsyncTaskHelper asyncTaskHelper;
    private String params;
    /*Json*/
    private JSONObject jsonObject;
    private JSONArray jsonArray;
    private String status;
    private String msg;
    private int mLastLoadHash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
    }

    @Override
    protected void onLayout() {

        ivPic = (ImageView) findViewById(R.id.iv_p_pic);
        tvName = (TextView) findViewById(R.id.tv_p_name);
        tvMobile = (TextView) findViewById(R.id.tv_p_mobile);
        tvEmail = (TextView) findViewById(R.id.tv_p_email);

        tvEdit = (TextView) findViewById(R.id.tv_p_edit);

        tvChangePassword = (TextView) findViewById(R.id.tv_p_changepwd);

        setData();

        tvChangePassword.setOnClickListener(this);
        tvEdit.setOnClickListener(this);


    }

    private void setData() {


        tvName.setText(SettingPreffrences.getName(getApplicationContext()));

        if (TextUtils.isEmpty(SettingPreffrences.getMobile(getApplicationContext()))) {
            tvMobile.setVisibility(View.GONE);
        } else {
            tvMobile.setVisibility(View.VISIBLE);
            tvMobile.setText(SettingPreffrences.getMobile(getApplicationContext()));
        }

        if (SettingPreffrences.getUserid(this) == "0") {
            tvEdit.setVisibility(View.GONE);
        }

        tvEmail.setText(SettingPreffrences.getEmail(getApplicationContext()));
        new Thread(new Runnable() {
            @Override
            public void run() {
                Glide.get(getApplicationContext()).clearDiskCache();
                Glide.get(getApplicationContext()).clearMemory();
            }
        });
        Glide.with(getApplicationContext()).load(SettingPreffrences.getPhoto(getApplicationContext())).signature(new StringSignature(UUID.randomUUID().toString())).placeholder(R.drawable.ic_profile_default).into(ivPic);


    }

    public <T> void loadNextImage(@NonNull T model,
                                  @NonNull BitmapTransformation... transformations) {
        //noinspection MagicNumber
        int hash = model.hashCode() + 31 * Arrays.hashCode(transformations);
        if (mLastLoadHash == hash) return;
        Glide.with(getApplicationContext()).load(model).asBitmap().transform(transformations).into(mCurrentTarget);
        mLastLoadHash = hash;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.tv_p_changepwd:

                startActivity(ChangePassword.class);

                break;

            case R.id.tv_p_edit:

                startActivity(EditProfile.class);
                break;

        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if (SettingPreffrences.getProfileEdited(getApplicationContext())) {

            tvName.setText(SettingPreffrences.getName(getApplicationContext()));

            if (TextUtils.isEmpty(SettingPreffrences.getMobile(getApplicationContext()))) {
                tvMobile.setVisibility(View.GONE);
            } else {
                tvMobile.setVisibility(View.VISIBLE);
                tvMobile.setText(SettingPreffrences.getMobile(getApplicationContext()));
            }

            tvEmail.setText(SettingPreffrences.getEmail(getApplicationContext()));
            if (EditProfile.filePath != null) {

                if (!TextUtils.isEmpty(EditProfile.filePath)) {

                    ivPic.setImageBitmap(BitmapFactory.decodeFile(EditProfile.filePath));
                } else {

                    ivPic.setImageResource(R.drawable.ic_profile_default);
                }
            } else {
                ivPic.setImageResource(R.drawable.ic_profile_default);
            }

            SettingPreffrences.setProfileEdited(getApplicationContext(), false);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {


            case R.id.profile:

                logout();
                break;
        }


        return super.onOptionsItemSelected(item);
    }

    private void logout() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener()

                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        PreferenceManager.getDefaultSharedPreferences(getBaseContext()).
                                edit().clear().apply();

                        SettingPreffrences.setLoginDone(getApplicationContext(), false);
                        SettingPreffrences.setDisclaimer(getApplicationContext(),false);
                        SettingPreffrences.setContinueAsGuest(getApplicationContext(), false);
                        SettingPreffrences.setName(getApplicationContext(), "");
                        SettingPreffrences.setPhoto(getApplicationContext(), "");
                        SettingPreffrences.setUserid(getApplicationContext(), "");
                        SettingPreffrences.setEmail(getApplicationContext(), "");
                        ActivationCode.deleteAll(ActivationCode.class);


                        Intent intent = new Intent(Profile.this, Splash.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();

                    }
                }

        );

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    private class DiaporamaViewTarget extends ViewTarget<ImageView, Bitmap> {

        private BitmapDrawable mLoadedDrawable;
        private Target mPreviousTarget;

        public DiaporamaViewTarget(ImageView view) {
            super(view);
        }

        @Override
        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
            mLoadedDrawable = new BitmapDrawable(ivPic.getResources(), resource);
            // display the loaded image
            mCurrentTarget = mPreviousTarget;
        }

    }
}
