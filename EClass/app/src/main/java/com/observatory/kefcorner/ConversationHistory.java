package com.observatory.kefcorner;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.youtube.player.internal.s;
import com.observatory.AsyncTaskMain.AsyncTaskHelper;
import com.observatory.AsyncTaskMain.OnTaskComplete;
import com.observatory.adapters.ConversationHistoryAdapter;
import com.observatory.adapters.chatMessageModel;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.DateUtil;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ConversationHistory extends BaseActivity {

    RecyclerView message_Data_view;
    EditText message_input;
    ImageView send_message;
    String doubt_id_intent = "", doubt_title_intent = "";
    ConversationHistoryAdapter adapter;
    List<chatMessageModel> msgList = new ArrayList<>();
    chatMessageModel model;
    SimpleDateFormat inputDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat outputDate = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    Date date = null;

    /*Webservice*/
    private AsyncTaskHelper asyncTaskHelper;
    private String params;
    private JSONObject jsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.conversation_history);

    }

    @Override
    protected void onLayout() {
        message_Data_view = findViewById(R.id.message_Data_view);
        message_input = findViewById(R.id.input_message_id);
        send_message = findViewById(R.id.send_message_id);

        /*InputMethodManager manager = (InputMethodManager) activity.getSystemService(
                Context.INPUT_METHOD_SERVICE
        );
        manager.hideSoftInputFromWindow(message_input.getWindowToken(), 0);*/

        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            doubt_id_intent = bundle.getString("doubt_id");
            doubt_title_intent = bundle.getString("doubt_title");
            actionBar.setTitle(doubt_title_intent);
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        message_Data_view.setLayoutManager(linearLayoutManager);

        getDoubtsByDoubtId(doubt_id_intent);

        /*msgList = new ArrayList<>();
        model = new chatMessageModel(
                question_intent,
                chatMessageModel.MSG_TYPE_SENT,
                time_intent);
        msgList.add(model);
        adapter = new ConversationHistoryAdapter(msgList);
        message_Data_view.setAdapter(adapter);*/

        send_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msgContent = message_input.getText().toString();
                if (!TextUtils.isEmpty(msgContent)){
                    updateDoubtById(Integer.parseInt(doubt_id_intent),msgContent, SettingPreffrences.getUserid(getApplicationContext()));
                    /*chatMessageModel model1 = new chatMessageModel(
                            msgContent,
                            chatMessageModel.MSG_TYPE_SENT,
                            DateUtil.getCurrentDateTimeInFormat("dd-MM-yyyy HH:mm")
                    );
                    msgList.add(model1);
                    int newMsgPosition = msgList.size() - 1;
                    adapter.notifyItemInserted(newMsgPosition);
                    message_Data_view.scrollToPosition(newMsgPosition);
                    message_input.setText("");*/
                }
            }
        });
    }

    private void getDoubtsByDoubtId(String doubtID){
        if (CommonFunctions.isNetworkConnected(getApplicationContext())){

            CommonFunctions.showDialog(this);
            try {

                params = "doubtID=" + URLEncoder.encode(doubtID, "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.KEF_Host + NetworkUrl.KEF_GetByDoubtId, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {
                        CommonFunctions.hideDialog();
                        Log.d("getDoubtsByDoubtId","response: "+response);
                        if (!response.equals("error")){
                            getParticularDoubtResponse(response);
                        }
                    }
                },"GET");

                asyncTaskHelper.execute();

            }catch (Exception e){
                Log.e("Error: GetDoubts: ",e.getMessage());
            }

        }else {
            alert("Internet Connection not available");
        }
    }

    private void getParticularDoubtResponse(String response){
        try {

            JSONArray responseArray = new JSONArray(response);
            for (int i = 0; i < responseArray.length(); i++){
                jsonObject = responseArray.getJSONObject(i);
                date = inputDate.parse(jsonObject.getString("CreatedOn"));
                if (jsonObject.getString("UserType").equals("ST")){
                    model = new chatMessageModel(
                            jsonObject.getString("Message"),
                            chatMessageModel.MSG_TYPE_SENT,
                            outputDate.format(date),
                            jsonObject.getString("UserType"));
                }else {
                    model = new chatMessageModel(
                            jsonObject.getString("Message"),
                            chatMessageModel.MSG_TYPE_RECEIVED,
                            outputDate.format(date),
                            jsonObject.getString("UserType"));
                }
                msgList.add(model);
            }
            adapter = new ConversationHistoryAdapter(msgList);
            message_Data_view.setAdapter(adapter);
            message_Data_view.scrollToPosition(msgList.size()-1);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void updateDoubtById(int DoubtID, String Message, String UserID){
        if (CommonFunctions.isNetworkConnected(getApplicationContext())){

            CommonFunctions.showDialog(this);
            try {

                params = "DoubtID=" + URLEncoder.encode(String.valueOf(DoubtID), "UTF-8")
                        +"&Message=" + URLEncoder.encode(Message, "UTF-8")
                        +"&UserID=" + URLEncoder.encode(UserID, "UTF-8");

                asyncTaskHelper = new AsyncTaskHelper(getApplicationContext(), NetworkUrl.KEF_Host + NetworkUrl.KEF_UpdateDoubt, params, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {
                        CommonFunctions.hideDialog();
                        Log.d("getUpdateDoubt","response: "+response);
                        if (!response.equals("error")){
                            chatMessageModel model1 = new chatMessageModel(
                                    Message,
                                    chatMessageModel.MSG_TYPE_SENT,
                                    DateUtil.getCurrentDateTimeInFormat("yyyy-MM-dd HH:mm:ss"),
                            "ST");
                            msgList.add(model1);
                            int newMsgPosition = msgList.size() - 1;
                            adapter.notifyItemInserted(newMsgPosition);
                            message_Data_view.scrollToPosition(newMsgPosition);
                            message_input.setText("");
                        }
                    }
                },"POST");

                asyncTaskHelper.execute();

            }catch (Exception e){
                Log.e("Error: GetDoubts: ",e.getMessage());
            }

        }else {
            alert("Internet Connection not available");
        }
    }
}