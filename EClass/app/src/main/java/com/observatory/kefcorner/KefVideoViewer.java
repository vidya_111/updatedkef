package com.observatory.kefcorner;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;

import com.observatory.Assessment.AssessmentTypesActivity;
import com.observatory.AsyncTaskMain.AsyncTaskHelper;
import com.observatory.AsyncTaskMain.OnTaskComplete;
import com.observatory.database.KefCornerStatistics;
import com.observatory.utils.BaseActivity;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.CustomVideoView;
import com.observatory.utils.DateUtil;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;

import org.json.JSONObject;

import java.net.URLEncoder;

public class KefVideoViewer extends BaseActivity {

    CustomVideoView videoView;
    String data = "";
    String GalleryID;
    KefCornerStatistics StatsModel;
    public boolean isAdded = false;
    ProgressDialog progressBar;
    /*Webservice*/
    private AsyncTaskHelper asyncTaskHelper;
    private String params, params1;
    private JSONObject jsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionBar.hide();
        setContentView(R.layout.activity_view_video);
    }


    @Override
    protected void onLayout() {
        videoView = findViewById(R.id.videoViewKEF);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            data = bundle.getString("video_data");
            GalleryID = bundle.getString("gallery_id");
        }

        progressBar = new ProgressDialog(KefVideoViewer.this);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setMessage("Please wait while we prepare the video..");
        progressBar.show();

        videoView.setVideoURI(Uri.parse(data));

        videoView.setMediaController(new MediaController(this));

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {


                mp.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                    @Override
                    public void onBufferingUpdate(MediaPlayer mp, int percent) {
                        if (percent == 100) {
                            progressBar.dismiss();
                            videoView.start();
                            videoView.requestFocus();
                        }
                    }
                });

                videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                          isAdded = true;
                      /*  if (isAdded && StatsModel != null) {
                            StatsModel.setAccessDuration(Double.parseDouble(conversionValue(videoView.getCurrentPosition())));
                            long row = StatsModel.update();
                            Log.e("sync","row updated "+row);
                        } else {*/
                            StatsModel = new KefCornerStatistics(
                                    Integer.parseInt(SettingPreffrences.getUserid(getApplicationContext())),
                                    Integer.parseInt(GalleryID),
                                    Double.parseDouble(conversionValue(videoView.getCurrentPosition())),
                                    DateUtil.getCurrentDateTimeInFormat("YYYY-MM-dd HH:mm:ss")
                            );
                            long row = StatsModel.save();
                            Log.e("sync","row inserted "+row);
                        //}
                    }
                });

                videoView.setPlayPauseListener(new CustomVideoView.PlayPauseListener() {
                    @Override
                    public void onPlay() {
                        Log.e("sync","video play ");
                    }

                    @Override
                    public void onPause() {
                        Log.e("sync","video paused");
                    }
                });
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("KEfVideo1", "stopsAt: " + videoView.getCurrentPosition() + " duration: " + videoView.getDuration() + "   " + isAdded);

        if (!isAdded) {
           // isAdded = true;
            StatsModel = new KefCornerStatistics(
                    Integer.parseInt(SettingPreffrences.getUserid(getApplicationContext())),
                    Integer.parseInt(GalleryID),
                    Double.parseDouble(conversionValue(videoView.getCurrentPosition())),
                    DateUtil.getCurrentDateTimeInFormat("YYYY-MM-dd HH:mm:ss")
            );
            StatsModel.save();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("KEfVideo2", "stopsAt: " + videoView.getCurrentPosition() + " duration: " + videoView.getDuration());
        if (videoView != null && videoView.canPause()) {
            videoView.pause();
        }
    }

    //changed logic by @vidya for seconds more than 60 , on 31st July 2021
    public String conversionValue(long millis) {
        long minutes = 0, seconds = 0;
        String sec, result;
        //  minutes = (int)(millis/1000)/60;
        seconds = (millis / 1000);
        if (seconds > 59) {
            minutes = seconds / 60;
            seconds = seconds % 60;
        }
        if (seconds < 10) {
            sec = "0" + seconds;
        } else {
            sec = seconds + "";
        }
        result = minutes + "." + sec;
        return result;
    }
}