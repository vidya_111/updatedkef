package com.observatory.AsyncTaskMain;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.observatory.Assessment.ConstantManager;
import com.observatory.Assessment.DataBase.SQLiteDB;
import com.observatory.Assessment.Model.AssessmentModel;
import com.observatory.Assessment.Model.QuestionModel;
import com.observatory.database.Master;
import com.observatory.database.Statistics;
import com.observatory.database.userStatistics;
import com.observatory.kefcorner.Splash;
import com.observatory.utils.CommonFunctions;
import com.observatory.utils.Constants;
import com.observatory.utils.EmailHandler;
import com.observatory.utils.NetworkUrl;
import com.observatory.utils.SettingPreffrences;
import com.orm.query.Condition;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import io.fabric.sdk.android.services.concurrency.AsyncTask;

public class SyncDataHelper extends TimerTask {
    TimerAsyncTask atask;
    Context appContext;
    private SQLiteDB dbhelper;
    private boolean isForeground;

    public SyncDataHelper(Context appContext) {
        this.appContext = appContext;
    }

    final class TimerAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Log.e("Sync", "Started");
                try {
                    autoSyncStatistics();
                } catch (Exception e1) {
                    Log.e("SyncError", e1.getMessage());
                }
                try {
                    if (ConstantManager.DB_PATH != null && !ConstantManager.DB_PATH.isEmpty()) {
                        dbhelper = SQLiteDB.getInstance(appContext, ConstantManager.DB_PATH);
                        syncPendingData();
                    }
                } catch (Exception e1) {
                    Log.e("SyncError", e1.getMessage());
                }
                try {
                    autoSyncLoginHistory();
                } catch (Exception e1) {
                    Log.e("SyncError", e1.getMessage());
                }
                Log.e("Sync", "Finished");
            } catch (Exception e) {
                Log.e("SyncError", e.getMessage());
            }
            return null;
        }
    }

    public void run() {
        atask = new TimerAsyncTask();
        atask.execute();
    }


    ////////////////// Sync for digital content added by @vidya on 21st July 2021
    private void autoSyncStatistics() {
        try {
            long lastSyncedId = SettingPreffrences.getLastSyncedMcqId(appContext);
            List<userStatistics> statisticsList;

            if (lastSyncedId != -1) {
                statisticsList = Select.from(userStatistics.class).where("ID > " + lastSyncedId).list();

            } else {
                statisticsList = Select.from(userStatistics.class).list();
            }

            // there is data to sync
            if (statisticsList.size() > 0) {
                String statsJson = formatStatisticsToJsonNew(statisticsList);
                Log.d("KefSyncStat SDH", "size: " + statisticsList.size() + " json: " + statsJson);
                callSyncStatisticsWebService(statsJson, statisticsList);
            }
        } catch (Exception e) {
            Log.e("SyncError", e.getMessage());
        }
    }

    public String formatStatisticsToJsonNew(List<userStatistics> statisticsList) {

        String statsAsJson;

        try {
            JSONArray statsJsonArr = new JSONArray();

            for (userStatistics statistics : statisticsList) {
                if (!statistics.isIs_synced()) {
                    JSONObject aStatJson = new JSONObject();
                    aStatJson.put("StatUserId", statistics.getUserID());
                    aStatJson.put("courseId", statistics.getCourseID());
                    aStatJson.put("subjectId", statistics.getSubjectID());
                    aStatJson.put("chapterId", statistics.getChapterID());
                    aStatJson.put("chapterId", statistics.getChapterID());
                    aStatJson.put("contentTypeId", statistics.getContentTypeID());
                    aStatJson.put("contentName", statistics.getContentName());
                    aStatJson.put("contentDuration", statistics.getContentDuration());
                    aStatJson.put("AccessDuration", statistics.getAccessDuration());
                    aStatJson.put("AccessDatetime", statistics.getAccessDatetime());
                    statsJsonArr.put(aStatJson);
                }
            }

            statsAsJson = statsJsonArr.toString();

        } catch (JSONException e) {
            statsAsJson = "Error formatting Statistics data into JSON for syncing purposes";
        }

        return statsAsJson;
    }

    public String getMedium() {
        String medium;

        List<Master> masterList = Master.listAll(Master.class);
        if (masterList.size() > 0) {
            Master master = masterList.get(0);
            medium = master.getMedium();
        } else {
            medium = "Medium could not be found";
        }

        return medium;
    }

    private void callSyncStatisticsWebService(String json, final List<userStatistics> statisticsToBeUpdated) {
        try {
            if (CommonFunctions.isNetworkConnected(appContext)) {

                String userId = SettingPreffrences.getUserid(appContext).isEmpty() ? "0" : SettingPreffrences.getUserid(appContext);
                String token = SettingPreffrences.getToken(appContext).isEmpty() ? "0000" : SettingPreffrences.getToken(appContext);
                //String deviceId = CommonFunctions.getUniqueDeviceId(appContext);

                Log.d("KefSyncStat", "Parameters: " + "userID: " + userId + " token: " + token + " userStat: " + json);

                Ion.with(appContext).load(NetworkUrl.KEF_Localhost + NetworkUrl.KEF_SyncStatistics)


                        /*.setBodyParameter("userId", userId)
                        .setBodyParameter("token", token)
                        .setBodyParameter("medium", "")
                        .setBodyParameter("email", "")
                        .setBodyParameter("imeiNo", deviceId)
                        .setBodyParameter("startDate", "")
                        .setBodyParameter("endDate", "")
                        .setBodyParameter("userStat", json)*/

                        .setBodyParameter("os", Splash.OS)
                        .setBodyParameter("make", Splash.MAKE)
                        .setBodyParameter("model", Splash.MODEL)
                        .setBodyParameter("UserID", userId)
                        .setBodyParameter("token", token)
                        .setBodyParameter("medium", getMedium())
                        .setBodyParameter("email", "")
                        .setBodyParameter("startDate", "")
                        .setBodyParameter("endDate", "")
                        .setBodyParameter("userStat", json).asString().setCallback(new FutureCallback<String>() {

                    @Override
                    public void onCompleted(Exception e, String result) {
                        try {
                            if (e == null) {

                                Log.d("userStatResponse", ":: " + result);
                                updateSyncState(statisticsToBeUpdated);
                            } else {
                                Log.d("userStatResponse", "failed");
                            }
                        } catch (Exception ex) {
                            Log.e("SyncError", ex.getMessage());
                        }
                    }
                });
            }
        } catch (Exception e) {

        }
    }

    private void updateSyncState(List<userStatistics> statisticsList) {
        try {
            userStatistics lastSyncedStat = statisticsList.get(statisticsList.size() - 1);
            SettingPreffrences.setLastSyncedMcqId(appContext, lastSyncedStat.getId());
            //Initially commented uncommented by divyesh 17/05/2019
            for (userStatistics statistics : statisticsList) {
                statistics.setIs_synced(true);
                Log.d("Subjects : ", "Content name before DB update -> " + statistics.getContentName());
                Statistics.save(statistics);  // @vidya changed method to "save" on 28th Dec 2020, earlier it was "update"
            }
        } catch (Exception e) {
            Log.e("SyncError", e.getMessage());
        }
        //uncommented by divyesh 17/05/2019
    }

    //////////////////Sync for assessment added by @vidya on 21st July 2021
    public void syncPendingData() {
        try {
            if (CommonFunctions.isNetworkConnected(appContext)) {
                ArrayList<AssessmentModel> pendingList = new ArrayList<>();
                pendingList = dbhelper.getSyncPendingIndividualId();
                Log.d("AssessmentLogs", "pending list size " + pendingList.size());
                if (pendingList.size() > 0) {
                    // for (int i = 0; i < pendingList.size(); i++) {
                    Log.d("PendingList", "IndividualID: " + pendingList.get(0).getIndividualID());
                    Log.d("PendingList", "TotalQuestions: " + pendingList.get(0).getTestTotalQuestion());
                    getDetailsFromPendingId(pendingList.get(0).getIndividualID());
                    // }
                } else {
                    Log.d("AssessmentLogs", "hideDialog");
                }
            }
        } catch (Exception e) {
            Log.e("SyncError", e.getMessage());
        }
    }

    public void getDetailsFromPendingId(String individualId) {
        ArrayList<AssessmentModel> dataList = new ArrayList<>();
        dataList = dbhelper.getSyncPendingDetailsByID(individualId);

        if (dataList.size() > 0) {
            for (int i = 0; i < dataList.size(); i++) {
                Log.d("PendingList11", "TotalQuestion: " + dataList.get(i).getTestTotalQuestion() + "Size: " + dataList.size());
                insertDetailsToServer(
                        individualId,
                        dataList.get(i).getTestScoreSummaryDetailsId(),
                        dataList.get(i).getStudentUserCode(),
                        SettingPreffrences.getCourseId(appContext),
                        dataList.get(i).getCourseContentCode(),
                        Integer.parseInt(String.valueOf(Double.valueOf(dataList.get(i).getTestTotalMarks()).intValue())),
                        Integer.parseInt(String.valueOf(Double.valueOf(dataList.get(i).getTotalMarksObtain()).intValue())),
                        dataList.get(i).getTestTotalQuestion(),
                        dataList.get(i).getCounterRight(),
                        dataList.get(i).getCounterWrong(),
                        dataList.get(i).getLastAccessTime(),
                        "1",
                        "KEFM"
                );
            }
        }

    }

    public void insertDetailsToServer(String TestScoreIndividualDetailID, String TestScoreSummaryDetailID, String UserID, String CourseID,
                                      String SubjectID, int TestTotalMarks, int TestMarksObtained, String TotalQuestion, int RightAnswerCount,
                                      int WrongAnswerCount, String TestAttemptedDatetime, String TestAttemptedCount, String AppType) {

        StringBuilder str = new StringBuilder();
        ArrayList<QuestionModel> questionList = new ArrayList<>();
        questionList = dbhelper.getSyncPendingQuestions(TestScoreIndividualDetailID);

        if (questionList.size() > 0) {
            for (int i = 0; i < questionList.size(); i++) {
                str.append(createXmlData(
                        questionList.get(i).getTestResultDetailsId(),
                        questionList.get(i).getQuestionCode(),
                        questionList.get(i).getYourAns(),
                        questionList.get(i).getRightAnsflag(),
                        questionList.get(i).getRightAnsMarks(),
                        Integer.parseInt(String.valueOf(questionList.get(i).getRightMObt().intValue())),
                        questionList.get(i).getAttemptedTestDate()
                ));
            }
        }

        StringBuilder finalQuestionData = FinalXmlData(str.toString());

        JsonObject data = new JsonObject();
        data.addProperty("TestScoreIndividualDetailID", TestScoreIndividualDetailID);
        data.addProperty("TestScoreSummaryDetailID", TestScoreSummaryDetailID);
        data.addProperty("UserID", UserID);
        data.addProperty("EdzamCourseID", CourseID);
        data.addProperty("SubjectID", SubjectID);
        data.addProperty("SchoolID", SettingPreffrences.getSchoolId(appContext));
        data.addProperty("TestTotalMarks", TestTotalMarks);
        data.addProperty("TestMarksObtained", TestMarksObtained);
        data.addProperty("TotalQuestion", TotalQuestion);
        data.addProperty("RightAnswerCount", String.valueOf(RightAnswerCount));
        data.addProperty("WrongAnswerCount", String.valueOf(WrongAnswerCount));
        data.addProperty("TestAttemptedDatetime", TestAttemptedDatetime);
        data.addProperty("TestAttemptedCount", TestAttemptedCount);
        data.addProperty("AppType", AppType);
        data.addProperty("QuestionXML", finalQuestionData.toString());

        Log.d("PendingList22", "JsonData: " + data.toString());

        if (CommonFunctions.isNetworkConnected(appContext)) {
            try {
                AsyncTaskHelper asyncTaskHelper = new AsyncTaskHelper(appContext, NetworkUrl.KEF_mcqDataHost + NetworkUrl.KEF_insertMcqData, data, new OnTaskComplete() {
                    @Override
                    public void getResponse(String response) {
                        try {
                            Log.d("PendingList22", "ApiResponse -> InsertUpdateMCQTestResultDetails: " + response);
                            if (!response.equals("error")) {
                                boolean result = dbhelper.updateSyncAssessmentData(TestScoreIndividualDetailID);
                                if (result) {
                                    syncPendingData();
                                }
                            }
                        } catch (Exception e) {
                            Log.e("SyncError", e.getMessage());
                        }
                    }
                }, "POST");

                asyncTaskHelper.execute();

            } catch (Exception e) {
                Log.d("PendingList22", "ApiResponse -> InsertUpdateMCQTestResultDetails: Error :" + e.getMessage());
                e.printStackTrace();
            }
        }

    }

    private StringBuilder FinalXmlData(String value) {
        StringBuilder str = new StringBuilder();
        str.append("<Questions>");
        str.append(value);
        str.append("</Questions>");
        return str;
    }

    private StringBuilder createXmlData(String id, String questionCode, String attemptedAns, int isAnsCorrect,
                                        String questionMarks, int QuestionMarksObtain, String dateTime) {
        String isAnswerCorrect_value = "";
        if (isAnsCorrect == 0) {
            isAnswerCorrect_value = "False";
        } else {
            isAnswerCorrect_value = "True";
        }

        StringBuilder builder = new StringBuilder();
        builder.append("<Question>");
        builder.append("<TestScoreResultDetailId>");
        builder.append(id);
        builder.append("</TestScoreResultDetailId>");
        builder.append("<QuestionCode>");
        builder.append(questionCode);
        builder.append("</QuestionCode>");
        builder.append("<AttemptedAnswer>");
        builder.append(attemptedAns);
        builder.append("</AttemptedAnswer>");
        builder.append("<IsAnswerCorrect>");
        builder.append(isAnswerCorrect_value);
        builder.append("</IsAnswerCorrect>");
        builder.append("<QuestionMarks>");
        builder.append(questionMarks);
        builder.append("</QuestionMarks>");
        builder.append("<QuestionMarksObtained>");
        builder.append(QuestionMarksObtain);
        builder.append("</QuestionMarksObtained>");
        builder.append("<QuestionAttemptedDateTime>");
        builder.append(dateTime);
        builder.append("</QuestionAttemptedDateTime>");
        builder.append("</Question>");
        return builder;
    }


    ///sync history before showing in app @vidya

    private void autoSyncLoginHistory() {

        long lastSyncedId = SettingPreffrences.getLastSyncedLoginHistoryId(appContext);
        List<com.observatory.database.UserHistory> userHistoryList;

        if (lastSyncedId != -1) {
            userHistoryList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com.observatory.database.UserHistory.class).where("id > " + lastSyncedId).orderBy("id asc").list(); /* ordering in ascending so that latest appear at the end
					. */

        } else {
//			userHistoryList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com
//					.observatory.database.UserHistory.class)
//					.list();

            userHistoryList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com.observatory.database.UserHistory.class).orderBy("id asc").list(); /* ordering in ascending so that latest appear at the end
					. */
        }

        // there is data to sync
        if (userHistoryList.size() > 0) {
            String statsJson = formatLoginHistoryToJson(userHistoryList);
            callSyncLoginHistoryWebService(statsJson, userHistoryList);
        }
    }

    private String formatLoginHistoryToJson(List<com.observatory.database.UserHistory> arrayUserHistory) {

        JSONArray mainJsonArr = new JSONArray();

        ArrayList<String> courseList = getCourseList();

        for (int i = 0; i < courseList.size(); i++) {

            JSONObject jsonObject = new JSONObject();

            try {

                jsonObject.put("medium", courseList.get(i));

                JSONArray dataJsonArr = new JSONArray();

                for (com.observatory.database.UserHistory userHistory : arrayUserHistory) {

                    if (userHistory.getMedium().equals(courseList.get(i))) {

                        try {

                            JSONObject dateTimeJson = new JSONObject();
                            dateTimeJson.put("date", userHistory.getDate());
                            dateTimeJson.put("time", userHistory.getTime());
                            dataJsonArr.put(dateTimeJson);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

                // send data only if current medium has login data
                if (dataJsonArr.length() > 0) {
                    jsonObject.put("data", dataJsonArr);
                    mainJsonArr.put(jsonObject);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Log.d("Tag", "formatLoginHistoryToJson: " + mainJsonArr.toString());

        return mainJsonArr.toString();
    }

    private ArrayList<String> getCourseList() {

        String userid = SettingPreffrences.getUserid(appContext);

        ArrayList<String> courseList = new ArrayList<>();
        ArrayList<com.observatory.database.UserHistory> userHistoryArrayList = (ArrayList<com.observatory.database.UserHistory>) Select.from(com.observatory.database.UserHistory.class).groupBy("medium").orderBy("id asc").where(Condition.prop("userId").eq((userid.isEmpty() ? "0" : userid))).list();

        for (com.observatory.database.UserHistory userHistory : userHistoryArrayList) {
            courseList.add(userHistory.getMedium());
        }

        return courseList;
    }

    private void callSyncLoginHistoryWebService(String json, final List<com.observatory.database.UserHistory> userHistoryList) {

        if (CommonFunctions.isNetworkConnected(appContext)) {

            String userId = SettingPreffrences.getUserid(appContext).isEmpty() ? "0" : SettingPreffrences.getUserid(appContext);
            String token = SettingPreffrences.getToken(appContext).isEmpty() ? "0000" : SettingPreffrences.getToken(appContext);
            String deviceId = CommonFunctions.getUniqueDeviceId(appContext);

            Ion.with(appContext).load(NetworkUrl.syncLoginHistory)
                    .setBodyParameter("userId", userId)
                    .setBodyParameter("token", token)
                    .setBodyParameter("os", Build.VERSION.RELEASE)
                    .setBodyParameter("make", Build.MANUFACTURER)
                    .setBodyParameter("model", Build.MODEL)
                    .setBodyParameter("historyDetails", json)
                    .asString().setCallback(new FutureCallback<String>() {

                @Override
                public void onCompleted(Exception e, String result) {

                    if (e == null) {
                        // update last synced Id of UserHistory table
                        Log.d("ApiResponse", "UserHistoryAutoSync: " + result);
                        com.observatory.database.UserHistory lastSyncedUserHistory = userHistoryList.get(userHistoryList.size() - 1);
                        SettingPreffrences.setLastSyncedLoginHistoryId(appContext, lastSyncedUserHistory.getId());
                    } else {
                        new EmailHandler().sendMail("phone@millicent.in", "KEF Sync", e.getMessage());
                    }
                }
            });
        }

    }

}