package com.observatory.AsyncTaskMain;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonObject;
import com.observatory.utils.Network;

/**
 * Created by Anuj M on 12/11/2014.
 */
public class AsyncTaskHelper extends AsyncTask<String, Void, String> {

    private OnTaskComplete taskComplete;
    private Context context;
    private String params;
    private String response = "";
    private Network callWebService;
    private String method = "";
    private String url = "";
    private JsonObject jsonParams;

    public AsyncTaskHelper(Context context, String url, String params, OnTaskComplete taskComplete, String method) {
        this.taskComplete = taskComplete;
        this.context = context;
        this.url = url;
        this.params = params;
        this.method = method;
        callWebService = new Network(context);
    }

    public AsyncTaskHelper(Context context, String url, JsonObject params, OnTaskComplete taskComplete, String method) {
        this.taskComplete = taskComplete;
        this.context = context;
        this.url = url;
        this.jsonParams = params;
        this.method = method;
        callWebService = new Network(context);
    }

    @Override
    protected String doInBackground(String... param) {

        Log.d("getData","checking1: "+url+"  "+params+"  "+method);
        if (method.equals("GET")) {
            response = callWebService.connectToServer(url + "?" + params, "", method);
        } else {
            if (url.contains("InsertUpdateMCQTestResultDetails")){
                Log.d("JsonParam",":: "+jsonParams);
                response = callWebService.postRequest(url, jsonParams, method);
            }else {
                if (url.contains("GetAssessmentProductDBDetails")){
                    response = callWebService.connectToServer(url + "?" + params, "", method);
                }else {
                    response = callWebService.connectToServer(url, params, method);
                }

            }

        }


        return response;
    }

    @Override
    protected void onPreExecute() {


    }

    @Override
    protected void onPostExecute(String response) {
        response = response.replaceAll("<[^>]*>", "").replaceAll("\n", "");
        taskComplete.getResponse(response);


    }
}
