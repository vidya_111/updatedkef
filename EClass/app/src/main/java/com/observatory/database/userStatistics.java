package com.observatory.database;

import com.orm.SugarRecord;

public class userStatistics extends SugarRecord {

    private String userID;
    private String courseID;
    private String subjectID;
    private String chapterID;
    private String contentTypeID;
    private String contentName;
    private String contentDuration;
    private String accessDuration;
    private String accessDatetime;
    private String createdOn;
    private String score;

    private boolean is_synced;

    public userStatistics(){}

    public userStatistics(String userID, String courseID, String subjectID, String chapterID, String contentTypeID, String contentName, String contentDuration, String accessDuration, String accessDatetime, String createdOn, String score) {
        this.userID = userID;
        this.courseID = courseID;
        this.subjectID = subjectID;
        this.chapterID = chapterID;
        this.contentTypeID = contentTypeID;
        this.contentName = contentName;
        this.contentDuration = contentDuration;
        this.accessDuration = accessDuration;
        this.accessDatetime = accessDatetime;
        this.createdOn = createdOn;
        this.score = score;
    }

    public userStatistics(String userID, String courseID, String subjectID, String chapterID, String contentTypeID, String contentName, String contentDuration, String accessDuration, String accessDatetime, String createdOn) {
        this.userID = userID;
        this.courseID = courseID;
        this.subjectID = subjectID;
        this.chapterID = chapterID;
        this.contentTypeID = contentTypeID;
        this.contentName = contentName;
        this.contentDuration = contentDuration;
        this.accessDuration = accessDuration;
        this.accessDatetime = accessDatetime;
        this.createdOn = createdOn;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getCourseID() {
        return courseID;
    }

    public void setCourseID(String courseID) {
        this.courseID = courseID;
    }

    public String getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(String subjectID) {
        this.subjectID = subjectID;
    }

    public String getChapterID() {
        return chapterID;
    }

    public void setChapterID(String chapterID) {
        this.chapterID = chapterID;
    }

    public String getContentTypeID() {
        return contentTypeID;
    }

    public void setContentTypeID(String contentTypeID) {
        this.contentTypeID = contentTypeID;
    }

    public String getContentName() {
        return contentName;
    }

    public void setContentName(String contentName) {
        this.contentName = contentName;
    }

    public String getContentDuration() {
        return contentDuration;
    }

    public void setContentDuration(String contentDuration) {
        this.contentDuration = contentDuration;
    }

    public String getAccessDuration() {
        return accessDuration;
    }

    public void setAccessDuration(String accessDuration) {
        this.accessDuration = accessDuration;
    }

    public String getAccessDatetime() {
        return accessDatetime;
    }

    public void setAccessDatetime(String accessDatetime) {
        this.accessDatetime = accessDatetime;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public boolean isIs_synced() {
        return is_synced;
    }

    public void setIs_synced(boolean is_synced) {
        this.is_synced = is_synced;
    }

}
