package com.observatory.database;

import com.observatory.kefcorner.Splash;
import com.observatory.utils.App;

import java.util.Random;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by gaurav on 4/5/17.
 */

public class RealmDb {

    private static RealmConfiguration realmConfiguration;

    public static void makeRealmConfiguration() {

        String ebola = "fggd8348a3rfGS4rs!QI";
        Random random = new Random();
        String password = Splash.hexEncoded +
                ebola.substring(7, random.nextInt(3 - 2) + 12).toLowerCase() +
                App.pathIssuer + "#";

        byte[] key = new byte[64];

        int p = 0;

        for (int i = 0; i <= 63; ++i) {

            key[i] = (byte) (password.charAt(p) % 255);

            if (p == password.length() - 1) {
                p = 0;
            } else {
                ++p;
            }

        }

        realmConfiguration = new RealmConfiguration.Builder()
                .assetFile("default.realm")
                .encryptionKey(key)
                .build();

    }


    public static Realm getRealm() {
        if (realmConfiguration == null) {
            makeRealmConfiguration();
        }
        return Realm.getInstance(realmConfiguration);
    }


}
