package com.observatory.database;

import com.orm.SugarRecord;

/**
 * Created by anuj on 03/04/17.
 */

public class OfflineUsers extends SugarRecord {



    private String name;
    private String email;
    private String password;
    private String createdAt;

    public OfflineUsers() {
    }

    public OfflineUsers( String name, String email, String password, String createdAt) {

        this.name = name;
        this.email = email;
        this.password = password;
        this.createdAt = createdAt;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
