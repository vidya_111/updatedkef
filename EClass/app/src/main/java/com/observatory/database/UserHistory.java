package com.observatory.database;

import com.orm.SugarRecord;

/**
 * Created by anuj on 09/03/17.
 */

public class UserHistory extends SugarRecord {


    private String date;
    private String time;
    private String medium;
    private String user_id;


    public UserHistory() {
    }

    public UserHistory(String date, String time, String userId, String medium) {
        this.date = date;
        this.time = time;
        this.user_id = userId;
        this.medium = medium;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUserId() {
        return user_id;
    }

    public void setUserId(String userId) {
        this.user_id = userId;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }
}
