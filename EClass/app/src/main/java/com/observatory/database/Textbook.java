package com.observatory.database;

import com.orm.SugarRecord;

public class Textbook extends SugarRecord {
    private String subjectId;
    private String contentName;
    private String folderName;
    private String subjectDisplayName;

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getContentName() {
        return contentName;
    }

    public void setContentName(String contentName) {
        this.contentName = contentName;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getSubjectDisplayName() {
        return subjectDisplayName;
    }

    public void setSubjectDisplayName(String subjectDisplayName) {
        this.subjectDisplayName = subjectDisplayName;
    }
}
