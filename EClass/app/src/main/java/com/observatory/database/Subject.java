package com.observatory.database;

import com.orm.SugarRecord;

/**
 * Created by rohit on 23/01/15.
 */

// Represents a chapter. Class name should be called 'Chapter'. Requires refactoring so currently leaving as it is.
    // This class is also used for listing for Multiple Stds in Splash screen.
public class Subject extends SugarRecord {


    private String displaySubjectName;
    private String subjectName;
    private String chapterName;
    private String folderName;

    private String fontName;

    private String content;
    private String mindMap;
    private String qNa;
    private String pdf;
    private String mcq;


    private String subjectId;
    private String chapterId;

    public Subject() {

    }

    public String getSubjectName() {
        return subjectName;
    }

    // The encrypted subject name which is also that subject's sheet name and folder name
    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMindMap() {
        return mindMap;
    }

    public void setMindMap(String mindMap) {
        this.mindMap = mindMap;
    }

    public String getqNa() {
        return qNa;
    }

    public void setqNa(String qNa) {
        this.qNa = qNa;
    }

    public String getDisplaySubjectName() {
        return displaySubjectName;
    }

    public void setDisplaySubjectName(String displaySubjectName) {
        this.displaySubjectName = displaySubjectName;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getMcq() {
        return mcq;
    }

    public void setMcq(String mcq) {
        this.mcq = mcq;
    }

    public String getFontName() {
        return fontName;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getChapterId() {
        return chapterId;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

}



