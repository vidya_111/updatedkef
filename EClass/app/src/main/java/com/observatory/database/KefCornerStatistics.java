package com.observatory.database;

import com.orm.SugarRecord;

public class KefCornerStatistics extends SugarRecord {

    private int userID;
    private int GalleryId;
    private double AccessDuration;
    private String AccessDateTime;

    private boolean is_synced;

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getGalleryId() {
        return GalleryId;
    }

    public void setGalleryId(int galleryId) {
        GalleryId = galleryId;
    }

    public Double getAccessDuration() {
        return AccessDuration;
    }

    public void setAccessDuration(Double accessDuration) {
        AccessDuration = accessDuration;
    }

    public String getAccessDateTime() {
        return AccessDateTime;
    }

    public void setAccessDateTime(String accessDateTime) {
        AccessDateTime = accessDateTime;
    }

    public boolean isIs_synced() {
        return is_synced;
    }

    public void setIs_synced(boolean is_synced) {
        this.is_synced = is_synced;
    }




    public KefCornerStatistics() { }

    public KefCornerStatistics(int userID, int galleryId, double accessDuration, String accessDateTime) {
        this.userID = userID;
        GalleryId = galleryId;
        AccessDuration = accessDuration;
        AccessDateTime = accessDateTime;
    }
}
