package com.observatory.database;

import com.orm.SugarRecord;

public class LastContent extends SugarRecord {

    private String user_id;
    private String device_id;
    private String medium;
    private String subject;
    private String chapter;
    private int content_type;
    private String content_name;
    private int duration;
    private String created_at;
    private String score;
    private int key;

    private boolean is_synced;

    private String videoPath;
    private int lastDuration;


    public LastContent() {
    }

    public LastContent(String user_id, String deviceId, String medium, String subject, String chapter, String content_name, int content_type, int duration, String created_at,String videoPath,int lastduration,int key) {
        this.user_id = user_id;
        this.device_id = deviceId;
        this.medium = medium;
        this.subject = subject;
        this.chapter = chapter;
        this.content_name = content_name;
        this.content_type = content_type;
        this.duration = duration;
        this.created_at = created_at;
        this.videoPath=videoPath;
        this.lastDuration=lastduration;
        this.key=key;
    }

   /* public LastContent(String user_id, String deviceId, String medium, String subject, String chapter, String content_name, int content_type, int duration, String created_at, String score) {
        this.user_id = user_id;
        this.device_id = deviceId;
        this.medium = medium;
        this.subject = subject;
        this.chapter = chapter;
        this.content_name = content_name;
        this.content_type = content_type;
        this.duration = duration;
        this.created_at = created_at;
        this.score = score;
    }*/

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getContent_name() {
        return content_name;
    }

    public void setContent_name(String content_name) {
        this.content_name = content_name;
    }

    public int getContent_type() {
        return content_type;
    }

    public void setContent_type(int content_type) {
        this.content_type = content_type;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public boolean is_synced() {
        return is_synced;
    }

    public void setIs_synced(boolean is_synced) {
        this.is_synced = is_synced;
    }

    public String getScore() {
        return score;
    }

    public String getvideoPath() {
        return videoPath;
    }

    public void setvideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public int getlastDuration() {
        return lastDuration;
    }

    public void setlastDuration(int lastDuration) {
        this.lastDuration = lastDuration;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }
}
