package com.observatory.database;

import java.util.List;

/**
 * Created by anuj on 04/04/17.
 */

public class StatDetails {

    public boolean isHeader;
    public String header;
    public String noOfTimeSeen;
    public String duration;
    public String lastSeen;
    public boolean isEmpty;
    public List<StatDetails> innerVideo;


}
