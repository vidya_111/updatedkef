package com.observatory.database;

import com.orm.SugarRecord;

/**
 * Created by gaurav on 26/8/17.
 */

public class ActivationCode extends SugarRecord {

    private String activationCode;

    public ActivationCode() {
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

}

