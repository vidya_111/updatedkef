package com.observatory.mcqmodels;

/**
 * Created by Anuj on 10-06-2015.
 */
public class McqTestModel {

    private int id;
    private String name;


    public McqTestModel() {
    }

    public McqTestModel(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
