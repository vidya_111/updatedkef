package com.observatory.mcqmodels;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Anuj on 22-05-2015.
 */
public class SubjectsModel implements Parcelable {

    public int subject_id;
    public int standard_id;
    public String standard_name;
    public String subject_name;
    public String subject_image;
    public String font;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.subject_id);
        dest.writeInt(this.standard_id);
        dest.writeString(this.standard_name);
        dest.writeString(this.subject_name);
        dest.writeString(this.subject_image);
        dest.writeString(this.font);
    }

    public SubjectsModel() {
    }

    protected SubjectsModel(Parcel in) {
        this.subject_id = in.readInt();
        this.standard_id = in.readInt();
        this.standard_name = in.readString();
        this.subject_name = in.readString();
        this.subject_image = in.readString();
        this.font = in.readString();
    }

    public static final Creator<SubjectsModel> CREATOR = new Creator<SubjectsModel>() {
        public SubjectsModel createFromParcel(Parcel source) {
            return new SubjectsModel(source);
        }

        public SubjectsModel[] newArray(int size) {
            return new SubjectsModel[size];
        }
    };
}
