package com.observatory.mcqmodels;

/**
 * Created by Anuj on 21-05-2015.
 */
public class CommonModel {


    private int id;
    private String content;
    private int image;

    public CommonModel() {
    }

    public CommonModel(String content, int image) {
        this.content = content;
        this.image = image;
    }

    public CommonModel(int id, String content, int image) {
        this.id = id;
        this.content = content;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CommonModel(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
