package com.observatory.mcqmodels;

/**
 * Created by Anuj on 22-05-2015.
 */
public class LanguageModel {

    private int languageId;
    private String languageName;
    private String languageImages;

    public LanguageModel() {
    }

    public LanguageModel(int languageId, String languageName, String languageImages) {
        this.languageId = languageId;
        this.languageName = languageName;
        this.languageImages = languageImages;
    }

    public int getLanguageId() {
        return languageId;
    }

    public void setLanguageId(int languageId) {
        this.languageId = languageId;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public String getLanguageImages() {
        return languageImages;
    }

    public void setLanguageImages(String languageImages) {
        this.languageImages = languageImages;
    }
}
