package com.observatory.mcqmodels;

/**
 * Created by Anuj on 22-05-2015.
 */
public class StandardModel {

    private int standardId;
    private int languageId;
    private String LanguageName;
    private String standardName;
    private String standardImages;

    public StandardModel() {
    }

    public StandardModel(int standardId, int languageId, String languageName, String standardName, String standardImages) {
        this.standardId = standardId;
        this.languageId = languageId;
        LanguageName = languageName;
        this.standardName = standardName;
        this.standardImages = standardImages;
    }

    public int getStandardId() {
        return standardId;
    }

    public void setStandardId(int standardId) {
        this.standardId = standardId;
    }

    public int getLanguageId() {
        return languageId;
    }

    public void setLanguageId(int languageId) {
        this.languageId = languageId;
    }

    public String getLanguageName() {
        return LanguageName;
    }

    public void setLanguageName(String languageName) {
        LanguageName = languageName;
    }

    public String getStandardName() {
        return standardName;
    }

    public void setStandardName(String standardName) {
        this.standardName = standardName;
    }

    public String getStandardImages() {
        return standardImages;
    }

    public void setStandardImages(String standardImages) {
        this.standardImages = standardImages;
    }
}
