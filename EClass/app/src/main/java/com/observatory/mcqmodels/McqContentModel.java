package com.observatory.mcqmodels;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Anuj on 23-05-2015.
 */
public class McqContentModel implements Parcelable {
    public static final String MCQ_SERIAL_NO = "mcqSerialNumber";
    public static final String MCQ_NAME = "mcqName";
    public static final String MCQ_FONT = "mcqFont";

    public int mcq_content_id;
    public int mcq_id;
    public String mcq_name;

    public String question;
    public String font;
    public int isQuesHtml;
    public String isQuesImage;

    public String option1;
    public int isOpt1Html;
    public String font1;
    public String isOpt1Image;

    public int isOpt2Html;
    public String isOpt2Image;
    public String font2;
    public String option2;

    public int isOpt3Html;
    public String isOpt3Image;
    public String font3;
    public String option3;

    public int isOpt4Html;
    public String isOpt4Image;
    public String font4;
    public String option4;

    public int selected_option = -1;
    public int correct_ans_option;

    public boolean questionAlreadyAttempted = false;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mcq_content_id);
        dest.writeInt(this.mcq_id);
        dest.writeString(this.mcq_name);
        dest.writeString(this.font);
        dest.writeInt(this.isQuesHtml);
        dest.writeString(this.isQuesImage);
        dest.writeString(this.question);
        dest.writeInt(this.isOpt1Html);
        dest.writeString(this.isOpt1Image);
        dest.writeString(this.font1);
        dest.writeString(this.option1);
        dest.writeInt(this.isOpt2Html);
        dest.writeString(this.isOpt2Image);
        dest.writeString(this.font2);
        dest.writeString(this.option2);
        dest.writeInt(this.isOpt3Html);
        dest.writeString(this.isOpt3Image);
        dest.writeString(this.font3);
        dest.writeString(this.option3);
        dest.writeInt(this.isOpt4Html);
        dest.writeString(this.isOpt4Image);
        dest.writeString(this.font4);
        dest.writeString(this.option4);
        dest.writeInt(this.selected_option);
        dest.writeInt(this.correct_ans_option);
    }

    public McqContentModel() {
    }

    protected McqContentModel(Parcel in) {
        this.mcq_content_id = in.readInt();
        this.mcq_id = in.readInt();
        this.mcq_name = in.readString();
        this.font = in.readString();
        this.isQuesHtml = in.readInt();
        this.isQuesImage = in.readString();
        this.question = in.readString();
        this.isOpt1Html = in.readInt();
        this.isOpt1Image = in.readString();
        this.font1 = in.readString();
        this.option1 = in.readString();
        this.isOpt2Html = in.readInt();
        this.isOpt2Image = in.readString();
        this.font2 = in.readString();
        this.option2 = in.readString();
        this.isOpt3Html = in.readInt();
        this.isOpt3Image = in.readString();
        this.font3 = in.readString();
        this.option3 = in.readString();
        this.isOpt4Html = in.readInt();
        this.isOpt4Image = in.readString();
        this.font4 = in.readString();
        this.option4 = in.readString();
        this.selected_option = in.readInt();
        this.correct_ans_option = in.readInt();
    }

    public static final Creator<McqContentModel> CREATOR = new Creator<McqContentModel>() {
        public McqContentModel createFromParcel(Parcel source) {
            return new McqContentModel(source);
        }

        public McqContentModel[] newArray(int size) {
            return new McqContentModel[size];
        }
    };
}
