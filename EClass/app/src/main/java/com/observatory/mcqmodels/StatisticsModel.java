package com.observatory.mcqmodels;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Anuj on 29-08-2015.
 */
public class StatisticsModel implements Parcelable {

    public int mcq_id;
    public String mcq_name;
    public String date;
    public String time;
    public int ques_answered;
    public float score;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mcq_id);
        dest.writeString(this.mcq_name);
        dest.writeString(this.date);
        dest.writeString(this.time);
        dest.writeInt(this.ques_answered);
        dest.writeFloat(this.score);
    }

    public StatisticsModel() {
    }

    protected StatisticsModel(Parcel in) {
        this.mcq_id = in.readInt();
        this.mcq_name = in.readString();
        this.date = in.readString();
        this.time = in.readString();
        this.ques_answered = in.readInt();
        this.score = in.readFloat();
    }

    public static final Creator<StatisticsModel> CREATOR = new Creator<StatisticsModel>() {
        public StatisticsModel createFromParcel(Parcel source) {
            return new StatisticsModel(source);
        }

        public StatisticsModel[] newArray(int size) {
            return new StatisticsModel[size];
        }
    };
}
