package com.observatory.mcqmodels;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Anuj on 22-05-2015.
 */
public class McqModel implements Parcelable {


   public int mcq_id;
   public String chapter_name;
   public int has_mcq;
   public int has_qb;
   public String qb_path;
    public String font;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mcq_id);
        dest.writeString(this.chapter_name);
        dest.writeInt(this.has_mcq);
        dest.writeInt(this.has_qb);
        dest.writeString(this.qb_path);
        dest.writeString(this.font);
    }

    public McqModel() {
    }

    protected McqModel(Parcel in) {
        this.mcq_id = in.readInt();
        this.chapter_name = in.readString();
        this.has_mcq = in.readInt();
        this.has_qb = in.readInt();
        this.qb_path = in.readString();
        this.font = in.readString();
    }

    public static final Creator<McqModel> CREATOR = new Creator<McqModel>() {
        public McqModel createFromParcel(Parcel source) {
            return new McqModel(source);
        }

        public McqModel[] newArray(int size) {
            return new McqModel[size];
        }
    };
}
