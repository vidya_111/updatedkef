# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Applications/Android Studio.app/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

#-keep class com.orm.** {
#*;
#}

#-keep class com.orm.** {
#*;
#}

#-keepclassmembers class com.orm.**{
#}

#-useuniqueclassmembernames
#-keepattributes SourceFile,LineNumberTable
#-allowaccessmodification

#-keep public class * extends android.app.Activity
#-keep public class * extends android.app.Application
#-keep public class * extends android.app.Service
#-keep public class * extends android.content.BroadcastReceiver
#-keep public class * extends android.content.ContentProvider
#-keep public class * extends android.preference.Preference
#-keep public class com.android.vending.billing.IInAppBillingService
#-keep public class * extends android.view.View {
 #   public <init>(android.content.Context);
 #   public <init>(android.content.Context, android.util.AttributeSet);
 #   public <init>(android.content.Context, android.util.AttributeSet, int);
#}
#-keepclasseswithmembers class * {
 #   public <init>(android.content.Context, android.util.AttributeSet);
#}
#-keepclasseswithmembers class * {
 #   public <init>(android.content.Context, android.util.AttributeSet, int);
#}
#-keepclassmembers class * extends android.content.Context {
 #   public void *(android.view.View);
 #   public void *(android.view.MenuItem);
#}

#-keep class com.observatory.database.Master

#-keep class com.observatory.database.Subject

#-keepnames class com.observatory.database.Master {
#    *;
#}
#-keepclassmembernames class com.observatory.database.Master {
#    *;
#}

#-keepnames class com.observatory.database.Subject {
#    *;
#}
#-keepclassmembernames class com.observatory.database.Subject {
#    *;
#}

-keep class com.observatory.database.Subject { *; }

-keep class com.observatory.database.Master { *; }

-keep class com.observatory.database.Statistics { *; }

-keep class com.observatory.database.userStatistics { *; }

-keep class com.observatory.database.UserHistory { *; }

-keep class com.observatory.database.Statistics { *; }

-keep class com.observatory.database.userStatistics { *; }

-keep class com.observatory.database.OfflineUsers { *; }

-keep class com.observatory.kefcorner.Splash_typeB { *; }

-keep class com.parse.* { *; }

-dontwarn com.parse.**

#-keep class com.orm.SugarRecord<T> { *; }

-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception
-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**

-keep class com.google.**
-dontwarn com.google.**


#mailing progaurd

-keep class javax.** {*;}
-keep class com.sun.** {*;}
-keep class myjava.** {*;}
-keep class org.apache.harmony.** {*;}
-keep public class Mail {*;}
-dontwarn java.awt.**
-dontwarn java.beans.Beans
-dontwarn javax.security.**
-keep class com.sun.mail.handlers.** {*;}

-ignorewarnings

-keep class * {
    public private *;
}