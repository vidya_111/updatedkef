/**
 * Automatically generated file. DO NOT MODIFY
 */
package net.lingala.zip4j;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "net.lingala.zip4j";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
}
